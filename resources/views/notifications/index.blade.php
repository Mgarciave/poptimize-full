@extends('layout.login')


@section('content')

	<div class="box-notifications">
		<a href="{{ route('notification.create')}}"> Crear notificación</a>
	</div>

	<div class="box-notifications">
		@foreach($notifications as $notification)
			<p><strong> {{ $notification->name }}</strong></p>
			<p> {{ $notification->description }} </p>
			<p> {{ $notification->date }} </p>
		@endforeach

	</div>

@stop