@extends('layout.login')


@section('content')

	<div class="box-notifications">
		
		{!! Form::open(['route' => 'notification.store', 'method' => 'POST', 'class' => 'form-horizontal' ]) !!} 

			{!! Form::text('titulo', null, ['class' => 'form-control', 'placeholder' => 'Título de Notificación' ]) !!}	 

			{!! Form::text('fecha', null, ['class' => 'form-control', 'placeholder' => 'Fecha' ]) !!}

			{!! Form::textarea('comentario', null, ['class' => 'form-control', 'size' => '30x5', 'placeholder' => 'Comentario' ]) !!}

			<div class="form-group">
					<div class="col-sm-12">
	            		{!! Form::submit('Agendar Notificación', ['class' => 'btn btn-submit']) !!}
					</div>
				</div>	
	        </div>

		{!! Form::close() !!}

	</div>

@stop