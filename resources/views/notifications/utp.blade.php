<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
<div style="width:600px;">
	<p style="font-size:14px; font-weight:bold;">
	Tienes un mensaje del día <strong style="color:#f4cc46;">{{$fecha}}</strong>
	</p>
	<div style="width:100%; height:2px; background: #5cbec1; display: block;"></div><p style="font-size:14px; font-weight:bold;">El mensaje dice:</p><span style="font-size:14px; font-weight:bold;font-style: italic; white-space: pre-line;">{{$mensaje}}</p><p style="font-size:14px;font-weight:bold; margin-bottom:0px;">UTP / Directivos</p><p style="font-size: 14px;font-weight:bold; margin-top: 0px;">Colegio {{$colegio}}</p></div>
</body>
</html>