@extends('layout.main')


@section('content')

<div class="ui modal add-course-modal">
  <i class="close icon"></i>
  <div class="header">
    Añadir curso
  </div>
  <div class="content">
    <div class="ui grid">
      <div class="six wide column">
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Non, atque.</p>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore eos iusto iste, eligendi eum id nulla rerum qui neque hic?</p>
      </div>
      <div class="ten wide column">
        {!! Form::open(['route' => 'courses.store', 'method' => 'POST', 'class' => 'ui form' ]) !!}
          <div class="field">
            <label>Colegio</label>
            <div class="field">
              <div class="field">
                <div class="ui fluid search selection dropdown">
                  <input type="hidden" name="course[school]">
                  <i class="dropdown icon"></i>
                  <div class="default text">Selecciona el colegio</div>
                  <div class="menu">
                    <div class="item" data-value="col1"><i class="book icon"></i>Colegio 1</div>
                    <div class="item" data-value="col2"><i class="book icon"></i>Colegio 2</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="field">
            <label>Curso</label>
            <div class="fields">
              <div class="twelve wide field">
                <input type="text" name="course[level]" placeholder="Nivel">
              </div>
              <div class="four wide field">
                <div class="ui fluid search selection dropdown">
                  <input type="hidden" name="course[letter]">
                  <i class="dropdown icon"></i>
                  <div class="default text">Letra</div>
                  <div class="menu">
                    <div class="item" data-value="col1"><i class="book icon"></i>A</div>
                    <div class="item" data-value="col2"><i class="book icon"></i>B</div>
                    <div class="item" data-value="col2"><i class="book icon"></i>C</div>
                    <div class="item" data-value="col2"><i class="book icon"></i>D</div>
                    <div class="item" data-value="col2"><i class="book icon"></i>E</div>
                    <div class="item" data-value="col2"><i class="book icon"></i>F</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="field">
            <label>Asignatura</label>
            <div class="field">
              <div class="field">
                <div class="ui fluid search selection dropdown">
                  <input type="hidden" name="couse[field]">
                  <i class="dropdown icon"></i>
                  <div class="default text">Selecciona la asignatura</div>
                  <div class="menu">
                    <div class="item" data-value="col1"><i class="book icon"></i>Matemáticas</div>
                    <div class="item" data-value="col2"><i class="book icon"></i>Historia</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        {!! Form::close() !!}
      </div>
    </div>
  </div>
  <div class="actions">
    <button class="ui button cancel-button">Cancelar</button>
    {!! Form::submit('Continuar', ['class' => 'ui button ok-button']) !!}
  </div>
</div>

<div class="ui small basic course-added modal">
  <div class="ui icon header">
    <i class="checked calendar icon"></i>
    Curso añadido
  </div>
  <div class="content">
    <p>El curso ya fue registrado. En los próximos segundos serás redireccionado al calendario de programación.</p>
  </div>
  <div class="actions">
    <a href="course-calendar.html" class="ui green ok inverted button">
      <i class="checkmark icon"></i>
      Ir al calendario
    </a>
  </div>
  <small class="right-align">Si no haces click, te enviaremos al calendario en <span id="countdown">COUNTER</span> ;)</small>
</div>

<div class="pusher">
  <!-- Site content !-->
  <div class="ui grid">
    <div class="segment-content ten wide computer sixteen wide tablet column centered">
      <div class="ui segment">
        <h2 class="ui dividing header">
          Mis cursos
        </h2>
        <div class="ui grid">
          <div class="eight wide computer eight wide tablet column">
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium temporibus, quam sed facilis consequatur reiciendis?</p>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cumque, inventore temporibus, repellat delectus dolores ad earum debitis incidunt nemo cupiditate!</p>
          </div>
          <div class="eight wide eight wide tablet column">
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsum consequuntur non, consectetur. Veniam, alias, repellat.</p>
            <button class="ui button centered add-course"><i class="plus icon"></i> Añadir nuevo curso</button>
          </div>
        </div>
        <div class="ui grid">
          <div class="twelve wide computer sixteen wide tablet column centered">
            <table class="ui celled table">
              <thead>
                <tr>
                  <th>Colegio</th>
                  <th>Curso</th>
                  <th>Asignatura</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                @foreach($courses_teacher as $cursoProfe)
                <tr>
                  <td>
                    <div class="ui ribbon label"><a href="{{ route('units', $cursoProfe->asignatura_id) }}" data-content="Haz click aquí para ver esta planificación" class="school-course">ESCUELA DE PARVULOS Y ESPECIAL MUNDO PALABRA DE BUIN</a></div>
                  </td>
                  <td>{{ $cursoProfe->curso }} <span>{{ $cursoProfe->letter }}</span></td>
                  <td>$cursoProfe->asignatura</td>
                  <td>
                    <div class="ui text menu">
                      <div class="ui dropdown item">
                        <i class="write icon"></i> Acciones
                        <i class="dropdown icon"></i>
                        <div class="menu">
                          <div class="item">Editar</div>
                          <div class="item">Eliminar</div>
                        </div>
                      </div>
                    </div>
                  </td>
                </tr>

        		    @endforeach
              </tbody>
              <tfoot>
              </tfoot>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@stop
