@extends('layout.login')

@section('content')

<div id="register" class="ui middle aligned center aligned grid">
	<div class="four wide column">

	</div>
	<div class="four wide column">
		{!! Form::open(['route' => 'users.store', 'method' => 'POST', 'class' => 'ui form' ]) !!}

			<div class="field">
				<label>Nombre de usuario</label>
				{!! Form::text('username', null, ['class' => '', 'placeholder' => 'Nombre de usuario' ]) !!}
			</div>

			<div class="field">
				<label>Nombre</label>
				{!! Form::text('name', null, ['class' => '', 'placeholder' => 'Nombre docente' ]) !!}
			</div>

			<div class="field">
				<label>E-mail</label>
				{!! Form::email('email', null , ['class' => '', 'placeholder' => 'E-mail' ]) !!}
			</div>

			<div class="field">
				<label>Contraseña</label>
				{!! Form::password('password', ['class' => '', 'placeholder' => 'Contraseña']) !!}
			</div>

			<div class="field">
				<label>Tipo de registro</label>
				<select name="type" class="ui dropdown">
  					<option value="docente">Docente</option>
  					<option value="colegio">Colegio</option>
				</select>
			</div>

			{!! Form::submit('Registrate', ['class' => 'continue']) !!}

			@if (session('message'))
				<div class="ui success message">
		    		{{ session('message') }}
				</div>
			@endif

			@if (count($errors) > 0)
          <div class="ui error message">
              <ul>
                  @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                  @endforeach
              </ul>
          </div>
      @endif

				<p class="p-form gris">Si ya estás registrado, <a href="login" class="font-yellow">Inicia sesión aquí</a> </p>

				{!! Form::close() !!}
	</div>
</div>

@stop
