@extends('layout.main')

@section('content')

	<div class="box-units">
		@foreach($units as $unit)
			<div class="box-unit">
				<div class="title">
				<a href="{{ route('units.show', $unit->id)}}">
					{{ $unit->name }}
				</a>
					
				</div>
				<div class="hrs">
					{{ $unit->hrs }}
				</div>	
				<div class="description-min">
					{{ $unit->purpose_min }}
				</div>
			</div>

		@endforeach
	</div>

@stop