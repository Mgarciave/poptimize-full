@extends('layout.main')


@section('content')

	<div class="unit-details">
		@foreach($dataUnit as $data)
			<div class="title">{{ $data->name }} - {{ $data->hrs }}</div>

			<span><strong>Propósito: </strong></span>
			<div class="purpose">
				{{ $data->purpose }}
			</div>
		@endforeach

		<span><strong>Conocimientos Previos: </strong></span>
		
			@foreach($pknow as $pk)
				<div class="Pknowledge"> {{ $pk->description }} </div>
			@endforeach

		<span><strong>Palabras Claves: </strong></span>
			@foreach($tags as $tag)
				
				<div class="tags"> {{ $tag->name }} - {!! Form::checkbox('agree', 1, $tag->checked) !!}</div>
			@endforeach

		<span><strong>Conocimientos: </strong></span>
			@foreach($know as $knowledge)
				<div class="knowledge"> {{ $knowledge->description }} </div>
			@endforeach

		<span><strong>Objetivos de Aprendizaje (OA): </strong></span>
			{{-- @foreach($oa as $oa) --}}
				<div class="oa"> {{ $oa }} </div>
			{{-- @endforeach --}}


		<span><strong>Habilidades</strong></span>
		@foreach($abilities as $ability)
			<div class="abilities"> {{ $ability->description }} </div>
		@endforeach

		<span><strong>Habilidades</strong></span>
		@foreach($attitudes as $attitude)
			<div class="attitudes"> {{ $attitude->description }} </div>
		@endforeach


	</div>

@stop