@extends('layout.main')


@section('content')

<div class="ui modal event-detail">
  <i class="close icon"></i>
  <div class="header">
    Añadir curso
  </div>
  <div class="content">
    <div class="ui grid">
      <div class="sixteen wide column">
        <div class="ui top attached tabular menu">
          <a class="active item" data-tab="first">Clase</a>
          <a class="item" data-tab="second">Evento</a>
        </div>
        <div class="ui bottom attached active tab segment" data-tab="first">
          <h2>¿Qué haré en esta clase?</h2>
          <p>Unidad I: Propósito</p>
          <div class="ui grid">
            <div class="eight wide column">
              <div class="ui form">
                <div class="inline field">
                  <div class="ui checkbox">
                    <input type="checkbox" tabindex="0" class="hidden">
                    <label>OA 1</label>
                  </div>
                </div>
                <div class="inline field">
                  <div class="ui checkbox">
                    <input type="checkbox" tabindex="0" class="hidden">
                    <label>OA 2</label>
                  </div>
                </div>
                <div class="inline field">
                  <div class="ui checkbox">
                    <input type="checkbox" tabindex="0" class="hidden">
                    <label>OA 3</label>
                  </div>
                </div>
                <div class="inline field">
                  <div class="ui checkbox">
                    <input type="checkbox" tabindex="0" class="hidden">
                    <label>OA 4</label>
                  </div>
                </div>
              </div>
            </div>
            <div class="ui vertical divider">-</div>
            <div class="eight wide column">
              <div class="ui form">

                <div class="inline field">
                  <div class="ui checkbox">
                    <input type="checkbox" tabindex="0" class="hidden">
                    <label>Suma</label>
                  </div>
                </div>
                <div class="inline field">
                  <div class="ui checkbox">
                    <input type="checkbox" tabindex="0" class="hidden">
                    <label>Resta</label>
                  </div>
                </div>
                <div class="inline field">
                  <div class="ui checkbox">
                    <input type="checkbox" tabindex="0" class="hidden">
                    <label>Multiplicación</label>
                  </div>
                </div>
                <div class="inline field">
                  <div class="ui checkbox">
                    <input type="checkbox" tabindex="0" class="hidden">
                    <label>División</label>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="ui bottom attached tab segment" data-tab="second">
          <h2>Notificar</h2>
          <p>Tipo:</p>
          <div class="ui form">
            <div class="fields">
              <div class="field">
                <div class="ui checkbox">
                  <input type="checkbox" name="example">
                  <label>Evaluación</label>
                </div>
              </div>
              <div class="field">
                <div class="ui checkbox">
                  <input type="checkbox" name="example">
                  <label>Actividad</label>
                </div>
              </div>
              <div class="field">
                <div class="ui checkbox">
                  <input type="checkbox" name="example">
                  <label>Tarea</label>
                </div>
              </div>
            </div>
          </div>
          <h2>Descripción</h2>
          <div class="ui form">
            <div class="field">
              <textarea></textarea>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="actions">
    <button class="ui button cancel-button">Cancelar</button>
    <button class="ui button ok-button">Guardar</button>
  </div>
</div>

<div id="side-nav" class="ui sidebar inverted vertical menu">
  <img src="../dist/img/just-logo.png" alt="" id="logo">
  <a href="courses.html" class="item">
    <i class="student icon"></i>
    Cursos
  </a>
  <a href="" id="beta-menu" class="item">
    <i class="browser icon"></i>
    Calendario
  </a>
  <a href="profile.html" class="item">
    <i class="child icon"></i>
    Perfil
  </a>
</div>
<div class="pusher">
  <!-- Site content !-->
  <div id="nav" class="ui top attached menu">
    <div class="ui dropdown icon item">
      <i class="sidebar icon"></i>
    </div>
    <img src="../dist/img/text-logo.png" alt="">
    <div class="right menu">
      <div class="ui right aligned category search item">
        <div class="ui transparent icon input">
          <input class="prompt" type="text" placeholder="Buscar">
          <i class="search link icon"></i>
        </div>
        <div class="results"></div>
      </div>
    </div>
  </div>
  <div class="ui grid">
    <div class="twelve wide computer sixteen wide tablet column centered segment-content">
      <div class="ui segment">
        <h2 class="ui dividing header">
          Programación de la asignatura
        </h2>
        <div class="ui grid">
          <div class="three wide computer five wide tablet column">
            <h3>Historia</h3>
            <!--<button class="ui button add-class"><i class="plus icon"></i> Añadir clase</button>-->
            <div class="ui fluid search normal selection dropdown">
              <input type="hidden" name="country">
              <i class="dropdown icon"></i>
              <div class="default text">Selecciona el color <img class="colorpicker" src="../dist/img/colorpicker.png" alt=""></div>
                <div class="menu">
                <div class="item class-color" data-color="#39cd6b"><i class="color-label green-label"></i> Verde</div>
                <div class="item class-color" data-color="#ef5361"><i class="color-label red-label"></i> Rojo</div>
                <div class="item class-color" data-color="#fed048"><i class="color-label yellow-label"></i> Amarillo</div>
              </div>
            </div>
            <small class="text-center">Arrastra y suelta los bloques de clases en el calendario</small>
            <div class="added-blocks">
              <div class="ui button added-item" data-current-color="orange" data-hex="" data-title="Clase de Historia" >Asignatura</div><br><br>
            </div>
            <div class="ui divider"></div>
            <a href="courses.html" id="go-to-units" class="ui button save-calendar">Guardar programación</a>
          </div>
          <div class="thirteen wide computer eleven wide tablet column">
            <div id="fullcalendar-detail"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@stop
