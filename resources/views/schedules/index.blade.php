@extends('layout.login')

@section('content')

	<ul>
		@foreach($asignaturas as $asignatura)
			<li> {{ $asignatura->course }} - {{ $asignatura->name }} </li>
		@endforeach
	</ul>

	<div class="calendar-title">Horario</div>

	<div class="schedule"></div>

@stop