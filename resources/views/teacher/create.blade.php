@extends('layout.login')

@section('content')

<div id="finish-profile" class="ui middle aligned center aligned grid">
	<div class="four wide column">
		<h1>Esta es tu primera vez con nosotros!</h1>
		<p>Completa tu perfil con los datos solicitados a continuación para poder entrar.</p>
	</div>
	<div class="six wide column">
		{!! Form::open(['route' => 'teachers.store', 'method' => 'POST', 'class' => 'ui form' ]) !!}
		<div class="field">
	    <label class="float-left">Fecha de nacimiento</label>
	    {!! Form::text('birth_date', null , ['class' => '', 'placeholder' => '19/04/1000' ]) !!}
	  </div>
		<div class="ui divider"></div>
		<div class="field">
	    <label class="float-left">Teléfono</label>
			{!! Form::text('phone', null, ['class' => '', 'placeholder' => '982480244' ]) !!}
	  </div>
		<div class="ui divider"></div>
		<div class="field">
	    <label class="float-left">Profesión</label>
			{!! Form::text('profession', null , ['class' => '', 'placeholder' => 'Profesión' ]) !!}
	  </div>
		<div class="ui divider"></div>
		<div class="field">
	    <label class="float-left">Especialidad / Mención</label>
			{!! Form::text('specialty', null , ['class' => '', 'placeholder' => 'Diferencial' ]) !!}
	  </div>
		<div class="ui divider"></div>
		<div class="field">
	    <label class="float-left">Años de experiencia</label>
			{!! Form::input('number', 'years_experience', null , ['class' => '', 'placeholder' => '0', 'min' => '0' ]) !!}
	  </div>
		<div class="ui divider"></div>
		<div class="field">
	    <label class="float-left">Foto</label>
			{!! Form::file('photo', ['class' => '']) !!}
	  </div>
		<div class="ui divider"></div>
		{!! Form::submit('Completar registro', ['class' => 'continue']) !!}
		{!! Form::close() !!}
	</div>
</div>

@stop
