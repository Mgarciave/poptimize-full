@extends('layout.login')

@section('content')

	<div id="profile" class="ui middle aligned center aligned grid">
		<div class="four wide column">
			<h2>Perfil</h2>
			<img src="http://semantic-ui.com/images/avatar2/large/kristy.png" alt="" class="ui avatar avatar-profile image">
			<a href="#" class="edit-photo">Editar foto</a>
			<h1>{{ Auth::user()->username}}</h1>
			<a href="mailto:#">{{ Auth::user()->email }}</a>
		</div>
		<div class="six wide column">
			{!! Form::model( $teacher->id, ['route' => ['teachers.update', $teacher->id], 'class' => '', 'method' => 'PUT']) !!}
			<div class="ui top attached tabular menu" id="edit-data" toolbar-tip="{content: '#toolbar-options', position: 'top'}">
				<a class="active item" data-tab="first">Datos Personales</a>
				<a class="item" data-tab="second">Datos Profesionales</a>
				{!! Form::submit('Guardar cambios', ['class' => 'ui primary button']) !!}
			</div>
				<div class="ui bottom attached active tab segment" data-tab="first">
					<table class="ui celled table">
					  <tbody>
					    <tr>
					      <td><label class="float-left">Nombre</label></td>
					      <td><div class="ui input">{!! Form::text('name', $teacher->name , ['class' => '' ]) !!}</div></td>
					    </tr>
					    <tr>
					      <td><label class="float-left">Usuario</label></td>
					      <td><div class="ui input">{!! Form::text('username', $teacher->username , ['class' => '' ]) !!}</div></td>
					    </tr>
					    <tr>
					      <td><label class="float-left">E-mail</label></td>
					      <td><div class="ui input">{!! Form::text('email', $teacher->email , ['class' => '' ]) !!}</div></td>
					    </tr>
							<tr>
					      <td><label class="float-left">Contraseña</label></td>
					      <td>*************</td>
					    </tr>
							<tr>
					      <td><label class="float-left">Fecha de Nacimiento</label></td>
					      <td><div class="ui input">{!! Form::text('birth_date', $teacher->birth_date , ['class' => '' ]) !!}</div></td>
					    </tr>
							<tr>
					      <td><label class="float-left">Teléfono</label></td>
					      <td><div class="ui input">{!! Form::text('phone', $teacher->phone , ['class' => '' ]) !!}</div></td>
					    </tr>
					  </tbody>
					</table>
				</div>
				<div class="ui bottom attached tab segment" data-tab="second">
					<table class="ui celled table">
					  <tbody>
					    <tr>
					      <td><label class="float-left">Profesión</label></td>
					      <td><div class="ui input">{!! Form::text('profession', $teacher->profession, ['class' => '' ]) !!}</div></td>
					    </tr>
					    <tr>
					      <td><label class="float-left">Especialidad/Mención</label></td>
					      <td><div class="ui input">{!! Form::text('specialty', $teacher->specialty , ['class' => '' ]) !!}</div></td>
					    </tr>
					    <tr>
					      <td><label class="float-left">Años de experiencia</label></td>
					      <td><div class="ui input">{!! Form::text('years_experience', $teacher->years_experience , ['class' => '' ]) !!}</div></td>
					    </tr>
							<tr>
					      <td><label class="float-left">Asociado al colegio</label></td>
					      <td><div class="ui input">{!! Form::text('school', $teacher->school_id , ['class' => '' ]) !!}</div></td>
					    </tr>
					  </tbody>
					</table>
				</div>
				{!! Form::close() !!}
		</div>
	</div>

@stop
