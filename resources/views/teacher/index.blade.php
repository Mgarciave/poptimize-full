@extends('layout.main')

@section('content')

@foreach($profes as $profe)
	<div id="profile" class="ui middle aligned center aligned grid">
		<div class="four wide column">
			<h2>Perfil</h2>
			<img src="http://semantic-ui.com/images/avatar2/large/kristy.png" alt="" class="ui avatar avatar-profile image">
			<a href="#" class="edit-photo">Editar foto</a>
			<h1>{{ Auth::user()->username}}</h1>
			<a href="mailto:#">{{ Auth::user()->email }}</a>
		</div>
		<div class="six wide column">
			<div class="ui top attached tabular menu" id="edit-data" toolbar-tip="{content: '#toolbar-options', position: 'top'}">
				<a class="active item" data-tab="first">Datos Personales</a>
				<a class="item" data-tab="second">Datos Profesionales</a>
				<a href="{{ route('teachers.edit', Auth::id() ) }}" class="ui primary button">Editar datos</a>
			</div>
			<div class="ui bottom attached active tab segment" data-tab="first">
				<table class="ui celled table">
				  <tbody>
				    <tr>
				      <td><label class="float-left">Nombre</label></td>
				      <td>{{ Auth::user()->name }}</td>
				    </tr>
				    <tr>
				      <td><label class="float-left">Usuario</label></td>
				      <td>{{ Auth::user()->username }}</td>
				    </tr>
				    <tr>
				      <td><label class="float-left">E-mail</label></td>
				      <td>{{ Auth::user()->email }}</td>
				    </tr>
						<tr>
				      <td><label class="float-left">Contraseña</label></td>
				      <td>*************</td>
				    </tr>
						<tr>
				      <td><label class="float-left">Fecha de Nacimiento</label></td>
				      <td>{{ $profe->birth_date }}</td>
				    </tr>
						<tr>
				      <td><label class="float-left">Teléfono</label></td>
				      <td>{{ $profe->phone }}</td>
				    </tr>
				  </tbody>
				</table>
			</div>
			<div class="ui bottom attached tab segment" data-tab="second">
				<table class="ui celled table">
				  <tbody>
				    <tr>
				      <td><label class="float-left">Profesión</label></td>
				      <td>{{ $profe->profession }}</td>
				    </tr>
				    <tr>
				      <td><label class="float-left">Especialidad/Mención</label></td>
				      <td>{{ $profe->specialty }}</td>
				    </tr>
				    <tr>
				      <td><label class="float-left">Años de experiencia</label></td>
				      <td>{{ $profe->years_experience }}</td>
				    </tr>
						<tr>
				      <td><label class="float-left">Asociado al colegio</label></td>
				      <td></td>
				    </tr>
				  </tbody>
				</table>
			</div>
		</div>
	</div>
@endforeach

@stop
