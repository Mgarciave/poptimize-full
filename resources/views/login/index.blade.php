@extends('layout.login')

@section('content')

<div class="ui centered equal height grid main-content login">
	<!--<div class="four wide computer four wide tablet column centered row">-->
		<div class="four wide computer six wide tablet column">
			<img src="img/logo-horizontal.png" alt="">
			<h1>Hola ;)</h1>
			<h2>Bienvenido</h2>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam
				libero fuga aut, deserunt voluptatem quae.</p>
		</div>
		<div class="four wide computer six wide tablet column">
			<h3 id="login-register-heading">Login</h3>
			<p id="login-register-subheading">¿No tienes cuenta? <a href="javascript:;" id="init-form-toggle">Crear cuenta acá</a></p>
				{!! Form::open(['route' => 'login', 'id' => 'login-form', 'class' => 'ui large form']) !!}
					<div class="ui stacked">
						<div class="field">
							<div class="ui left icon input">
								<i class="user icon"></i>
								{!! Form::text('email', null, ['placeholder' => 'Dirección de correo', 'id' => 'email' ]) !!}
							</div>
						</div>
						<div class="field">
							<div class="ui left icon input">
								<i class="lock icon"></i>
								{!! Form::password('password', ['id' => 'password', 'placeholder' => 'Contraseña']) !!}
							</div>
						</div>
						{!! Form::submit('Login', ['class' => 'ui fluid large teal submit button login', 'id' => 'login']) !!}
					</div>

					<div class="ui error message"></div>

				{!! Form::close() !!}
				{!! Form::open(['route' => 'users.store','id' => 'register-form', 'method' => 'POST', 'class' => 'ui form hide' ]) !!}
					<div class="ui stacked">
						<div class="field">
							<div class="ui left icon input">
								<i class="user icon"></i>
								{!! Form::text('username', null, ['placeholder' => 'Nombre de usuario', 'id' => 'username' ]) !!}
							</div>
						</div>
						<div class="field">
							<div class="ui left icon input">
								<i class="lock icon"></i>
								{!! Form::text('name', null, ['placeholder' => 'Nombre docente', 'id' => 'name' ]) !!}
							</div>
						</div>
						<div class="field">
							<div class="ui left icon input">
								<i class="lock icon"></i>
								{!! Form::email('email', null , ['id' => 'email', 'placeholder' => 'E-mail' ]) !!}
								<input type="email" name="email" placeholder="Email" id="email">
							</div>
						</div>
						<div class="field">
							<div class="ui left icon input">
								<i class="lock icon"></i>
								{!! Form::password('password', ['id' => 'password', 'placeholder' => 'Contraseña']) !!}
							</div>
						</div>
						<div class="field">
							<div class="ui left icon input">
								<i class="lock icon"></i>
								<select class="ui fluid dropdown">
									<option value="docente">Docente</option>
									<option value="colegio">Colegio</option>
								</select>
							</div>
						</div>
						{!! Form::submit('Registrarme', ['class' => 'ui fluid large teal submit button register', 'id' => 'register']) !!}

					</div>

					<div class="ui error message"></div>

				{!! Form::close() !!}
		</div>
	<!--</div>-->
</div>


@stop
