@extends('layout.login')

@section('content')

	<div class="complete col-sm-12">
		Primera vez que inicias sesión, completa tu perfil para finalizar el registro.
	</div>


	<div class="alert">
		@if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        @if (session('message'))
			<div class="alert alert-success">
	    		{{ session('message') }}
	    		<style>
					.panel-title{display:none;}
					.panel-body{display:none;}
					.panel-footer{display:none;} 
				</style>
			</div>
		@endif
	</div>

	<div class="form-content">
		{!! Form::open(['route' => 'school.store', 'method' => 'POST', 'class' => 'form-horizontal' ]) !!} 

            <div class="form-group">
                <label class="col-sm-6 control-label">Dirección :</label>
                <div class="col-sm-6">
                    {!! Form::text('address', null , ['class' => 'form-control', 'placeholder' => 'Avenida/Calle #1234' ]) !!}
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-6 control-label">Teléfono :</label>
                <div class="col-sm-6">
                    {!! Form::text('phone', null, ['class' => 'form-control', 'placeholder' => '982480244' ]) !!}

                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-6 control-label">Comuna :</label>
                <div class="col-sm-6">
                    {!! Form::text('commune', null , ['class' => 'form-control', 'placeholder' => 'Comuna' ]) !!}
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-6 control-label">Región :</label>
                <div class="col-sm-6">
                    {!! Form::select('region', $region , ['class' => 'form-add relative']) !!}
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-6 control-label">Dependencia :</label>
                <div class="col-sm-6">
                    {!! Form::text('dependence', null , ['class' => 'form-control', 'placeholder' => 'Dependencia' ]) !!}
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-6 control-label">Sostenedor :</label>
                <div class="col-sm-6">
                    {!! Form::text('holder', null , ['class' => 'form-control', 'placeholder' => 'Nombre del sostenedor' ]) !!}
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-6 control-label">Director :</label>
                <div class="col-sm-6">
                    {!! Form::text('director', null , ['class' => 'form-control', 'placeholder' => 'Nombre director' ]) !!}
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-6 control-label">Foto (Opcional) :</label>
                <div class="col-sm-6">
					{!! Form::file('photo', ['class' => 'form-control']) !!}
                </div>
            </div>

			<div class="form-group">
				<div class="col-sm-12">
            		{!! Form::submit('Completar registro', ['class' => 'btn btn-submit']) !!}
				</div>
			</div>	
        </div><!-- panel-body -->

        {!! Form::close() !!}

	</div>

@stop