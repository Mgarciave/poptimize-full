@extends('layout.login')

@section('content')

	<article class="box-profile">
		<div class="bar-title">
            <figure class="image left round">
               <img src="" alt="avatar" height="100" width="100">
            </figure>
            <p class="gray relative user-title">Colegio : {{ Auth::user()->name }}</p>  

			 <div class="btn-edit right">
			 	<a href=" {{ route('school') }} ">
			 		Volver
			 	</a>
			 </div>
		</div>
        
        <section class="panel">
        	
        {!! Form::model( $school->id, ['route' => ['teachers.update', $school->id], 'class' => 'form-horizontal', 'method' => 'PUT']) !!}

				<div class="form-group">
                    <label class="col-sm-6 control-label">Datos Personales :</label>
                </div>
				
				<div class="form-group">
                    <label class="col-sm-4 control-label">Nombre:</label>
                    
                    <div class="col-sm-4">
                        {!! Form::text('name', $school->name , ['class' => 'form-control' ]) !!}
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label">Username :</label>
                    
                    <div class="col-sm-4">
                        {!! Form::text('username', $school->username , ['class' => 'form-control' ]) !!}
                    </div>
                </div>

                 <div class="form-group">
                    <label class="col-sm-4 control-label">Contraseña :</label>
                    
                    <div class="col-sm-4">
                        {!! Form::password('password', null , ['class' => 'form-control', 'placeholder' => 'nueva contraseña' ]) !!}
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label">E-mail :</label>
                    
                    <div class="col-sm-4">
                        {!! Form::text('email', $school->email , ['class' => 'form-control' ]) !!}
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label">Dirección:</label>
                    
                    <div class="col-sm-4">
                        {!! Form::text('address', $school->address , ['class' => 'form-control' ]) !!}
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label">Teléfono :</label>
                    
                    <div class="col-sm-4">
                        {!! Form::text('phone', $school->phone , ['class' => 'form-control' ]) !!}
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label">Comuna :</label>
                    
                    <div class="col-sm-4">
                        {!! Form::text('commune', $school->commune , ['class' => 'form-control' ]) !!}
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label">Región :</label>
                    
                    <div class="col-sm-4">
                        {!! Form::text('region', $region , $school->region_id, ['class' => 'form-control' ]) !!}
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-6 control-label">Datos Específicos :</label>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label">Código RBD :</label>
                    
                    <div class="col-sm-4">
                        {!! Form::text('code_rbd', $school->code_rbd, ['class' => 'form-control' ]) !!}
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label">Dependecia :</label>
                    
                    <div class="col-sm-4">
                        {!! Form::text('dependencia', $school->dependecia , ['class' => 'form-control' ]) !!}
                    </div>
                </div>

                 <div class="form-group">
                    <label class="col-sm-4 control-label">Sostenedor :</label>
                    
                    <div class="col-sm-4">
                        {!! Form::text('holder', $school->holder , ['class' => 'form-control' ]) !!}
                    </div>
                </div>

                 <div class="form-group">
                    <label class="col-sm-4 control-label">Director :</label>
                    
                    <div class="col-sm-4">
                        {!! Form::text('director', $school->director , ['class' => 'form-control' ]) !!}
                    </div>
                </div>

            	{!! Form::submit('Editar Perfil Colegio', ['class' => 'btn btn-primary mr5 right']) !!}
            
            {!! Form::close() !!}
           
        </section>	
	</article>

@stop