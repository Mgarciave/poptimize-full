@extends('layout.login')

@section('content')

	<article class="box-profile">
		<div class="bar-title">
            <figure class="image left round">
               <img src="" alt="avatar" height="100" width="100">
            </figure>
            <p class="gray relative user-title">Colegio : {{ Auth::user()->name }}</p>  

			 <div class="btn-edit right">
			 	<a href=" {{ route('school.edit', Auth::id() ) }} ">
			 		Editar datos
			 	</a>
			 </div>
		</div>
        
        <section class="panel">

        	@foreach($colegio as $colegio)
	            <table class="table">  
	            	<tr>
	            		<td><strong>Datos Personales : </strong></td>
	            	</tr>                     
	                <tr>
	                    <td>Nombre :</td>
	                    <td class="gray">{{ Auth::user()->name }}</td>    
	                </tr>
	                <tr>
	                    <td>Usuario :</td>
	                    <td class="gray">{{ Auth::user()->username}}</td>
	                </tr>
	                <tr>
	                    <td>E-mail :</td>
	                    <td class="gray">{{ Auth::user()->email }}</td>
	                </tr>
	                <tr>
	                    <td>Contraseña :</td>
	                    <td class="gray">******</td>
	                </tr>
	                <tr>
	                    <td>Dirección :</td>
	                    <td class="gray">{{ $colegio->address }}</td>    
	                </tr>
	                <tr>
	                    <td>Teléfono :</td>
	                    <td class="gray">{{ $colegio->phone }}</td>    
	                </tr>
	                <tr>
	                    <td>Comuna :</td>
	                    <td class="gray">{{ $colegio->commune }}</td>    
	                </tr>
	                <tr>
	                    <td>Región :</td>
	                    <td class="gray">{{ $colegio->region }}</td>    
	                </tr>
	            	<tr>
	            		<td><strong>Datos Específicos : </strong></td>
	            	</tr>
	            	<tr>
	            		<td>Código RBD : </td>
	            		<td class="gray">{{ $colegio->code_rbd }}</td>  
	            	</tr>    
	                <tr>
	                    <td>Dependencia :</td>
	                    <td class="gray">{{ $colegio->dependence }}</td>    
	                </tr>
	                <tr>
	                    <td>Sostenedor :</td>
	                    <td class="gray">{{ $colegio->holder }}</td>    
	                </tr>
	                <tr>
	                    <td>Director:</td>
	                    <td class="gray">{{ $colegio->director }}</td>    
	                </tr>
	            </table>
            @endforeach
        </section>	
	</article>

@stop