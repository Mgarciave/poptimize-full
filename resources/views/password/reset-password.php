<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
  <link rel="icon" href="favicon.ico" />
  <link rel="stylesheet" href="../bower_components/semantic/dist/semantic.css">
  <link rel="stylesheet" href="../bower_components/sweetalert/dist/sweetalert.css">
  <link rel="stylesheet" href="../bower_components/pickadate/lib/themes/classic.css">
  <link rel="stylesheet" href="../bower_components/pickadate/lib/themes/classic.date.css">
  <link rel="stylesheet" media="screen" type="text/css" href="styles/datepicker.css" />
  <link rel="stylesheet" href="../dist/css/main.css">
  <script src="../bower_components/js-cookie/src/js.cookie.js"></script>

 
  <!-- Piwik -->
  <script type="text/javascript">
    var _paq = _paq || [];
    _paq.push(['trackPageView']);
    _paq.push(['enableLinkTracking']);
    (function() {
      var u="//poptimize.cl:8080/";
      _paq.push(['setTrackerUrl', u+'piwik.php']);
      _paq.push(['setSiteId', '1']);
      var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
      g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
    })();
  </script>
  <noscript><p><img src="//poptimize.cl:8080/piwik.php?idsite=1" style="border:0;" alt="" /></p></noscript>
  <!-- End Piwik Code -->
	<?php if( !isset($reset)){ header("location: http://poptimize-api.app/index.html");} ?>
	<title>Reinicio de contraseña - Poptimize</title>
</head>
<body>

<div class="ui centered equal height middle aligned grid" style="width: 30%; padding-top: 2%; margin-left: 39%;" >
	<div class="ui centered one column segment">
		<img class="ui small image centered" src="../img/logo-login.png" alt="">
		<h1 class="ui centered" >Reinicio de Contraseña</h1>
		  <div class="four wide computer six wide tablet column">
			<form id="login-form" class="ui large form">
	            <div class="ui stacked">
	              <div class="field">
	                <div class="ui left icon input">
	                  <i class="lock icon"></i>
	                  <input type="password" name="password1" placeholder="Contraseña" id="password1">
	                </div>
	              </div>
	              <div class="field">
	                <div class="ui left icon input">
	                  <i class="lock icon"></i>
	                  <input type="password" name="password2" placeholder="Confirmar contraseña" id="password2">
	                </div>
	              </div>
	              <input type="hidden" id="tokenmail" value="<?php echo $reset ?>">
	              <button id="login" class="ui fluid large teal submit button confirm" style="background-color: #38c2c8; border-radius: 40px; width: 60%; margin: 0 auto;">Confirmar</button>
	            </div>
	         </form>
	      </div>

	</div>
</div>





<script src="../bower_components/jquery/dist/jquery.js"></script>}
<script src="../bower_components/moment/moment.js"></script>
<script src="../bower_components/semantic/dist/semantic.js"></script>
<script src="../bower_components/sweetalert/dist/sweetalert.min.js"></script>
<script src="/scripts/config.js"></script> 
<script type="text/javascript">

	$(document).on('click', '.confirm', function(e){
		e.preventDefault();
		var pass1= $('#password1').val();
		var pass2= $('#password2').val();
		var email = $('#tokenmail').val();
		if(!pass1 || !pass2){
			swal({
                title: 'Error',
                text: 'Por favor revisa que todos los campos estén con la información correspondiente.',
                type: 'error',
                confirmButtonText: "Continuar",
            });
		}
		if(pass1 != pass2){
			swal({
                title: 'Error',
                text: 'Las contraseñas no coinciden.',
                type: 'error',
                confirmButtonText: "Continuar",
            });
		}
		else{

			 $.ajax({
	          url:ajax_url+'/v1/user/resetpass',
	          type: 'post',
	          dataType: 'json',
	          data: { mail: email, pass: pass1},
	          success: function(data){
	          	if(data.change == 'ok'){
			          swal({
		                title: '¡Ya puedes ingresar a Poptimize!',
		                text: 'Tu contraseña fue cambiada con éxito.',
		                type: 'success',
		                confirmButtonText: "Continuar",
		           	  },
		           	  function(){
		           	  	window.location = '/login.html';
		           	  });
	          	}
	          	else{
	          		swal({
		                title: 'Error',
		                text: 'La contraseña debe poseer por lo menos 6 caracteres.',
		                type: 'error',
		                confirmButtonText: "Continuar",
		           	  });
	          	}
	          }
	      });

		}
	});
</script>
</body>
</html>