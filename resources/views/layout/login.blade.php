<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Document</title>
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
  <link rel="stylesheet" href="<?php echo asset('bower_components/semantic/dist/semantic.css'); ?>">
  <link rel="stylesheet" href="<?php echo asset('bower_components/sweetalert/dist/sweetalert.css'); ?>">
  <link rel="stylesheet" href="<?php echo asset('css/main.css'); ?>">
</head>
<body>

          @yield('content')

          <script src="<?php echo asset('bower_components/jquery/dist/jquery.js'); ?>"></script>
          <script src="<?php echo asset('bower_components/semantic/dist/semantic.js'); ?>"></script>
          <script src="<?php echo asset('bower_components/sweetalert/dist/sweetalert.min.js'); ?>"></script>
          <script src="<?php echo asset('js/ui.js'); ?>"></script>
    </body>
    </html>
