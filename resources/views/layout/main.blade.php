<!-- index.html -->
    <!DOCTYPE html>
    <html>
    <head>
      <meta charset="UTF-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
      <!-- SCROLLS -->
      <!-- load bootstrap and fontawesome via CDN -->
      <!--<link rel="stylesheet" href="../bower_components/bootstrap/dist/css/bootstrap.css" />-->
      <link rel="stylesheet" href="<?php echo asset('bower_components/bootstrap/dist/css/bootstrap.css'); ?>">
      <link rel="stylesheet" href="<?php echo asset('bower_components/smalot-bootstrap-datetimepicker/css/bootstrap-datetimepicker.css'); ?>">
      <link rel="stylesheet" href="<?php echo asset('bower_components/semantic/dist/semantic.css'); ?>">
      <link rel="stylesheet" href="<?php echo asset('bower_components/fullcalendar/dist/fullcalendar.css'); ?>">
      <link rel="stylesheet" href="<?php echo asset('bower_components/font-awesome/css/font-awesome.css'); ?>">
      <link rel="stylesheet" href="<?php echo asset('bower_components/sweetalert/dist/sweetalert.css'); ?>">
      <link rel="stylesheet" href="<?php echo asset('css/style.css'); ?>">

      <!-- SPELLS -->
      <!-- load angular and angular route via CDN -->
      <script src="<?php echo asset('bower_components/jquery/dist/jquery.js'); ?>"></script>
      <script src="<?php echo asset('bower_components/jquery-ui/jquery-ui.js'); ?>"></script>
      <script src="<?php echo asset('bower_components/jquery-ui/ui/draggable.js'); ?>"></script>
      <script src="<?php echo asset('bower_components/semantic/dist/semantic.js'); ?>"></script>
      <script src="<?php echo asset('bower_componentS/moment/min/moment-with-locales.js'); ?>"></script>
      <script src="<?php echo asset('bower_components/fullcalendar/dist/fullcalendar.js'); ?>"></script>
      <script src="<?php echo asset('bower_components/fullcalendar/dist/lang/es.js'); ?>"></script>
      <script src="<?php echo asset('bower_components/smalot-bootstrap-datetimepicker/js/bootstrap-datetimepicker.js'); ?>"></script>
      <script src="<?php echo asset('bower_components/smalot-bootstrap-datetimepicker/js/locales/bootstrap-datetimepicker.es.js'); ?>" charset="UTF-8"></script>
      <script src="<?php echo asset('bower_components/sweetalert/dist/sweetalert.min.js'); ?>"></script>
      <script src="<?php echo asset('js/main.js'); ?>"></script>
    </head>
    <body>

        <!-- HEADER AND NAVBAR -->

        <div class="ui inverted left vertical sidebar menu">
          <a href="/" class="item">
            <i class="fa fa-home"></i>
            Inicio
          </a>
          <a href="#profile" class="item">
            <i class="fa fa-user"></i>
            Perfil
          </a>
          <a href="#calendar" class="item">
            <i class="fa fa-calendar"></i>
            Calendario
          </a>
          <a class="item">
            <i class="fa fa-briefcase"></i>
            Cursos
          </a>
          <a class="item">
            <i class="fa fa-sign-out"></i>
            Salir
          </a>
        </div>
        <div class="pusher">
          <div class="ui top attached menu">
            <div class="ui dropdown icon item main-menu-icon">
              <i class="sidebar icon"></i>
            </div>
            <div class="ui dropdown icon item">
              <i class="alarm outline icon"></i>
              <div class="menu">
                <div class="item">
                  <i class="dropdown icon"></i>
                  <span class="text">Hay 2 evaluaciones cercanas</span>
                  <div class="menu">
                    <div class="item"><strong>Matemáticas</strong> (Curso X)</div>
                    <div class="item"><strong>Historia</strong> (Curso Y)</div>
                  </div>
                </div>
                <div class="item">
                  El día viernes se celebra el día de ...
                </div>
              </div>
            </div>
            <div class="ui grid">
              <div class="three column row">

              </div>
            </div>
            <div class="menu right row">
              <div class="ui category search item">
                <div class="ui transparent icon input">
                  <input class="prompt" type="text" placeholder="Buscar...">
                  <i class="search link icon"></i>
                </div>
                <div class="results"></div>
              </div>
              <div class="ui dropdown profile icon item">
                <input type="hidden" name="gender">
                <img src="http://semantic-ui.com/images/avatar2/large/kristy.png" alt="" class="ui avatar image">
                <name>{{ Auth::user()->name }}</name>
                <div class="menu">
                  <div class="item" data-value="male">

                    <div class="ui card">
                      <div class="image">
                        <img src="http://semantic-ui.com/images/avatar2/large/kristy.png">
                      </div>
                      <div class="content">
                        <a class="header">{{ Auth::user()->name }}</a>
                        <div class="meta">
                          <span class="date">Registrada el dd/mm/YYYY</span>
                        </div>
                        <div class="description">
                          Docente.
                        </div>
                      </div>
                      <div class="extra content">
                        <a href="{{ route('teachers') }}"></a>
                          <i class="fa fa-user" aria-hidden="true"></i>
                          Mi perfil
                        </a>
                        <br>
                        <br>
                        <a href="{{ route('logout') }}">
                          <i class="fa fa-sign-out" aria-hidden="true"></i>
                          Cerrar sesión
                        </a>
                      </div>
                    </div>

                  </div>
                </div>
              </div>
            </div>
          </div>
          @yield('content')


  </div>
  <script src="<?php echo asset('bower_components/jquery/dist/jquery.js'); ?>"></script>
  <script src="<?php echo asset('bower_components/jquery-ui/jquery-ui.js'); ?>"></script>
  <script src="<?php echo asset('bower_components/jqueryui-touch-punch/jquery.ui.touch-punch.js'); ?>"></script>
  <script src="<?php echo asset('bower_components/semantic/dist/semantic.js'); ?>"></script>
  <script src="<?php echo asset('bower_components/sweetalert/dist/sweetalert.min.js'); ?>"></script>
  <script src="<?php echo asset('bower_components/moment/moment.js'); ?>"></script>
  <script src="<?php echo asset('js/custom-fc.js'); ?>"></script>
  <script src="<?php echo asset('bower_components/fullcalendar/dist/lang/es.js'); ?>"></script>
  <script src="<?php echo asset('js/ui.js'); ?>"></script>
</body>
</html>
