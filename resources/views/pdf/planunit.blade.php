<!DOCTYPE html>
  <head>
  <title>Planificación</title> 	
  {!! Html::style('../public/css/pdf.css') !!}

    <style>
    @import 'https://fonts.googleapis.com/css?family=Roboto';body{font-family: 'Roboto', sans-serif;}</style>

    <link rel="stylesheet" href="../public/css/pdf.css">
    <link rel="stylesheet" href="<?php echo asset('css/gato.css'); ?>">
    <style>
  .flyleaf {
    page-break-after: avoid;
    text-align:center;
    margin-top: 20px;
  }

  .header, .footer {

  }

  .header {
    margin-top: 50px;
  }

  .footer {

  }

  .table {
    margin-top: 70px;
    /*page-break-before: always*/
    page-break-after: always;

  }
  .titleno{
    font-size: 1rem; 
    margin: 0 0 0 0;
  }
  .textno{
    font-size: 0.9rem;
    margin: 0 0 0 0;
  }
  .breaker{
    margin-top: 8px;  
  }

  .footer {

  }
</style>
  </head>
<body style="font-family:'Helvetica'; font-size: 10pt;">
    <div class="footer" style="">
      <!--<p class="footer-content" style="color:#48c1c7;font-weight:900;font-size:11px;margin-top:50px">Av Italia 850 || www.poptimize.cl</p>-->
      <p class="pagenum" style="display:inline-block;text-align:center;margin-top:-30px;margin-bottom:-100px;"></p>
      <img src="http://poptimize-api.app/dist/img/footer.png" alt="" width="40%;" />
    </div>

    <main>
    <?php if($fullarr != ''){  ?>
    @foreach($fullarr as $courses)
      <div class="flyleaf">
        <img src="http://poptimize-api.app/dist/img/logo-pdf.png" style="width: 20%;" alt="" />
      </div>
      <div><h1 style="text-align:center;">Planificación de Clase</h1></div>
      
      <div style="page-break-after: always;">
        <div>
    	    <p class="titleno">
    	    	<b>Asignatura: </b> {{$courses['asign']}}
    	    </p>
    	    <p class="titleno">
    	    	<b>Nivel: </b> {{$courses['course']}}
    	    </p>
    	    <p class="titleno">
    	    	<b>Unidad: </b> {{$courses['unit']}}
    	    </p>
          <p class="titleno">
            <b>Fecha: </b> {{$courses['formated']}}
          </p>
        </div>
        <br>
        @if($courses['arrtag'] != '')
        <div>
        <p class="titleno"><b>Palabras Clave</b></p>
        <p class="textno">
        @for($i = 0; $i<count($courses['arrtag']); $i++)
          @if($i<count($courses['arrtag'])-1)
            {{$courses['arrtag'][$i]['name']}}, 
          @else
            {{$courses['arrtag'][$i]['name']}}.
          @endif
        @endfor
        </p>
      </div>
      <br>
      @endif
      @if($courses['arrat'] != '')
      <div>
        <p class="titleno"><b>Actitudes</b></p>
        @foreach($courses['arrat'] as $at)
        <p class="textno">{{$at['desc']}}</p> 
        @endforeach
      </div>
      <br>
      @endif
      @if($courses['arrab'] != '')
      <div>
      <p class="titleno"><b>Habilidades</b></p>
        @foreach($courses['arrab'] as $ab)
        <p class="textno">{{$ab['desc']}}</p> 
        @endforeach
      </div>
      <br>
      @endif
      <div>
      @if($courses['arroas'] != '')
        <div>
          <p class="titleno"><b>Objetivos de Aprendizaje</b></p>
          @foreach($courses['arroas'] as $oas)
          <p class="textno" style="word-wrap: break-word;">
              <b>{{$oas['name']}}:</b> {{$oas['desc']}}
          </p>
          @endforeach
        </div>
        <br>
        @endif
        @if( $courses['scheduleData'][0]->init_text != '' || $courses['scheduleData'][0]->mid_text != '' || $courses['scheduleData'][0]->end_text != '' || $courses['scheduleData'][0]->res_text != ''|| $courses['scheduleData'][0]->eval_text != '' || $courses['scheduleData'][0]->ec_text != '')
        <div>
          <p class="titleno">
            <b>Actividades de aprendizaje</b>
          </p>
          <br>
            <?php if($courses['scheduleData'][0]->init_text != ''){ ?>
            <p class="titleno">
              <b>Inicio</b>
            </p>
            <p class="textno" style="white-space:pre-line; word-wrap: break-word;">
              {{$courses['scheduleData'][0]->init_text}}
            </p>
            <br>
          <?php } ?> 
          <?php if($courses['scheduleData'][0]->mid_text != ''){ ?>
            <p class="titleno">
              <b>Desarrollo</b>
            </p>
            <p class="textno" style="white-space:pre-line; word-wrap: break-word;">
              {{$courses['scheduleData'][0]->mid_text}}
            </p>
            <br>
          <?php } ?>
          <?php if($courses['scheduleData'][0]->end_text != ''){ ?>
            <p class="titleno">
              <b>Cierre</b>
            </p>
            <p class="textno" style="white-space:pre-line; word-wrap: break-word;">
              {{$courses['scheduleData'][0]->end_text}}
            </p>
            <br>
          <?php } ?>
          <?php if($courses['scheduleData'][0]->res_text != ''){ ?>
            <p class="titleno">
              <b>Recursos de Aprendizaje</b>
            </p>
            <p class="textno" style="white-space:pre-line; word-wrap: break-word;">
              {{$courses['scheduleData'][0]->res_text}}
            </p>
            <br>
          <?php } ?>
          <?php if($courses['scheduleData'][0]->eval_text != ''){ ?>
            <p class="titleno">
              <b>Evaluación</b>
            </p>
            <p class="textno" style="white-space:pre-line; word-wrap: break-word;">
              {{$courses['scheduleData'][0]->eval_text}}
            </p>
            <br>
          <?php } ?>
          <?php if($courses['scheduleData'][0]->ec_text != ''){ ?>
            <p class="titleno">
              <b>Adecuación Curricular</b>
            </p>
            <p class="textno" style="white-space:pre-line; word-wrap: break-word;">
              {{$courses['scheduleData'][0]->ec_text}}
            </p>
            <br>
          <?php } ?>
        </div>
        @endif
      </div>
    </div>
       @endforeach
       <?php }else { ?>
         <div class="flyleaf">
        <img src="http://poptimize-api.app/dist/img/logo-pdf.png" style="width: 20%;" alt="" />
      </div>
      <div><h1 style="text-align:center;">Planificación de Clase</h1></div>
       <h1 style="text-align: center;">¡Aun no has planificado este mes!</h1>
       <?php } ?>
    </main>

</body>

</html>