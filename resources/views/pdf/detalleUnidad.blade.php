<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Detalle unidad</title>
    <link rel="stylesheet" href="<?php echo asset('css/pdf.css'); ?>">
    <style>
    @import 'https://fonts.googleapis.com/css?family=Roboto';body{font-family: 'Roboto', sans-serif;}</style>
    <style>
  .flyleaf {
    page-break-after: avoid;
    text-align:center;
    margin-top: 20px;
  }

  .header, .footer {

  }

  .header {
    margin-top: 50px;
  }

  .footer {

  }

  .table {
    margin-top: 70px;
    /*page-break-before: always*/
    page-break-after: always;

  }

  .footer {

  }
</style>
  </head>
  <body style="font-family:'Helvetica'">

    <div class="flyleaf">
      <img src="http://app.poptimize.cl/img/logo-pdf.png" style="width: 20%;" alt="" />
    </div>



  <div class="footer" style="">
    <!--<p class="footer-content" style="color:#48c1c7;font-weight:900;font-size:11px;margin-top:50px">Av Italia 850 || www.poptimize.cl</p>-->
    <p class="pagenum" style="display:inline-block;text-align:center;margin-top:-30px;margin-bottom:-100px;"></p>
    <img src="http://app.poptimize.cl/dist/img/footer.png" alt="" width="40%;" />

  </div>

    <main style="margin-top:-60px;">
      <div id="details" class="clearfix">
        <div id="invoice">
          <h1 style="text-align:center;font-size:28px">Detalle de {{ $unitName }} - {{ $subjectName }}</h1>
          <h2 style="text-align:center;font-size:25px">{{ $unit_hrs . ' horas de la unidad'}}</h2>
        </div>
      </div>

          <table cellspacing="0" cellpadding="0" class="table table-borderer" style="border: 1px solid #ddd;margin-bottom:20px; margin-top: -20px;">
            <thead>
              <tr>
                <th style="padding: 8px; border-bottom: 1px solid #ddd; text-align:left;background-color:#35afaf;opacity:.9;color:white" class="desc">Propósito</th>
              </tr>
            </thead>
            <tbody>
              @foreach($dataUnit as $data)
                <tr>
                  <td style="padding: 8px; border-bottom: 1px solid #ddd;text-align:justify">
                    {{ $data->purpose }}
                  </td>
                </tr>
              @endforeach
            </tbody>
          </table>
          <?php if (sizeof($pknow)> 0 ){?>
          <h2 style="padding: 8px; border: 1px solid #ddd; text-align:left;font-size:15px;margin-top:20px;background-color:#35afaf;opacity:.9;color:white">Conocimientos Previos</h2>
              @foreach($pknow as $pk)
                <li style="padding: 8px; border: 1px solid #ddd; list-style: none;">
                  {{ $pk->description }}
                </li>
              @endforeach
          <?php } ?>


          <?php if (sizeof($know)> 0 ){?>
          <h2 style="padding: 8px; border: 1px solid #ddd; text-align:left;font-size:15px;margin-top: 20px;background-color:#35afaf;opacity:.9;color:white">Conocimientos</h2>
              @foreach($know as $know)
                <li style="padding: 8px; border: 1px solid #ddd; list-style: none;">
                    {{ $know->description }}
                </li>
              @endforeach
          <?php } ?>

          <?php if (sizeof($tags)> 0 ){?>
          <h2 style="padding: 8px; border: 1px solid #ddd; text-align:left;font-size:15px;margin-top: 20px;background-color:#35afaf;opacity:.9;color:white">Palabras claves</h2>

          @foreach($tags as $tag)
            <li style="padding: 8px; border: 1px solid #ddd;list-style: none;">
              {{ $tag->name }}
            </li>

          @endforeach
          <?php } ?>


          <h2 style="padding: 8px; border: 1px solid #ddd; text-align:left;font-size:15px;margin-top: 20px;background-color:#35afaf;opacity:.9;color:white">Objetivos de aprendizaje</h2>


              @foreach($oas as $oa)

                 <li style="padding: 8px; border: 1px solid #ddd; list-style: none;">
                    {{ $oa->description_min }}
                 </li>


              @endforeach

          <?php if (sizeof($abilities)> 0 ){?>
          <h2 style="padding: 8px; border: 1px solid #ddd; text-align:left;font-size:15px;margin-top: 20px;background-color:#35afaf;opacity:.9;color:white">Habilidades</h2>


              @foreach($abilities as $ability)

                <li style="padding: 8px; border: 1px solid #ddd;list-style: none;">
                  {{ $ability->description}}
                </li>
              @endforeach
          <?php } ?>

          <h2 style="padding: 8px; border: 1px solid #ddd; text-align:left;font-size:15px;margin-top: 20px;background-color:#35afaf;opacity:.9;color:white">Actitudes</h2>


          <?php if (sizeof($attitudes)> 0 ){?>
           @foreach($attitudes as $attitude)

                <li style="padding: 8px; border: 1px solid #ddd;list-style: none;">
                  {{ $attitude->description }}
                </li>
              @endforeach
          <?php } ?>
    </main>


  </body>
</html>
