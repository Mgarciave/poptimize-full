<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Unidades</title>
    {!! Html::style('../public/css/pdf.css') !!}

    <style>
    @import 'https://fonts.googleapis.com/css?family=Roboto';body{font-family: 'Roboto', sans-serif;}</style>

    <link rel="stylesheet" href="../public/css/pdf.css">
    <link rel="stylesheet" href="<?php echo asset('css/gato.css'); ?>">
    <style>
  .flyleaf {
    page-break-after: avoid;
    text-align:center;
    margin-top: 20px;
  }

  .header, .footer {

  }

  .header {
    margin-top: 50px;
  }

  .footer {

  }

  .table {
    margin-top: 70px;
    /*page-break-before: always*/
    page-break-after: always;

  }

  .footer {

  }
</style>
  </head>
  <body style="font-family:'Helvetica'">

    <div class="flyleaf">
      <img src="http://app.poptimize.cl/img/logo-pdf.png" style="width: 20%;" alt="" />
    </div>



  <div class="footer" style="">
    <!--<p class="footer-content" style="color:#48c1c7;font-weight:900;font-size:11px;margin-top:50px">Av Italia 850 || www.poptimize.cl</p>-->
    <p class="pagenum" style="display:inline-block;text-align:center;margin-top:-30px;margin-bottom:-100px;"></p>
    <img src="http://app.poptimize.cl/dist/img/footer.png" alt="" width="40%;" />

  </div>

<table>


    <main>
      <div id="details" >
        <div id="invoice">


          <h1 style="text-align:center;font-size:28px">Unidades de {{ $name }}</h1>
        </div>
      </div>

          <?php $i=0; ?>
          <table cellspacing="0" cellpadding="0" class="table table-borderer" style="border: 1px solid #ddd">
            <thead>
              <tr>
                <th style="padding: 8px; border-bottom: 1px solid #ddd; text-align:left;background-color:#35afaf;opacity:.9;color:white;font-size:15px" class="desc">Unidad</th>
                <th style="padding: 8px; border-bottom: 1px solid #ddd; text-align:left;background-color:#35afaf;opacity:.9;color:white;font-size:15px" class="unit">Horas</th>
                <th style="padding: 8px; border-bottom: 1px solid #ddd; text-align:left;background-color:#35afaf;opacity:.9;color:white;font-size:15px" class="total">Propósito</th>
              </tr>
            </thead>
            <tbody>

            @foreach($units as $unit)
              <tr>
                <td style="padding: 8px; border-bottom: 1px solid #ddd" class="no">  {{ $units[$i]->name }} </td>
                <td style="padding: 8px; border-bottom: 1px solid #ddd" class="desc">{{ $units[$i]->hrs }}</td>
                <td style="padding: 8px; border-bottom: 1px solid #ddd" class="unit">{{ $units[$i]->purpose_min }} </td>
              </tr>
              <?php $i++; ?>
            @endforeach
            </tbody>
          </table>
        </main>
        </table>

  </body>


</html>
