<?php ini_set("date.timezone", "America/Santiago"); setlocale(LC_ALL, 'es_CL'); ?>
<!DOCTYPE html>
<html lang="es-ES">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>Detalle unidad</title>
    <link rel="stylesheet" href="<?php echo asset('css/pdf.css'); ?>">
    <style>
  .flyleaf {
    page-break-after: avoid;
    text-align:center;
    margin-top: 20px;
  }

  .header, .footer {

  }

  .header {
    margin-top: 50px;
  }

  .footer {

  }

  .table {
    margin-top: 70px;
    /*page-break-before: always*/
    page-break-after: always;

  }
  .titleno{
    font-size: 1rem; 
    margin-top: 0px;
    margin-bottom: 0px;
    margin-left: 6px;
    margin-right: 6px;

  }
  .allinfo * {
      page-break-after:avoid;
      page-break-before:avoid;
  }
  .textno{
    font-size: 0.9rem;
    margin-top: 0px;
    margin-bottom: 0px;
    margin-left: 6px;
    margin-right: 6px;
  }
  .breaker{
    margin-top: 8px;  
  }

  .footer {
    background-color: white;
  }
  .plan{
    padding:8px; border-bottom:1px solid #ddd; text-align:center; background-color:#35afaf; opacity:.9; color:white; margin-top: 0px;
  }
</style>
  </head>
  <body>

    <div class="flyleaf">
      <img src="http://poptimize-api.app/img/logo-pdf.png" alt="" style="width: 20%;margin-bottom:-80px;" />
    </div>

<div class="header">

</div>

  <div class="footer" style="position: fixed; bottom: 0;">
    <!--<p class="footer-content" style="color:#48c1c7;font-weight:900;font-size:11px;margin-top:50px">Av Italia 850 || www.poptimize.cl</p>-->
    <p class="pagenum" style="display:inline-block;text-align:center;margin-top:-30px;margin-bottom:10px; padding-top: 10px;"></p>
    <img src="http://poptimize-api.app/dist/img/footer.png" alt="" width="40%;" />

  </div>

<!--           <table cellspacing="0" cellpadding="0" class="table table-borderer" style="border: 1px solid #ddd;margin-bottom:20px;width:100%">
            <thead>
              <tr>
                <th style="padding: 8px; border-bottom: 1px solid #ddd; text-align:center;background-color:#35afaf;opacity:.9;color:white" class="desc">Planificación</th> -->
    <!--             <p style="padding: 8px; border-bottom: 1px solid #ddd; text-align:center;background-color:#35afaf;opacity:.9;color:white" class="desc"><b>Planificación</b></p> -->
<!--               </tr>
            </thead>
            <tbody> -->
            <main style="margin: 0.5in;">
              <?php echo '<p class="plan" style="margin-top: 60px; margin-bottom:0px;"><b>Planificación</b></p>'; ?>
              <?php if(count($calendar)>0){ ?>
                <?php  foreach($calendar as $cal){ if($cal['description'] != '' || sizeof($cal['oas']) > 0 ) {
                    $date1 = strftime("%A %e de %B", mktime(0, 0, 0, date('m',strtotime($cal['inicio'])), date('d',strtotime($cal['inicio'])), date('Y',strtotime($cal['inicio']))));
                    $date = utf8_encode($date1);
                    $oa = '';
                    $tag = '';
                    echo '<div class="allinfo" style="border: 1px solid #ddd; width:100%;">';
                    // echo '<tr>';
                    // echo  '<td style="padding: 8px; border-bottom: 1px solid #ddd;text-align:justify;">';
                    echo  '<p style="font-size:22px;font-weight:bold; text-align: center; margin-bottom:0px; margin-top: 5px;">';
                    echo $cal['curso'].' '.$cal['letra'].' / '.$cal['asignatura']. '</p><p style="font-size:16px;font-weight:bold; text-align: center;  margin-top: 0px; margin-bottom:5px;">'.$date.'</p>';
                    if(isset($cal['colegio'])){
                      echo '<p style="font-size:16px; font-weight:bold; text-align: center; margin: 0 0 0 0;">'.$cal['colegio'].'</p><p style="border-bottom: 0.5px solid black; text-align:center;margin-bottom:10px;"></p>';
                    }
                    else{
                      echo '<p style="border-bottom: 0.5px solid black; text-align:center;margin-bottom:10px;"></p>';
                    }
                    if(sizeof($cal['oas']) > 0){
                      echo '<p class="titleno" style="color:gray;"><b>'.$cal['unit'].'</b></p>';
                      echo '<p class="textno" style="color:gray;"><b>Objetivos de Aprendizaje</b></p>';
                      foreach($cal['oas'] as $oas){
                        $oa .= '<p class="textno" style="color:gray;"><b>'.$oas->text.':</b> '.$oas->description_min.'</p>';
                      }
                      echo $oa;
                      echo '<p class="breaker"></p>';
                    }
                    if(count($cal['attitudes']) > 0){
                      echo '<p style="color: gray;" class="textno" ><b>Actitudes</b></p>';
                      foreach($cal['attitudes'] as $at) {
                        echo '<p style="color: gray;" class="textno" >'.$at->description.'</p>';
                      }
                      echo '<p class="breaker"></p>';;
                    }
                    if(count($cal['abilities']) > 0){
                      echo '<p style="color: gray;" class="textno"><b>Habilidades</b></p>';
                      foreach($cal['abilities'] as $ab) {
                        echo '<p style="color: gray;" class="textno">'.$ab->description.'</p>';
                      }
                      echo '<p class="breaker"></p>';;
                    }
                    if(sizeof($cal['tags']) > 0){
                      $tag = '<p class="textno" style="color:gray;"><b>Palabras Clave:</b> ';
                      foreach ($cal['tags'] as $key => $value) {
                        if($key < (sizeof($cal['tags']) - 1 )){
                          $tag .= $value->name.', ';
                        }
                        else{
                          $tag .= $value->name.'.';
                        }
                      }
                      $tag .= '</p>';
                      echo $tag;
                      echo '<p class="breaker"></p>';
                    }
                    echo $cal['description'] != '' ? '<p class="textno" ><b>Descripción </b></p><p class="textno" style="word-wrap: break-word;"> '.nl2br($cal['description']).'</p><p class="breaker"></p>' : '';
                    if($cal['init_text'] != '' || $cal['mid_text'] != '' || $cal['end_text'] != '' || $cal['res_text'] != '' || $cal['eval_text'] != '' || $cal['ec_text'] != '' ){
                      echo '<p class="titleno"><b>Actividades de Aprendizaje</b></p><p class="breaker"></p>';
                      if( $cal['init_text'] != ''){
                        echo '<p class="textno" style="word-wrap: break-word;"><b>Inicio: </b></p><p class="textno" style="word-wrap: break-word;"> '.nl2br($cal['init_text']).'</p><p class="breaker"></p>';
                      }
                      if( $cal['mid_text'] != ''){
                        echo '<p class="textno" style="word-wrap: break-word;"><b>Desarrollo: </b></p><p class="textno" style="word-wrap: break-word;"> '.nl2br($cal['mid_text']).'</p><p class="breaker"></p>';
                      }
                      if( $cal['end_text'] != ''){
                        echo '<p class="textno" style="word-wrap: break-word;"><b>Cierre: </b></p><p class="textno" style="word-wrap: break-word;"> '.nl2br($cal['end_text']).'</p><p class="breaker"></p>';
                      }
                      if( $cal['res_text'] != ''){
                        echo '<p class="textno" style="word-wrap: break-word;"><b>Recursos de Aprendizaje: </b></p><p class="textno" style="word-wrap: break-word;"> '.nl2br($cal['res_text']).'</p><p class="breaker"></p>';
                      }
                      if( $cal['eval_text'] != ''){
                        echo '<p class="textno" style="word-wrap: break-word;"><b>Evaluación: </b></p><p class="textno" style="word-wrap: break-word;"> '.nl2br($cal['eval_text']).'</p><p class="breaker"></p>';
                      }
                      if( $cal['ec_text'] != ''){
                        echo '<p class="textno" style="word-wrap: break-word;"><b>Adecuación Curricular: </b></p><p class="textno" style="word-wrap: break-word;"> '.nl2br($cal['ec_text']).'</p><p class="breaker"></p>';
                      }
                    }
                    echo '</div>';
                    // echo  '</td>';
                    // echo '</tr>';
                } }?>
              <?php }else{

                echo '<p>No existen planificaciones en este mes.</p>';
              }?>
              </main>
<!--             </tbody>
          </table> -->


  </body>
</html>
