<!DOCTYPE html>
  <head>
  <title>Planificación</title> 	
  {!! Html::style('../public/css/pdf.css') !!}

    <style>
    @import 'https://fonts.googleapis.com/css?family=Roboto';body{font-family: 'Roboto', sans-serif;}</style>

    <link rel="stylesheet" href="../public/css/pdf.css">
    <link rel="stylesheet" href="<?php echo asset('css/gato.css'); ?>">
    <style>
  .flyleaf {
    page-break-after: avoid;
    text-align:center;
    margin-top: 20px;
  }

  .header, .footer {

  }

  .header {
    margin-top: 50px;
  }

  .footer {

  }

  .table {
    margin-top: 70px;
    /*page-break-before: always*/
    page-break-after: always;

  }

  .footer {

  }
</style>
  </head>
<body style="font-family:'Helvetica'; font-size: 10pt;">
    <div class="footer" style="">
      <!--<p class="footer-content" style="color:#48c1c7;font-weight:900;font-size:11px;margin-top:50px">Av Italia 850 || www.poptimize.cl</p>-->
      <p class="pagenum" style="display:inline-block;text-align:center;margin-top:-30px;margin-bottom:-100px;"></p>
      <img src="http://poptimize-api.app/dist/img/footer.png" alt="" width="40%;" />
    </div>

    <main> 
     @foreach($allinfo as $course) 
      <div class="flyleaf">
        <img src="http://poptimize-api.app/dist/img/logo-pdf.png" style="width: 20%;" alt="" />
      </div>
      <div><h1 style="text-align:center;">{{$allinfo[0]['title']}}</h1></div>     
        <div style="page-break-after: always;">
            <div><h2 style="text-align:center;">{{$course['teachInfo'][0][0]}}</h2></div>
            <div>
              <p><b>Clases planificadas: </b> {{$course['teachInfo'][0][4]}}%&nbsp;({{$course['teachInfo'][0][3]}} clases de {{$course['teachInfo'][0][2]}})</p>
            </div>
            <div>
              <p><b>Objetivos de Aprendizaje planificados: </b> {{$course['teachInfo'][0][7]}}%&nbsp;({{$course['teachInfo'][0][6]}} objetivos de {{$course['teachInfo'][0][5]}})</p>
            </div>
            <div>
              <p><b>Objetivos de Aprendizaje planificados</b></p>
              @foreach($course['oaInfo']['oaseen'] as $oaseen)
                <p><b>{{$oaseen['OA']}}:&nbsp; </b>{{$oaseen['desc']}}</p>
              @endforeach
            </div>
            <p><b>Objetivos de Aprendizaje sin planificar</b></p>
              @foreach($course['oaInfo']['oaunseen'] as $oaunseen)
                <p><b>{{$oaunseen['OA']}}:&nbsp; </b>{{$oaunseen['desc']}}</p>
              @endforeach
            </div>
        </div>
      @endforeach
    </main>
  </body>
</html>