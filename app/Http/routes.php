<?php

if (Request::is("*")) {
    header("Access-Control-Allow-Origin: ".$_ENV['API_URL']);
    header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Authorization, If-Modified-Since, Cache-Control, Pragma");
    header("Access-Control-Allow-Credentials: true");
}

  Route::group(array('prefix' => 'v1', 'namespace' => 'Api'), function()
  {

      Route::post('/session', function(){
        $user = Auth::user();
        return $user;
      });

      //Route::get('', 'PdfController@invoice');
      Route::get('/verification', 'UsersController@verify');
      Route::get('/reset', 'UsersController@resetPassword');
      Route::resource('/contact', 'UsersController@contact');
      Route::resource('/user/resetpass', 'UsersController@resetGo');
      Route::resource('/report/units/print/{id_subject}/', 'PdfController@invoice');
      Route::resource('/report/schedule/print', 'PdfController@schedule');
      Route::resource('/report/teacher/printutp', 'PdfController@printUtpTeacher');
      Route::resource('/report/asign/printutp', 'PdfController@printUtpAsign');
      Route::resource('/report/level/printutp', 'PdfController@printUtpLevel');
      Route::resource('/report/unit/print/{id_unit}/', 'PdfController@detailUnit');
      Route::resource('/report/calendar/print', 'PdfController@calendarPrint');
      Route::resource('/report/calendar/full', 'PdfController@calendarFull');
      Route::resource('/report/calendar/week', 'PdfController@calendarWeek');
      Route::resource('/report/calendar/print/{mes}/', 'PdfController@calendarPrint');
      Route::resource('/users/auth', 'AuthController@authData');
      Route::resource('/users/sendutpmail', 'UsersController@utpMailer');
      Route::resource('/users/logout', 'AuthController@logout');
      Route::resource('/users/auth/get', 'AuthController@checkAuth');
      Route::resource('/users/auth/edit', 'AuthController@editProfile');
      Route::resource('/users/auth/avatar_upload', 'AuthController@avatarUpload');
      Route::resource('/users/create', 'UsersController@store');
      Route::resource('/users/login', 'LoginController@postLogin');
      Route::resource('/users/login-adm', 'LoginController@postLoginAdm');
      Route::resource('/profile/finish/teacher', 'TeacherController@store');
      Route::resource('/users/reset', 'UsersController@mailReset');
      Route::resource('/courses/list', 'CoursesController@create');
      Route::resource('/courses/get', 'CoursesController@getCourses');
      Route::resource('/courses/add', 'CoursesController@store');
      Route::resource('/schedule/extra-oa/', 'ScheduleController@getExtraOa');
      Route::resource('/schedule/deleteclass/', 'ScheduleController@deleteClass');
      Route::resource('/schedule/asignteach/', 'ScheduleController@teachByAsign');
      Route::resource('/schedule/courseteach/', 'ScheduleController@teachByClass');
      Route::resource('/courses/delete/', 'CoursesController@delete');
      Route::resource('/courses/course_id', 'CoursesController@get_course_id');
      Route::resource('/units/list/{id}/', 'UnitController@unidades');
      Route::resource('/units/show/{id}/', 'UnitController@show');
      Route::resource('/units/save/{subject_id}/', 'UnitController@store');
      Route::resource('/schedule/getdropdowns', 'ScheduleController@getAdmDropdowns');
      Route::resource('/schedule/getconme', 'ScheduleController@conmeModal');
      Route::resource('/schedule/erase-file', 'ScheduleController@eraseFile');
      Route::resource('/schedule/teachasig', 'ScheduleController@getTeacherAsig');
      Route::resource('/schedule/teachdata', 'ScheduleController@getTeachData');
      Route::resource('/schedule/teachcourses', 'ScheduleController@getTeachCourses');
      Route::resource('/schedule/teachoas', 'ScheduleController@getTeachOas');
      Route::resource('/schedule/getclassinfo', 'ScheduleController@getClassInfo');
      Route::resource('/schedule/utpcalendar', 'ScheduleController@utpCalendar');
      Route::resource('/schedule/add', 'ScheduleController@store');
      Route::resource('/schedule/analit', 'ScheduleController@getAnalit');      
      Route::resource('/schedule/holidays', 'ScheduleController@holidays');
      Route::resource('/schedule/conme', 'ScheduleController@conme');
      Route::resource('/schedule/update-schedule', 'ScheduleController@updateCourses');
      Route::resource('/schedule/clean', 'ScheduleController@clean');
      Route::resource('/schedule/list-adm', 'ScheduleController@admSchedules');
      Route::resource('/schedule/oa-list', 'ScheduleController@getObjName');
      Route::resource('/schedule/list', 'ScheduleController@index');
      Route::resource('/schedule/cron', 'ScheduleController@cron');
      Route::resource('/schedule/unit', 'ScheduleController@unit');
      Route::resource('/schedule/plan_data', 'ScheduleController@planData');
      Route::resource('/schedule/reschedule', 'ScheduleController@moveCourses');
      Route::resource('/report/planification/month', 'PdfController@monthData');
      Route::resource('/report/planification/full', 'PdfController@allCourseData');
      Route::resource('/report/planification/print/{schedule_id}', 'PdfController@planificationPrint');
      Route::resource('/schedule/update', 'ScheduleController@update');
      Route::resource('/schedule/file-upload', 'ScheduleController@fileUpload');
      Route::resource('/events/add', 'EventsController@createEvent');
      Route::resource('/notifications/add', 'NotificationsController@store');
      Route::resource('/notifications/list', 'NotificationsController@index');
      Route::resource('/notifications/delete', 'NotificationsController@delete');
      Route::resource('/objectives/add', 'ObjectivesController@store');
      Route::resource('/objectives/list', 'ObjectivesController@index');
      Route::resource('/objectives/update', 'ObjectivesController@update');
      Route::resource('/tags/add', 'TagController@store');
      Route::resource('/tags/list', 'TagController@index');
      Route::resource('/abilities/add', 'AbilitiesController@store');
      Route::resource('/abilities/list', 'AbilitiesController@index');
      Route::resource('/attitudes/add', 'AttitudesController@store');
      Route::resource('/attitudes/list', 'AttitudesController@index');
      Route::resource('/feedback', 'FeedbackController@index');
});

//});
