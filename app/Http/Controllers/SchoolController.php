<?php

namespace App\Http\Controllers;

//use Illuminate\Http\Request;

use App\Models\User;
use App\Models\Region;
use App\Models\School;

use Auth;
use Input;
use Validator;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Request;

class SchoolController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $username = Auth::user()->username;

        $colegio = \DB::table('school')
                        ->join('region', 'school.region_id', '=', 'region.id')
                        ->select('school.name', 'username', 'email', 'address', 'phone', 'commune', 'school.region_id', 'code_rbd', 'dependence', 'holder', 'director', 'region.name as region')
                        ->where('username', '=', $username)
                        ->get();


        return view('school.index', compact('colegio'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $region = Region::all()->lists('name', 'id');

        return view('school.create', compact('region'));
        //return "Profile School (= ";
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make(Request::all(), 
        [
            //'name'     => 'required|max:50',
            //'username' => 'required|max:50',
            //'password' => 'required|min:6',
            //'email'    => 'required'
            'holder'   => 'required|max:50',
            'director' => 'required|max:50',
            'address'  => 'required|max:50',
            'phone'    => 'required|numeric|min:8',
        ]);

        if ($validator->fails()) 
        {
            return redirect('registrationProfileSchool')
                        ->withErrors($validator)
                        ->withInput();
        }

        $name     = Auth::user()->name;
        $username = Auth::user()->username;
        $mail     = Auth::user()->email;

        $id = Auth::user()->id;
        
        $school = new School();
        $school->name     = $name;
        $school->username = $username;
        $school->email    = $mail;
        $school->address  = Input::get('address');
        $school->phone    = Input::get('phone');
        $school->commune  = Input::get('commune');
        $school->region   = Input::get('region');
        $school->code_rbd = 1234;
        $school->dependence = Input::get('dependence');
        $school->holder     = Input::get('holder');
        $school->director   = Input::get('director');
        $school->save();

        $user = User::find($id);
        $user->new = 1;
        $user->save();

        return redirect('dashboard');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $school = School::find($id);
        $region = Region::all()->lists('name', 'id');

        return view('school.edit', compact('school', 'region'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
