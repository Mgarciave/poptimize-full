<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ScheduleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $teacher_id = Auth::id();

        //Listar Cursos.

        $asignaturas = \DB::table('subject_teacher')
                            ->join('subjects', 'subject_teacher.subject_id', '=', 'subjects.id')
                            ->select('subjects.id', 'name', 'course')
                            ->where('subject_teacher.teacher_id', '=', $teacher_id)
                            ->get();

        return view('schedules.index', compact('asignaturas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function scheduleCreate($id)
    {
      $subject = Subject::find($id);
      return view('schedules.create', compact('subject'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $data = $request->all();
      $teacher = Teacher::with('school')->find(auth()->id());
      $schedule = new Schedule($request->all());
      $schedule->teacher_id = Auth::id();
      $schedule->school_id = Input::get('school');  //Cambiar luego.
      $schedule->color = Input::get('color');
      $schedule->save();
      return "Horario Guardado.";
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
