<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Models\Tag_Teacher;

use DB;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class TagController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

      if($request->input('block_id')){
        $tags = DB::table('tag_teacher')
                      ->select('*')
                      ->where('subject_id','=',$request->input('subject'))
                      ->where('unit_id','=',$request->input('unit'))
                      ->where('teacher_id','=',auth()->id())
                      ->where('block_id','=',$request->input('block_id'))
                      ->get();
        return $tags;
      }

      $tags = DB::table('tag_teacher')
                    ->select('*')
                    ->where('subject_id','=',$request->input('subject'))
                    ->where('unit_id','=',$request->input('unit'))
                    ->where('teacher_id','=',auth()->id())
                    ->get();
      return $tags;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user_id = auth()->id();
        //$request->unitOa;  // subject_id, unit_id, tag_id...
        //$unitTag =  ['26','103','796','797'];

        /*$tamUnitTag = count($unitTag, COUNT_RECURSIVE);

        $subject_id = $unitTag[0];
        $unit_id = $unitTag[1];

        for ($i=2; $i < $tamUnitTag ; $i++) {

            $unitTags = new Tag_Teacher();
            $unitTags->subject_id = $subject_id;
            $unitTags->teacher_id = auth()->id();
            $unitTags->unit_id = $unit_id;
            $unitTags->checked = 1;
            $unitTags->tag_id = $unitTag[$i];

           $unitTags->save();
        }

        return "Tag - Docente - Asignatura Guardado!";*/
        DB::table('tag_teacher')
                  ->where('unit_id', '=', $request->input('unit'))
                  ->where('subject_id','=',$request->input('subject'))
                  ->where('teacher_id','=',auth()->id())
                  ->where('block_id','=',$request->input('id'))
                  ->delete();
        $tags = $request->input('data');
        foreach($tags as $tag){
          if($tag['tag']!=''){
            $unitTags = new Tag_Teacher();
            $unitTags->subject_id = $request->input('subject');
            $unitTags->teacher_id = auth()->id();
            $unitTags->unit_id = $request->input('unit');
            $unitTags->block_id = $request->input('id');
            $unitTags->checked = 1;
            $unitTags->tag_id = $tag['tag'];
            $unitTags->text = $tag['text'];

           $unitTags->save();
         }
        }

        return "Tag - Docente - Asignatura Guardado!";
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
