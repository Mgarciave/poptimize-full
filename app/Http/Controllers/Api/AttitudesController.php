<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Models\Attitude;

use DB;
use Auth;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class AttitudesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $teacher_id = Auth::id();
        $id = $request->input('unit');

        //* TODO: cambiar la query para no depender de las unidades al ir a buscarlas.
      if($request->input('block_id')){
        $attitudes = \DB::table('attitude_unit')
                          ->join('attitudes', 'attitude_unit.attitude_id', '=', 'attitudes.id')
                          ->join('attitude_teacher', 'attitude_teacher.attitude_id','=','attitudes.id')
                          ->select('attitudes.description', 'attitudes.id')
                          ->where('attitude_unit.unit_id', '=', $id)
                          ->where('attitude_teacher.teacher_id','=',$teacher_id)
                          ->where('attitude_teacher.block_id','=', $request->input('block_id'))
                          ->get();
        // $abilities = DB::table('attitude_teacher')
        //               ->select('*')
        //               ->where('subject_id','=',$request->input('subject'))
        //               ->where('unit_id','=',$request->input('unit'))
        //               ->where('teacher_id','=',auth()->id())
        //               ->where('block_id','=',$request->input('block_id'))
        //               ->get();
        return $attitudes;
      }

      $attitudes = \DB::table('attitude_unit')
              ->join('attitudes', 'attitude_unit.attitude_id', '=', 'attitudes.id')
              ->join('attitude_teacher', 'attitude_teacher.attitude_id','=','attitudes.id')
              ->select('attitudes.description', 'attitudes.id')
              ->where('attitude_unit.unit_id', '=', $id)
              ->where('attitude_teacher.teacher_id','=',$teacher_id)
              ->where('attitude_teacher.block_id','=', $request->input('block_id'))
              ->get();
      return $attitudes;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $teacher_id = Auth::id();

        DB::table('attitude_teacher')
                  ->where('teacher_id','=',$teacher_id)
                  ->where('block_id','=',$request->input('id'))
                  ->delete();
        $attitudes = $request->input('data');
        foreach($attitudes as $attitude){
          if($attitude['attitude']!=''){
            DB::table('attitude_teacher')->insert(['attitude_id' => $attitude['attitude'], 'block_id' => $request->input('id'), 'teacher_id' => $teacher_id]);
         }
        }

        return "Tag - Docente - Asignatura Guardado!";
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
