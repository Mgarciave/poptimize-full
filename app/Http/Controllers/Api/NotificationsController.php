<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use DB;

use Auth;
use Input;
use App\Models\Notification;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class NotificationsController extends Controller
{
  public function index()
  {

      $notifications  = \DB::table('notifications')
                          ->select('*')
                          ->get();

      return compact('notifications');
  }

  public function delete(Request $request){
    return DB::table('notifications')->where('id', '=', Input::get('id'))->delete();

  }

  public function store(Request $request)
  {
      $title    = Input::get('titulo');
      $date     = Input::get('fecha');
      $date_end     = Input::get('fecha_end');
      //$end_date     = Input::get('end_date');
      $comment  = Input::get('comentario');

      $note = new Notification();
      $note->name = $title;
      $note->description = $comment;
      $note->date = $date;
      $note->end_date = $date_end;
      $note->course = Input::get('course');
      $note->letter = Input::get('letter');
      $note->subject = Input::get('subject');
      $note->teacher_id = Auth::id();
      $note->save();

      return array('msg'=>'Guardado');
  }
}
