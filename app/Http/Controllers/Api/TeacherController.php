<?php

namespace App\Http\Controllers\Api;

use Auth;
use Input;
use Validator;
use App\Models\User;
use App\Models\Teacher;

use Request;
use App\Http\Controllers\Controller;

class TeacherController extends Controller
  {
    public function store(Request $request)
    {
      $validator = Validator::make(Request::all(),
      [
          //'name'     => 'required|max:50',
          //'username' => 'required|max:50',
          //'password' => 'required|min:6',
          'profession' => 'max:100',
          'specialty'  => 'max:50',
          'phone'      => 'numeric|min:8'
      ]);

      if ($validator->fails())
      {
          /*return redirect('registrationProfile')
                      ->withErrors($validator)
                      ->withInput();*/
          return response()->json($validator->messages(), 200);
      }

      $name     = Input::get('name');
      $username = Input::get('username');
      $mail     = Input::get('email');

      $id = Input::get('id');

      $teacher = new Teacher();
      $teacher->name     = $name;
      $teacher->username = $username;
      $teacher->email    = $mail;
      $teacher->birth_date = Input::get('birth_date');
      $teacher->phone      = Input::get('phone');
      $teacher->specialty  = Input::get('specialty');
      $teacher->years_experience = Input::get('years_experience');
      $teacher->profession = Input::get('profession');
      $teacher->user_id    = $id;
      $teacher->id = $id;
      $teacher->save();

      $user = User::find($id);
      $user->new = 1;
      $user->save();

      return array('Perfil completo');
  }
}
