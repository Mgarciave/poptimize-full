<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Models\Unit;
use App\Models\Tag;
use App\Models\Teacher_Subject_Unit;
use Auth;

use DB;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class UnitController extends Controller
{
  public function unidades($id, Request $request)
  {
      $subject_id = $id;

      $subject = \DB::table('subjects')
                      ->select('name', 'course', 'weekly_hours')
                      ->where('id', '=', $subject_id)
                      ->get();

      $units = \DB::table('units')
                      ->select('name', 'purpose_min', 'hrs', 'id')
                      ->where('subject_id' ,'=', $subject_id)
                      ->get();

      //$order = Teacher_Subject_Unit::where('subject_id', $id)->get();

      $orderU = \DB::table('teacher_subject_unit')
                   ->join('units', 'teacher_subject_unit.unit_id', '=', 'units.id')
                   ->select('name', 'units.id', 'purpose_min', 'hrs', 'units.subject_id', 'order')
                   ->where('units.subject_id', $subject_id)
                   ->where('teacher_subject_unit.teacher_id','=',Auth::id())

                   ->where('teacher_subject_unit.letter','=',$request->input('letter'))
                   ->where('teacher_subject_unit.level','=',$request->input('level'))
                   ->where('teacher_subject_unit.school','=',$request->input('school'))

                   ->orderby('order')
                   ->get();

      return compact('subject', 'units', 'orderU');

  }

  public function show($id)
  {
      //return "Show id: ". $id;

      $dataUnit = \DB::table('units')
                      ->select('units.name', 'units.purpose', 'units.hrs')
                      ->where('id', '=', $id)
                      ->get();

      $abilities = \DB::table('ability_unit')
                          ->join('abilities', 'ability_unit.ability_id', '=', 'abilities.id')
                          ->select('abilities.name_ability', 'abilities.description', 'abilities.id')
                          ->where('ability_unit.unit_id', '=', $id)
                          ->get();

      $pknow = \DB::table('PreviousKnowledge')
                      ->select('description')
                      ->where('unit_id', '=', $id)
                      ->get();

      $know = \DB::table('Knowledge')
                      ->select('description')
                      ->where('unit_id', '=', $id)
                      ->get();

      $tags = \DB::table('tag_unit')
                  ->join('tags', 'tag_unit.tag_id', '=', 'tags.id')
                  ->select('tags.name', 'tags.checked', 'tags.id')
                  ->where('tag_unit.unit_id', '=', $id)
                  ->get();

                  $oas = \DB::table('objective_unit')
                    ->join('units', 'objective_unit.unit_id', '=','units.id')
                    ->join('objectives', 'objective_unit.objective_id', '=', 'objectives.id')
                    ->select('num_obj', 'description', 'description_min', 'name_eje', 'objectives.id')
                    ->where('objective_unit.unit_id', $id)
                    ->orderBy('objectives.id')
                    ->get();

      $attitudes = \DB::table('attitude_unit')
                          ->join('attitudes', 'attitude_unit.attitude_id', '=', 'attitudes.id')
                          ->select('attitudes.description', 'attitudes.id')
                          ->where('attitude_unit.unit_id', '=', $id)
                          ->get();

      //return $abilities;

      return compact('dataUnit', 'abilities', 'pknow', 'tags', 'know', 'oas', 'abilities', 'attitudes' );
  }

  public function store(Request $request)
  {
      $order = [];

      /*$order[0] = $request->get('unidad1');
      $order[1] = $request->get('unidad2');
      $order[2] = $request->get('unidad3');
      $order[3] = $request->get('unidad4');
      $order[4] = $request->get('unidad5');
      $order[5] = $request->get('unidad6');
      $order[6] = $request->get('unidad7');*/

      $subject_id = $request->get('subject_id');

      $unit_id = [];

      /*$unit_id[0] = $request->get('unit_id_1');
      $unit_id[1] = $request->get('unit_id_2');
      $unit_id[2] = $request->get('unit_id_3');
      $unit_id[3] = $request->get('unit_id_4');
      $unit_id[4] = $request->get('unit_id_5');
      $unit_id[5] = $request->get('unit_id_6');
      $unit_id[6] = $request->get('unit_id_7');*/

      foreach($request->input('arr') as $unit){
        DB::table('teacher_subject_unit')
            ->where('unit_id', '=', $unit[0])
            ->where('subject_id','=',$subject_id)
            ->where('level','=',$request->input('level'))
            ->where('letter','=',$request->input('letter'))
            ->where('school','=',$request->input('school'))
            ->where('teacher_id','=',auth()->id())
            ->delete();

        $unit_order = new Teacher_Subject_Unit();
        $unit_order->teacher_id = auth()->id();
        $unit_order->unit_id    = $unit[0];
        $unit_order->subject_id = $subject_id;
        $unit_order->order      = $unit[1];
        $unit_order->school     = $request->input('school');
        $unit_order->letter     = $request->input('letter');
        $unit_order->level      = $request->input('level');

        $unit_order->save();
      }

      for ($i=0; $i < 7; $i++) {
          /*DB::table('teacher_subject_unit')
              ->where('unit_id', '=', $unit_id[$i])
              ->where('subject_id','=',$subject_id)
              ->where('teacher_id','=',auth()->id())
              ->delete();

          $unit_order = new Teacher_Subject_Unit();
          $unit_order->teacher_id = auth()->id();
          $unit_order->unit_id    = $unit_id[$i];
          $unit_order->subject_id = $subject_id;
          $unit_order->order      = $order[$i];*/

          //$unit_order->save();
      }

      //return array('code'=>0);
      return $request->all();
  }


}
