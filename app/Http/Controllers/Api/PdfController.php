<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use Input;
use Auth;

use DB;

use App\Models\Tag;
use App\Models\Unit;
use App\Models\Subject;
use App\Models\Calendar;

use App\Http\Requests;
use App\Http\Controllers\Controller;

ini_set('max_execution_time', 300);

class PdfController extends Controller
{
    public function schedule(){

      $course_id  = Input::get('course_id', false);
        $letter     = Input::get('letter', false);
        $subject_id = Input::get('subject_id', false);

        //return $course_id;

        $events = Calendar::with('course_subject_teacher.course', 'course_subject_teacher.subject')
                            ->whereHas('course_subject_teacher', function ($query) {
                                $query->where('teacher_id', auth()->id())
                                        ->where('course_id', Input::get('course_id', false))
                                        ->where('subject_id', Input::get('subject_id', false))
                                      ->whereHas('course', function ($query) {
                                            $query->where('letter', 'B');
                                        });
                            })->get();

        return $events;

      $view =  \View::make('pdf.horario')->render();
      #return $view;
      $pdf = \App::make('dompdf.wrapper');
      $pdf->loadHTML($view)->setPaper('a4', 'landscape');
      #return \View::make($template, compact('applications', 'worker'));
      return $pdf->stream('horario.pdf');
    }



    public function invoice($id)
    {

        $subject_id = $id;

        $invoice = ['1','2','3','4'];

        $subject =  \DB::table('subjects')
                        ->select('name', 'course', 'id')
                        ->where('id', $subject_id)
                        ->get();

        $name = $subject[0]->name;

        //$order = Teacher_Subject_Unit::where('subject_id', $id)->get();

        $order = DB::select('select * from teacher_subject_unit where subject_id = ?', [$id]);

        $units  = \DB::table('teacher_subject_unit')
                    ->join('units', 'teacher_subject_unit.unit_id', '=', 'units.id')
                    ->select('name', 'units.id', 'purpose_min', 'hrs', 'units.subject_id', 'order')
                    ->where('units.subject_id', $subject_id)
                    ->where('teacher_subject_unit.letter','=',Input::get('letter', false))
                    ->where('teacher_subject_unit.level','=',Input::get('level', false))
                    ->where('teacher_subject_unit.teacher_id','=',Auth::id())
                    ->orderby('order')
                    //teacher_subject_unit.letter = "B" AND teacher_subject_unit.level = 8
                    ->get();


        //return compact('name', 'units', 'invoice', 'order');

        $view =  \View::make('pdf.invoice', compact('name', 'units', 'invoice', 'order'))->render();
        // return $view;
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($view);
        #return \View::make($template, compact('applications', 'worker'));
        return $pdf->stream('unidades.pdf');
    }

    public function printUtpLevel(Request $request){
      $coursename = $request->input('level');

    $utp_id = Auth::id();

    // $utp_id = 153;



    $query = DB::table('user_utp')->select('user_utp.school')->where('teacher_id','=',$utp_id)->get();
    $school = $query[0]->school;

    $query1 = DB::table('courses')->select('*')->where('courses.name','=',$coursename)->join('Schedule as t','courses.id','=','t.course')->where('t.school','=',$school)->groupBy(['t.subject_id','t.course'])->get();

    $arrTeach = [];

    foreach ($query1 as $value) {
      $arrTeach[] = array(
        'teach_id' => $value->teacher_id,
        'asig_id' => $value->subject_id,
        'letter' => $value->letter

        );
    }

    $allinfo = [];

      foreach ($arrTeach as $teachData) {
        $arr = [];
        $teacher_id = $teachData['teach_id'];
        $subject = $teachData['asig_id'];
        $letter = $teachData['letter'];

        $query0 = DB::table('users')->select('name')->where('id','=',$teacher_id)->get();

        $query = DB::table('subjects')->select('name','course')->where('id','=',$subject)->get();

        $query1 = DB::table('Schedule')->select('*')->where('teacher_id','=',$teacher_id)->where('subject_id','=',$subject)->where('letter','=',$letter)->get();
        $totalClass = sizeof($query1);
        $info = 0;
        foreach ($query1 as $value1) {
          if(strlen($value1->description) > 0 || strlen($value1->init_text) >0)
           $info++;
        }
        $query2 = \DB::table('units')->where('subject_id', '=', $subject)->join('objective_unit', 'units.id', '=', 'objective_unit.unit_id')->join('objectives', 'objective_unit.objective_id','=','objectives.id')->groupBy('num_obj')->get();
        $oatotal = sizeof($query2);

        $query3 = \DB::table('Schedule')->join('objective_teacher','Schedule.id','=','objective_teacher.block_id')->select('objective_teacher.text')->where('Schedule.subject_id', '=', $subject)->where('Schedule.teacher_id', '=', $teacher_id)->where('Schedule.letter','=',$letter)->groupBy('objective_teacher.text')->get();
        $oaplan = sizeof($query3);

        $percentClass = $info;
        $percentOA = $oaplan;
        $name = $query0[0]->name.' / '.$query[0]->name.' / '.$query[0]->course.' '.$letter ;
        $idcan = $letter.$subject;
        $idcanoa = $letter.$subject.'OA';

        $arr[] = [$name,$idcan,$totalClass,$percentClass,floor(($percentClass*100)/$totalClass),$oatotal,$percentOA,floor(($percentOA*100)/$oatotal)];

        $query1 = \DB::table('units')->where('subject_id', '=', $subject)->join('objective_unit', 'units.id', '=', 'objective_unit.unit_id')->join('objectives', 'objective_unit.objective_id','=','objectives.id')->select('objectives.*')->groupBy('num_obj')->get();
        // $oatotal = sizeof($query1);
        // var_dump($query1);

        $query2 = \DB::table('Schedule')->join('objective_teacher','Schedule.id','=','objective_teacher.block_id')->select('objective_teacher.*')->where('Schedule.subject_id', '=', $subject)->where('Schedule.teacher_id', '=', $teacher_id)->where('Schedule.letter','=',$letter)->groupBy('objective_teacher.text')->get();
        // $oaplan = sizeof($query2);
        // var_dump($query2);

        $oaseen = [];
        $oaunseen = [];

        $flag = 0;
        foreach ($query1 as $value) {
          foreach ($query2 as $value1) {
            if($value->id == $value1->objective_id){
              $flag = 1;
            }
          }
          if($flag == 1){
            $oaseen[] = array(
              'OA' => 'OA '.$value->num_obj,
              'desc' => $value->description_min
              );
          }
          else{
            $oaunseen[] = array(
              'OA' => 'OA '.$value->num_obj,
              'desc' => $value->description_min
              );
          }
          $flag = 0;
        }

        $fullArr = array(
          'oaseen' => $oaseen,
          'oaunseen' => $oaunseen
          );
        
        $allinfo[] = array('teachInfo' => $arr, 'oaInfo' => $fullArr, 'title' => 'Reporte de Cobertura Curricular - Curso');
      }

      // var_dump($allinfo);
      $view =  \View::make('pdf.utpteacher', compact('allinfo'))->render();
      #return $view;
      $pdf = \App::make('dompdf.wrapper');
      $pdf->loadHTML($view);
      #return \View::make($template, compact('applications', 'worker'));
      return $pdf->stream('utpteacher.pdf');
      // return $allinfo;


  }
    public function printUtpAll(){
      
      // $utp_id = 153;
      $utp_id = Auth::id();

      $query = DB::table('user_utp')->select('user_utp.school')->where('teacher_id','=',$utp_id)->get();
      $school = $query[0]->school;

      $query1 = DB::table('Schedule')->select('*')->where('school','=',$school)->groupBy(['teacher_id','subject_id','course'])->get();

      $arrTeach = [];

      foreach ($query1 as $value) {
        $arrTeach[] = array(
          'teach_id' => $value->teacher_id,
          'asig_id' => $value->subject_id,
          'letter' => $value->letter

          );
      }

      $allinfo = [];

      foreach ($arrTeach as $teachData) {
        $arr = [];
        $teacher_id = $teachData['teach_id'];
        $subject = $teachData['asig_id'];
        $letter = $teachData['letter'];

        $query0 = DB::table('users')->select('name')->where('id','=',$teacher_id)->get();

        $query = DB::table('subjects')->select('name','course')->where('id','=',$subject)->get();

        $query1 = DB::table('Schedule')->select('*')->where('teacher_id','=',$teacher_id)->where('subject_id','=',$subject)->where('letter','=',$letter)->get();
        $totalClass = sizeof($query1);
        $info = 0;
        foreach ($query1 as $value1) {
          if(strlen($value1->description) > 0 || strlen($value1->init_text) >0)
           $info++;
        }
        $query2 = \DB::table('units')->where('subject_id', '=', $subject)->join('objective_unit', 'units.id', '=', 'objective_unit.unit_id')->join('objectives', 'objective_unit.objective_id','=','objectives.id')->groupBy('num_obj')->get();
        $oatotal = sizeof($query2);

        $query3 = \DB::table('Schedule')->join('objective_teacher','Schedule.id','=','objective_teacher.block_id')->select('objective_teacher.text')->where('Schedule.subject_id', '=', $subject)->where('Schedule.teacher_id', '=', $teacher_id)->where('Schedule.letter','=',$letter)->groupBy('objective_teacher.text')->get();
        $oaplan = sizeof($query3);

        $percentClass = $info;
        $percentOA = $oaplan;
        $name = $query0[0]->name.' / '.$query[0]->name.' / '.$query[0]->course.' '.$letter ;
        $idcan = $letter.$subject;
        $idcanoa = $letter.$subject.'OA';

        $arr[] = [$name,$idcan,$totalClass,$percentClass,floor(($percentClass*100)/$totalClass),$oatotal,$percentOA,floor(($percentOA*100)/$oatotal)];

        $query1 = \DB::table('units')->where('subject_id', '=', $subject)->join('objective_unit', 'units.id', '=', 'objective_unit.unit_id')->join('objectives', 'objective_unit.objective_id','=','objectives.id')->select('objectives.*')->groupBy('num_obj')->get();
        // $oatotal = sizeof($query1);
        // var_dump($query1);

        $query2 = \DB::table('Schedule')->join('objective_teacher','Schedule.id','=','objective_teacher.block_id')->select('objective_teacher.*')->where('Schedule.subject_id', '=', $subject)->where('Schedule.teacher_id', '=', $teacher_id)->where('Schedule.letter','=',$letter)->groupBy('objective_teacher.text')->get();
        // $oaplan = sizeof($query2);
        // var_dump($query2);

        $oaseen = [];
        $oaunseen = [];

        $flag = 0;
        foreach ($query1 as $value) {
          foreach ($query2 as $value1) {
            if($value->id == $value1->objective_id){
              $flag = 1;
            }
          }
          if($flag == 1){
            $oaseen[] = array(
              'OA' => 'OA '.$value->num_obj,
              'desc' => $value->description_min
              );
          }
          else{
            $oaunseen[] = array(
              'OA' => 'OA '.$value->num_obj,
              'desc' => $value->description_min
              );
          }
          $flag = 0;
        }

        $fullArr = array(
          'oaseen' => $oaseen,
          'oaunseen' => $oaunseen
          );
        
        $allinfo[] = array('teachInfo' => $arr, 'oaInfo' => $fullArr, 'title' => 'Reporte de Cobertura Curricular');
      }

      // var_dump($allinfo);
      $view =  \View::make('pdf.utpteacher', compact('allinfo'))->render();
      #return $view;
      $pdf = \App::make('dompdf.wrapper');
      $pdf->loadHTML($view);
      #return \View::make($template, compact('applications', 'worker'));
      return $pdf->stream('utpteacher.pdf');
      // return $allinfo;

    }

    public function printUtpAsign(Request $request){
      $asignname = $request->input('asign');
      // $utp_id = 153;
      $utp_id = Auth::id();

      $query = DB::table('user_utp')->select('user_utp.school')->where('teacher_id','=',$utp_id)->get();
      $school = $query[0]->school;

      $query1 = DB::table('subjects')->select('*')->where('subjects.name','=',$asignname)->join('Schedule as t','subjects.id','=','t.subject_id')->where('t.school','=',$school)->groupBy('t.course')->orderBy('subjects.order')->get();

      $arrTeach = [];

      foreach ($query1 as $value) {
        $arrTeach[] = array(
          'teach_id' => $value->teacher_id,
          'asig_id' => $value->subject_id,
          'letter' => $value->letter

          );
      }

      $allinfo = [];

      foreach ($arrTeach as $teachData) {
        $arr = [];
        $teacher_id = $teachData['teach_id'];
        $subject = $teachData['asig_id'];
        $letter = $teachData['letter'];

        $query0 = DB::table('users')->select('name')->where('id','=',$teacher_id)->get();

        $query = DB::table('subjects')->select('name','course')->where('id','=',$subject)->get();

        $query1 = DB::table('Schedule')->select('*')->where('teacher_id','=',$teacher_id)->where('subject_id','=',$subject)->where('letter','=',$letter)->get();
        $totalClass = sizeof($query1);
        $info = 0;
        foreach ($query1 as $value1) {
          if(strlen($value1->description) > 0 || strlen($value1->init_text) >0)
           $info++;
        }
        $query2 = \DB::table('units')->where('subject_id', '=', $subject)->join('objective_unit', 'units.id', '=', 'objective_unit.unit_id')->join('objectives', 'objective_unit.objective_id','=','objectives.id')->groupBy('num_obj')->get();
        $oatotal = sizeof($query2);

        $query3 = \DB::table('Schedule')->join('objective_teacher','Schedule.id','=','objective_teacher.block_id')->select('objective_teacher.text')->where('Schedule.subject_id', '=', $subject)->where('Schedule.teacher_id', '=', $teacher_id)->where('Schedule.letter','=',$letter)->groupBy('objective_teacher.text')->get();
        $oaplan = sizeof($query3);

        $percentClass = $info;
        $percentOA = $oaplan;
        $name = $query0[0]->name.' / '.$query[0]->name.' / '.$query[0]->course.' '.$letter ;
        $idcan = $letter.$subject;
        $idcanoa = $letter.$subject.'OA';

        $arr[] = [$name,$idcan,$totalClass,$percentClass,floor(($percentClass*100)/$totalClass),$oatotal,$percentOA,floor(($percentOA*100)/$oatotal)];

        $query1 = \DB::table('units')->where('subject_id', '=', $subject)->join('objective_unit', 'units.id', '=', 'objective_unit.unit_id')->join('objectives', 'objective_unit.objective_id','=','objectives.id')->select('objectives.*')->groupBy('num_obj')->get();
        // $oatotal = sizeof($query1);
        // var_dump($query1);

        $query2 = \DB::table('Schedule')->join('objective_teacher','Schedule.id','=','objective_teacher.block_id')->select('objective_teacher.*')->where('Schedule.subject_id', '=', $subject)->where('Schedule.teacher_id', '=', $teacher_id)->where('Schedule.letter','=',$letter)->groupBy('objective_teacher.text')->get();
        // $oaplan = sizeof($query2);
        // var_dump($query2);

        $oaseen = [];
        $oaunseen = [];

        $flag = 0;
        foreach ($query1 as $value) {
          foreach ($query2 as $value1) {
            if($value->id == $value1->objective_id){
              $flag = 1;
            }
          }
          if($flag == 1){
            $oaseen[] = array(
              'OA' => 'OA '.$value->num_obj,
              'desc' => $value->description_min
              );
          }
          else{
            $oaunseen[] = array(
              'OA' => 'OA '.$value->num_obj,
              'desc' => $value->description_min
              );
          }
          $flag = 0;
        }

        $fullArr = array(
          'oaseen' => $oaseen,
          'oaunseen' => $oaunseen
          );
        
        $allinfo[] = array('teachInfo' => $arr, 'oaInfo' => $fullArr, 'title' => 'Reporte de Cobertura Curricular - Asignatura');
      }

      // var_dump($allinfo);
      $view =  \View::make('pdf.utpteacher', compact('allinfo'))->render();
      #return $view;
      $pdf = \App::make('dompdf.wrapper');
      $pdf->loadHTML($view);
      #return \View::make($template, compact('applications', 'worker'));
      return $pdf->stream('utpteacher.pdf');
      // return $allinfo;
      
    }

    public function printUtpTeacher(Request $request){

      $teacher_id = $request->input('id');
      $query = DB::table('Schedule')->select('*')->where('teacher_id','=',$teacher_id)->get();
      $asig_data = '';
      foreach ($query as $value) {
        $asig_data[] = array('asig_id' => $value->subject_id, 'letter' =>$value->letter);
      }

      $allinfo = [];

      $asig_data = array_unique($asig_data, SORT_REGULAR);

      foreach ($asig_data as $teachData) {
        $arr = [];
        $subject = $teachData['asig_id'];
        $letter = $teachData['letter'];

        $query0 = DB::table('users')->select('name')->where('id','=',$teacher_id)->get();

        $query = DB::table('subjects')->select('name','course')->where('id','=',$subject)->get();

        $query1 = DB::table('Schedule')->select('*')->where('teacher_id','=',$teacher_id)->where('subject_id','=',$subject)->where('letter','=',$letter)->get();
        $totalClass = sizeof($query1);
        $info = 0;
        foreach ($query1 as $value1) {
          if(strlen($value1->description) > 0 || strlen($value1->init_text) >0)
           $info++;
        }
        $query2 = \DB::table('units')->where('subject_id', '=', $subject)->join('objective_unit', 'units.id', '=', 'objective_unit.unit_id')->join('objectives', 'objective_unit.objective_id','=','objectives.id')->groupBy('num_obj')->get();
        $oatotal = sizeof($query2);

        $query3 = \DB::table('Schedule')->join('objective_teacher','Schedule.id','=','objective_teacher.block_id')->select('objective_teacher.text')->where('Schedule.subject_id', '=', $subject)->where('Schedule.teacher_id', '=', $teacher_id)->where('Schedule.letter','=',$letter)->groupBy('objective_teacher.text')->get();
        $oaplan = sizeof($query3);

        $percentClass = $info;
        $percentOA = $oaplan;
        $name = $query0[0]->name.' / '.$query[0]->name.' / '.$query[0]->course.' '.$letter ;
        $idcan = $letter.$subject;
        $idcanoa = $letter.$subject.'OA';

        $arr[] = [$name,$idcan,$totalClass,$percentClass,floor(($percentClass*100)/$totalClass),$oatotal,$percentOA,floor(($percentOA*100)/$oatotal)];

        $query1 = \DB::table('units')->where('subject_id', '=', $subject)->join('objective_unit', 'units.id', '=', 'objective_unit.unit_id')->join('objectives', 'objective_unit.objective_id','=','objectives.id')->select('objectives.*')->groupBy('num_obj')->get();
        // $oatotal = sizeof($query1);
        // var_dump($query1);

        $query2 = \DB::table('Schedule')->join('objective_teacher','Schedule.id','=','objective_teacher.block_id')->select('objective_teacher.*')->where('Schedule.subject_id', '=', $subject)->where('Schedule.teacher_id', '=', $teacher_id)->where('Schedule.letter','=',$letter)->groupBy('objective_teacher.text')->get();
        // $oaplan = sizeof($query2);
        // var_dump($query2);

        $oaseen = [];
        $oaunseen = [];

        $flag = 0;
        foreach ($query1 as $value) {
          foreach ($query2 as $value1) {
            if($value->id == $value1->objective_id){
              $flag = 1;
            }
          }
          if($flag == 1){
            $oaseen[] = array(
              'OA' => 'OA '.$value->num_obj,
              'desc' => $value->description_min
              );
          }
          else{
            $oaunseen[] = array(
              'OA' => 'OA '.$value->num_obj,
              'desc' => $value->description_min
              );
          }
          $flag = 0;
        }

        $fullArr = array(
          'oaseen' => $oaseen,
          'oaunseen' => $oaunseen
          );
        
        $allinfo[] = array('teachInfo' => $arr, 'oaInfo' => $fullArr, 'title' => 'Reporte de Cobertura Curricular - Profesor');
      }

      // var_dump($allinfo);
      $view =  \View::make('pdf.utpteacher', compact('allinfo'))->render();
      #return $view;
      $pdf = \App::make('dompdf.wrapper');
      $pdf->loadHTML($view);
      #return \View::make($template, compact('applications', 'worker'));
      return $pdf->stream('utpteacher.pdf');
      // return $allinfo;

    }

    public function allCourseData(Request $request){
    
    $id = Auth::id();
    $course_id = $request->input('course_id');
    $subject_id = $request->input('subject_id');

    $arrsch = '';
    $query = DB::table('Schedule')->select('*')->where('teacher_id','=',$id)->where('course','=',$course_id)->where('subject_id','=',$subject_id)->orderBy('Schedule.hora_inicio','asc')->get();

    foreach ($query as $q) {
      if(sizeof($q->init_text) ){
        $query1 = DB::table('objective_teacher')->select('*')->where('block_id', '=', $q->id)->get();
        if(sizeof($query1) > 0){
          $arrsch[] = $q->id;
        }
      }
    }
    $calendar = '';
    if($arrsch != ''){
      foreach ($arrsch as $schedule_id) {
        $number = $schedule_id;
        

        $object = DB::table('objective_teacher')->select('*')->where('block_id', '=', $number)->get();
        if($object==NULL){
          return NULL;
        }
        //('select * from objective_teacher where block_id = ? order desc limit 1', [$number]);
        $unit_id = $object[0]->unit_id;
        $subject_id = $object[0]->subject_id;

        $query = DB::table('units')->select('name')->where('id', "=", $unit_id)->get();
        $query1 = DB::table('subjects')->select('name', 'course')->where('id', '=', $subject_id)->get();

        $unit = $query[0]->name;
        $asign = $query1[0]->name;
        $course = $query1[0]->course;

        $oas = DB::select( DB::raw("SELECT * FROM objectives, objective_teacher WHERE objectives.id = objective_teacher.objective_id AND objective_teacher.block_id = ".$number." AND objective_teacher.teacher_id =".Auth::id()) );

        $abilities = DB::table('ability_unit')->select('description')->join('abilities','abilities.id','=','ability_unit.ability_id')->join('ability_teacher','abilities.id','=','ability_teacher.ability_id')->where('ability_unit.unit_id','=',$unit_id)->where('ability_teacher.block_id','=',$number)->get();

        $attitudes = DB::table('attitude_unit')->select('description')->join('attitudes','attitudes.id','=','attitude_unit.attitude_id')->join('attitude_teacher','attitudes.id','=','attitude_teacher.attitude_id')->where('attitude_unit.unit_id','=',$unit_id)->where('attitude_teacher.block_id','=',$number)->get();

        $tags = DB::table('tag_unit')->select('name')->join('tags','tags.id','=','tag_unit.tag_id')->join('tag_teacher','tags.id','=','tag_teacher.tag_id')->where('tag_unit.unit_id','=',$unit_id)->where('tag_teacher.block_id','=',$number)->get();


        $scheduleData = DB::table('Schedule')->select('*')->where('id','=',$number)->get() [0];

        $calendar[] = array(
          'id' => $number,
          'unit' => $unit,
          'inicio' => $scheduleData->hora_inicio,
          'letra' => $scheduleData->letter,
          'fin' => $scheduleData->hora_fin,
          'asignatura' => $asign,
          'curso' => $course,
          'color' => $scheduleData->color,
          'abilities' => $abilities,
          'tags' => $tags,
          'attitudes' => $attitudes,
          'description' => $scheduleData->description,
          'oas' => $oas,
          'init_text' => $scheduleData->init_text,
          'mid_text' => $scheduleData->mid_text,
          'end_text' => $scheduleData->end_text,
          'res_text' => $scheduleData->res_text,
          'eval_text' => $scheduleData->eval_text,
          'ec_text' => $scheduleData->ec_text

          );

      }
    }
      $view =  \View::make('pdf.calendar', compact('calendar','nombreMes'))->render();
      $pdf = \App::make('dompdf.wrapper');
      $pdf->loadHTML($view)->setPaper('a4', 'portrait');
      return $pdf->stream('planunit.pdf');
    // var_dump($fullarr);
    }

    public function monthData(Request $request){

    $id = Auth::id();
    $nombreMes = $request->input('mes');
    $num_mes = '';
    $subject_id = $request->input('subject_id');
    $letra = $request->input('letra');

    switch ($nombreMes) {
      case 'Enero':
        $num_mes = '01';
        break;
      case 'Febrero':
        $num_mes = '02';
        break;
      case 'Marzo':
        $num_mes = '03';
        break;
      case 'Abril':
        $num_mes = '04';
        break;
      case 'Mayo':
        $num_mes = '05';
        break;
      case 'Junio':
        $num_mes = '06';
        break;
      case 'Julio':
        $num_mes = '07';
        break;
      case 'Agosto':
        $num_mes = '08';
        break;
      case 'Septiembre':
        $num_mes = '09';
        break;
      case 'Octubre':
        $num_mes = '10';
        break;
      case 'Noviembre':
        $num_mes = '11';
        break;
      case 'Diciembre':
        $num_mes = '12';
        break;
    }

    $arrsch = '';
    $query = DB::table('Schedule')->select('*')->where('subject_id','=', $subject_id)->where('teacher_id','=',$id)->where('letter', '=', $letra)->orderBy('Schedule.hora_inicio','asc')->get();

    foreach ($query as $q) {
      $mes_check = explode('-', $q->hora_inicio) [1];
      if($mes_check == $num_mes && sizeof($q->init_text) ){
        $query1 = DB::table('objective_teacher')->select('*')->where('block_id', '=', $q->id)->get();
        if(sizeof($query1) > 0){
          $arrsch[] = $q->id;
        }
      }
    }
    $calendar = [];
    if($arrsch != ''){
      foreach ($arrsch as $schedule_id) {
        $number = $schedule_id;
        

        $object = DB::table('objective_teacher')->select('*')->where('block_id', '=', $number)->get();
        if($object==NULL){
          return NULL;
        }
        //('select * from objective_teacher where block_id = ? order desc limit 1', [$number]);
        $unit_id = $object[0]->unit_id;
        $subject_id = $object[0]->subject_id;

        $query = DB::table('units')->select('name')->where('id', "=", $unit_id)->get();
        $query1 = DB::table('subjects')->select('name', 'course')->where('id', '=', $subject_id)->get();

        $unit = $query[0]->name;
        $asign = $query1[0]->name;
        $course = $query1[0]->course;

        $oas = DB::select( DB::raw("SELECT * FROM objectives, objective_teacher WHERE objectives.id = objective_teacher.objective_id AND objective_teacher.block_id = ".$number." AND objective_teacher.teacher_id =".Auth::id()." GROUP BY objectives.num_obj") );

        $abilities = DB::table('ability_unit')->select('description')->join('abilities','abilities.id','=','ability_unit.ability_id')->join('ability_teacher','abilities.id','=','ability_teacher.ability_id')->where('ability_unit.unit_id','=',$unit_id)->where('ability_teacher.block_id','=',$number)->get();

        $attitudes = DB::table('attitude_unit')->select('description')->join('attitudes','attitudes.id','=','attitude_unit.attitude_id')->join('attitude_teacher','attitudes.id','=','attitude_teacher.attitude_id')->where('attitude_unit.unit_id','=',$unit_id)->where('attitude_teacher.block_id','=',$number)->get();

        $tags = DB::table('tag_unit')->select('name')->join('tags','tags.id','=','tag_unit.tag_id')->join('tag_teacher','tags.id','=','tag_teacher.tag_id')->where('tag_unit.unit_id','=',$unit_id)->where('tag_teacher.block_id','=',$number)->get();


        $scheduleData = DB::table('Schedule')->select('*')->where('id','=',$number)->get() [0];

        $calendar[] = array(
          'id' => $number,
          'unit' => $unit,
          'inicio' => $scheduleData->hora_inicio,
          'letra' => $scheduleData->letter,
          'fin' => $scheduleData->hora_fin,
          'asignatura' => $asign,
          'curso' => $course,
          'color' => $scheduleData->color,
          'abilities' => $abilities,
          'tags' => $tags,
          'attitudes' => $attitudes,
          'description' => $scheduleData->description,
          'oas' => $oas,
          'init_text' => $scheduleData->init_text,
          'mid_text' => $scheduleData->mid_text,
          'end_text' => $scheduleData->end_text,
          'res_text' => $scheduleData->res_text,
          'eval_text' => $scheduleData->eval_text,
          'ec_text' => $scheduleData->ec_text

          );

      }
    }
      $view =  \View::make('pdf.calendar', compact('calendar','nombreMes'))->render();
      // return $view;
      $pdf = \App::make('dompdf.wrapper');
      $pdf->loadHTML($view)->setPaper('a4', 'portrait');
      return $pdf->stream('planunit.pdf');
    // var_dump($fullarr);
  }

    public function planificationPrint($schedule_id){

      $number = $schedule_id;
  
      $calendar = [];
      $object = DB::table('objective_teacher')->select('*')->where('block_id', '=', $number)->get();
        if(sizeof($object) > 0){
            $unit_id = $object[0]->unit_id;
            $queryun = DB::table('units')->select('name')->where('id', "=", $unit_id)->get();
            $unit = $queryun[0]->name;
          }
          else{
            $unit = '';
          }

        $oas = DB::select( DB::raw("SELECT * FROM objectives, objective_teacher WHERE objectives.id = objective_teacher.objective_id AND objective_teacher.block_id = ".$number." AND objective_teacher.teacher_id =".Auth::id()." GROUP BY objectives.num_obj") );

        $abilities = DB::table('ability_teacher')->select('abilities.description')->join('abilities','ability_teacher.ability_id','=','abilities.id')->where('ability_teacher.block_id','=',$number)->get();

        $attitudes = DB::table('attitude_teacher')->select('attitudes.description')->join('attitudes','attitude_teacher.attitude_id','=','attitudes.id')->where('attitude_teacher.block_id','=',$number)->get();

        $tags = DB::table('tag_teacher')->select('tags.name')->join('tags','tag_teacher.tag_id','=','tags.id')->where('tag_teacher.block_id','=',$number)->get();

        $scheduleData = DB::table('Schedule')->select('Schedule.*','subjects.name','subjects.course as p')->join('subjects','subjects.id','=','Schedule.subject_id')->where('Schedule.id','=',$number)->get() [0];

        $fecha = $scheduleData->hora_inicio;

        $fecha = explode(' ', $fecha) [0];

        $fecha = explode('-', $fecha) [1];

        switch ($fecha) {
          case '01':
            $fecha = 'Enero';
            break;
          case '02':
            $fecha = 'Febrero';
            break;
          case '03':
            $fecha = 'Marzo';
            break;
          case '04':
            $fecha = 'Abril';
            break;
          case '05':
            $fecha = 'Mayo';
            break;
          case '06':
            $fecha = 'Junio';
            break;
          case '07':
            $fecha = 'Julio';
            break;
          case '08':
            $fecha = 'Agosto';
            break;
          case '09':
            $fecha = 'Septiembre';
            break;
          case '10':
            $fecha = 'Octubre';
            break;
          case '11':
            $fecha = 'Noviembre';
            break;
          case '12':
            $fecha = 'Diciembre';
            break;
        }

        $nombreMes = $fecha;

        $calendar[] = array(
          'id' => $number,
          'unit' => $unit,
          'inicio' => $scheduleData->hora_inicio,
          'letra' => $scheduleData->letter,
          'fin' => $scheduleData->hora_fin,
          'asignatura' => $scheduleData->name,
          'curso' => $scheduleData->p,
          'color' => $scheduleData->color,
          'abilities' => $abilities,
          'tags' => $tags,
          'attitudes' => $attitudes,
          'description' => $scheduleData->description,
          'oas' => $oas,
          'init_text' => $scheduleData->init_text,
          'mid_text' => $scheduleData->mid_text,
          'end_text' => $scheduleData->end_text,
          'res_text' => $scheduleData->res_text,
          'eval_text' => $scheduleData->eval_text,
          'ec_text' => $scheduleData->ec_text

          );
      // // foreach ($dataoa as $oa) {
      //   var_dump($oa['name']);
      // }
      // // var_dump($query2);

      $view =  \View::make('pdf.calendar', compact('calendar','nombreMes'))->render();
      $pdf = \App::make('dompdf.wrapper');
      $pdf->loadHTML($view)->setPaper('a4', 'portrait');
      return $pdf->stream('planunit.pdf');
    // var_dump($fullarr);

    }

    public function detailUnit($id)
    {
        //return "Works!".$id;
        $unit_id = $id;

        $unit = Unit::all()->find($id);

        $unitName = $unit->name;
        $subject_id = $unit->subject_id;
        $unit_hrs = $unit->hrs;

        $subject = Subject::all()->find($subject_id);
        $subjectName = $subject->name.' - '. $subject->course;

        $dataUnit = \DB::table('units')
                        ->select('name', 'purpose', 'hrs')
                        ->where('id', '=', $unit_id)
                        ->get();

        $pknow = \DB::table('PreviousKnowledge')
                        ->select('description')
                        ->where('unit_id', '=', $unit_id)
                        ->get();

        $know = \DB::table('Knowledge')
                        ->select('description')
                        ->where('unit_id', '=', $unit_id)
                        ->get();


        $tags = \DB::table('tag_unit')
                    ->join('tags', 'tag_unit.tag_id', '=', 'tags.id')
                    ->select('tags.name', 'tags.checked')
                    ->where('tag_unit.unit_id', '=', $unit_id)
                    ->get();

        $oas = \DB::table('objective_unit')
                    ->join('units', 'objective_unit.unit_id', '=','units.id')
                    ->join('objectives', 'objective_unit.objective_id', '=', 'objectives.id')
                    ->select('num_obj', 'description', 'description_min', 'name_eje')
                    ->where('objective_unit.unit_id',$unit_id)
                    ->get();

        $attitudes = \DB::table('attitude_unit')
                            ->join('attitudes', 'attitude_unit.attitude_id', '=', 'attitudes.id')
                            ->select('attitudes.description')
                            ->where('attitude_unit.unit_id', $id)
                            ->get();

        $abilities = \DB::table('ability_unit')
                            ->join('abilities', 'ability_unit.ability_id', '=', 'abilities.id')
                            ->select('abilities.name_ability', 'abilities.description')
                            ->where('ability_unit.unit_id', '=', $unit_id)
                            ->get();


        $view =  \View::make('pdf.detalleUnidad', compact('unitName', 'subjectName', 'unit_hrs', 'dataUnit', 'pknow', 'know', 'tags', 'oas', 'abilities', 'attitudes'))->render();

        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($view)->setPaper('a4', 'portrait');
        return $pdf->stream('invoice.pdf');

    }

    public function calendarWeek(Request $request){
      $events = $request->input('events');
      $results = DB::table('Schedule')->select('Schedule.*','subjects.name','subjects.course as p')->whereIn('Schedule.id',$events)->join('subjects','Schedule.subject_id','=','subjects.id')->orderBy('Schedule.hora_inicio','asc')->get();

      
      foreach($results as $r){
          $object = DB::table('objective_teacher')->select('*')->where('block_id','=',$r->id)->get();
          if(sizeof($object) > 0){
            $unit_id = $object[0]->unit_id;
            $queryun = DB::table('units')->select('name')->where('id', "=", $unit_id)->get();
            $unit = $queryun[0]->name;
          }
          else{
            $unit = '';
          }
          //$oas = DB::table('objective_teacher')->select('*')->where('block_id','=',$r->id)->get();
          $oas = DB::select( DB::raw("SELECT * FROM objectives, objective_teacher WHERE objectives.id = objective_teacher.objective_id AND objective_teacher.block_id = ".$r->id." AND objective_teacher.teacher_id =".Auth::id()) );


          $tags = DB::table('tag_teacher')->select('tags.name')->join('tags', 'tag_teacher.tag_id','=','tags.id')->where('tag_teacher.block_id','=',$r->id)->get();
          // $tags = DB::select( DB::raw("SELECT * FROM tag_teacher WHERE block_id = ".$r->id." AND teacher_id = ".Auth::id()) );
          $abilities = DB::table('ability_teacher')->select('description')->join('abilities', 'ability_teacher.ability_id','=','abilities.id')->where('ability_teacher.block_id','=',$r->id)->get();
          $attitudes = DB::table('attitude_teacher')->select('description')->join('attitudes', 'attitude_teacher.attitude_id','=','attitudes.id')->where('attitude_teacher.block_id','=',$r->id)->get();
          // $abilities = DB::select( DB::raw("SELECT * FROM ability_teacher WHERE block_id = ".$r->id." AND teacher_id = ".Auth::id()) );
          // $attitudes = DB::select( DB::raw("SELECT * FROM attitude_teacher WHERE block_id = ".$r->id." AND teacher_id = ".Auth::id()) );
          $res[] = array(
            'id' => $r->id,
            'unit' => $unit,
            'curso'=>$r->p,
            'letra'=>$r->letter,
            'colegio'=>$r->school,
            'asignatura' => $r->name,
            'inicio' => $r->hora_inicio,
            'fin' => $r->hora_fin,
            'color' => $r->color,
            'oas' => $oas,
            'description' => $r->description,
            'tags' => $tags,
            'abilities' => $abilities,
            'attitudes' => $attitudes,
            'init_text' => $r->init_text,
            'mid_text' => $r->mid_text,
            'end_text' => $r->end_text,
            'res_text' => $r->res_text,
            'eval_text' => $r->eval_text,
            'ec_text' => $r->ec_text
          );
        }

        $calendar = $res;


        // return $res;
        //return $calendar;

        //if(count($calendar)>1){
          $view =  \View::make('pdf.calendar', compact('calendar', 'nombreMes'))->render();
          $pdf = \App::make('dompdf.wrapper');
          $pdf->loadHTML($view)->setPaper('a4', 'portrait');
          return $pdf->stream('calendar.pdf');
        //}else{
        //  return array('code'=>500, 'msg'=>'No hay suficiente data');
        //}

    }

    public function calendarFull(){
      $teacher_id = Auth::id();

      $results = DB::table('Schedule')->select('Schedule.*','subjects.name','subjects.course as p')->where('teacher_id','=',$teacher_id)->join('subjects','Schedule.subject_id','=','subjects.id')->orderBy('Schedule.hora_inicio','asc')->get();

      foreach($results as $r){
          $object = DB::table('objective_teacher')->select('*')->where('block_id','=',$r->id)->get();
          if(sizeof($object) > 0){
            $unit_id = $object[0]->unit_id;
            $queryun = DB::table('units')->select('name')->where('id', "=", $unit_id)->get();
            $unit = $queryun[0]->name;
          }
          else{
            $unit = '';
          }
          //$oas = DB::table('objective_teacher')->select('*')->where('block_id','=',$r->id)->get();
          $oas = DB::select( DB::raw("SELECT * FROM objectives, objective_teacher WHERE objectives.id = objective_teacher.objective_id AND objective_teacher.block_id = ".$r->id." AND objective_teacher.teacher_id =".Auth::id()) );


          $tags = DB::table('tag_teacher')->select('tags.name')->join('tags', 'tag_teacher.tag_id','=','tags.id')->where('tag_teacher.block_id','=',$r->id)->get();
          // $tags = DB::select( DB::raw("SELECT * FROM tag_teacher WHERE block_id = ".$r->id." AND teacher_id = ".Auth::id()) );
          $abilities = DB::table('ability_teacher')->select('description')->join('abilities', 'ability_teacher.ability_id','=','abilities.id')->where('ability_teacher.block_id','=',$r->id)->get();
          $attitudes = DB::table('attitude_teacher')->select('description')->join('attitudes', 'attitude_teacher.attitude_id','=','attitudes.id')->where('attitude_teacher.block_id','=',$r->id)->get();
          // $abilities = DB::select( DB::raw("SELECT * FROM ability_teacher WHERE block_id = ".$r->id." AND teacher_id = ".Auth::id()) );
          // $attitudes = DB::select( DB::raw("SELECT * FROM attitude_teacher WHERE block_id = ".$r->id." AND teacher_id = ".Auth::id()) );
          $res[] = array(
            'id' => $r->id,
            'unit' => $unit,
            'curso'=>$r->p,
            'letra'=>$r->letter,
            'colegio'=>$r->school,
            'asignatura' => $r->name,
            'inicio' => $r->hora_inicio,
            'fin' => $r->hora_fin,
            'color' => $r->color,
            'oas' => $oas,
            'description' => $r->description,
            'tags' => $tags,
            'abilities' => $abilities,
            'attitudes' => $attitudes,
            'init_text' => $r->init_text,
            'mid_text' => $r->mid_text,
            'end_text' => $r->end_text,
            'res_text' => $r->res_text,
            'eval_text' => $r->eval_text,
            'ec_text' => $r->ec_text
          );
        }

        $calendar = $res;


        // return $res;
        //return $calendar;

        //if(count($calendar)>1){
          $view =  \View::make('pdf.calendar', compact('calendar', 'nombreMes'))->render();
          $pdf = \App::make('dompdf.wrapper');
          $pdf->loadHTML($view)->setPaper('a4', 'portrait');
          return $pdf->stream('calendar.pdf');
        //}else{
        //  return array('code'=>500, 'msg'=>'No hay suficiente data');
        //}

    }

    public function calendarPrint(Request $request)
    {
    $id = Auth::id();
    $nombreMes = $request->input('mes');
    $num_mes = '';

    switch ($nombreMes) {
      case 'Enero':
        $num_mes = '01';
        break;
      case 'Febrero':
        $num_mes = '02';
        break;
      case 'Marzo':
        $num_mes = '03';
        break;
      case 'Abril':
        $num_mes = '04';
        break;
      case 'Mayo':
        $num_mes = '05';
        break;
      case 'Junio':
        $num_mes = '06';
        break;
      case 'Julio':
        $num_mes = '07';
        break;
      case 'Agosto':
        $num_mes = '08';
        break;
      case 'Septiembre':
        $num_mes = '09';
        break;
      case 'Octubre':
        $num_mes = '10';
        break;
      case 'Noviembre':
        $num_mes = '11';
        break;
      case 'Diciembre':
        $num_mes = '12';
        break;
    }

    $arrsch = '';
    $query = DB::table('Schedule')->select('*')->where('teacher_id','=',$id)->orderBy('Schedule.hora_inicio','asc')->get();

    foreach ($query as $q) {
      $mes_check = explode('-', $q->hora_inicio) [1];
      if($mes_check == $num_mes && sizeof($q->init_text) ){
        $query1 = DB::table('objective_teacher')->select('*')->where('block_id', '=', $q->id)->get();
        if(sizeof($query1) > 0){
          $arrsch[] = $q->id;
        }
      }
    }
    $calendar = [];
    if($arrsch != ''){
      foreach ($arrsch as $schedule_id) {
        $number = $schedule_id;
        

        $object = DB::table('objective_teacher')->select('*')->where('block_id', '=', $number)->get();
        if($object==NULL){
          return NULL;
        }
        //('select * from objective_teacher where block_id = ? order desc limit 1', [$number]);
        $unit_id = $object[0]->unit_id;
        $subject_id = $object[0]->subject_id;

        $query = DB::table('units')->select('name')->where('id', "=", $unit_id)->get();
        $query1 = DB::table('subjects')->select('name', 'course')->where('id', '=', $subject_id)->get();

        $unit = $query[0]->name;
        $asign = $query1[0]->name;
        $course = $query1[0]->course;

        $oas = DB::select( DB::raw("SELECT * FROM objectives, objective_teacher WHERE objectives.id = objective_teacher.objective_id AND objective_teacher.block_id = ".$number." AND objective_teacher.teacher_id =".Auth::id()." GROUP BY objectives.num_obj") );

        $abilities = DB::table('ability_unit')->select('description')->join('abilities','abilities.id','=','ability_unit.ability_id')->join('ability_teacher','abilities.id','=','ability_teacher.ability_id')->where('ability_unit.unit_id','=',$unit_id)->where('ability_teacher.block_id','=',$number)->get();

        $attitudes = DB::table('attitude_unit')->select('description')->join('attitudes','attitudes.id','=','attitude_unit.attitude_id')->join('attitude_teacher','attitudes.id','=','attitude_teacher.attitude_id')->where('attitude_unit.unit_id','=',$unit_id)->where('attitude_teacher.block_id','=',$number)->get();

        $tags = DB::table('tag_unit')->select('name')->join('tags','tags.id','=','tag_unit.tag_id')->join('tag_teacher','tags.id','=','tag_teacher.tag_id')->where('tag_unit.unit_id','=',$unit_id)->where('tag_teacher.block_id','=',$number)->get();


        $scheduleData = DB::table('Schedule')->select('*')->where('id','=',$number)->get() [0];

        $calendar[] = array(
          'id' => $number,
          'unit' => $unit,
          'inicio' => $scheduleData->hora_inicio,
          'letra' => $scheduleData->letter,
          'fin' => $scheduleData->hora_fin,
          'asignatura' => $asign,
          'curso' => $course,
          'color' => $scheduleData->color,
          'abilities' => $abilities,
          'tags' => $tags,
          'attitudes' => $attitudes,
          'description' => $scheduleData->description,
          'oas' => $oas,
          'init_text' => $scheduleData->init_text,
          'mid_text' => $scheduleData->mid_text,
          'end_text' => $scheduleData->end_text,
          'res_text' => $scheduleData->res_text,
          'eval_text' => $scheduleData->eval_text,
          'ec_text' => $scheduleData->ec_text

          );

      }
    }
      $view =  \View::make('pdf.calendar', compact('calendar','nombreMes'))->render();
      $pdf = \App::make('dompdf.wrapper');
      $pdf->loadHTML($view)->setPaper('a4', 'portrait');
      return $pdf->stream('planunit.pdf');
    // var_dump($fullarr);


    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
