<?php

namespace App\Http\Controllers\Api;

use Request;
use App\Http\Controllers\Controller;

use DB;

use Input;
use Validator;
use App\Models\User;
use App\Models\Login;
use App\Models\Teacher;
use Illuminate\Http\Response;
use HTML;
use Auth;
use Mail;
use Redirect;
use Session;

use Illuminate\Http\RedirectResponse;

class UsersController extends Controller
{
  public function resetGo(Request $request){
    $mail = Request::all() ['mail'];
    $password = Request::all() ['pass'];

    if(strlen($password) < 6){
      return ['change' => 'no'];
    }

    $criptedpass = bcrypt($password);

    DB::table('users')->where('email','=',$mail)->update(['password' => $criptedpass, 'remember_token' => str_random(30)]);

    return ['change' => 'ok'];


  }

  public function utpMailer(Request $request){
    setlocale(LC_ALL, 'es_CL');

    $utp_id = Auth::id();

    $queryschool = DB::table('user_utp')->select('school')->where('teacher_id','=',$utp_id)->get();

    $school = $queryschool[0]->school;

    $teachers_id = Request::all() ['teach_ids'];
    $mensaje = Request::all() ['message'];
    $mails = [];
    $fecha = strftime("%A %e de %B", mktime(0,0,0, date('m'), date('d'), date('Y')));

    foreach ($teachers_id as $value) {
      $query = DB::table('users')->select('email')->where('id','=',$value)->get() [0];
      $mails[] = $query->email;       
    }

    Mail::send('notifications.utp', ['mensaje' => $mensaje, 'fecha' => $fecha, 'colegio' => $school], function($message) use($mails){
        $message->from('no-reply@poptimize.cl', 'Poptimize');

        $message->to($mails);

        $message->subject('Mensaje de UTP / Directivos');
      });

    

    return 'ok';

  }

  public function contact(Request $request){

    $validator = Validator::make(Request::all(),
      [
          'name'  => 'required|max:100',
          'mensaje' => 'required|max:500',
          'email'    => 'required|email'
      ]);

    if ($validator->fails())
      {
          /*return redirect('registration')
                      ->withErrors($validator)
                      ->withInput();*/
          //return response()->json($validator->messages(), 200);
          return 'nope';
          //return $this->toJson();
      }

      $from = Request::all() ['email'];
      $mensaje = Request::all() ['mensaje'];
      $name = Request::all() ['name'];


      Mail::send('notifications.contact', ['from' => $from, 'mensaje' => $mensaje, 'name' => $name], function($message){
        $message->from('no-reply@poptimize.cl', 'Poptimize');

        $message->to('hola@poptimize.cl');

        $message->subject('Contacto');
      });

      return 'yes';





  }

  public function resetPassword(Request $request){

    $token = Request::all() ['token'];

    $query = DB::table('users')->select('*')->where('remember_token','=',$token)->get();

    if($query==NULL){
      return Redirect::to('index.html');
    }

    $id = $query[0]->id;

    // DB::table('users')->where('id','=',$id)->update(['remember_token' => str_random(30)]);

    return view('password.reset-password')->with('reset',$query[0]->email);

  }

  public function mailReset(Request $request){

    $mail = Request::all() ['email'];

    $query = DB::table('users')->select('*')->where('email','=',$mail)->get();


    if(is_null($query[0]->remember_token)){
      DB::table('users')->where('id','=',$query[0]->id)->update(['remember_token' => str_random(30)]);
    }

    $query = DB::table('users')->select('*')->where('email','=',$mail)->get();

    $code = $query[0]->remember_token;

    $url ='http://app.poptimize.cl/v1/reset?token='.$code;

    Mail::send('password.reset', ['url' => $url], function($message) use($mail) {
        $message->from('no-reply@poptimize.cl', 'Poptimize');

        $message->to($mail);

        $message->subject('Contraseña Olvidada');
      });

    return ['message' => 'ok'];

  }

  public function verify(Request $request){
    $code = Request::all() ['ver'];

    $query = DB::table('users')->select('id','verificated','remember_token')->where('remember_token','=',$code)->get();

    if($query[0]->verificated == 1){
      //redireccionar a login
      return Redirect::to('verified.html');
    }
    else{
      if($code == $query[0]->remember_token){
        DB::table('users')->where('remember_token','=',$code)->update(['verificated' => 1]);
        return Redirect::to('verified.html');
      }
      else{
        return Redirect::to('login.html');
      }
    }
  }

  public function store(Request $request)
  {
      $validator = Validator::make(Request::all(),
      [
          'name'  => 'required|max:100',
          'username' => 'required|max:100',
          'password' => 'required|min:6',
          'email'    => 'required|email'
      ]);

      if ($validator->fails())
      {
          /*return redirect('registration')
                      ->withErrors($validator)
                      ->withInput();*/
          //return response()->json($validator->messages(), 200);
          return response()->json($validator->messages(), 422);
          //return $this->toJson();
      }

      $confirmationCode = str_random(30);

      //return "User creado";
      // Login::create([
      //     'username'    => Input::get('username'),
      //     'confirmation_code' => $confirmationCode
      // ])->user()->
      // User::create([
      //     'username'    => Input::get('username'),
      //     'password'    => bcrypt(Input::get('password')),
      //     'name'        => Input::get('name'),
      //     'email'       => Input::get('email')
      // ]);

      /*if (Input::get('type') == '1') {
          $perfil = 1;
      } else {
          $perfil = 2;
      }*/

      $firstLogin = 0;

      $user = new User(Request::all());
      $user->profile_id = 1;
      $user->login_id   = 1;
      $user->password   = bcrypt(Input::get('password'));
      $user->new        = $firstLogin;
      $user->remember_token = $confirmationCode;

      $user->save();

      DB::table('teachers')->insert([

            'id' => $user->id,
            'username' => $user->username,
            'email' => $user->email,
            'user_id' => $user->id
      ]);

      $url = 'http://app.poptimize.cl/v1/verification?ver='.$confirmationCode;

      $mailuser = $user->email;

      Mail::send('registration.email', ['url' => $url], function($message) use($mailuser) {
        $message->from('no-reply@poptimize.cl', 'Poptimize');

        $message->to($mailuser);

        $message->subject('Verificación de Correo');
      });

      // $email = Input::get('username');
      // $password = Input::get('password');
      // if (Auth::attempt(array('username' => $email, 'password' => $password))){
      //     $teacher = Teacher::where('id',Auth::user()->id)->get();
      //     //return array(Auth::user(), $teacher);
      //     return array('message' => 'Su usuario ha sido creado. Ahora puede iniciar sesión.','id'=>$user->id, 'auth'=> array(Auth::user(), $teacher));
      // }
      //return 'failed';



      return array('message' => 'Su usuario ha sido creado. Hemos enviado un correo de confirmación para la activación de su cuenta.','id'=>$user->id);

  }
}
