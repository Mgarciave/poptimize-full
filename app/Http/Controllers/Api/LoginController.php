<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Input;
use HTML;
use Auth;
use App\Models\Teacher;

class LoginController extends Controller
{
  public function postLogin()
  {
      //$credentials = Input::only('username', 'password');

      if (Auth::attempt(['username' => Input::get('username'), 'password' => Input::get('password')])) {
          //return redirect('dashboard');
          $profile  = Auth::user()->profile_id;
          $username = Auth::user()->username;
          $id       = Auth::user()->id;
          $isNew    = Auth::user()->new;
          $utp = Auth::user()->utp;
          $verificated = Auth::user()->verificated;

          if ($utp == 1) {
              return array('code'=>1);
              //return redirect('registrationProfile');
          }
          elseif ($verificated == 0) {
            return array('code'=>2);
              //return redirect('registrationProfileSchool');
          }
          else {
              return array('message' => 'dashboard', 'code'=>3);
          }



          //return "No Tiene colegio";
              //return "Es colegio";

      }
      elseif(Auth::attempt(['email' => Input::get('username'), 'password' => Input::get('password')])){
          $profile  = Auth::user()->profile_id;
          $username = Auth::user()->username;
          $id       = Auth::user()->id;
          $isNew    = Auth::user()->new;
          $utp = Auth::user()->utp;
          $verificated = Auth::user()->verificated;

          if ($utp == 1) {
              return array('code'=>1);
              //return redirect('registrationProfile');
          }
          elseif ($verificated == 0) {
            return array('code'=>2);
              //return redirect('registrationProfileSchool');
          }
          else {
              return array('message' => 'dashboard', 'code'=>3);
          }

      }

      //return redirect('login')->with('message', 'User o Password incorrectos.');
      return array('message' => 'User o Password incorrectos.', 'code'=>0);
  }

  public function postLoginAdm()
  {
      $credentials = Input::only('username', 'password');

      if (Auth::attempt($credentials)) {
          //return redirect('dashboard');
          $utp = Auth::user()->utp;

          $profile  = Auth::user()->profile_id;
          $username = Auth::user()->username;
          $id       = Auth::user()->id;
          $isNew    = Auth::user()->new;

              if($utp == 0){
                return array('message' => 'Usuario no UTP.', 'code' => 0);
              }
              else{
                return array('message' => 'dashboard', 'code'=>1, 'user_id' => $id);
              }



          //return "No Tiene colegio";
              //return "Es colegio";

      }

      //return redirect('login')->with('message', 'User o Password incorrectos.');
      return array('message' => 'User o Password incorrectos.', 'code'=>0);
  }

}
