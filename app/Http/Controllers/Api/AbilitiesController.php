<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Models\Ability;

use DB;
use Auth;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class AbilitiesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $teacher_id = Auth::id();
        $id = $request->input('unit');

        //* TODO: cambiar la query para no depender de las unidades al ir a buscarlas.
      if($request->input('block_id')){
        $abilities = \DB::table('ability_unit')
                          ->join('abilities', 'ability_unit.ability_id', '=', 'abilities.id')
                          ->join('ability_teacher', 'ability_teacher.ability_id','=','abilities.id')
                          ->select('abilities.name_ability', 'abilities.description', 'abilities.id')
                          ->where('ability_unit.unit_id', '=', $id)
                          ->where('ability_teacher.teacher_id','=',$teacher_id)
                          ->where('ability_teacher.block_id','=', $request->input('block_id'))
                          ->get();
        // $abilities = DB::table('ability_teacher')
        //               ->select('*')
        //               ->where('subject_id','=',$request->input('subject'))
        //               ->where('unit_id','=',$request->input('unit'))
        //               ->where('teacher_id','=',auth()->id())
        //               ->where('block_id','=',$request->input('block_id'))
        //               ->get();
        return $abilities;
      }

      $abilities = \DB::table('ability_unit')
                    ->join('abilities', 'ability_unit.ability_id', '=', 'abilities.id')
                    ->join('ability_teacher', 'ability_teacher.ability_id','=','abilities.id')
                    ->select('abilities.name_ability', 'abilities.description', 'abilities.id')
                    ->where('ability_unit.unit_id', '=', $id)
                    ->where('ability_teacher.teacher_id','=',$teacher_id)
                    ->get();
      return $abilities;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $teacher_id = Auth::id();
        //$request->unitOa;  // subject_id, unit_id, tag_id...
        //$unitTag =  ['26','103','796','797'];

        /*$tamUnitTag = count($unitTag, COUNT_RECURSIVE);

        $subject_id = $unitTag[0];
        $unit_id = $unitTag[1];

        for ($i=2; $i < $tamUnitTag ; $i++) {

            $unitTags = new Tag_Teacher();
            $unitTags->subject_id = $subject_id;
            $unitTags->teacher_id = auth()->id();
            $unitTags->unit_id = $unit_id;
            $unitTags->checked = 1;
            $unitTags->tag_id = $unitTag[$i];

           $unitTags->save();
        }

        return "Tag - Docente - Asignatura Guardado!";*/
        DB::table('ability_teacher')
                  ->where('teacher_id','=',$teacher_id)
                  ->where('block_id','=',$request->input('id'))
                  ->delete();
        $abilities = $request->input('data');
        foreach($abilities as $ability){
          if($ability['ability']!=''){
            DB::table('ability_teacher')->insert(['ability_id' => $ability['ability'], 'block_id' => $request->input('id'), 'teacher_id' => $teacher_id]);
         }
        }

        return "Tag - Docente - Asignatura Guardado!";
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
