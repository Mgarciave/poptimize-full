<?php
namespace App\Http\Controllers\Api;

use Auth;
use Input;
use Validator;
use App\Models\User;
use App\Models\Teacher;
use App\Models\Schedule;

use DB;
use File;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

ini_set('memory_limit', '1024M');

class ScheduleController extends Controller
{
  public function index(Request $request)
  {
      if($request->input('id')){
        $teacher_id = Auth::id();

        $schedule = \DB::table('Schedule')
                      ->select('Schedule.file')
                      ->join('subjects', 'subjects.id', '=', 'Schedule.subject_id')
                      ->where('teacher_id', '=', Auth::id())
                      ->where('Schedule.id','=',$request->input('id'))
                      ->get();
        foreach($schedule as $file){
          $arr[] = array('url'=>url('/').'/uploads/'.$file->file,'name'=>$file->file);
        }

        return $arr;
      }

      $teacher_id = Auth::id();

      $schedule = \DB::table('Schedule')
                    ->select('*','Schedule.id as main_id', 'Schedule.course as course_id')
                    ->join('subjects', 'subjects.id', '=', 'Schedule.subject_id')
                    ->where('teacher_id', '=', Auth::id())
                    ->get();
      return $schedule;
  }

  public function eraseFile(Request $request){
    $id = $request->input('id');
    DB::table('Schedule')->where('id','=',$id)->update(['file' => '']);
    return 'ok';
  }
  public function humanFileSize($size,$unit="") {
  if( (!$unit && $size >= 1<<30) || $unit == "GB")
    return number_format($size/(1<<30),2)."GB";
  if( (!$unit && $size >= 1<<20) || $unit == "MB")
    return number_format($size/(1<<20),2)."MB";
  if( (!$unit && $size >= 1<<10) || $unit == "KB")
    return number_format($size/(1<<10),2)."KB";
  return number_format($size)." bytes";
  }


  public function getTeacherAsig(Request $request){
    $query = DB::table('Schedule')->select('Schedule.*')->join('subjects','Schedule.subject_id','=','subjects.id')->where('teacher_id','=',$request->teacher_id)->orderBy('subjects.order','asc')->get();
    $asig_data = [];
    foreach ($query as $value) {
      $asig_data[] = array('asig_id' => $value->subject_id, 'letter' =>$value->letter);
    }
    $asignData = array_unique($asig_data, SORT_REGULAR);

    $infoClass = [];

    foreach ($asignData as $asign) {
      $subject = $asign['asig_id'];
      $teacher_id = $request->teacher_id;
      $letter = $asign['letter'];

      //GET TEACHGRAPHS

      $query0 = DB::table('users')->select('name')->where('id','=',$teacher_id)->get();

      $query = DB::table('subjects')->select('name','course')->where('id','=',$subject)->get();

      $query1 = DB::table('Schedule')->select('*')->where('teacher_id','=',$teacher_id)->where('subject_id','=',$subject)->where('letter','=',$letter)->get();
      $totalClass = sizeof($query1);
      $info = 0;
      foreach ($query1 as $value1) {
        if(strlen($value1->description) > 0 || strlen($value1->init_text) >0)
         $info++;
      }
      $query2 = \DB::table('units')->where('subject_id', '=', $subject)->join('objective_unit', 'units.id', '=', 'objective_unit.unit_id')->count();
      $oatotal = $query2;

      $query3 = \DB::table('objective_teacher')->select('objective_id')->where('subject_id', '=', $subject)->where('teacher_id', '=', $teacher_id)->groupBy('objective_id')->get();
      $oaplan = sizeof($query3);

      $percentClass = $info;
      $percentOA = $oaplan;
      $name = $query0[0]->name.' / '.$query[0]->name.' / '.$query[0]->course.' '.$letter ;
      $idcan = $letter.$subject;
      $idcanoa = $letter.$subject.'OA';

      $arrGraph = [$name,$idcan,$totalClass,$percentClass,$oatotal,$percentOA,$idcanoa];

      //END TEACHGRAPS

      //GETTEACHCLASES

      $query = DB::table('Schedule')->select('*')->where('teacher_id','=',$teacher_id)->where('subject_id','=',$subject)->where('letter','=',$letter)->orderBy('hora_inicio','asc')->get();

      $coursesEmpty = [];
      $courses = [];

      foreach ($query as $value) {
        $dateClass = explode(' ', $value->hora_inicio) [0];
        if(strlen($value->init_text) > 0 || strlen($value->description) > 0 ){
          $courses[] = array(
            'date' => $dateClass,
            'id' => $value->id
            );
        }
        else{
          $coursesEmpty[] = array(
            'date' => $dateClass,
            'id' => $value->id
            );
        }
      }

      $fullInfo = array(
        'plancourses' => $courses,
        'noplancourses' => $coursesEmpty
        );

      //ENDTEACHCLASES

      //GETTEACHOAS

      $query1 = \DB::table('units')->where('subject_id', '=', $subject)->join('objective_unit', 'units.id', '=', 'objective_unit.unit_id')->join('objectives', 'objective_unit.objective_id','=','objectives.id')->select('objectives.*')->groupBy('num_obj')->get();
      // $oatotal = sizeof($query1);
      // var_dump($query1);

      $query2 = \DB::table('Schedule')->join('objective_teacher','Schedule.id','=','objective_teacher.block_id')->select('objective_teacher.*')->where('Schedule.subject_id', '=', $subject)->where('Schedule.teacher_id', '=', $teacher_id)->where('Schedule.letter','=',$letter)->groupBy('objective_teacher.text')->get();
      // $oaplan = sizeof($query2);
      // var_dump($query2);

      $oaseen = [];
      $oaunseen = [];

      $flag = 0;
      foreach ($query1 as $value) {
        foreach ($query2 as $value1) {
          if($value->id == $value1->objective_id){
            $flag = 1;
          }
        }
        if($flag == 1){
          $oaseen[] = array(
            'OA' => 'OA '.$value->num_obj,
            'desc' => $value->description_min
            );
        }
        else{
          $oaunseen[] = array(
            'OA' => 'OA '.$value->num_obj,
            'desc' => $value->description_min
            );
        }
        $flag = 0;
      }

      $fullArr = array(
        'oaseen' => $oaseen,
        'oaunseen' => $oaunseen
        );

      //ENDTEACHOAS

      $infoClass[] = array(
        'graphInfo' => $arrGraph,
        'classInfo' => $fullInfo,
        'oasInfo' => $fullArr
        );

    }

    return $infoClass;


  }

  public function teachByAsign(Request $request){ // PRUEBA DE UNA FUNCION PARA OBTENER INFO
    $asignname = $request->input('asignname');
    $utp_id = Auth::id();

    $query = DB::table('user_utp')->select('user_utp.school')->where('teacher_id','=',$utp_id)->get();
    $school = $query[0]->school;

    $query1 = DB::table('subjects')->select('*')->where('subjects.name','=',$asignname)->join('Schedule as t','subjects.id','=','t.subject_id')->where('t.school','=',$school)->groupBy(['t.teacher_id','t.course'])->orderBy('subjects.order','asc')->get();

    $arrTeach = [];

    foreach ($query1 as $value) {
      $arrTeach[] = array(
        'teach_id' => $value->teacher_id,
        'asig_id' => $value->subject_id,
        'letter' => $value->letter

        );
    }

     $infoClass = [];

    foreach ($arrTeach as $asign) {
      $subject = $asign['asig_id'];
      $teacher_id = $asign['teach_id'];
      $letter = $asign['letter'];

      //GET TEACHGRAPHS

      $query0 = DB::table('users')->select('name')->where('id','=',$teacher_id)->get();

      $query = DB::table('subjects')->select('name','course')->where('id','=',$subject)->get();

      $query1 = DB::table('Schedule')->select('*')->where('teacher_id','=',$teacher_id)->where('subject_id','=',$subject)->where('letter','=',$letter)->get();
      $totalClass = sizeof($query1);
      $info = 0;
      foreach ($query1 as $value1) {
        if(strlen($value1->description) > 0 || strlen($value1->init_text) >0)
         $info++;
      }
      $query2 = \DB::table('units')->where('subject_id', '=', $subject)->join('objective_unit', 'units.id', '=', 'objective_unit.unit_id')->count();
      $oatotal = $query2;

      $query3 = \DB::table('objective_teacher')->select('objective_id')->where('subject_id', '=', $subject)->where('teacher_id', '=', $teacher_id)->groupBy('objective_id')->get();
      $oaplan = sizeof($query3);

      $percentClass = $info;
      $percentOA = $oaplan;
      $name = $query0[0]->name.' / '.$query[0]->name.' / '.$query[0]->course.' '.$letter ;
      $idcan = $letter.$subject;
      $idcanoa = $letter.$subject.'OA';

      $arrGraph = [$name,$idcan,$totalClass,$percentClass,$oatotal,$percentOA,$idcanoa];

      //END TEACHGRAPS

      //GETTEACHCLASES

      $query = DB::table('Schedule')->select('*')->where('teacher_id','=',$teacher_id)->where('subject_id','=',$subject)->where('letter','=',$letter)->orderBy('hora_inicio','asc')->get();

      $coursesEmpty = [];
      $courses = [];

      foreach ($query as $value) {
        $dateClass = explode(' ', $value->hora_inicio) [0];
        if(strlen($value->init_text) > 0 || strlen($value->description) > 0 ){
          $courses[] = array(
            'date' => $dateClass,
            'id' => $value->id
            );
        }
        else{
          $coursesEmpty[] = array(
            'date' => $dateClass,
            'id' => $value->id
            );
        }
      }

      $fullInfo = array(
        'plancourses' => $courses,
        'noplancourses' => $coursesEmpty
        );

      //ENDTEACHCLASES

      //GETTEACHOAS

      $query1 = \DB::table('units')->where('subject_id', '=', $subject)->join('objective_unit', 'units.id', '=', 'objective_unit.unit_id')->join('objectives', 'objective_unit.objective_id','=','objectives.id')->select('objectives.*')->groupBy('num_obj')->get();
      // $oatotal = sizeof($query1);
      // var_dump($query1);

      $query2 = \DB::table('Schedule')->join('objective_teacher','Schedule.id','=','objective_teacher.block_id')->select('objective_teacher.*')->where('Schedule.subject_id', '=', $subject)->where('Schedule.teacher_id', '=', $teacher_id)->where('Schedule.letter','=',$letter)->groupBy('objective_teacher.text')->get();
      // $oaplan = sizeof($query2);
      // var_dump($query2);

      $oaseen = [];
      $oaunseen = [];

      $flag = 0;
      foreach ($query1 as $value) {
        foreach ($query2 as $value1) {
          if($value->id == $value1->objective_id){
            $flag = 1;
          }
        }
        if($flag == 1){
          $oaseen[] = array(
            'OA' => 'OA '.$value->num_obj,
            'desc' => $value->description_min
            );
        }
        else{
          $oaunseen[] = array(
            'OA' => 'OA '.$value->num_obj,
            'desc' => $value->description_min
            );
        }
        $flag = 0;
      }

      $fullArr = array(
        'oaseen' => $oaseen,
        'oaunseen' => $oaunseen
        );

      //ENDTEACHOAS

      $infoClass[] = array(
        'graphInfo' => $arrGraph,
        'classInfo' => $fullInfo,
        'oasInfo' => $fullArr
        );

    }

    return $infoClass;

  }

  public function teachByClass(Request $request){
    $coursename = $request->input('coursename');

    $utp_id = Auth::id();

    // $utp_id = 153;



    $query = DB::table('user_utp')->select('user_utp.school')->where('teacher_id','=',$utp_id)->get();
    $school = $query[0]->school;

    $query1 = DB::table('courses')->select('*')->where('courses.name','=',$coursename)->join('Schedule as t','courses.id','=','t.course')->where('t.school','=',$school)->groupBy(['t.subject_id','t.course'])->get();

    $arrTeach = [];

    foreach ($query1 as $value) {
      $arrTeach[] = array(
        'teach_id' => $value->teacher_id,
        'asig_id' => $value->subject_id,
        'letter' => $value->letter

        );
    }

    $infoClass = [];

    foreach ($arrTeach as $asign) {
      $subject = $asign['asig_id'];
      $teacher_id = $asign['teach_id'];
      $letter = $asign['letter'];

      //GET TEACHGRAPHS

      $query0 = DB::table('users')->select('name')->where('id','=',$teacher_id)->get();

      $query = DB::table('subjects')->select('name','course')->where('id','=',$subject)->get();

      $query1 = DB::table('Schedule')->select('*')->where('teacher_id','=',$teacher_id)->where('subject_id','=',$subject)->where('letter','=',$letter)->get();
      $totalClass = sizeof($query1);
      $info = 0;
      foreach ($query1 as $value1) {
        if(strlen($value1->description) > 0 || strlen($value1->init_text) >0)
         $info++;
      }
      $query2 = \DB::table('units')->where('subject_id', '=', $subject)->join('objective_unit', 'units.id', '=', 'objective_unit.unit_id')->count();
      $oatotal = $query2;

      $query3 = \DB::table('objective_teacher')->select('objective_id')->where('subject_id', '=', $subject)->where('teacher_id', '=', $teacher_id)->groupBy('objective_id')->get();
      $oaplan = sizeof($query3);

      $percentClass = $info;
      $percentOA = $oaplan;
      $name = $query0[0]->name.' / '.$query[0]->name.' / '.$query[0]->course.' '.$letter ;
      $idcan = $letter.$subject;
      $idcanoa = $letter.$subject.'OA';

      $arrGraph = [$name,$idcan,$totalClass,$percentClass,$oatotal,$percentOA,$idcanoa];

      //END TEACHGRAPS

      //GETTEACHCLASES

      $query = DB::table('Schedule')->select('*')->where('teacher_id','=',$teacher_id)->where('subject_id','=',$subject)->where('letter','=',$letter)->orderBy('hora_inicio','asc')->get();

      $coursesEmpty = [];
      $courses = [];

      foreach ($query as $value) {
        $dateClass = explode(' ', $value->hora_inicio) [0];
        if(strlen($value->init_text) > 0 || strlen($value->description) > 0 ){
          $courses[] = array(
            'date' => $dateClass,
            'id' => $value->id
            );
        }
        else{
          $coursesEmpty[] = array(
            'date' => $dateClass,
            'id' => $value->id
            );
        }
      }

      $fullInfo = array(
        'plancourses' => $courses,
        'noplancourses' => $coursesEmpty
        );

      //ENDTEACHCLASES

      //GETTEACHOAS

      $query1 = \DB::table('units')->where('subject_id', '=', $subject)->join('objective_unit', 'units.id', '=', 'objective_unit.unit_id')->join('objectives', 'objective_unit.objective_id','=','objectives.id')->select('objectives.*')->groupBy('num_obj')->get();
      // $oatotal = sizeof($query1);
      // var_dump($query1);

      $query2 = \DB::table('Schedule')->join('objective_teacher','Schedule.id','=','objective_teacher.block_id')->select('objective_teacher.*')->where('Schedule.subject_id', '=', $subject)->where('Schedule.teacher_id', '=', $teacher_id)->where('Schedule.letter','=',$letter)->groupBy('objective_teacher.text')->get();
      // $oaplan = sizeof($query2);
      // var_dump($query2);

      $oaseen = [];
      $oaunseen = [];

      $flag = 0;
      foreach ($query1 as $value) {
        foreach ($query2 as $value1) {
          if($value->id == $value1->objective_id){
            $flag = 1;
          }
        }
        if($flag == 1){
          $oaseen[] = array(
            'OA' => 'OA '.$value->num_obj,
            'desc' => $value->description_min
            );
        }
        else{
          $oaunseen[] = array(
            'OA' => 'OA '.$value->num_obj,
            'desc' => $value->description_min
            );
        }
        $flag = 0;
      }

      $fullArr = array(
        'oaseen' => $oaseen,
        'oaunseen' => $oaunseen
        );

      //ENDTEACHOAS

      $infoClass[] = array(
        'graphInfo' => $arrGraph,
        'classInfo' => $fullInfo,
        'oasInfo' => $fullArr
        );

    }

    return $infoClass;
  }

  public function utpCalendar(Request $request){

    $course_name = $request->input('course_name');
    $letter = $request->input('course_letter');
    $utp_id = Auth::id();
    
    $query = DB::table('user_utp')->select('user_utp.school')->where('teacher_id','=',$utp_id)->get();
    $school = $query[0]->school;

    $query1 = DB::table('subjects')->select('*','Schedule.id as main_id', 'Schedule.course as course_id', 'subjects.course as course')->where('subjects.course','=',$course_name)->join('Schedule','subjects.id','=','Schedule.subject_id')->where('Schedule.letter','=',$letter)->where('Schedule.school','=',$school)->orderBy('hora_inicio','asc')->get();

    return $query1;

  }

  public function getClassInfo(Request $request){


    $number = $request->id;
      
      $object = DB::table('objective_teacher')->select('*')->where('block_id', '=', $number)->get();
      if($object!=NULL){
        $unit_id = $object[0]->unit_id;
        $subject_id = $object[0]->subject_id;

        $query = DB::table('units')->select('name')->where('id', "=", $unit_id)->get();
        $query1 = DB::table('subjects')->select('name', 'course')->where('id', '=', $subject_id)->get();

        $unit = $query[0]->name;
        $asign = $query1[0]->name;
        $course = $query1[0]->course;

        $query2 = DB::table('objective_unit')->select('objective_id')->where('unit_id', '=', $unit_id)->get();
        $arroa = [];
        foreach ($query2 as $value) {
          $queryoa = DB::table('objectives')->select('num_obj','description_min')->where('id','=',$value->objective_id)->get();
          $arroa[] = array(
            'name' => 'OA '.strval($queryoa[0]->num_obj),
            'desc' => $queryoa[0]->description_min
            );
        }
        $dataoa = $arroa;

        $arrab = [];
        $query3 = DB::table('ability_unit')->select('description')->join('abilities','abilities.id','=','ability_unit.ability_id')->join('ability_teacher','abilities.id','=','ability_teacher.ability_id')->where('ability_unit.unit_id','=',$unit_id)->where('ability_teacher.block_id','=',$number)->get();
        foreach ($query3 as $value) {
          $arrab[]= array(
            'desc' => $value->description,
            );
        }

        $arrat = [];
        $query4 = DB::table('attitude_unit')->select('description')->join('attitudes','attitudes.id','=','attitude_unit.attitude_id')->join('attitude_teacher','attitudes.id','=','attitude_teacher.attitude_id')->where('attitude_unit.unit_id','=',$unit_id)->where('attitude_teacher.block_id','=',$number)->get();
        foreach ($query4 as $value) {
          $arrat[]= array(
            'desc' => $value->description,
            );
        }

        $arrtag = [];
        $query5 = DB::table('tag_unit')->select('name')->join('tags','tags.id','=','tag_unit.tag_id')->join('tag_teacher','tags.id','=','tag_teacher.tag_id')->where('tag_unit.unit_id','=',$unit_id)->where('tag_teacher.block_id','=',$number)->get();
        foreach ($query5 as $value) {
          $arrtag[]= array(
            'name' => $value->name,
            );
        }

        $scheduleData = DB::table('Schedule')->select('*')->where('id','=',$number)->get();

        $date = explode(' ', $scheduleData[0]->hora_inicio) [0];
        $formated = date("d-m-Y", strtotime($date));
        $type = $scheduleData[0]->type;
        $letter = $scheduleData[0]->letter;
        $file = $scheduleData[0]->file;

        $teachquery = DB::table('teachers')->select('teachers.name')->join('Schedule','Schedule.teacher_id','=','teachers.id')->where('Schedule.id','=',$number)->get();
        $teacher_name = $teachquery[0]->name;

        $arroas = [];
        $oaschedule = DB::table('objective_teacher')->select('text')->where('block_id','=', $number)->get();
        foreach ($oaschedule as $value) {
          foreach($arroa as $value1){
            if(substr($value->text,0,-1) == $value1['name']){
              $arroas[] = array(
                'name' => substr($value->text,0,-1),
                'desc' => $value1['desc']
                );
            }
              // var_dump('expression');          
          }
        }  
      }
      else{
        $scheduleData = DB::table('Schedule')->select('*')->where('id','=',$number)->get();
        $teachquery = DB::table('users')->select('users.name')->join('Schedule','Schedule.teacher_id','=','users.id')->where('Schedule.id','=',$number)->get();
        $teacher_name = $teachquery[0]->name;
        $date = explode(' ', $scheduleData[0]->hora_inicio) [0];
        $formated = date("d-m-Y", strtotime($date));
        $letter = $scheduleData[0]->letter;
        $file = $scheduleData[0]->file;
        $query5 = DB::table('subjects')->select('course', 'name')->where('id','=',$scheduleData[0]->subject_id)->get();
        $arroas = '';
        $unit = '';
        $asign = $query5[0]->name;
        $course = $query5[0]->course;
        $type = $scheduleData[0]->type;
        $arrab = '';
        $arrat = '';
        $arrtag = '';
      }
      //('select * from objective_teacher where block_id = ? order desc limit 1', [$number]);
      
    $fullArr = array(
      'schedule_id' => $number,
      'unit' => $unit,
      'asign' => $asign,
      'course' => $course,
      'formated' => $formated,
      'scheduleData' => $scheduleData,
      'arroas' => $arroas,
      'type' => $type,
      'arrab' => $arrab,
      'arrat' => $arrat,
      'arrtag' => $arrtag,
      'letter' => $letter,
      'file' => $file,
      'teacherName' => $teacher_name

      );
    return $fullArr;

  }

public function getExtraOa(Request $request){
    $subject = $request->sub;
    $teacher_id = Auth::id();
    $letter = $request->let;

    $query1 = \DB::table('units')->where('subject_id', '=', $subject)->join('objective_unit', 'units.id', '=', 'objective_unit.unit_id')->join('objectives', 'objective_unit.objective_id','=','objectives.id')->select('objectives.*')->groupBy('num_obj')->get();
    // $oatotal = sizeof($query1);
    // var_dump($query1);

    $query2 = \DB::table('Schedule')->join('objective_teacher','Schedule.id','=','objective_teacher.block_id')->select('objective_teacher.*')->where('Schedule.subject_id', '=', $subject)->where('Schedule.teacher_id', '=', $teacher_id)->where('Schedule.letter','=',$letter)->groupBy('objective_teacher.text')->get();
    // $oaplan = sizeof($query2);
    // var_dump($query2);

    $oaseen = [];
    $oaunseen = [];

    $flag = 0;
    foreach ($query1 as $value) {
      foreach ($query2 as $value1) {
        if($value->id == $value1->objective_id){
          $flag = 1;
        }
      }
      if($flag == 1){
        $oaseen[] = array(
          'OA' => 'OA '.$value->num_obj,
          'desc' => $value->description_min
          );
      }
      else{
        $oaunseen[] = array(
          'OA' => 'OA '.$value->num_obj,
          'desc' => $value->description_min
          );
      }
      $flag = 0;
    }

    $fullArr = array(
      'oaseen' => $oaseen,
      'oaunseen' => $oaunseen
      );
    return $fullArr;


  }

 public function getTeachOas(Request $request){
    $subject = $request->asig;
    $teacher_id = $request->teacher_id;
    $letter = $request->let;

    $query1 = \DB::table('units')->where('subject_id', '=', $subject)->join('objective_unit', 'units.id', '=', 'objective_unit.unit_id')->join('objectives', 'objective_unit.objective_id','=','objectives.id')->select('objectives.*')->groupBy('num_obj')->get();
    // $oatotal = sizeof($query1);
    // var_dump($query1);

    $query2 = \DB::table('Schedule')->join('objective_teacher','Schedule.id','=','objective_teacher.block_id')->select('objective_teacher.*')->where('Schedule.subject_id', '=', $subject)->where('Schedule.teacher_id', '=', $teacher_id)->where('Schedule.letter','=',$letter)->groupBy('objective_teacher.text')->get();
    // $oaplan = sizeof($query2);
    // var_dump($query2);

    $oaseen = [];
    $oaunseen = [];

    $flag = 0;
    foreach ($query1 as $value) {
      foreach ($query2 as $value1) {
        if($value->id == $value1->objective_id){
          $flag = 1;
        }
      }
      if($flag == 1){
        $oaseen[] = array(
          'OA' => 'OA '.$value->num_obj,
          'desc' => $value->description_min
          );
      }
      else{
        $oaunseen[] = array(
          'OA' => 'OA '.$value->num_obj,
          'desc' => $value->description_min
          );
      }
      $flag = 0;
    }

    $fullArr = array(
      'oaseen' => $oaseen,
      'oaunseen' => $oaunseen
      );
    return $fullArr;


  }

  public function getTeachCourses(Request $request){
    $subject = $request->asig;
    $teacher_id = $request->teacher_id;
    $letter = $request->let;

    $query = DB::table('Schedule')->select('*')->where('teacher_id','=',$teacher_id)->where('subject_id','=',$subject)->where('letter','=',$letter)->get();

    $coursesEmpty = [];
    $courses = [];

    foreach ($query as $value) {
      $dateClass = explode(' ', $value->hora_inicio) [0];
      if(strlen($value->init_text) > 0 || strlen($value->description) > 0 ){
        $courses[] = array(
          'date' => $dateClass,
          'id' => $value->id
          );
      }
      else{
        $coursesEmpty[] = array(
          'date' => $dateClass,
          'id' => $value->id
          );
      }
    }

    $fullInfo = array(
      'plancourses' => $courses,
      'noplancourses' => $coursesEmpty
      );

    return $fullInfo;

  }

  public function getTeachData(Request $request){
    $subject = $request->asig;
    $teacher_id = $request->teacher_id;
    $letter = $request->let;

    $query0 = DB::table('users')->select('name')->where('id','=',$teacher_id)->get();

    $query = DB::table('subjects')->select('name','course')->where('id','=',$subject)->get();

    $query1 = DB::table('Schedule')->select('*')->where('teacher_id','=',$teacher_id)->where('subject_id','=',$subject)->where('letter','=',$letter)->get();
    $totalClass = sizeof($query1);
    $info = 0;
    foreach ($query1 as $value1) {
      if(strlen($value1->description) > 0 || strlen($value1->init_text) >0)
       $info++;
    }
    $query2 = \DB::table('units')->where('subject_id', '=', $subject)->join('objective_unit', 'units.id', '=', 'objective_unit.unit_id')->count();
    $oatotal = $query2;

    $query3 = \DB::table('objective_teacher')->select('objective_id')->where('subject_id', '=', $subject)->where('teacher_id', '=', $teacher_id)->groupBy('objective_id')->get();
    $oaplan = sizeof($query3);

    $percentClass = $info;
    $percentOA = $oaplan;
    $name = $query0[0]->name.' / '.$query[0]->name.' / '.$query[0]->course.' '.$letter ;
    $idcan = $letter.$subject;
    $idcanoa = $letter.$subject.'OA';

    return $arr = [$name,$idcan,$totalClass,$percentClass,$oatotal,$percentOA,$idcanoa];
    
  }

  public function getAdmDropdowns(){
    $utp_id = Auth::id();

    // $utp_id = 153;



    $query = DB::table('user_utp')->select('user_utp.school')->where('teacher_id','=',$utp_id)->get();

    if(sizeof($query) > 0 ){
          $school = $query[0]->school;      
    }
    else{
      return array('error' => 'error');
    }
    $query1 = DB::table('Schedule')->select('Schedule.teacher_id', 'users.name')->where('school','=',$school)->join('users','Schedule.teacher_id','=','users.id')->groupBy('users.id')->get();

    $arrTeach = [];
      if(sizeof($query1)){
      foreach ($query1 as $value) {
        $arrTeach[] = array(
            'nombre' => $value->name,
            'teacher_id' => $value->teacher_id
          );
      }
    }

    $arrAsign = [];

    $query2 = DB::table('Schedule')->select('subjects.name')->where('Schedule.school','=',$school)->join('subjects','Schedule.subject_id','=','subjects.id')->groupBy(['subjects.name'])->orderBy('subjects.id')->get();

    foreach ($query2 as $value) {
      $arrAsign[] = array('name' => $value->name);
    }

    $arrCourses = [];

    $query3 = DB::table('Schedule')->select('subjects.course')->where('Schedule.school','=',$school)->join('subjects','Schedule.subject_id','=','subjects.id')->groupBy(['subjects.course'])->get();

    foreach ($query3 as $value) {
      $arrCourses[] = array('name' => $value->course);
    }

    $arrCalenCourses = [];

    $query4 = DB::table('Schedule')->select('subjects.course','Schedule.letter','subjects.course')->where('Schedule.school','=',$school)->join('subjects','Schedule.subject_id','=','subjects.id')->groupBy(['subjects.course','Schedule.letter'])->get();

    foreach ($query4 as $value) {
      $arrCalenCourses[] = array('name' => $value->course.' '.$value->letter, 'curso' => $value->course, 'letter' => $value->letter);
    }

    return $fullArr = [$arrTeach,$arrCourses,$arrAsign, $arrCalenCourses];

    
  }

  public function admSchedules(Request $request){
    $teacher_id = Auth::id();
    // $teacher_id=153;
      $schedule = \DB::table('Schedule')
                    ->select('*','Schedule.id as main_id', 'Schedule.course as course_id')
                    ->join('subjects', 'subjects.id', '=', 'Schedule.subject_id')
                    ->where('teacher_id', '=', $teacher_id)
                    ->where('letter','=', $request->input('let'))
                    ->where('subject_id', '=', $request->input('sub'))
                    ->orderBy('Schedule.hora_inicio','asc')
                    ->get();

      foreach ($schedule as $value) {
        $value->description = htmlspecialchars($value->description, ENT_QUOTES);
      }
      return $schedule;

  }

  public function updateCourses(Request $request){

    $teacher_id = Auth::id();
    $query = DB::table('Schedule')->select('*')->where('teacher_id', '=', $teacher_id)
                    ->where('letter','=', $request->input('let'))
                    ->where('subject_id', '=', $request->input('sub'))
                    ->orderBy('hora_inicio','asc')
                    ->get();
    $newSchedules = $request->input('schedules');
    for($i = 0; $i < sizeof($newSchedules); $i++){
      foreach ($query as $value) {
        if($value->id == $newSchedules[$i]['id']){
          $newSchedules[$i]['desc'] = $value->description;
          $newSchedules[$i]['file'] = $value->file;
          $newSchedules[$i]['init_text'] = $value->init_text;
          $newSchedules[$i]['mid_text'] = $value->mid_text;
          $newSchedules[$i]['end_text'] = $value->end_text;
          $newSchedules[$i]['res_text'] = $value->res_text;
          $newSchedules[$i]['eval_text'] = $value->eval_text;
          $newSchedules[$i]['ec_text'] = $value->ec_text;          
          $newSchedules[$i]['desc'] = $value->description;
          $newSchedules[$i]['type'] = $value->type;

        }
      }
    }
    $query1 = DB::table('objective_teacher')->select('*')->where('subject_id', '=', $request->input('sub'))->where('teacher_id', '=', $teacher_id)->get();
    //dd($query1);
    $query2 = DB::table('tag_teacher')->select('*')->where('subject_id', '=', $request->input('sub'))->where('teacher_id', '=', $teacher_id)->get();

    $query3 = DB::table('ability_teacher')->select('*')->where('teacher_id','=',$teacher_id)->get();

    $query4 = DB::table('attitude_teacher')->select('*')->where('teacher_id','=',$teacher_id)->get();

    $datos = array();

    for($i = 0; $i < sizeof($newSchedules); $i++){
      if($query[$i]->id != $newSchedules[$i]['id']){
        DB::table('Schedule')->where('id', $query[$i]->id)->update(['description' => $newSchedules[$i]['desc'], 'file' => $newSchedules[$i]['file'], 'init_text' => $newSchedules[$i]['init_text'], 'mid_text' => $newSchedules[$i]['mid_text'], 'end_text' => $newSchedules[$i]['end_text'], 'res_text' => $newSchedules[$i]['res_text'], 'eval_text' => $newSchedules[$i]['eval_text'], 'ec_text' => $newSchedules[$i]['ec_text'] , 'type' => $newSchedules[$i]['type']]);
        foreach ($query1 as $oa) {
          if($oa->block_id == $newSchedules[$i]['id']){
            DB::table('objective_teacher')->where('id','=', $oa->id)->update(['block_id' => $query[$i]->id]);
          }
        }
        foreach ($query2 as $tag) {
            if($tag->block_id == $newSchedules[$i]['id']){
              DB::table('tag_teacher')->where('id','=', $tag->id)->update(['block_id' => $query[$i]->id]);
            }
        }
        foreach ($query3 as $ability) {
            if($ability->block_id == $newSchedules[$i]['id']){
              DB::table('ability_teacher')->where('id','=',$ability->id)->update(['block_id' => $query[$i]->id]);
            }
        }
        foreach ($query4 as $attitude) {
            if($attitude->block_id == $newSchedules[$i]['id']){
              DB::table('attitude_teacher')->where('id','=',$attitude->id)->update(['block_id' => $query[$i]->id]);
            }
        }
      } 
    }

  return 'ok';

  }

  public function deleteClass(Request $request){
    $id = $request->id;
    DB::table('Schedule')->where('id','=',$id)->delete();
    DB::table('objective_teacher')->where('block_id','=',$id)->delete();
    DB::table('tag_teacher')->where('block_id','=',$id)->delete();

    return 'ok';
  }

  public function getAnalit(Request $request){
    // $teacher_id = 268;
     $teacher_id = Auth::id();
    $query = \DB::table('Schedule')->select('*')->where('teacher_id', '=', $teacher_id)
                    ->where('letter','=', $request->input('let'))
                    ->where('subject_id', '=', $request->input('sub'))
                    ->orderBy('hora_inicio','asc')
                    ->get();
    $scheduletotal = sizeof($query);
    $splan = 0;
    foreach ($query as $value) {
      if(strlen($value->init_text)>0 || strlen($value->mid_text)>0 || strlen($value->end_text)>0 || strlen($value->res_text)>0 || strlen($value->ec_text)>0 || strlen($value->eval_text)>0 || strlen($value->description)>0 ){
        $splan++;
      }
    }

    $query1 = \DB::table('units')->where('subject_id', '=', $request->input('sub'))->join('objective_unit', 'units.id', '=', 'objective_unit.unit_id')->join('objectives', 'objective_unit.objective_id','=','objectives.id')->groupBy('num_obj')->get();
    $oatotal = sizeof($query1);
    // var_dump($query1);

    $query2 = \DB::table('Schedule')->join('objective_teacher','Schedule.id','=','objective_teacher.block_id')->select('objective_teacher.text')->where('Schedule.subject_id', '=', $request->input('sub'))->where('Schedule.teacher_id', '=', $teacher_id)->where('Schedule.letter','=',$request->input('let'))->groupBy('objective_teacher.text')->get();
    $oaplan = sizeof($query2);
    // var_dump($query2);

    $arr = array(
      'scheduletotal' => $scheduletotal,
      'splan' => $splan,
      'oatotal' => $oatotal,
      'oaplan' => $oaplan
      );

    return $arr;

  }

  public function holidays() {
    $query = \DB::table('holidays')->select('*')->get();
    $arr = [];
    foreach ($query as $value) {
      $date = explode(' ', $value->date) [0];
      $date_init = $date.' 07:00:00';
      $date_end = $date.' 19:00:00'; 
      $arr[] = array(
        'date_init' => $date_init,
        'date_end' => $date_end,
        'name' => $value->desc
        );
    }

    return $arr;
  }

  public function conme() {
    $query = \DB::table('conmemoration')->select('*')->groupBy('date')->get();
    $arr = [];
    foreach ($query as $value) {
      $date = explode(' ', $value->date) [0];
      $date_init = $date.' 07:00:00';
      $date_end = $date.' 07:59:00'; 
      $arr[] = array(
        'date_init' => $date_init,
        'date_end' => $date_end,
        'name' => $value->desc
        );
    }

    return $arr;
  }

  public function conmeModal(Request $request) {
    $date = explode('T',$request->input('date')) [0];
    $query = DB::table('conmemoration')->select('*')->where('date','like','%'.$date.'%')->get();

    return $query;
  }


  public function cron()
  {
      $teacher_id = Auth::id();

      //$startTime = date("m", strtotime('+30 minutes', strtotime(date('Y-m-d H:i:s'))));

      $schedule = \DB::table('Schedule')
                    ->select('*','Schedule.id as main_id', 'Schedule.course as course_id', 'subjects.*')
                    ->join('subjects', 'subjects.id', '=', 'Schedule.subject_id')
                    ->join('users','users.id','=','Schedule.teacher_id')
                    ///->where('teacher_id', '=', Auth::id())
                    //->where('Schedule.reminder','=',date('Y-m-d H:i:s'))
                    ->whereDay('Schedule.reminder', '=', date('d'))
                    ->whereMonth('Schedule.reminder', '=', date('m'))
                    ->whereYear('Schedule.reminder', '=', date('Y'))
                    ->whereNotNull('Schedule.reminder')
                    //->where('Schedule.id','=',2)
                    ->whereRaw('HOUR(TIME(Schedule.reminder)) = "'.date('H').'" AND MINUTE(TIME(Schedule.reminder)) = "'.date('i').'"')
                    ->get();

      //return $schedule;

      foreach($schedule as $message){
        setlocale(LC_ALL, 'es_CL');
        //$msg[]['subject'] = '[Recordatorio Poptimize] '.$message->description;
        //$msg[]['body'] = '<h1>Poptimize<h1><p>Recuerda que tienes una programación en '.$message->name.' para el curso '.$message->course.' '.$message->letter.'</p><br><br><p>El recordatorio dice:</p><br><br><p>'.$message->description.'</p>';

        $subject = '[Recordatorio Poptimize]';
        //$body = '<img src="http://api-poptimize.fgacv.net/img/header.png"><br><div style="width:600px;"><p style="font-size:26px; font-weight:bold;">Recuerda que tienes una programación en <span style="color: #5dbfc2;">'.$message->name.'</span> para el curso <span style="color:#df5287">'.$message->course.' '.$message->letter.'</span> el día <strong style="color:#f4cc46;">'.date('l d \d\e\ F',strtotime($message->reminder)).'</strong></p><br><div style="width:100%; height:10px; background: #5cbec1; display: block;"></div><br><p style="font-size:26px; font-weight:bold;">El recordatorio dice:</p><p style="font-size:26px; font-weight:bold;font-style: italic;">'.$message->description.'</p><br><p style="font-size:20px;color:#5cbec1;font-weight:bold;">Un abrazo</p><br><img src="http://api-poptimize.fgacv.net/img/logo-mail.jpg"><br><p style="font-size: 26px;color: #5cbec1;font-weight:bold;">Equipo Pop</p></div>';
        $body = '<div style="width:600px;"><p style="font-size:14px; font-weight:bold;">Recuerda que tienes una programación en <span style="color: #5dbfc2;">'.$message->name.'</span> para el curso <span style="color:#df5287">'.$message->course.' '.$message->letter.'</span> el día <strong style="color:#f4cc46;">'.strftime("%A %e de %B", mktime(0, 0, 0, date('m',strtotime($message->reminder)), date('d',strtotime($message->reminder)), date('Y',strtotime($message->reminder)))).'</strong></p><br><div style="width:100%; height:2px; background: #5cbec1; display: block;"></div><br><p style="font-size:14px; font-weight:bold;">El recordatorio dice:</p><span style="font-size:14px; font-weight:bold;font-style: italic; ">'.nl2br($message->description).'</p><p style="font-size:14px;color:#5cbec1;font-weight:bold; margin-bottom:0px;">Un abrazo</p><p style="font-size: 14px;color: #5cbec1;font-weight:bold; margin-top: 0px;">Equipo Poptimize</p></div>';

        $headers = "From: Recordatorio Pop <no-reply@poptimize.cl>\r\n";
        $headers .= "BCC: jlizama@poptimize.cl, mmaray@poptimize.cl, mgarcia@poptimize.cl\r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=UTF-8\r\n";
        $headers .= "Reply-To: no-reply@poptimize.cl" . "\r\n";

        mail($message->email, $subject, $body, $headers);
        //mail('felipe@fgacv.net', $subject, $body, $headers);

      }

      return $schedule;

      //return array('actual' => date('Y-m-d H:i:s'), 'menos' => date('Y-m-d H:i:s', $newTime));


      /*foreach($msg as $m){
        $subject = $m['subject'];
        $body = $m['body'];

        $headers = "From: no-reply@poptimize.cl\r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=UTF-8\r\n";

        mail('felipe@fgacv.net', $subject, $body, $headers);
      }*/
  }

  public function clean()
  {
    //Schedule::where('teacher_id','=',Auth::id())->delete();
    return 200;
  }

  public function getObjName(Request $request){
    $block = $request->input('block');
    // $id = 153;
    $id = Auth::id();
    $arr = [];
    $finalArr = [];
    $query = DB::table('objective_teacher')->select('*')->where('block_id', '=', $block)->where('teacher_id', '=', $id)->get();
    if(sizeof($query)>0){
      foreach ($query as $value) {
          $query1 = DB::table('objectives')->select('*')->where('id', '=', $value->objective_id)->get();
          foreach ($query1 as $value1) {
            $arr[] = array(
            'name' => $value->text,
            'text' => $value1->description_min
            );
          }
      }
      $query2 = DB::table('units')->select('name')->where('id','=', $query[0]->unit_id)->get();

      $arrab = '';
      $query3 = DB::table('ability_unit')->select('description')->join('abilities','abilities.id','=','ability_unit.ability_id')->join('ability_teacher','abilities.id','=','ability_teacher.ability_id')->where('ability_unit.unit_id','=',$query[0]->unit_id)->where('ability_teacher.block_id','=',$block)->get();
      foreach ($query3 as $value) {
        $arrab[]= array(
          'desc' => $value->description,
          );
      }

      $arrat = '';
      $query4 = DB::table('attitude_unit')->select('description')->join('attitudes','attitudes.id','=','attitude_unit.attitude_id')->join('attitude_teacher','attitudes.id','=','attitude_teacher.attitude_id')->where('attitude_unit.unit_id','=',$query[0]->unit_id)->where('attitude_teacher.block_id','=',$block)->get();
      foreach ($query4 as $value) {
        $arrat[]= array(
          'desc' => $value->description,
          );
      }

      $arrtag = '';
      $query5 = DB::table('tag_unit')->select('name')->join('tags','tags.id','=','tag_unit.tag_id')->join('tag_teacher','tags.id','=','tag_teacher.tag_id')->where('tag_unit.unit_id','=',$query[0]->unit_id)->where('tag_teacher.block_id','=',$block)->get();
      foreach ($query5 as $value) {
        $arrtag[]= array(
          'name' => $value->name,
          );
      }

      $finalArr[] = $arr;
      $finalArr[] = $query2[0];
      $finalArr[] = $arrab;
      $finalArr[] = $arrat;
      $finalArr[] = $arrtag;
    }
    
    return $finalArr;

  }

  public function moveCourses(Request $request){
    $id = Auth::id();
    $date = $request->input('date');
    $quantity = $request->input('quant');
    $subject_id = $request->input('subject_id');
    $letra = $request->input('let');
    $query = DB::table('Schedule')->select('*')->where('subject_id','=', $subject_id)->where('teacher_id','=',$id)->where('letter', '=', $letra)->orderBy('hora_inicio','desc')->get();
    //dd($query);
    $query1 = DB::table('objective_teacher')->select('*')->where('subject_id', '=', $subject_id)->where('teacher_id', '=', $id)->get();
    //dd($query1);
    $query2 = DB::table('tag_teacher')->select('*')->where('subject_id', '=', $subject_id)->where('teacher_id', '=', $id)->get();

    $query3 = DB::table('ability_teacher')->select('*')->where('teacher_id','=',$id)->get();

    $query4 = DB::table('attitude_teacher')->select('*')->where('teacher_id','=',$id)->get();


    $flagDate = 0;
    foreach ($query as $value) {
      $checkDate = explode(' ', $value->hora_inicio) [0];
      if($checkDate == $date){
        $flagDate = 1;
        break;
      }
    }
    if($flagDate == 0 ){
      return 'error';
    }
    $moves = 0;
    foreach ($query as $value) {
      if($value->init_text != NULL || $value->description != NULL || self::checkoa($query1, $value->id) ){
        break; //dance
      }
      $moves++;
    }

    if($quantity > $moves){
      return 'error';
    }
    
    for($i=$moves; $i<sizeof($query); $i++){
      $datecycle = explode(' ', $query[$i]->hora_inicio) [0];
      if( strtotime($date) > strtotime($datecycle) ) {
        break; //dance
      }
      DB::table('Schedule')->where('id', $query[$i-$quantity]->id)->update(['description' => $query[$i]->description, 'reminder' => $query[$i]->reminder, 'file' => $query[$i]->file, 'init_text' => $query[$i]->init_text, 'mid_text' => $query[$i]->mid_text, 'end_text' => $query[$i]->end_text, 'res_text' => $query[$i]->res_text, 'eval_text' => $query[$i]->eval_text, 'ec_text' => $query[$i]->ec_text, 'type' => $query[$i]->type ]);
      DB::table('Schedule')->where('id', $query[$i]->id)->update(['description' => '', 'reminder' => '', 'file' => '', 'init_text' => '', 'mid_text' => '', 'end_text' => '', 'res_text' => '', 'eval_text' => '', 'ec_text' => '', 'type' => '0' ]);
      foreach ($query1 as $oa) {
          if($oa->block_id == $query[$i]->id){
            DB::table('objective_teacher')->where('id', $oa->id)->update(['block_id' => $query[$i-$quantity]->id]);
          }
      }
      foreach ($query2 as $tag) {
          if($tag->block_id == $query[$i]->id){
            DB::table('tag_teacher')->where('id', $tag->id)->update(['block_id' => $query[$i-$quantity]->id]);
          }
      }
      foreach ($query3 as $ability) {
            if($ability->block_id == $query[$i]->id){
              DB::table('ability_teacher')->where('id','=',$ability->id)->update(['block_id' => $query[$i-$quantity]->id]);
            }
        }
        foreach ($query4 as $attitude) {
            if($attitude->block_id == $query[$i]->id){
              DB::table('attitude_teacher')->where('id','=',$attitude->id)->update(['block_id' => $query[$i-$quantity]->id]);
            }
        }  
    }

    return 'ok';
    
  }

  public function checkoa(array $oas, $block_id){
    foreach ($oas as $value) {
      if($value->id == $block_id){
        return true;
      }        
    }
  return false;
  }

  public function fileUpload(Request $request)
  {

    //return $request->file('file');
    //return $_FILES["file"]["name"];

       $destinationPath="uploads";
       $file = $request->file('file');
       $bytes = File::size($file);
       if($bytes > 5242880){
        return array('code' => 'error');
       }
       else{
         $filename=$file->getClientOriginalName();
         $request->file('file')->move($destinationPath,$filename);

         DB::table('Schedule')
                 ->where('id', $request->input('le_value'))
                 ->update(
                   [
                     'file' => $filename,
                   ]
                 );
        $arr[] = array('url'=>url('/').'/uploads/'.$filename,'name'=>$filename);
        return array('code' => 'ok', 'file' => $arr);

       }

    //return $request->file('file');
    // Check if image file is a actual image or fake image
    //if(isset($_POST["submit"])) {
        //$check = getimagesize($_FILES["file"]["tmp_name"]);
        //if($check !== false) {

          //  $uploadOk = 1;
        //} else {
          //  echo "File is not an image.";
            //$uploadOk = 0;
        //}
    //}

  }

  public function planData(Request $request){
    $id = Auth::id();

    $date = $request->input('date');
    $subject_id = $request->input('subject_id');
    $letra = $request->input('let');
    $query = DB::table('Schedule')->select('*')->where('subject_id','=', $subject_id)->where('teacher_id','=',$id)->where('letter', '=', $letra)->get();

    foreach ($query as $value) {
      $fecha = explode(' ',$value->hora_inicio) [0];
      if($fecha == $date){
        $query1 = DB::table('objective_teacher')->select('*')->where('block_id','=',$value->id)->get();
        $query2 = DB::table('attitude_teacher')->select('*')->where('block_id','=',$value->id)->get();
        $query3 = DB::table('tag_teacher')->select('*')->where('block_id','=',$value->id)->get();
        if(count($query1) > 0 || count($query2) > 0 || count($query3) > 0 || $value->description != ''){
          return $value->id;
        }
        else{
          return NULL;
        }
      }
    }
  }

  public function update(Request $request)
  {
    DB::table('Schedule')
            ->where('id', $request->input('id'))
            ->update(
              [
                'description' => $request->input('description'),
                'reminder' => $request->input('reminder'),
                'init_text' => $request->input('init_text'),
                'mid_text' => $request->input('mid_text'),
                'end_text' => $request->input('end_text'),
                'res_text' => $request->input('res_text'),
                'eval_text' => $request->input('eval_text'),
                'ec_text' => $request->input('ec_text'),
                'type' => $request->input('type')
              ]
            );
    return array('code'=>200);
  }

  public function unit(Request $request){

    $data_id = $request->input('id');
    $query = DB::table('objective_teacher')->select('unit_id')->where('block_id', $data_id)->limit(1)->get();
    if($query == NULL){
     return $query;
    }
    $unit_id = $query[0]->unit_id;
    $query1 = DB::table('units')->select('name')->where('id', $unit_id)->get();
    $unit_number = explode(' ', $query1[0]->name) [1];
    return $unit_id;

  }

  public function store(Request $request)
  {
    $teacher_id = Auth::id();
    $data = $request->all();
    $query = DB::table('holidays')->select('*')->get();
    foreach ($query as $value) {
      $value->date = explode(' ', $value->date) [0];  
    }
    foreach($data['new_e'] as $d){
      switch($d['day_name']){
        case 'lunes':
          $d['day_name'] = 'monday';
          break;
        case 'martes':
          $d['day_name'] = 'tuesday';
          break;
        case 'miércoles':
          $d['day_name'] = 'wednesday';
          break;
        case 'jueves':
          $d['day_name'] = 'thursday';
          break;
        case 'viernes':
          $d['day_name'] = 'friday';
          break;
    }
    if( $d['day_name'] == 'monday' || $d['day_name'] == 'tuesday'){
      $first = strtotime("last ".$d['day_name']." of february 2017 - 1 week");
    }
    else{
      $first = strtotime("last ".$d['day_name']." of february 2017");
    }
    $todo = DB::table('teachers')->select('*')->where('id','=',$teacher_id)->get();
    
    $formated = date('Y-m-d', $first);
    $lastday = str_replace('/','-',$todo[0]->end_date);
    $flag = 0;

      $day = $first;

      //return (dd($formated));
      for ($i=0; $i <= 365 ; $i++) {
        # code...
        if($i==0){
          $future_date[$i] = $formated;
        }else{
          $anterior = $i-1;
          $future_date[$i] = date('Y-m-d',strtotime("+7 days", strtotime($future_date[$anterior])));
        }
        if(date('Y-m-d', strtotime($future_date[$i])) > date('Y-m-d', strtotime($lastday))){
          array_pop($future_date);
          break;
        }
      }
      foreach($future_date as $_day){
        foreach ($query as $value) {
          if($value->date == $_day){
            $flag = 1;
          }

        }
        if($flag == 0){
          $teacher = Teacher::with('school')->find(auth()->id());
            $schedule = new Schedule($request->all());
            $schedule->teacher_id = Auth::id();
            $schedule->school_record_id = $d['school_id'];  //Cambiar luego.
            $schedule->color = $d['color'];
            $schedule->hora_inicio = $_day . ' '.explode(' ',$d['start'])[1];
            $schedule->hora_fin = $_day . ' '.explode(' ',$d['end'])[1];
            $schedule->subject_id = intval($d['subject_id']);
            $schedule->course = $d['level'];
            $schedule->letter = $d['letter'];
            $schedule->school = $d['school_name'];
            $schedule->save();
        }
        $flag = 0;
    //     //$d_ =  $_day . ' '.explode(' ',$d['start'])[1];
    //     //$d_[] = "first ".$d['day_name']." of 2016";
    //     //$day += 7 * 86400;

    }
    //   /*do {
    //       $d_[] =  date('Y-m-d', $day) . ' '.$d['start_time'];
    //       //$d_[] = "first ".$d['day_name']." of 2016";
    //       /*$day += 7 * 86400;
    //       $teacher = Teacher::with('school')->find(auth()->id());
    //       $schedule = new Schedule($request->all());
    //       $schedule->teacher_id = Auth::id();
    //       $schedule->school_record_id = $d['school_id'];  //Cambiar luego.
    //       $schedule->color = $d['color'];
    //       $schedule->hora_inicio = $d_;
    //       $schedule->hora_fin = $d['end'];
    //       $schedule->subject_id = intval($d['subject_id']);
    //       $schedule->save();

    //   } while ($day < $lastday);*/

    //   //return $d;
    }
    /*$teacher = Teacher::with('school')->find(auth()->id());
    $schedule = new Schedule($request->all());
    $schedule->teacher_id = Auth::id();
    $schedule->school_record_id = Input::get('school_record_id');  //Cambiar luego.
    $schedule->color = Input::get('color');
    $schedule->hora_inicio = Input::get('hora_inicio');
    $schedule->hora_fin = Input::get('hora_fin');
    $schedule->subject_id = Input::get('subject_id');
    $schedule->save();*/
    return array('code' => 200, 'msg' => Auth::id());
    //return $data;*/
    //return $d_;
  }
}

