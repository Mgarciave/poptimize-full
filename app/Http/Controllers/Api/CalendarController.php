<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use Auth;

use Input;

use App\Models\Teacher;
use App\Models\Calendar;
use App\Models\Objective_Tag_Teacher;
use App\Models\Course_Subject_Teacher;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class CalendarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        //If course?
        $course = Teacher::with('course_subject_teacher.course', 'course_subject_teacher.subject')->find(auth()->id());

        $events = Calendar::with('course_subject_teacher.course', 'course_subject_teacher.subject')
                            ->whereHas('course_subject_teacher', function ($query) {
                                $query->where('teacher_id', auth()->id());
                            })
                           ->get();

            return compact('events');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $school = Teacher::with('school')->find(auth()->id());

        $calendar = new Calendar($request->all());
        $calendar->name_class = Input::get('nro_clase');
        $calendar->course_subject_teacher_id = Input::get('subject_id');
        $calendar->school_id = $school->id;
        $calendar->hora = Input::get('hora');
        $calendar->date  = Input::get('calendar_date');
        $calendar->color = Input::get('color');

        $calendar->save();

        return "Evento guardado.";
        //Si no es un modal, acÃ¡ redirige a la vista de calendario con un mensaje.
       // return redirect('calendar')->with('message', 'Evento guardado!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
