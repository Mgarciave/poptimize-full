<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use Input;
use Auth;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class FeedbackController extends Controller
{
    public function index(Request $request)
    {
      //return $request->all();

      $subject = '[Feedback] Nuevo mensaje';
      $to = 'contacto@poptimize.cl';

      $headers = "From: ".$request->input('name')." <" . strip_tags($request->input('email')) . ">\r\n";
      $headers .= "MIME-Version: 1.0\r\n";
      $headers .= "Content-Type: text/html; charset=UTF-8\r\n";

      $msg = '<h1>Nuevo Feedback</h1>';
      $msg .= '<p><strong>De:</strong> '.$request->input('name').' (<a href="mailto:'.$request->input('email').'">'.$request->input('email').'</a>)</p>';
      $msg .= '<p><strong>Mensaje: </strong>'.$request->input('message').'</p>';

      mail($to, $subject, $msg, $headers);

    }

}
