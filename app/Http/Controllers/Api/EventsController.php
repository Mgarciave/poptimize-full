<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;


use App\Models\Unit;
use App\Models\Subject;
use App\Models\Objective;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class EventsController extends Controller
{

    public function createEvent()
    {
        //Incluir id de la asignatura.
        $subject_id = 1;
        //Pasar id de unidad.
        $nro_unidad = 1;

        $unit = Subject::with('units')->where('id', $subject_id)->get();

        $keywords = Unit::with('tags')->where('subject_id',$subject_id)
                    ->where('id', 1)
                    ->get();

        $objetivos = Unit::with('objectives')->where('subject_id',$subject_id)
                                            ->where('id', 1)
                                            ->first();

        //return $unit;
        //return $keywords;
        //return $objetivos;
        return compact('objetivos','keywords', 'unit', 'nro_unidad');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
