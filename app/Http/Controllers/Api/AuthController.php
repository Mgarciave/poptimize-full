<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Input;
use HTML;
use Auth;
use App\Models\Teacher;

use Intervention\Image\Facades\Image as Image;

class AuthController extends Controller
{
    public function authData(Request $request){
      $email = $request->input('username');
      $password = $request->input('password');
      if (Auth::attempt(array('username' => $email, 'password' => $password))){
          $teacher = Teacher::where('id',Auth::user()->id)->get();
          return array(Auth::user(), $teacher);
      }
      return 'failed';
    }

    public function checkAuth(){
      if(Auth::user()){
        $teacher = Teacher::where('user_id',Auth::user()->id)->get();
        return array(Auth::user(), $teacher);
      }else{
        return array('code'=>500,'msg'=>'Not logged');
      }
    }

    public function editProfile(Request $request){
      $inputs = $request->all();
      $user = Auth::user();
      $teacher = Teacher::where('id',$user->id)->get();
      $user->name =  isset($inputs['data']['name']) ? $inputs['data']['name'] : $user->name;
      $user->username =  isset($inputs['data']['username']) ? $inputs['data']['username'] : $user->username;
      $user->email =  isset($inputs['data']['email']) ? $inputs['data']['email'] : $user->email;
      $user->password   = isset($inputs['data']['password']) ? bcrypt($inputs['data']['password']) : $user->password;

      $date = explode('/',$inputs['data']['birth_date']);

      if(count($date)==3){

        if(is_numeric($date[0]) && is_numeric($date[1]) && is_numeric($date[2])){
            if(array(checkdate($date[0], $date[1], $date[2]))){

              $upd = Teacher::where('id', $user->id)
                  ->update([
                      'birth_date' => isset($inputs['data']['birth_date']) ? $inputs['data']['birth_date'] : $teacher[0]->birth_date,
                      'phone' => isset($inputs['data']['phone']) ? $inputs['data']['phone'] : $teacher[0]->phone,
                      'profession' => isset($inputs['data']['profession']) ? $inputs['data']['profession'] : $teacher[0]->profession,
                      'specialty' => isset($inputs['data']['specialty']) ? $inputs['data']['specialty'] : $teacher[0]->specialty,
                      'years_experience' => isset($inputs['data']['years_experience']) ? $inputs['data']['years_experience'] : $teacher[0]->birth_date,
                      'init_date' => isset($inputs['data']['init_date']) ? $inputs['data']['init_date'] : $teacher[0]->init_date,
                      'end_date' => isset($inputs['data']['end_date']) ? $inputs['data']['end_date'] : $teacher[0]->end_date
                    ]);
              if($user->save()){
                return array('code'=>200, $upd);
              }else{
                return array('code'=>500);
              }
            }else{
              return array('code'=>500,'msg' => 'La fecha ingresada no es válida');
            }
          }else{
            return array('code'=>500,'msg' => 'La fecha ingresada no es válida', count($date));
          }
      }else{
        return array('code'=>500,'msg' => 'La fecha ingresada no es válida', count($date));
      }
      //return $teacher[0]->birth_date;
    }

    public function avatarUpload(Request $request){
      $image = Input::file('image');
      $filename  = time();
      $path = public_path('profilepics/' . $filename);
      Image::make($image->getRealPath())->save($path.'_original.' . $image->getClientOriginalExtension());
      Image::make($image->getRealPath())->resize(250, 250)->save($path.'.' . $image->getClientOriginalExtension());
      $user = Auth::user();
      $teacher = Teacher::where('id',$user->id)->get();
      $avatar = '/profilepics/'.$filename.'.'.$image->getClientOriginalExtension();
      Teacher::where('id', $user->id)
          ->update(['avatar'=>$avatar]);
      return $avatar;
    }

    public function logout(){
      return Auth::logout();
    }
}
