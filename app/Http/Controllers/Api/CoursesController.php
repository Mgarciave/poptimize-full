<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use Auth;
use Input;

use App\Models\Course;
use App\Models\Region;
use App\Models\School;
use App\Models\Subject;
use App\Models\Teacher;
use App\Models\Commune;
use App\Models\Calendar;
use App\Models\School_record;
use App\Models\School_Teacher;
use App\Models\Subject_record;
use App\Models\Course_Subject_Teacher;

use DB;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class CoursesController extends Controller
{

  public function getCourses(){

    $courses_teacher = \DB::table('courses_subjects_teachers')
                        ->join('courses', 'courses.id', '=', 'courses_subjects_teachers.course_id')
                        ->join('subjects', 'subjects.id', '=', 'courses_subjects_teachers.subject_id')
                        ->select('courses.id as cid', 'courses.name as curso', 'courses.letter', 'subjects.name as asignatura', 'subjects.id as asignatura_id', 'courses_subjects_teachers.*')
                        ->where('teacher_id', Auth()->id())
                        ->get();
    $total = 0;
    foreach ($courses_teacher as $key) {
      $total++;
    }

    $query = DB::table('teachers')->select('courses')->where('id','=', Auth()->id())->get();

    $user_courses = $query[0]->courses;

    if($total >= $user_courses){
      return 0;
    }
    else{
      return 1;
    }


  }


  public function get_course_id(Request $request){
    return DB::table('courses')
                    ->select('id')
                    ->where('name', '=', $request->input('name') )
                    ->where('letter', '=', $request->input('letter') )
                    ->get();
  }

  public function create()
  {

    $courses  = \DB::table('courses')
                ->select('name', 'id')
                ->GroupBy('name')
                ->get();

    $letters = \DB::table('courses')
                ->select('letter')
                ->GroupBy('letter')
                ->get();

    $school  = School_record::all();

    foreach($school as $key => $value){
      $school_formated[] = array('id' => $key, 'name' => $value['name'], 'commune' => $value['commune_id']);
    }

    $subject = Subject_record::all()->lists('name', 'id');

    $IfExistCourse = \DB::table('subject_teacher')
                    ->select('teacher_id', 'subject_id')
                    ->where('teacher_id', '=', Auth()->id() )
                    ->get();


    $courses_teacher = \DB::table('courses_subjects_teachers')
                        ->join('courses', 'courses.id', '=', 'courses_subjects_teachers.course_id')
                        ->join('subjects', 'subjects.id', '=', 'courses_subjects_teachers.subject_id')
                        ->select('courses.id as cid', 'courses.name as curso', 'courses.letter', 'subjects.name as asignatura', 'subjects.id as asignatura_id', 'courses_subjects_teachers.*')
                        ->where('teacher_id', Auth()->id())
                        ->get();

                        // select courses.id, courses.name as curso, courses.letter, subjects.name as as asignatura, subjects.id as asignatura_id where teacher_id = 1

      /*
      join `teachers` on `teachers`.`id` = `courses_subjects_teachers`.`teacher_id`
      join `school_record_teacher` on `school_record_teacher`.`teacher_id` = `courses_subjects_teachers`.`teacher_id`
      join school_records on school_records.id = school_record_teacher.school_record_id
      */

      $region = Region::all();
      $comuna = Commune::all();

      if ($IfExistCourse == [] ) {
          //return "No hay";
          $have = '0';
          //$courses_teacher = [];
          return array(
            'status' => 'no hay',
            'compact' => compact('subject', 'courses', 'have', 'letters', 'courses_teacher', 'school_formated', 'region', 'comuna'),
            'letters' => $letters,
            'courses' => $courses,
            //'schools' => $school,
            'subjects' => $subject
          );
          //return compact('subject', 'courses', 'school', 'have', 'letters', 'courses_teacher');
      }
      else {
          //return "Hay";
          $have = '1';
          return array(
            'e' => $IfExistCourse,
            'status' => 'hay',
            'data'=>$courses_teacher,
            'letters' => $letters,
            'courses' => $courses,
            //'schools' => $school,
            'subjects' => $subject,
            'compact' => compact('subject', 'courses', 'have', 'letters', 'courses_teacher', 'school_formated', 'region', 'comuna'));
          //return view('courses.create', compact('subject', 'courses', 'school', 'have', 'letters', 'courses_teacher'));
      }
  }

  public function store(Request $request)
  {
    $course_name = $request->get('course'); //nombre_curso. Ej. 1 Básico.

      $school     = $request->get('colegio');
      $region    = $request->get('region');
      $commune     = $request->get('commune');
      $school_name     = $request->get('colegio_name');
      $subject    = $request->get('asignatura'); //nombre_asignatura. Ej. Ciencias Naturales. //nombre_asignatura. Ej. Ciencias Naturales.

      $letter     = $request->get('letter');

      $comuna = $request->get('comuna');

      $ifbasic = $course_name;

      $asignatura_id = \DB::table('subjects')
                          ->select('id')
                          ->where('subjects.course', '=', $ifbasic)
                          ->where('subjects.name', '=', $subject)
                          ->value('id');



      $course_id = \DB::table('courses')
                      ->select('id')
                      ->where('name', '=', $course_name)
                      ->where('letter', '=', $letter)
                      ->value('id');

      //return $asignatura_id .'--'. $course_id; //26 -- 7

      //Id_Profe
      $id = Auth::id();
      //está ok.
      $courses = Course::find($course_id);
      $courses->teacher()->attach($id);

      //Está ok.
      $course = Course::find($course_id);
      //$course->school()->attach($school);
      $course->school()->attach($school);

      //Esta ok.
      //Esta ok. Para v2.0 - Modificar para agregar - id_Profe, id_comuna, nombre_colegio.
      //$school_t = School_record::find($school);
      //$school_t->teacher()->attach($id);

      $subject_t = Subject::find($asignatura_id);
      $subject_t->teacher()->attach($id);

      $ifExist = Course_Subject_Teacher::select('course_id', 'subject_id', 'teacher_id')
                  ->where('course_id', $course_id)
                  ->where('subject_id', $asignatura_id)
                  ->where('teacher_id', $id)
                  ->get();

     if(count($ifExist) != 0) {
            //return "El curso con la asignatura seleccionada, ya está registrada.";
            return array('code'=>500, 'Oh! El curso con la asignatura seleccionada, ya está registrada!');
        }
        else
        {

          $relation = new Course_Subject_Teacher();
          $relation->course_id = $course_id;
          $relation->subject_id = $asignatura_id;
          $relation->teacher_id = $id;
          $relation->school_name = $school_name;
          $relation->commune = $commune;
          $relation->region = $region;
          $relation->save();

          $school_t = new School_Teacher();
          $school_t->name = $school;
          $school_t->teacher_id = auth()->id();
          $school_t->commune_id = $comuna;

          $events = Calendar::with('course_subject_teacher.course', 'course_subject_teacher.subject')
                            ->whereHas('course_subject_teacher', function ($query) {
                                $query->where('teacher_id', auth()->id());
                            })
                           ->get();


      return array('code'=>200, $events, 'aid' => $asignatura_id);
      //return $subject;

      }
  }

  public function delete(Request $request)
    {
      $teacher_id = Auth::id();
      // select * from `courses_subjects_teachers` where course_id = 63 and subject_id = 23

      $query = DB::table('Schedule')
                //->where('id','=',$request->get('course_id'))
                ->select('*')
                ->where('subject_id','=',$request->get('subject_id'))
                ->where('teacher_id','=',$teacher_id)
                ->where('school','=',$request->get('school_name'))
                ->where('course','=',$request->get('course_id'))
                ->get();

      foreach ($query as $value) {
        DB::table('objective_teacher')->where('block_id','=',$value->id)->delete();
        DB::table('tag_teacher')->where('block_id','=',$value->id)->delete();
        DB::table('attitude_teacher')->where('block_id','=',$value->id)->delete();
        DB::table('ability_teacher')->where('block_id','=',$value->id)->delete();
      }


      DB::table('Schedule')
                //->where('id','=',$request->get('course_id'))
                ->where('subject_id','=',$request->get('subject_id'))
                ->where('teacher_id','=',$teacher_id)
                ->where('school','=',$request->get('school_name'))
                ->where('course','=',$request->get('course_id'))
                ->delete();

      return DB::table('courses_subjects_teachers')
                ->where('subject_id','=',$request->get('subject_id'))
                ->where('teacher_id','=',$teacher_id)
                ->where('course_id','=',$request->get('course_id'))
                ->where('school_name','=',$request->get('school_name'))
                ->delete();
        //$course = Course_Subject_Teacher::where('subject_id',$id)->toSql();

        //$course_id = $course[0]->id;

        //$delete = Course_Subject_Teacher::find($course_id);

        //$delete->delete();

        //return array('msg'=>200);
        //return auth()->id();
      }
    }
