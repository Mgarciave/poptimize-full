<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use DB;
use App\Http\Controllers\Controller;

use App\Models\Objective_Teacher;

class ObjectivesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        if($request->input('block_id')){
          return DB::table('objective_teacher')->select('*')->where('block_id','=',$request->input('block_id'))->get();
        }

        $oas = DB::table('objective_teacher')->select('*')->where('block_id','=',$request->input('id'))->get();
        return $oas;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user_id = auth()->id();
        //$request->unitOa;  // subject_id, unit_id, oa_id...
        $unitOa =  $request->input('data');

        $tamUnitOa = count($unitOa);

        $subject_id = $unitOa['si'];
        $unit_id = $unitOa['uid'];
        $ud = $unitOa['id'];

        DB::table('objective_teacher')->where('block_id', '=', $request->input('id'))->delete();
        //for ($i=1; $i < ($tamUnitOa) ; $i++) {

        foreach($unitOa['data'] as $oa){
                  //$i = $i-1;
            $check = DB::table('objective_teacher')
                          ->select('*')
                          ->where('block_id','=',$ud)
                          ->where('unit_id','=',$unit_id)
                          ->where('subject_id','=',$subject_id)
                          ->where('teacher_id','=',auth()->id())
                          ->where('objective_id','=',$oa['id'])
                          ->get();
            //return count($check);
            if(count($check)==0){
              //return $i;
              $unitOas = new Objective_Teacher();
              $unitOas->subject_id = $subject_id;
              $unitOas->teacher_id = auth()->id();
              $unitOas->unit_id = $unit_id;
              //$unitOas->checked = 1;
              $unitOas->objective_id = $oa['id'];
              //echo $tamUnitOa;
              $unitOas->text = $oa['text'];
              $unitOas->color = $oa['class'];
              $unitOas->block_id = $ud;

             $unitOas->save();
            }

        }

        //return $i;
        return $request->input('data');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
      $arr = '';
      DB::table('objective_teacher')->where('block_id', '=', $request->input('id'))->where('teacher_id','=',auth()->id())->delete();
      foreach($request->input('data') as $object){
        if($object['oa']!=''){
          $arr[] = $object['text'];
          DB::table('objective_teacher')->insert(
              [
                'objective_id' => $object['oa'],
                'teacher_id' => auth()->id(),
                'unit_id' => $request->input('unit'),
                'subject_id' => $request->input('subject'),
                'block_id' => $request->input('id'),
                'text' => $object['text'],
                'color' => 'ui-droppable'
              ]
          );
        }
        //return $object['text'].'    '.$object['oa'];
      }
      return $arr;
      //return array_filter($request->input('data'));
      //DB::table('objective_teacher')->where('block_id', '=', $unitOa[0])->delete();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
