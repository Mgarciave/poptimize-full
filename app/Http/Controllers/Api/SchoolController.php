<?php

namespace App\Http\Controllers\Api;

use App\Models\User;
use App\Models\Region;
use App\Models\School;

use Auth;
use Input;
use Validator;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Request;

class SchoolController extends Controller
{
  public function index()
  {
      $username = Auth::user()->username;

      $colegio = \DB::table('school')
                      ->join('region', 'school.region_id', '=', 'region.id')
                      ->select('school.name', 'username', 'email', 'address', 'phone', 'commune', 'school.region_id', 'code_rbd', 'dependence', 'holder', 'director', 'region.name as region')
                      ->where('username', '=', $username)
                      ->get();


      return compact('colegio');
  }

  public function create()
  {
      $region = Region::all()->lists('name', 'id');

      return compact('region');
      //return "Profile School (= ";
  }

  public function store(Request $request)
  {
      $validator = Validator::make(Request::all(),
      [
          //'name'     => 'required|max:50',
          //'username' => 'required|max:50',
          //'password' => 'required|min:6',
          //'email'    => 'required'
          'holder'   => 'required|max:50',
          'director' => 'required|max:50',
          'address'  => 'required|max:50',
          'phone'    => 'required|numeric|min:8',
      ]);

      if ($validator->fails())
      {
          return redirect('registrationProfileSchool')
                      ->withErrors($validator)
                      ->withInput();
      }

      $name     = Auth::user()->name;
      $username = Auth::user()->username;
      $mail     = Auth::user()->email;

      $id = Auth::user()->id;

      $school = new School();
      $school->name     = $name;
      $school->username = $username;
      $school->email    = $mail;
      $school->address  = Input::get('address');
      $school->phone    = Input::get('phone');
      $school->commune  = Input::get('commune');
      $school->region   = Input::get('region');
      $school->code_rbd = 1234;
      $school->dependence = Input::get('dependence');
      $school->holder     = Input::get('holder');
      $school->director   = Input::get('director');
      $school->save();

      $user = User::find($id);
      $user->new = 1;
      $user->save();

      return array('code' => 200);
  }

  public function edit($id)
  {
      $school = School::find($id);
      $region = Region::all()->lists('name', 'id');

      return compact('school', 'region');
  }
}
