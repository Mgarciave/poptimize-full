<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Input;
use HTML;
use Auth;
use App\Models\Teacher;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class LoginController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getLogin()
    {
        return view('login.index');
    }

    public function postLogin()
    {
        $credentials = Input::only('username', 'password');
         
        if (Auth::attempt($credentials)) {  
            //return redirect('dashboard');
            $profile  = Auth::user()->profile_id;
            $username = Auth::user()->username;
            $id       = Auth::user()->id;
            $isNew    = Auth::user()->new;

            if ($profile == 1 && $isNew == 0) {
                //Completa Perfil
                return redirect('registrationProfile');
            }
            elseif ($profile == 2 && $isNew == 0) {
                return redirect('registrationProfileSchool');
            }
            else {
                return redirect('dashboard');
            }



            //return "No Tiene colegio";
                return "Es colegio";

        }

        return redirect('login')->with('message', 'User o Password incorrectos.');
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/login');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
