<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Unit;
use App\Models\Tag;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class UnitController extends Controller
{

    public function unidades($id)
    {
        $subject_id = $id;

        $subject = \DB::table('subjects')
                        ->select('name', 'course')
                        ->where('id', '=', $subject_id)
                        ->get();

        $units = \DB::table('units')
                        ->select('name', 'purpose_min', 'hrs', 'id')
                        ->where('subject_id' ,'=', $subject_id)
                        ->get();

        return view('unit.index', compact('subject', 'units'));
        
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        "Index";
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //return "Show id: ". $id;

        $dataUnit = \DB::table('units')
                        ->select('units.name', 'units.purpose', 'units.hrs')
                        ->where('id', '=', $id)
                        ->get();

        $abilities = \DB::table('ability_unit')
                            ->join('abilities', 'ability_unit.ability_id', '=', 'abilities.id')
                            ->select('abilities.name_ability', 'abilities.description')
                            ->where('ability_unit.unit_id', '=', $id)
                            ->get();

        $pknow = \DB::table('PreviousKnowledge')
                        ->select('description')
                        ->where('unit_id', '=', $id)
                        ->get();

        $know = \DB::table('Knowledge')
                        ->select('description')
                        ->where('unit_id', '=', $id)
                        ->get();

        $tags = \DB::table('tag_unit')
                    ->join('tags', 'tag_unit.tag_id', '=', 'tags.id')
                    ->select('tags.name', 'tags.checked')
                    ->where('tag_unit.unit_id', '=', $id)
                    ->get();

        $oa = '';

        $attitudes = \DB::table('attitude_unit')
                            ->join('attitudes', 'attitude_unit.attitude_id', '=', 'attitudes.id')
                            ->select('attitudes.description')
                            ->where('attitude_unit.unit_id', '=', $id)
                            ->get();

        //return $abilities;

        return view('unit.show', compact('dataUnit', 'abilities', 'pknow', 'tags', 'know', 'oa', 'abilities', 'attitudes' ));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
