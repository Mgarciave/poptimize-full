<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use Input;

use App\Models\Course;
use App\Models\Subject;
use App\Models\School;
use App\Models\School_record;
use App\Models\Subject_record;
use App\Models\Course_Subject_Teacher;

use App\Models\Teacher;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class CourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $courses  = \DB::table('courses')
                    ->select('name', 'id')
                    ->GroupBy('name')
                    ->get();

        $letters = \DB::table('courses')
                    ->select('letter')
                    ->GroupBy('letter')
                    ->get();

        $school  = School_record::all()->lists('name', 'id');
        $subject = Subject_record::all()->lists('name', 'name');

        $IfExistCourse = \DB::table('subject_teacher')
                        ->select('teacher_id', 'subject_id')
                        ->where('teacher_id', '=', Auth::id() )
                        ->get();


        $courses_teacher = \DB::table('courses_subjects_teachers')
                            ->join('courses', 'courses.id', '=', 'courses_subjects_teachers.course_id')
                            ->join('subjects', 'subjects.id', '=', 'courses_subjects_teachers.subject_id')
                            ->select('courses.id', 'courses.name as curso', 'courses.letter', 'subjects.name as asignatura', 'subjects.id as asignatura_id')
                            ->get();


        if ($IfExistCourse == [] ) {
            //return "No hay";
            $have = '0';
            $courses_teacher = [];
            return view('courses.create', compact('subject', 'courses', 'school', 'have', 'letters', 'courses_teacher'));
        }
        else {
            //return "Hay";
            $have = '1';
            return view('courses.create', compact('subject', 'courses', 'school', 'have', 'letters', 'courses_teacher'));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $course_name = Input::get('course');  //nombre_curso.

        $school     = Input::get('colegio');
        $subject    = Input::get('asignatura'); //nombre_asignatura.
        $letter     = Input::get('letter');

        $ifbasic = $course_name;

        $asignatura_id = \DB::table('subjects')
                            ->select('id')
                            ->where('subjects.course', '=', $ifbasic)
                            ->where('subjects.name', '=', $subject)
                            ->value('id');
                            //->pluck('id');

        $course_id = \DB::table('courses')
                        ->select('id')
                        ->where('name', '=', $course_name)
                        ->where('letter', '=', $letter)
                        ->value('id');

        //return $asignatura_id;

        //Id_Profe
        $id = Auth::id();
        //está ok.
        $courses = Course::find($course_id);
        $courses->teacher()->attach($id);

        //Está ok.
        $course = Course::find($course_id);
        $course->school()->attach($school);

        //Esta ok.
        $school_t = School_record::find($school);
        $school_t->teacher()->attach($id);

        //Validar asignatura correspondiente al curso. Si y sólo si, las hrs varian.

        $subject_t = Subject::find($asignatura_id);
        $subject_t->teacher()->attach($id);

        $relation = new Course_Subject_Teacher();
        $relation->course_id = $course_id;
        $relation->subject_id = $asignatura_id;
        $relation->teacher_id = $id;
        $relation->save();


        return view('calendar.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
