<?php

namespace App\Http\Controllers;

use Input;
use Validator;
use App\Models\User;
use App\Models\Login;
use App\Models\Teacher;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Request;
use Illuminate\Http\RedirectResponse;



class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('registration.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    

    public function store(Request $request)
    {
        $validator = Validator::make(Request::all(), 
        [
            'name'  => 'required|max:100',
            'username' => 'required|max:100',
            'password' => 'required|min:6',
            'email'    => 'required'
        ]);

        if ($validator->fails()) 
        {
            return redirect('registration')
                        ->withErrors($validator)
                        ->withInput();
        }
        
        $confirmationCode = str_random(30); 

        //return "User creado";
        // Login::create([
        //     'username'    => Input::get('username'),
        //     'confirmation_code' => $confirmationCode
        // ])->user()->
        // User::create([
        //     'username'    => Input::get('username'),
        //     'password'    => bcrypt(Input::get('password')),
        //     'name'        => Input::get('name'),
        //     'email'       => Input::get('email')
        // ]);

        if (Input::get('type') == 'docente') {
            $perfil = 1;
        } else {
            $perfil = 2;
        }

        $firstLogin = 0;

        $user = new User(Request::all());
        $user->profile_id = $perfil;
        $user->login_id   = 1;
        $user->password   = bcrypt(Input::get('password'));
        $user->new        = $firstLogin;
        $user->save();

        return redirect('login')->with('message', 'Su usuario ha sido creado. Ahora puede iniciar sesión.');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return "Edit";
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
