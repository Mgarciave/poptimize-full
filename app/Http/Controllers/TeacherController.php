<?php

namespace App\Http\Controllers;

//use Illuminate\Http\Request;

use Auth;
use Input;
use Validator;
use App\Models\User;
use App\Models\Teacher;

//use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Request;

class TeacherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $username = Auth::user()->username;

        $profes = \DB::table('teachers')
                        ->select('birth_date', 'phone', 'profession', 'specialty', 'years_experience')
                        ->where('username', '=', $username)
                        ->get();


        return view('teacher.index', compact('profes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('teacher.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make(Request::all(),
        [
            //'name'     => 'required|max:50',
            //'username' => 'required|max:50',
            //'password' => 'required|min:6',
            'profession' => 'required|max:100',
            'specialty'  => 'required|max:50',
            'phone'      => 'numeric|min:8'
        ]);

        if ($validator->fails())
        {
            return redirect('registrationProfile')
                        ->withErrors($validator)
                        ->withInput();
        }

        $name     = Auth::user()->name;
        $username = Auth::user()->username;
        $mail     = Auth::user()->email;

        $id = Auth::user()->id;

        $teacher = new Teacher();
        $teacher->name     = $name;
        $teacher->username = $username;
        $teacher->email    = $mail;
        $teacher->birth_date = Input::get('birth_date');
        $teacher->phone      = Input::get('phone');
        $teacher->specialty  = Input::get('specialty');
        $teacher->years_experience = Input::get('years_experience');
        $teacher->profession = Input::get('profession');
        $teacher->save();

        $user = User::find($id);
        $user->new = 1;
        $user->save();

        return redirect('dashboard');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $teacher = Teacher::find($id);

        return view('teacher.edit', compact('teacher'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make(Request::all(),
        [
            'name'       => 'required|max:50',
            'username'   => 'required|max:50|unique:teachers',
            'password'   => 'required|min:6',
            'profession' => 'required|max:100',
            'specialty'  => 'required|max:50',
            'phone'      => 'numeric|min:8'
        ]);

        if ($validator->fails())
        {
            return redirect('teachers/'.$id.'/edit')
                        ->withErrors($validator)
                        ->withInput();
        }

        $teacher = Teacher::find($id);
        $teacher->save();

        return redirect('teachers');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
