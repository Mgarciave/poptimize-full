<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use Auth;
use Input;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Course;
use App\Models\Subject;
use App\Models\School;
use App\Models\School_record;
use App\Models\Subject_record;
use App\Models\Course_Subject_Teacher;

use App\Models\Teacher;

class CoursesController extends Controller
{
  public function create()
  {

    $courses  = \DB::table('courses')
                ->select('name', 'id')
                ->GroupBy('name')
                ->get();

    $letters = \DB::table('courses')
                ->select('letter')
                ->GroupBy('letter')
                ->get();

    $school  = School_record::all()->lists('name', 'id');

    foreach($school as $key => $value){
      $school_formated[] = array('id' => $key, 'name' => $value);
    }

    $subject = Subject_record::all()->lists('name', 'id');

    $IfExistCourse = \DB::table('subject_teacher')
                    ->select('teacher_id', 'subject_id')
                    ->where('teacher_id', '=', Auth::id() )
                    ->get();


    $courses_teacher = \DB::table('courses_subjects_teachers')
                        ->join('courses', 'courses.id', '=', 'courses_subjects_teachers.course_id')
                        ->join('subjects', 'subjects.id', '=', 'courses_subjects_teachers.subject_id')
                        ->select('courses.id', 'courses.name as curso', 'courses.letter', 'subjects.name as asignatura', 'subjects.id as asignatura_id')
                        ->where('teacher_id', Auth()->id())
                        ->get();

      /*
      join `teachers` on `teachers`.`id` = `courses_subjects_teachers`.`teacher_id`
      join `school_record_teacher` on `school_record_teacher`.`teacher_id` = `courses_subjects_teachers`.`teacher_id`
      join school_records on school_records.id = school_record_teacher.school_record_id
      */


      if ($IfExistCourse == [] ) {
          //return "No hay";
          $have = '0';
          //$courses_teacher = [];
          return array(
            'status' => 'no hay',
            'compact' => compact('subject', 'courses', 'school', 'have', 'letters', 'courses_teacher'),
            'letters' => $letters,
            'courses' => $courses,
            'schools' => $school,
            'subjects' => $subject
          );
          //return compact('subject', 'courses', 'school', 'have', 'letters', 'courses_teacher');
      }
      else {
          //return "Hay";
          $have = '1';
          return array(
            'e' => $IfExistCourse,
            'status' => 'hay',
            'data'=>$courses_teacher,
            'letters' => $letters,
            'courses' => $courses,
            'schools' => $school,
            'subjects' => $subject,
            'compact' => compact('subject', 'courses', 'school', 'have', 'letters', 'courses_teacher', 'school_formated'));
          //return view('courses.create', compact('subject', 'courses', 'school', 'have', 'letters', 'courses_teacher'));
      }
  }

  public function store(Request $request)
  {
      $course_name = Input::get('course');  //nombre_curso.

      $school     = Input::get('colegio');
      $subject    = Input::get('asignatura'); //nombre_asignatura.
      $letter     = Input::get('letter');
      $school_id     = Input::get('school_id');

      $ifbasic = $course_name;

      $asignatura_id = \DB::table('subjects')
                          ->select('id')
                          ->where('subjects.course', '=', $ifbasic)
                          ->where('subjects.id', '=', $subject)
                          ->value('id');
                          //->pluck('id');

      $asignatura_id = $subject;

      $course_id = \DB::table('courses')
                      ->select('id')
                      ->where('name', '=', $course_name)
                      ->where('letter', '=', $letter)
                      ->value('id');

      //return $asignatura_id;

      //Id_Profe
      $id = Input::get('id');
      //está ok.
      $courses = Course::find($course_id);
      $courses->teacher()->attach($id);

      //Está ok.
      $course = Course::find($course_id);
      $course->school()->attach($school_id);

      //Esta ok.
      $school_t = School_record::find($school_id);
      $school_t->teacher()->attach($id);

      //Validar asignatura correspondiente al curso. Si y sólo si, las hrs varian.

      $subject_t = Subject::find($asignatura_id);
      $subject_t->teacher()->attach($id);

      $relation = new Course_Subject_Teacher();
      $relation->course_id = $course_id;
      $relation->subject_id = $asignatura_id;
      $relation->teacher_id = $id;
      $relation->save();

      return $course;


      return array('code'=>200, $courses);
      //return $subject;

  }
}
