<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
    protected $table = 'Schedule';

    protected $fillable = ['dia', 'hora_inicio', 'hora_fin', 'subject_id', 'teacher_id', 'school_id'];

    public $timestamps = false;


    public function course_subject_teacher()
	{
		return $this->belongsTo(Course_Subject_Teacher::class);
	}

	public function school()
	{
		return $this->belongsTo(School_record::class);
	}
}
