<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Login extends Model
{
    protected $table = 'login';

    protected $fillable = ['username', 'password', 'confirmation_code'];

    public $timestamps = false;

    public function Users()
	{
		return $this->hasOne(User::class);
	} 

}