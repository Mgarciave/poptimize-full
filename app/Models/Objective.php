<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Objective extends Model
{
    protected $table = 'objectives';

    protected $fillable = ['name_eje', 'num_obj', 'description', 'checked'];

    public $timestamps = false;

    public function units()
	{
		return $this->belongsToMany(Unit::class);
	}
}
