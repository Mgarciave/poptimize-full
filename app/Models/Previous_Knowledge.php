<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Previous_Knowledge extends Model
{
    protected $table = 'previous_knowledge';

    protected $fillable = ['description', 'unit_id'];

    public $timestamps = false;

    public function Units()
	{
		return $this->belongsTo(Unit::class);
	}
}
