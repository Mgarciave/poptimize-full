<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Objective_Teacher extends Model
{
    protected $table = 'objective_teacher';

    protected $fillable = ['objective_id', 'teacher_id', 'unit_id', 'checked', 'subject_id'];

    public $timestamps = false;

    public function objectives()
	{
		return $this->hasMany(Objective::class);
	}

	public function teachers()
	{
		return $this->hasMany(Teacher::class);
	}

	public function unit()
	{
		return $this->hasMany(Unit::class);
	}
}
