<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Region extends Model
{
    protected $table = 'region';

    protected $fillable = ['name'];

    public $timestamps = false;

    public function school()
	{
		return $this->hasMany(School::class);
	}

	public function commune()
	{
		return $this->hasMany(Commune::class);
	}
}