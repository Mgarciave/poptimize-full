<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{

	protected $table = 'courses';

    protected $fillable = ['name', 'letter'];

    public $timestamps = false;
    

    public function teacher()
	{
		return $this->belongsToMany(Teacher::class);
	}

	public function school()
	{
		return $this->belongsToMany(School_record::class);
	}

	public function course_subject_teacher()
	{
		return $this->hasOne(Course_Subject_Teacher::class);
	}
}