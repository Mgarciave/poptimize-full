<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected $table = 'notifications';

    protected $fillable = ['name', 'description', 'date', 'teacher_id'];

    public $timestamps = false;

    public function teachers()
	{
		return $this->belongsTo(Teacher::class);
	}

	public function subjects()
	{
		return $this->hasMany(Subject::class);
	}  
}