<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Subject_record extends Model
{
    protected $table = 'subject_records';

    protected $fillable = ['name'];
}
