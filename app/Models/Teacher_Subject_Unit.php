<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Teacher_Subject_Unit extends Model
{
    protected $table = 'teacher_subject_unit';

    protected $fillable = ['teacher_id', 'unit_id', 'subject_id', 'order'];

    public $timestamps = false;


	public function subject()
	{
		return $this->hasMany(Subject::class);
	}

	public function teacher()
	{
		return $this->hasMany(Teacher::class);
	}

  public function unit()
	{
		return $this->hasMany(Unit::class, 'id');
	}

}
