<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Calendar extends Model
{
    protected $table = 'calendars';

    protected $fillable = ['name_class', 'description', 'objects', 'tags', 'course_subject_teacher_id', 'school_id', 'date', 'color'];

    public $timestamps = false;


    public function course_subject_teacher()
	{
		return $this->belongsTo(Course_Subject_Teacher::class);
	}

	public function school()
	{
		return $this->belongsTo(School_record::class);
	}
}
