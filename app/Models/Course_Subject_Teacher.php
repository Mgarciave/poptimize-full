<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Course_Subject_Teacher extends Model
{
    protected $table = 'courses_subjects_teachers';

    protected $fillable = ['course_id', 'subject_id', 'teacher_id'];

    public $timestamps = false;

    public function course()
	{
		return $this->belongsTo(Course::class);
	}

	public function subject()
	{
		return $this->belongsTo(Subject::class);
	}

	public function teacher()
	{
		return $this->belongsTo(Teacher::class);
	}

	public function calendar()
	{
		return $this->hasOne(Calendar::class);
	}

	public function schedule()
	{
		return $this->hasOne(Schedule::class);
	}
}
