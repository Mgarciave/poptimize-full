<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $table = 'tags';

    protected $fillable = ['name', 'checked'];

    public $timestamps = false;

    public function units()
	{
		return $this->belongsToMany(Unit::class);
	}
}
