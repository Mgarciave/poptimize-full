<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class school_teacher extends Model
{
    protected $table = 'school_teacher';

    protected $fillable = ['name', 'teacher_id', 'commune_id'];

    public function teacher()
	{
		return $this->belongsToMany(Teacher::class);
	}
}
