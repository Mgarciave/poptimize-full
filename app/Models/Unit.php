<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Unit extends Model
{
    protected $table = 'units';

    protected $fillable = ['name', 'purpose', 'purpose_min', 'hrs'];

    public $timestamps = false;

    public function subjects()
	{
		return $this->belongsTo(Subject::class);
	}

	public function objectives()
	{
		return $this->belongsToMany(Objective::class);
	}

	public function attitudes()
	{
		return $this->belongsToMany(Attitude::class);
	}

	public function abilities()
	{
		return $this->belongsToMany(Ability::class);
	}

	public function tags()
	{
		return $this->belongsToMany(Tag::class);
	}

	public function previous_knowledge()
	{
		return $this->hasMany(Previous_Knowledge::class);
	}

	public function knowledge()
	{
		return $this->hasMany(Knowledge::class);
	}
}
