<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $table = 'profiles';

    protected $fillable = ['name'];

    public $timestamps = false;

    public function user()
	{
		return $this->hasMany(User::class);
	}
}