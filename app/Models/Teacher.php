<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Teacher extends Model
{
    protected $table = 'teachers';

    protected $fillable = ['username', 'name', 'birth_date', 'email', 'phone', 'profession', 'specialty', 'years_experience', 'avatar', 'user_id'];

    public $timestamps = false;

    public function schools()
	{
		return $this->belongsToMany(School::class);
	}

	public function courses()
	{
		return $this->belongsToMany(Course::class);
	}

	public function subjects()
	{
		return $this->belongsToMany(Subject::class);
	}

	public function notifications()
	{
		return $this->hasMany(Notification::class);
	}

	public function school()
	{
    return $this->belongsToMany(School_record::class);
	}

	public function course_subject_teacher()
	{
		return $this->hasOne(Course_Subject_Teacher::class);
	}
}
