<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tag_Teacher extends Model
{
    protected $table = 'tag_teacher';

    protected $fillable = ['tag_id', 'teacher_id', 'unit_id', 'subject_id'];

    public $timestamps = false;

	public function tags()
	{
		return $this->hasMany(Tag::class);
	}

	public function teachers()
	{
		return $this->hasMany(Teacher::class);
	}

	public function unit()
	{
		return $this->hasMany(Unit::class);
	}
}
