<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class School_record extends Model
{
    protected $table = 'school_records';

    protected $fillable = ['name', 'code_rbd'];

    public function course()
	{
		return $this->belongsToMany(Course::class);
	}

	public function teacher()
	{
		return $this->belongsToMany(Teacher::class);
	}
}
