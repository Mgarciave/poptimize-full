<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Commune extends Model
{
    protected $table = 'communes';

    protected $fillable = ['name', 'region_id'];

	public function region()
	{
		return $this->belongsTo(Region::class);
	}

}