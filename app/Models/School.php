<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class School extends Model
{
    protected $table = 'School';

    protected $fillable = ['username', 'name', 'address', 'commune', 'region', 'code_rbd', 'dependence', 'email', 'phone', 'holder', 'director'];

    public $timestamps = false;

    public function teacher()
	{
		return $this->belongsToMany(Teacher::class);
	}

	public function region()
	{
		return $this->belongsTo(Region::class);
	}


}