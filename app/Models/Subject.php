<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    protected $table = 'subjects';

    protected $fillable = ['name', 'course', 'annual_hours', 'weekly_hours', 'semester_grade'];

    public $timestamps = false;

    public function teacher()
	{
		return $this->belongsToMany(Teacher::class);
	}

	public function notifications()
	{
		return $this->hasMany(Notificacion::class);
	}

	public function course_subject_teacher()
	{
		return $this->hasOne(Course_Subject_Teacher::class);
	}

	public function units()
	{
		return $this->hasMany(Unit::class);
	}


}
