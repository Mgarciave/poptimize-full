<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ability extends Model
{
    protected $table = 'abilities';

    protected $fillable = ['name_ability', 'num_alibily', 'description'];

    public $timestamps = false;

    public function Units()
	{
		return $this->belongsToMany(Unit::class);
	}
}
