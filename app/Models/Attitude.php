<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Attitude extends Model
{
    protected $table = 'attitudes';

    protected $fillable = ['description'];

    public $timestamps = false;

    
    public function Units()
	{
		return $this->belongsToMany(Unit::class);
	}
}
