<?php

/* @VisitTime/index.twig */
class __TwigTemplate_1127e8fd0a3472f8a9cfd6ec75ae8c7a08910c7818907474bdd96850c317f6e6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"row\">

    <div class=\"col-md-6\">
        <h2 piwik-enriched-headline>";
        // line 4
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), array("VisitTime_LocalTime")), "html", null, true);
        echo "</h2>
        ";
        // line 5
        echo (isset($context["dataTableVisitInformationPerLocalTime"]) ? $context["dataTableVisitInformationPerLocalTime"] : $this->getContext($context, "dataTableVisitInformationPerLocalTime"));
        echo "
    </div>

    <div class=\"col-md-6\">
        <h2 piwik-enriched-headline>";
        // line 9
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), array("VisitTime_ServerTime")), "html", null, true);
        echo "</h2>
        ";
        // line 10
        echo (isset($context["dataTableVisitInformationPerServerTime"]) ? $context["dataTableVisitInformationPerServerTime"] : $this->getContext($context, "dataTableVisitInformationPerServerTime"));
        echo "
    </div>

</div>
";
    }

    public function getTemplateName()
    {
        return "@VisitTime/index.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  39 => 10,  35 => 9,  28 => 5,  24 => 4,  19 => 1,);
    }

    public function getSource()
    {
        return "<div class=\"row\">

    <div class=\"col-md-6\">
        <h2 piwik-enriched-headline>{{ 'VisitTime_LocalTime'|translate }}</h2>
        {{ dataTableVisitInformationPerLocalTime|raw }}
    </div>

    <div class=\"col-md-6\">
        <h2 piwik-enriched-headline>{{ 'VisitTime_ServerTime'|translate }}</h2>
        {{ dataTableVisitInformationPerServerTime|raw }}
    </div>

</div>
";
    }
}
