<?php

/* @VisitFrequency/index.twig */
class __TwigTemplate_48c2d0e6cc53be3a9f28bdc9d94fda226f7f8b4316e6a167b2a8e18d480d9e8a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo call_user_func_array($this->env->getFunction('postEvent')->getCallable(), array("Template.headerVisitsFrequency"));
        echo "

<h2 piwik-enriched-headline data-graph-id=\"VisitFrequency.getEvolutionGraph\">";
        // line 3
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), array("VisitFrequency_ColumnReturningVisits")), "html", null, true);
        echo "</h2>
    ";
        // line 4
        echo (isset($context["graphEvolutionVisitFrequency"]) ? $context["graphEvolutionVisitFrequency"] : $this->getContext($context, "graphEvolutionVisitFrequency"));
        echo "
<br/>

";
        // line 7
        $this->loadTemplate("@VisitFrequency/_sparklines.twig", "@VisitFrequency/index.twig", 7)->display($context);
        // line 8
        echo "
";
        // line 9
        echo call_user_func_array($this->env->getFunction('postEvent')->getCallable(), array("Template.footerVisitsFrequency"));
        echo "
";
    }

    public function getTemplateName()
    {
        return "@VisitFrequency/index.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  39 => 9,  36 => 8,  34 => 7,  28 => 4,  24 => 3,  19 => 1,);
    }

    public function getSource()
    {
        return "{{ postEvent(\"Template.headerVisitsFrequency\") }}

<h2 piwik-enriched-headline data-graph-id=\"VisitFrequency.getEvolutionGraph\">{{ 'VisitFrequency_ColumnReturningVisits'|translate }}</h2>
    {{ graphEvolutionVisitFrequency|raw }}
<br/>

{% include \"@VisitFrequency/_sparklines.twig\" %}

{{ postEvent(\"Template.footerVisitsFrequency\") }}
";
    }
}
