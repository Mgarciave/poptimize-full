<?php

/* @Referrers/indexWebsites.twig */
class __TwigTemplate_66df01621466d49ce1078e7d9279e4554dce8277ab6ac78a295b1a708cd3e4a9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"row\">

    <div class=\"col-md-6\">
        <h2 piwik-enriched-headline>";
        // line 4
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), array("Referrers_Websites")), "html", null, true);
        echo "</h2>
        ";
        // line 5
        echo (isset($context["websites"]) ? $context["websites"] : $this->getContext($context, "websites"));
        echo "
    </div>

    <div class=\"col-md-6\">
        <h2 piwik-enriched-headline>";
        // line 9
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), array("Referrers_Socials")), "html", null, true);
        echo "</h2>
        ";
        // line 10
        echo (isset($context["socials"]) ? $context["socials"] : $this->getContext($context, "socials"));
        echo "
    </div>

</div>
";
    }

    public function getTemplateName()
    {
        return "@Referrers/indexWebsites.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  39 => 10,  35 => 9,  28 => 5,  24 => 4,  19 => 1,);
    }

    public function getSource()
    {
        return "<div class=\"row\">

    <div class=\"col-md-6\">
        <h2 piwik-enriched-headline>{{ 'Referrers_Websites'|translate }}</h2>
        {{ websites|raw }}
    </div>

    <div class=\"col-md-6\">
        <h2 piwik-enriched-headline>{{ 'Referrers_Socials'|translate }}</h2>
        {{ socials|raw }}
    </div>

</div>
";
    }
}
