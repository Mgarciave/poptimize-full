<?php

/* @CoreHome/_singleReport.twig */
class __TwigTemplate_683a60ad9c6332e0a0ddd1151cef04be2e56167dbe3d087d31af9a0f8017ba08 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<h2 piwik-enriched-headline>";
        echo \Piwik\piwik_escape_filter($this->env, (isset($context["title"]) ? $context["title"] : $this->getContext($context, "title")), "html", null, true);
        echo "</h2>
";
        // line 2
        echo (isset($context["report"]) ? $context["report"] : $this->getContext($context, "report"));
    }

    public function getTemplateName()
    {
        return "@CoreHome/_singleReport.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  24 => 2,  19 => 1,);
    }

    public function getSource()
    {
        return "<h2 piwik-enriched-headline>{{ title }}</h2>
{{ report|raw }}";
    }
}
