<?php

/* @VisitorInterest/index.twig */
class __TwigTemplate_983b30e293fbaee3d8e19965ddfa2c2a9f907bf417fb9e320680d89ee972fca5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"row\">
    <div class=\"col-md-6\">
        <h2 piwik-enriched-headline>";
        // line 3
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), array("VisitorInterest_VisitsPerDuration")), "html", null, true);
        echo "</h2>
        ";
        // line 4
        echo (isset($context["dataTableNumberOfVisitsPerVisitDuration"]) ? $context["dataTableNumberOfVisitsPerVisitDuration"] : $this->getContext($context, "dataTableNumberOfVisitsPerVisitDuration"));
        echo "
    </div>
    <div class=\"col-md-6\">
        <h2 piwik-enriched-headline>";
        // line 7
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), array("VisitorInterest_VisitsPerNbOfPages")), "html", null, true);
        echo "</h2>
        ";
        // line 8
        echo (isset($context["dataTableNumberOfVisitsPerPage"]) ? $context["dataTableNumberOfVisitsPerPage"] : $this->getContext($context, "dataTableNumberOfVisitsPerPage"));
        echo "
    </div>
</div>

<div class=\"row\">
    <div class=\"col-md-6\">
        <h2 piwik-enriched-headline>";
        // line 14
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), array("VisitorInterest_visitsByVisitCount")), "html", null, true);
        echo "</h2>
        ";
        // line 15
        echo (isset($context["dataTableNumberOfVisitsByVisitNum"]) ? $context["dataTableNumberOfVisitsByVisitNum"] : $this->getContext($context, "dataTableNumberOfVisitsByVisitNum"));
        echo "
    </div>
    <div class=\"col-md-6\">
        <h2 piwik-enriched-headline>";
        // line 18
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), array("VisitorInterest_VisitsByDaysSinceLast")), "html", null, true);
        echo "</h2>
        ";
        // line 19
        echo (isset($context["dataTableNumberOfVisitsByDaysSinceLast"]) ? $context["dataTableNumberOfVisitsByDaysSinceLast"] : $this->getContext($context, "dataTableNumberOfVisitsByDaysSinceLast"));
        echo "
    </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "@VisitorInterest/index.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  60 => 19,  56 => 18,  50 => 15,  46 => 14,  37 => 8,  33 => 7,  27 => 4,  23 => 3,  19 => 1,);
    }

    public function getSource()
    {
        return "<div class=\"row\">
    <div class=\"col-md-6\">
        <h2 piwik-enriched-headline>{{ 'VisitorInterest_VisitsPerDuration'|translate }}</h2>
        {{ dataTableNumberOfVisitsPerVisitDuration|raw }}
    </div>
    <div class=\"col-md-6\">
        <h2 piwik-enriched-headline>{{ 'VisitorInterest_VisitsPerNbOfPages'|translate }}</h2>
        {{ dataTableNumberOfVisitsPerPage|raw }}
    </div>
</div>

<div class=\"row\">
    <div class=\"col-md-6\">
        <h2 piwik-enriched-headline>{{ 'VisitorInterest_visitsByVisitCount'|translate }}</h2>
        {{ dataTableNumberOfVisitsByVisitNum|raw }}
    </div>
    <div class=\"col-md-6\">
        <h2 piwik-enriched-headline>{{ 'VisitorInterest_VisitsByDaysSinceLast'|translate }}</h2>
        {{ dataTableNumberOfVisitsByDaysSinceLast|raw }}
    </div>
</div>
";
    }
}
