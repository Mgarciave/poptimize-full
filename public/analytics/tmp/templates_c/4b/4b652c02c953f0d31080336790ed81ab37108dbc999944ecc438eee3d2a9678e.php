<?php

/* @CoreHome/getDonateForm.twig */
class __TwigTemplate_991b4accd473bf03f6539da560717c212d1d57dc4c8d25ce60971f520ce469f0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->loadTemplate("@CoreHome/_donate.twig", "@CoreHome/getDonateForm.twig", 1)->display($context);
    }

    public function getTemplateName()
    {
        return "@CoreHome/getDonateForm.twig";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }

    public function getSource()
    {
        return "{% include \"@CoreHome/_donate.twig\" %}";
    }
}
