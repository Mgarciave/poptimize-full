<?php

/* @VisitFrequency/_sparklines.twig */
class __TwigTemplate_c7d29bb7fe86331ab60c13e97e1a888e90d6dfe5248907438b73f3430f5b145f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
";
        // line 2
        if ( !(isset($context["isWidget"]) ? $context["isWidget"] : $this->getContext($context, "isWidget"))) {
            // line 3
            echo "<div class=\"row\">
    <div class=\"col-md-6\">
";
        }
        // line 6
        echo "
        <div class=\"sparkline\">
            ";
        // line 8
        echo call_user_func_array($this->env->getFunction('sparkline')->getCallable(), array((isset($context["urlSparklineNbVisitsReturning"]) ? $context["urlSparklineNbVisitsReturning"] : $this->getContext($context, "urlSparklineNbVisitsReturning"))));
        echo "
            ";
        // line 9
        echo call_user_func_array($this->env->getFilter('translate')->getCallable(), array("VisitFrequency_ReturnVisits", (("<strong>" . call_user_func_array($this->env->getFilter('number')->getCallable(), array((isset($context["nbVisitsReturning"]) ? $context["nbVisitsReturning"] : $this->getContext($context, "nbVisitsReturning"))))) . "</strong>")));
        echo "
        </div>
        <div class=\"sparkline\">
            ";
        // line 12
        echo call_user_func_array($this->env->getFunction('sparkline')->getCallable(), array((isset($context["urlSparklineNbActionsReturning"]) ? $context["urlSparklineNbActionsReturning"] : $this->getContext($context, "urlSparklineNbActionsReturning"))));
        echo "
            ";
        // line 13
        echo call_user_func_array($this->env->getFilter('translate')->getCallable(), array("VisitFrequency_ReturnActions", (("<strong>" . call_user_func_array($this->env->getFilter('number')->getCallable(), array((isset($context["nbActionsReturning"]) ? $context["nbActionsReturning"] : $this->getContext($context, "nbActionsReturning"))))) . "</strong>")));
        echo "
        </div>
        <div class=\"sparkline\">
            ";
        // line 16
        echo call_user_func_array($this->env->getFunction('sparkline')->getCallable(), array((isset($context["urlSparklineActionsPerVisitReturning"]) ? $context["urlSparklineActionsPerVisitReturning"] : $this->getContext($context, "urlSparklineActionsPerVisitReturning"))));
        echo "
            ";
        // line 17
        echo call_user_func_array($this->env->getFilter('translate')->getCallable(), array("VisitFrequency_ReturnAvgActions", (("<strong>" . call_user_func_array($this->env->getFilter('number')->getCallable(), array((isset($context["nbActionsPerVisitReturning"]) ? $context["nbActionsPerVisitReturning"] : $this->getContext($context, "nbActionsPerVisitReturning")), 1))) . "</strong>")));
        echo "
        </div>

    ";
        // line 20
        if ( !(isset($context["isWidget"]) ? $context["isWidget"] : $this->getContext($context, "isWidget"))) {
            // line 21
            echo "    </div>
    <div class=\"col-md-6\">
    ";
        }
        // line 24
        echo "
        <div class=\"sparkline\">
            ";
        // line 26
        echo call_user_func_array($this->env->getFunction('sparkline')->getCallable(), array((isset($context["urlSparklineAvgVisitDurationReturning"]) ? $context["urlSparklineAvgVisitDurationReturning"] : $this->getContext($context, "urlSparklineAvgVisitDurationReturning"))));
        echo "
            ";
        // line 27
        $context["avgVisitDurationReturning"] = call_user_func_array($this->env->getFilter('sumtime')->getCallable(), array((isset($context["avgVisitDurationReturning"]) ? $context["avgVisitDurationReturning"] : $this->getContext($context, "avgVisitDurationReturning"))));
        // line 28
        echo "            ";
        echo call_user_func_array($this->env->getFilter('translate')->getCallable(), array("VisitFrequency_ReturnAverageVisitDuration", (("<strong>" . (isset($context["avgVisitDurationReturning"]) ? $context["avgVisitDurationReturning"] : $this->getContext($context, "avgVisitDurationReturning"))) . "</strong>")));
        echo "
        </div>
        <div class=\"sparkline\">
            ";
        // line 31
        echo call_user_func_array($this->env->getFunction('sparkline')->getCallable(), array((isset($context["urlSparklineBounceRateReturning"]) ? $context["urlSparklineBounceRateReturning"] : $this->getContext($context, "urlSparklineBounceRateReturning"))));
        echo "
            ";
        // line 32
        echo call_user_func_array($this->env->getFilter('translate')->getCallable(), array("VisitFrequency_ReturnBounceRate", (("<strong>" . call_user_func_array($this->env->getFilter('percent')->getCallable(), array((isset($context["bounceRateReturning"]) ? $context["bounceRateReturning"] : $this->getContext($context, "bounceRateReturning"))))) . "</strong>")));
        echo "
        </div>
        ";
        // line 34
        $this->loadTemplate("_sparklineFooter.twig", "@VisitFrequency/_sparklines.twig", 34)->display($context);
        // line 35
        echo "
";
        // line 36
        if ( !(isset($context["isWidget"]) ? $context["isWidget"] : $this->getContext($context, "isWidget"))) {
            // line 37
            echo "    </div>
</div>
";
        }
    }

    public function getTemplateName()
    {
        return "@VisitFrequency/_sparklines.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  103 => 37,  101 => 36,  98 => 35,  96 => 34,  91 => 32,  87 => 31,  80 => 28,  78 => 27,  74 => 26,  70 => 24,  65 => 21,  63 => 20,  57 => 17,  53 => 16,  47 => 13,  43 => 12,  37 => 9,  33 => 8,  29 => 6,  24 => 3,  22 => 2,  19 => 1,);
    }

    public function getSource()
    {
        return "
{% if not isWidget %}
<div class=\"row\">
    <div class=\"col-md-6\">
{% endif %}

        <div class=\"sparkline\">
            {{ sparkline(urlSparklineNbVisitsReturning) }}
            {{ 'VisitFrequency_ReturnVisits'|translate(\"<strong>\"~nbVisitsReturning|number~\"</strong>\")|raw }}
        </div>
        <div class=\"sparkline\">
            {{ sparkline(urlSparklineNbActionsReturning) }}
            {{ 'VisitFrequency_ReturnActions'|translate(\"<strong>\"~nbActionsReturning|number~\"</strong>\")|raw }}
        </div>
        <div class=\"sparkline\">
            {{ sparkline(urlSparklineActionsPerVisitReturning) }}
            {{ 'VisitFrequency_ReturnAvgActions'|translate(\"<strong>\"~nbActionsPerVisitReturning|number(1)~\"</strong>\")|raw }}
        </div>

    {% if not isWidget %}
    </div>
    <div class=\"col-md-6\">
    {% endif %}

        <div class=\"sparkline\">
            {{ sparkline(urlSparklineAvgVisitDurationReturning) }}
            {% set avgVisitDurationReturning=avgVisitDurationReturning|sumtime %}
            {{ 'VisitFrequency_ReturnAverageVisitDuration'|translate(\"<strong>\"~avgVisitDurationReturning~\"</strong>\")|raw }}
        </div>
        <div class=\"sparkline\">
            {{ sparkline(urlSparklineBounceRateReturning) }}
            {{ 'VisitFrequency_ReturnBounceRate'|translate(\"<strong>\"~bounceRateReturning|percent~\"</strong>\")|raw }}
        </div>
        {% include \"_sparklineFooter.twig\" %}

{% if not isWidget %}
    </div>
</div>
{% endif %}
";
    }
}
