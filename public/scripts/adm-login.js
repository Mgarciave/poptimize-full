$(document).on('click','#login', function(e){
        e.preventDefault();
        $.ajax({
          url:ajax_url+'/v1/users/login-adm',
          type: 'post',
          dataType: 'json',
          xhrFields: {
            withCredentials: true
          },
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          data: { username: $('#email').val(), password: $('#password').val() },
          success: function(data){

            var msg = '';
            var status = '';
            var callback_ = '';
            if(data.code==0){
              msg = 'Nombre de usuario o contraseña incorrectos.';
              status = 'error';
              title = ':(';
              swal({
                title: title,
                text: msg,
                type: status,
                confirmButtonText: "Continuar",
              });
            }else{
              Cookies.set('loggedAsAdm', data.user_id);
              window.location = '/dashboard-adm.html';
              return false;
            }

            /*swal({
              title: title,
              text: msg,
              type: status,
              confirmButtonText: "Continuar",
            },callback_);*/
          },
          error: function(data){
            console.log(data);
          }
        });
      });