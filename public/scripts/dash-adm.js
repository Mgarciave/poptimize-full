$.ajax({
    url:ajax_url+'/v1/schedule/getdropdowns',
    type: 'post',
    dataType: 'json',
    xhrFields: {
      withCredentials: true
    },
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    success: function(data){
        $.each(data[0], function(e,v){
            $('#selections-teachers').append('<div class="item" data-value="'+v.teacher_id+'">'+v.nombre+'</div>');
            $('#modalteach .menu').append('<div class="item" data-value="'+v.teacher_id+'">'+v.nombre+'</div>');
            $('.le_first').append('<div class="ui checkbox"><input type="checkbox" name="teachers[]" tabindex="0" data-id="'+v.teacher_id+'" data-name="'+v.nombre+' "><label>'+v.nombre+'</label></div><br><br>');
        });
        $.each(data[1], function(e,v){
            $('#selections-course').append('<div class="item" data-value="'+v.name+'">'+v.name+'</div>');
            $('#modallevel .menu').append('<div class="item" data-value="'+v.name+'">'+v.name+'</div>');
        });
        $.each(data[2], function(e,v){
            $('#selections-asign').append('<div class="item" data-value="'+v.name+'">'+v.name+'</div>');
            $('#modalasign .menu').append('<div class="item" data-value="'+v.name+'">'+v.name+'</div>');
        });
        $.each(data[3], function(e,v){
            $('#selections-cc').append('<div class="item" data-name="'+v.curso+'" data-letter="'+v.letter+'">'+v.name+'</div>');
        });
    }
});

$("#selectall").click(function(){
    $('input:checkbox').not(this).prop('checked', this.checked);
});

$('#messageutp').click(function(){
    $('.class-modal').modal('show');
});

$('.send-reminder').on('click', function(e){
    e.preventDefault();
    var id_array = []
    $("input[name='teachers[]']:checked").each(function(){
        id_array.push($(this).data('id'));
    });
    $('input:checkbox').removeAttr('checked');
    if(id_array == 0 || $('#message').val().length == 0){
        if(id_array == 0){
            swal({
                title: 'Error',
                text: 'Seleccione al menos un profesor',
                type: 'error',
                confirmButtonText: "Continuar",
            });
        }
        else{
            swal({
                title: 'Error',
                text: 'El mensaje es demasiado corto',
                type: 'error',
                confirmButtonText: "Continuar",
            });
        }
    }
    else{
        $.ajax({
            url:ajax_url+'/v1/users/sendutpmail',
            type: 'post',
            dataType: 'text',
            data: {teach_ids: id_array, message:$('#message').val()},
            xhrFields: {
              withCredentials: true
            },
            beforeSend: function(){
                $('#loader').modal('show');
            },
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(data){
                $('#loader').modal('hide');
                $('#message').val('');
                swal({
                    title: 'Mensaje Enviado',
                    text: 'El mensaje ha sido enviado exitosamente a los usuarios seleccionados',
                    type: 'success',
                    confirmButtonText: "Continuar"
                });
            }
        });
    }

});

$('#teachersel').dropdown({
    onChange: function(val){
   teach_id = val
    var att_info = $('#all-info');
    att_info.empty();
    $('#asignsel').dropdown('restore defaults');
    $('#coursesel').dropdown('restore defaults');
    $('#ccsel').dropdown('restore defaults');
    var classArray = [];
    var oaArray = [];
    if(teach_id){
        $.ajax({
            url:ajax_url+'/v1/schedule/teachasig',
            type: 'post',
            dataType: 'json',
            data: {teacher_id: teach_id},
            xhrFields: {
              withCredentials: true
            },
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(data){
                console.log(data);
                if(data.length == 0){
                    swal({
                        title: 'Error',
                        text: 'No existen planificaciones para su solicitud',
                        type: 'error',
                        confirmButtonText: "Continuar",
                      });
                }
                else{
                $.each(data, function(e,v){
                //     classArray = [];
                //     oaArray = [];
                var htmltext = null;
                htmltext ='<div class="ui sixteen wide row"><h1 class="ui dividing header">'+v.graphInfo[0]+'</h1></div>';
                htmltext += '<div class="ui attached row" id="context'+e+'"><div class="ui top attached tabular menu"><div class="active item" data-tab="first">Resumen</div><div class="item" data-tab="second" >Clases</div><div class="item" data-tab="third">Objetivos de Aprendizaje</div></div>';
                htmltext +='<div class="ui attached grid active tab centered segment" data-tab="first"><div class="five wide column"><canvas id="'+v.graphInfo[1]+'" width="300" heigth="300"></canvas></div>';
                htmltext +='<div class="five wide column"><canvas id="'+v.graphInfo[6]+'" width="300" heigth="300"></canvas></div></div>';
                classArray = [v.graphInfo[2], v.graphInfo[3],v.graphInfo[1]];
                oaArray = [v.graphInfo[4],v.graphInfo[5],v.graphInfo[6]];

                htmltext += '<div class="ui attached grid tab centered segment" data-tab="second">'; // COMIENZO DE LOS DATOS DE CURSO

                var totalClases = v.classInfo.plancourses.length;
                var noClases = v.classInfo.noplancourses.length;
                htmltext += '<div class="ui five wide column"><h1>Clases Planificadas: '+totalClases+'</h1><div id="plan" class="ui relaxed divided list scrollable" style="height: 200px; overflow-y: auto;">';
                $.each(v.classInfo.plancourses, function(e,v){
                    var parseo = v.date.split("-");
                    var date = parseo[2] + "/" + parseo[1];
                    htmltext += '<div class="item" data-id="'+v.id+'" style="cursor: pointer;" ><div class="content"><p class="header">Clase del día '+date+'</p></div></div>';
                });
                htmltext += '</div></div>';
                htmltext += '<div class="ui five wide column"><h1>Clases sin Planificar: '+noClases+'</h1><div id="noplan" class="ui relaxed divided list scrollable" style="height: 200px; overflow-y: auto;">';
                $.each(v.classInfo.noplancourses, function(e,v){
                    var parseo = v.date.split("-");
                    var date = parseo[2] + "/" + parseo[1];
                    htmltext += '<div class="item" data-id="'+v.id+'" ><div class="content"><p class="header">Clase del día '+date+'</p></div></div>';
                });
                htmltext += '</div></div>';
                htmltext += '</div>'; // FIN DE LOS DATOS DE CURSO
                htmltext += '<div class="ui attached grid tab centered segment" data-tab="third">'; // COMIENZO DE LOS DATOS

                var oas = v.oasInfo.oaseen.length;
                var nooas = v.oasInfo.oaunseen.length;
                htmltext += '<div class="ui five wide column"><h1>OA Planificados: '+oas+'</h1><div id="planoa" class="ui relaxed divided list scrollable" style="height: 200px; overflow-y: auto;">';
                $.each(v.oasInfo.oaseen, function(e,v){
                    htmltext += '<div class="item" ><div class="content"><p class="header" style="text-align: left;">'+v.OA+': '+v.desc+'</p></div></div>';                           
                });
                htmltext += '</div></div>';
                htmltext += '<div class="ui five wide column"><h1>OA sin Planificar: '+nooas+'</h1><div id="plannooa" class="ui relaxed divided list scrollable" style="height: 200px; overflow-y: auto;">';
                $.each(v.oasInfo.oaunseen, function(e,v){
                    htmltext += '<div class="item" ><div class="content"><p class="header" style="text-align: left;">'+v.OA+': '+v.desc+'</p></div></div>';                                
                });
                htmltext += '</div></div>';
                htmltext += '</div></div>'; // FIN DE LOS DATOS DE CURSO

                htmltext += '</div>';


                att_info.append(htmltext);

                $('#context'+e+' .tabular.menu .item').tab({
                    context: '#context'+e+''
                });

                renderGraph.apply(null,classArray);
                renderGraphoa.apply(null,oaArray);
                
                });

            }

            }

        });

    }
    }

});
$('#coursesel').dropdown({
    onChange: function(val){
    var course_name = val;
    var att_info = $('#all-info');
    att_info.empty();
    $('#asignsel').dropdown('restore defaults');
    $('#teachersel').dropdown('restore defaults');
    $('#ccsel').dropdown('restore defaults');
    if(course_name){
           $.ajax({
            url:ajax_url+'/v1/schedule/courseteach',
            type: 'post',
            dataType: 'json',
            data: {coursename: course_name},
            xhrFields: {
              withCredentials: true
            },
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(data){
                if(data.length == 0){
                    swal({
                        title: 'Error',
                        text: 'No existen planificaciones para su solicitud',
                        type: 'error',
                        confirmButtonText: "Continuar",
                      });
                }
                else{
                 
                $.each(data, function(e,v){
                //     classArray = [];
                //     oaArray = [];
                var htmltext = null;
                htmltext ='<div class="ui sixteen wide row"><h1 class="ui dividing header">'+v.graphInfo[0]+'</h1></div>';
                htmltext += '<div class="ui attached row" id="context'+e+'"><div class="ui top attached tabular menu"><div class="active item" data-tab="first">Resumen</div><div class="item" data-tab="second" >Clases</div><div class="item" data-tab="third">Objetivos de Aprendizaje</div></div>';
                htmltext +='<div class="ui attached grid active tab centered segment" data-tab="first"><div class="five wide column"><canvas id="'+v.graphInfo[1]+'" width="300" heigth="300"></canvas></div>';
                htmltext +='<div class="five wide column"><canvas id="'+v.graphInfo[6]+'" width="300" heigth="300"></canvas></div></div>';
                classArray = [v.graphInfo[2], v.graphInfo[3],v.graphInfo[1]];
                oaArray = [v.graphInfo[4],v.graphInfo[5],v.graphInfo[6]];

                htmltext += '<div class="ui attached grid tab centered segment" data-tab="second">'; // COMIENZO DE LOS DATOS DE CURSO

                var totalClases = v.classInfo.plancourses.length;
                var noClases = v.classInfo.noplancourses.length;
                htmltext += '<div class="ui five wide column"><h1>Clases Planificadas: '+totalClases+'</h1><div id="plan" class="ui relaxed divided list scrollable" style="height: 200px; overflow-y: auto;">';
                $.each(v.classInfo.plancourses, function(e,v){
                    var parseo = v.date.split("-");
                    var date = parseo[2] + "/" + parseo[1];
                    htmltext += '<div class="item" data-id="'+v.id+'" style="cursor: pointer;" ><div class="content"><p class="header">Clase del día '+date+'</p></div></div>';
                });
                htmltext += '</div></div>';
                htmltext += '<div class="ui five wide column"><h1>Clases sin Planificar: '+noClases+'</h1><div id="noplan" class="ui relaxed divided list scrollable" style="height: 200px; overflow-y: auto;">';
                $.each(v.classInfo.noplancourses, function(e,v){
                    var parseo = v.date.split("-");
                    var date = parseo[2] + "/" + parseo[1];
                    htmltext += '<div class="item" data-id="'+v.id+'" ><div class="content"><p class="header">Clase del día '+date+'</p></div></div>';
                });
                htmltext += '</div></div>';
                htmltext += '</div>'; // FIN DE LOS DATOS DE CURSO
                htmltext += '<div class="ui attached grid tab centered segment" data-tab="third">'; // COMIENZO DE LOS DATOS

                var oas = v.oasInfo.oaseen.length;
                var nooas = v.oasInfo.oaunseen.length;
                htmltext += '<div class="ui five wide column"><h1>OA Planificados: '+oas+'</h1><div id="planoa" class="ui relaxed divided list scrollable" style="height: 200px; overflow-y: auto;">';
                $.each(v.oasInfo.oaseen, function(e,v){
                    htmltext += '<div class="item" ><div class="content"><p class="header" style="text-align: left;">'+v.OA+': '+v.desc+'</p></div></div>';                           
                });
                htmltext += '</div></div>';
                htmltext += '<div class="ui five wide column"><h1>OA sin Planificar: '+nooas+'</h1><div id="plannooa" class="ui relaxed divided list scrollable" style="height: 200px; overflow-y: auto;">';
                $.each(v.oasInfo.oaunseen, function(e,v){
                    htmltext += '<div class="item" ><div class="content"><p class="header" style="text-align: left;">'+v.OA+': '+v.desc+'</p></div></div>';                                
                });
                htmltext += '</div></div>';
                htmltext += '</div></div>'; // FIN DE LOS DATOS DE CURSO

                htmltext += '</div>';


                att_info.append(htmltext);

                $('#context'+e+' .tabular.menu .item').tab({
                    context: '#context'+e+''
                });

                renderGraph.apply(null,classArray);
                renderGraphoa.apply(null,oaArray);
                
                });
   
                }
            }

       }); 
    }   
    }
});
$('#asignsel').dropdown({
    onChange: function(val){
    asign_name = val;
    var att_info = $('#all-info');
    att_info.empty();
    $('#teachersel').dropdown('restore defaults');
    $('#coursesel').dropdown('restore defaults');
    $('#ccsel').dropdown('restore defaults');
    if(asign_name){
       $.ajax({
            url:ajax_url+'/v1/schedule/asignteach',
            type: 'post',
            dataType: 'json',
            data: {asignname: asign_name},
            xhrFields: {
              withCredentials: true
            },
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(data){
                if(data.length == 0){
                    swal({
                        title: 'Error',
                        text: 'No existen planificaciones para su solicitud',
                        type: 'error',
                        confirmButtonText: "Continuar",
                      });
                }
                else{
                     $.each(data, function(e,v){
                //     classArray = [];
                //     oaArray = [];
                var htmltext = null;
                htmltext ='<div class="ui sixteen wide row"><h1 class="ui dividing header">'+v.graphInfo[0]+'</h1></div>';
                htmltext += '<div class="ui attached row" id="context'+e+'"><div class="ui top attached tabular menu"><div class="active item" data-tab="first">Resumen</div><div class="item" data-tab="second" >Clases</div><div class="item" data-tab="third">Objetivos de Aprendizaje</div></div>';
                htmltext +='<div class="ui attached grid active tab centered segment" data-tab="first"><div class="five wide column"><canvas id="'+v.graphInfo[1]+'" width="300" heigth="300"></canvas></div>';
                htmltext +='<div class="five wide column"><canvas id="'+v.graphInfo[6]+'" width="300" heigth="300"></canvas></div></div>';
                classArray = [v.graphInfo[2], v.graphInfo[3],v.graphInfo[1]];
                oaArray = [v.graphInfo[4],v.graphInfo[5],v.graphInfo[6]];

                htmltext += '<div class="ui attached grid tab centered segment" data-tab="second">'; // COMIENZO DE LOS DATOS DE CURSO

                var totalClases = v.classInfo.plancourses.length;
                var noClases = v.classInfo.noplancourses.length;
                htmltext += '<div class="ui five wide column"><h1>Clases Planificadas: '+totalClases+'</h1><div id="plan" class="ui relaxed divided list scrollable" style="height: 200px; overflow-y: auto;">';
                $.each(v.classInfo.plancourses, function(e,v){
                    var parseo = v.date.split("-");
                    var date = parseo[2] + "/" + parseo[1];
                    htmltext += '<div class="item" data-id="'+v.id+'" style="cursor: pointer;" ><div class="content"><p class="header">Clase del día '+date+'</p></div></div>';
                });
                htmltext += '</div></div>';
                htmltext += '<div class="ui five wide column"><h1>Clases sin Planificar: '+noClases+'</h1><div id="noplan" class="ui relaxed divided list scrollable" style="height: 200px; overflow-y: auto;">';
                $.each(v.classInfo.noplancourses, function(e,v){
                    var parseo = v.date.split("-");
                    var date = parseo[2] + "/" + parseo[1];
                    htmltext += '<div class="item" data-id="'+v.id+'" ><div class="content"><p class="header">Clase del día '+date+'</p></div></div>';
                });
                htmltext += '</div></div>';
                htmltext += '</div>'; // FIN DE LOS DATOS DE CURSO
                htmltext += '<div class="ui attached grid tab centered segment" data-tab="third">'; // COMIENZO DE LOS DATOS

                var oas = v.oasInfo.oaseen.length;
                var nooas = v.oasInfo.oaunseen.length;
                htmltext += '<div class="ui five wide column"><h1>OA Planificados: '+oas+'</h1><div id="planoa" class="ui relaxed divided list scrollable" style="height: 200px; overflow-y: auto;">';
                $.each(v.oasInfo.oaseen, function(e,v){
                    htmltext += '<div class="item" ><div class="content"><p class="header" style="text-align: left;">'+v.OA+': '+v.desc+'</p></div></div>';                           
                });
                htmltext += '</div></div>';
                htmltext += '<div class="ui five wide column"><h1>OA sin Planificar: '+nooas+'</h1><div id="plannooa" class="ui relaxed divided list scrollable" style="height: 200px; overflow-y: auto;">';
                $.each(v.oasInfo.oaunseen, function(e,v){
                    htmltext += '<div class="item" ><div class="content"><p class="header" style="text-align: left;">'+v.OA+': '+v.desc+'</p></div></div>';                                
                });
                htmltext += '</div></div>';
                htmltext += '</div></div>'; // FIN DE LOS DATOS DE CURSO

                htmltext += '</div>';


                att_info.append(htmltext);

                $('#context'+e+' .tabular.menu .item').tab({
                    context: '#context'+e+''
                });

                renderGraph.apply(null,classArray);
                renderGraphoa.apply(null,oaArray);
                
                });
                
                }//end else
            }
       });
     
    }
    }
});

$('#ccsel').dropdown({
    onChange: function(val){
    var arr = val.split(' ');
    var cursito = arr[0]+' '+arr[1][0].toUpperCase()+arr[1].slice(1);
    var course_name = cursito;
    var course_letter = arr[2].toUpperCase();
    console.log(cursito+' '+course_letter);
    var att_info = $('#all-info');
    att_info.empty();
    $('#asignsel').dropdown('restore defaults');
    $('#coursesel').dropdown('restore defaults');
    $('#teachersel').dropdown('restore defaults');
    att_info.append('<div id="pegado"></div>');
    if(course_name){
        $('#pegado').fullCalendar({
                    height: "auto",
                    eventAfterAllRender: function() {
                      $("#fullcalendar-detail").find('.fc-toolbar > div > h2').empty().append("<div>Curso / Asignatura</div>");
                      //$('.fc-day-header').text($('.fc-day-header').text().replace(/[0-9]/g, ''));
                      $('.fc-day-header').each(function(){
                      //console.log($(this).text($(this).text().replace(/[0-9]/g, '')));
                      });
                    },
                    editable: false,
                    defaultView: 'agendaWeek',
                    firstDay: 1,
                    allDaySlot: false,
                    minTime: '07:00:00',
                    maxTime: '20:00:00',
                    columnFormat: 'dddd D',
                    titleFormat: 'MMMM YYYY',
                    eventClick: function(calEvent, jsEvent, view) {
                        if($(this).hasClass('holi') || $(this).hasClass('conme')){
                            if($(this).hasClass('conme')){
                              $('#conme-content').empty();
                              $.ajax({
                                url: ajax_url+'/v1/schedule/getconme',
                                dataType: 'json',
                                type: 'post',
                                async: false,
                                xhrFields: {
                                  withCredentials: true
                                },
                                headers: {
                                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                },
                                data: {date: calEvent.start.format()},
                                success: function(dato){
                                  var conmes = '';
                                  $.each(dato, function(e,v){
                                    console.log(v.desc);
                                    conmes += '<p>'+v.desc+'</p>';
                                    if(v.file){
                                      conmes += '<a href="'+ajax_url+'dist/conmes/'+v.file+'" target="_blank" >'+v.file+'</a>';
                                    }
                                  });
                                  $('#conme-content').append(conmes);
                                  $('#conme-modal').modal('show');
                                }
                              });
                            }

                          }
                        else{
                        console.log(calEvent.id);
                        var content_modal = $('#detail-class');
                        content_modal.empty();
                        $.ajax({
                        url:ajax_url+'/v1/schedule/getclassinfo',
                        type: 'post',
                        dataType: 'json',
                        data: {id: calEvent.id},
                        xhrFields: {
                          withCredentials: true
                        },
                        headers: {
                          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        success: function(data){
                            var htmlModal = '<p class="titleno"><b>Profesor: </b>'+data.teacherName+'</b></p>';
                            htmlModal += '<p class="titleno"><b>Asignatura: </b>'+data.asign+'</b></p>';
                            htmlModal += '<p class="titleno"><b>Curso: </b>'+data.course+'</b></p>';
                            htmlModal += '<p class="titleno"><b>Fecha: </b>'+data.formated+'</b></p>';
                            if(data.type==0){
                              htmlModal += '<p class="titleno"><b>Tipo: </b>Clase</b></p><br>';
                            }
                            if(data.type==1){
                              htmlModal += '<p class="titleno"><b>Tipo: </b>Prueba</b></p><br>';
                            }
                            if(data.type==2){
                              htmlModal += '<p class="titleno"><b>Tipo: </b>Actividad/Trabajo</b></p><br>';
                            }
                            if(data.arroas.length > 0){
                                htmlModal += '<p class="titleno"><b>'+data.unit+'</b></p>';
                                htmlModal += '<p class="textno"><b>Objetivos de Aprendizaje:</b></p>';
                                $.each(data.arroas, function(e,v){
                                    htmlModal += '<p class="textno"><b>'+v.name+': </b>'+v.desc+'</p>'
                                });
                                htmlModal += '<br>';
                            }
                            else{
                                htmlModal += '<p class="textno"><b>Objetivos de Aprendizaje: </b>Ninguno</p><br>';
                            }
                            if(data.arrat.length > 0){
                              htmlModal += '<p class="titleno" ><b>Actitudes</b></p>'
                              $.each(data.arrat, function(e,v){
                                htmlModal += '<p class="textno">'+v.desc+'</p>'
                              });
                              htmlModal += '<br>';
                            }
                            if(data.arrab.length > 0){
                              htmlModal += '<p class="titleno"><b>Habilidades</b></p>'
                              $.each(data.arrab, function(e,v){
                                htmlModal += '<p class="textno">'+v.desc+'</p>'
                              });
                              htmlModal += '<br>';
                            }
                            if(data.arrtag.length > 0){
                              htmlModal += '<p class="titleno"><b>Palabras Clave</b></p><p class="textno">'
                              $.each(data.arrtag, function(e,v){
                                if(e < data.arrtag.length-1){
                                  htmlModal += v.name+', ';
                                }
                                else{
                                  htmlModal += v.name+'.';
                                }
                              });
                              htmlModal += '</p><br>';
                            }
                            if(data.scheduleData[0].description || data.scheduleData[0].init_text || data.scheduleData[0].mid_text || data.scheduleData[0].end_text || data.scheduleData[0].res_text || data.scheduleData[0].eval_text || data.scheduleData[0].ec_text){
                                if(data.scheduleData[0].description.length){
                                    htmlModal += '<p class="titleno"><b>Descripción</b></p><p class="textno">'+data.scheduleData[0].description.replace(/(\n)+/g, '<br />')+'</p><br>';       
                                }
                                if(data.scheduleData[0].init_text){
                                    htmlModal += '<p class="titleno"><b>Inicio</b></p><p class="textno">'+data.scheduleData[0].init_text.replace(/(\n)+/g, '<br />')+'</p><br>';       
                                }
                                if(data.scheduleData[0].mid_text){
                                    htmlModal += '<p class="titleno"><b>Desarrollo</b></p><p class="textno">'+data.scheduleData[0].mid_text.replace(/(\n)+/g, '<br />')+'</p><br>';       
                                }
                                if(data.scheduleData[0].end_end){
                                    htmlModal += '<p class="titleno"><b>Cierre</b></p><p class="textno">'+data.scheduleData[0].end_text.replace(/(\n)+/g, '<br />')+'</p><br>';       
                                }
                                if(data.scheduleData[0].res_text){
                                    htmlModal += '<p class="titleno"><b>Recursos de Aprendizaje</b></p><p class="textno">'+data.scheduleData[0].res_text.replace(/(\n)+/g, '<br />')+'</p><br>';       
                                }
                                if(data.scheduleData[0].eval_text){
                                    htmlModal += '<p class="titleno"><b>Evaluación</b></p><p class="textno">'+data.scheduleData[0].eval_text.replace(/(\n)+/g, '<br />')+'</p><br>';       
                                }
                                if(data.scheduleData[0].ec_text){
                                    htmlModal += '<p class="titleno"><b>Adecuación Curricular</b></p><p class="textno">'+data.scheduleData[0].ec_text.replace(/(\n)+/g, '<br />')+'</p><br>';       
                                }
                            }
                            else{

                                htmlModal += '<p class="titleno"><b>Actividades de Aprendizaje</b></p><p class="textno">Ninguno</p>';
                            }
                            if(data.file){
                                htmlModal += '<p class="titleno"><b>Archivo Adjunto</b></p><a href="'+ajax_url+'/uploads/'+data.file+'" target="_blank">'+data.file+'</a>';
                            }            

                            content_modal.append(htmlModal);
                            $('.class-detail').modal('show');



                        }
                    });
                    }

                    },
                    lang: 'es',
                    droppable: false,
                    slotDuration: '00:30:00',
                    slotLabelFormat: 'h:mm',
                    events: function(start, end, timezone, callback){ 
                        var events = [];
                        $.ajax({
                          url: ajax_url+'/v1/schedule/conme',
                          dataType: 'json',
                          type: 'post',
                          xhrFields: {
                            withCredentials: true
                          },
                          headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                          },
                          success: function(data){
                            $.each(data, function(e,v){
                              events.push({
                                allDay: false,
                                start: v.date_init,
                                end: v.date_end,
                                title: 'Efemérides',
                                className: 'conme',
                                color: '#ffffff',
                                textColor: '#000000',
                              });
                            });
                          }

                        });

                        $.ajax({
                          url: ajax_url+'/v1/schedule/holidays',
                          dataType: 'json',
                          type: 'post',
                          xhrFields: {
                            withCredentials: true
                          },
                          headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                          },
                          success: function(data){
                            $.each(data, function(e,v){
                              events.push({
                                allDay: false,
                                start: v.date_init,
                                end: v.date_end,
                                title: v.name,
                                className: 'holi',
                                color: '#d3d3d3'
                              });
                            });
                          }

                        });
                        $.ajax({
                            url: ajax_url+'/v1/schedule/utpcalendar',
                            type: 'post',
                            dataType: 'json',
                            data: {course_name: course_name, course_letter: course_letter},
                            xhrFields: {
                              withCredentials: true
                            },
                            headers: {
                              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            success: function(data){
                                console.log(data);
                                $.each(data, function(e,v){
                                  events.push({
                                    title: v.name+' / '+v.course+' '+v.letter,
                                    start: v.hora_inicio,
                                    end: v.hora_fin,
                                    allDay: false,
                                    className: v.color,
                                    color: v.color,
                                    id: v.main_id,
                                    type: v.type
                                  });
                                });
                                callback(events);      
                            }
                        });   
                    },
                    eventRender: function (event, element){
                        console.log(event);
                        if(element.hasClass('holi') || element.hasClass('conme')){
                            element.prepend( "<i class='alarm outline icon' style='float:left;'></i>" );
                        }
                        else{
                        if(event.type==0){
                          element.prepend( "<i class='book icon large' style='float:right; margin-top:2px;'></i>" );
                        }
                        if(event.type==1){
                         element.prepend( "<i class='file powerpoint outline icon large' style='float:right; margin-top:2px;'></i>" );
                        } 
                        if(event.type==2){
                         element.prepend( "<i class='users icon large' style='float:right; margin-top:2px;'></i>" );
                        }
                        element.attr('data-id',event.id);
                        $.ajax({
                          url: ajax_url+'/v1/objectives/list',
                          dataType: 'json',
                          type: 'post',
                          data: {id: event.id},
                          xhrFields: {
                            withCredentials: true
                          },
                          headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                          },
                          success: function(data){
                            console.log(data);
                            //alert(111);
                            $.each(data, function(e,v){
                              //element.append(v.text);
                              element.append('<div class="in-event with-popover '+v.color+'" data-id="'+v.objective_id+'">'+v.text+'</div>');
                            });
                          }
                        });
                        }
                      }

            }); 
     }    
    }
});
$('#modalteach').dropdown();
$('#modallevel').dropdown();
$('#modalasign').dropdown();


$('.le-print').on('click', function(){
    $('.print-data').modal('show');
});

  $('.plan-print').on('click',function(){
        $impselect = $('input[name=imp]:checked').val();
        if($impselect == 0){
            var teacher_id = $('#modalteach').dropdown('get value');
            if(teacher_id){
                var url = ajax_url+'/v1/report/teacher/printutp?id='+teacher_id;
                window.open(url, '_blank');
            }
        }
        if($impselect == 1){
            var level_name = $('#modallevel').dropdown('get value');
            if(level_name){
                var url = ajax_url+'/v1/report/level/printutp?level='+level_name;
                window.open(url, '_blank');
            }
        }
        if($impselect == 2){
            var asign_name = $('#modalasign').dropdown('get value');
            if(asign_name){
                var url = ajax_url+'/v1/report/asign/printutp?asign='+asign_name;
                window.open(url, '_blank');
            }
        }
        if($impselect == 3){
                var url = ajax_url+'/v1/report/all/printutp';
                window.open(url, '_blank');
        }
    });

// $('#ccsel').on('click', function(){
//     var course_name = $('#ccsel').find(".selected").data('name');
//     var course_letter = $('#ccsel').find(".selected").data('letter');
//     var att_info = $('#all-info');
//     $('#ccsel').dropdown('restore defaults');
//     att_info.empty();
//     $('#asignsel').dropdown('restore defaults');
//     $('#coursesel').dropdown('restore defaults');
//     $('#teachersel').dropdown('restore defaults');
//     att_info.append('<div id="pegado"></div>');
//     if(course_name){
//         $('#pegado').fullCalendar({
//                     height: "auto",
//                     eventAfterAllRender: function() {
//                       $("#fullcalendar-detail").find('.fc-toolbar > div > h2').empty().append("<div>Curso / Asignatura</div>");
//                       //$('.fc-day-header').text($('.fc-day-header').text().replace(/[0-9]/g, ''));
//                       $('.fc-day-header').each(function(){
//                       //console.log($(this).text($(this).text().replace(/[0-9]/g, '')));
//                       });
//                     },
//                     editable: false,
//                     defaultView: 'agendaWeek',
//                     firstDay: 1,
//                     allDaySlot: false,
//                     minTime: '07:00:00',
//                     maxTime: '20:00:00',
//                     columnFormat: 'dddd D',
//                     titleFormat: 'MMMM YYYY',
//                     eventClick: function(calEvent, jsEvent, view) {
//                         console.log(calEvent.id);
//                         var content_modal = $('#detail-class');
//                         content_modal.empty();
//                         $.ajax({
//                         url:ajax_url+'/v1/schedule/getclassinfo',
//                         type: 'post',
//                         dataType: 'json',
//                         data: {id: calEvent.id},
//                         xhrFields: {
//                           withCredentials: true
//                         },
//                         headers: {
//                           'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//                         },
//                         success: function(data){
//                             var htmlModal = '<p class="titleno"><b>Profesor: </b>'+data.teacherName+'</b></p>';
//                             htmlModal += '<p class="titleno"><b>Asignatura: </b>'+data.asign+'</b></p>';
//                             htmlModal += '<p class="titleno"><b>Curso: </b>'+data.course+'</b></p>';
//                             htmlModal += '<p class="titleno"><b>Fecha: </b>'+data.formated+'</b></p>';
//                             if(data.type==0){
//                               htmlModal += '<p class="titleno"><b>Tipo: </b>Clase</b></p><br>';
//                             }
//                             if(data.type==1){
//                               htmlModal += '<p class="titleno"><b>Tipo: </b>Prueba</b></p><br>';
//                             }
//                             if(data.type==2){
//                               htmlModal += '<p class="titleno"><b>Tipo: </b>Actividad/Trabajo</b></p><br>';
//                             }
//                             if(data.arroas.length > 0){
//                                 htmlModal += '<p class="titleno"><b>'+data.unit+'</b></p>';
//                                 htmlModal += '<p class="textno"><b>Objetivos de Aprendizaje:</b></p>';
//                                 $.each(data.arroas, function(e,v){
//                                     htmlModal += '<p class="textno"><b>'+v.name+': </b>'+v.desc+'</p>'
//                                 });
//                                 htmlModal += '<br>';
//                             }
//                             else{
//                                 htmlModal += '<p class="textno"><b>Objetivos de Aprendizaje: </b>Ninguno</p><br>';
//                             }
//                             if(data.arrat.length > 0){
//                               htmlModal += '<p class="titleno" ><b>Actitudes</b></p>'
//                               $.each(data.arrat, function(e,v){
//                                 htmlModal += '<p class="textno">'+v.desc+'</p>'
//                               });
//                               htmlModal += '<br>';
//                             }
//                             if(data.arrab.length > 0){
//                               htmlModal += '<p class="titleno"><b>Habilidades</b></p>'
//                               $.each(data.arrab, function(e,v){
//                                 htmlModal += '<p class="textno">'+v.desc+'</p>'
//                               });
//                               htmlModal += '<br>';
//                             }
//                             if(data.arrtag.length > 0){
//                               htmlModal += '<p class="titleno"><b>Palabras Clave</b></p><p class="textno">'
//                               $.each(data.arrtag, function(e,v){
//                                 if(e < data.arrtag.length-1){
//                                   htmlModal += v.name+', ';
//                                 }
//                                 else{
//                                   htmlModal += v.name+'.';
//                                 }
//                               });
//                               htmlModal += '</p><br>';
//                             }
//                             if(data.scheduleData[0].description || data.scheduleData[0].init_text || data.scheduleData[0].mid_text || data.scheduleData[0].end_text || data.scheduleData[0].res_text || data.scheduleData[0].eval_text || data.scheduleData[0].ec_text){
//                                 if(data.scheduleData[0].description.length){
//                                     htmlModal += '<p class="titleno"><b>Descripción</b></p><p class="textno">'+data.scheduleData[0].description.replace(/(\n)+/g, '<br />')+'</p><br>';       
//                                 }
//                                 if(data.scheduleData[0].init_text){
//                                     htmlModal += '<p class="titleno"><b>Inicio</b></p><p class="textno">'+data.scheduleData[0].init_text.replace(/(\n)+/g, '<br />')+'</p><br>';       
//                                 }
//                                 if(data.scheduleData[0].mid_text){
//                                     htmlModal += '<p class="titleno"><b>Desarrollo</b></p><p class="textno">'+data.scheduleData[0].mid_text.replace(/(\n)+/g, '<br />')+'</p><br>';       
//                                 }
//                                 if(data.scheduleData[0].end_end){
//                                     htmlModal += '<p class="titleno"><b>Cierre</b></p><p class="textno">'+data.scheduleData[0].end_text.replace(/(\n)+/g, '<br />')+'</p><br>';       
//                                 }
//                                 if(data.scheduleData[0].res_text){
//                                     htmlModal += '<p class="titleno"><b>Recursos de Aprendizaje</b></p><p class="textno">'+data.scheduleData[0].res_text.replace(/(\n)+/g, '<br />')+'</p><br>';       
//                                 }
//                                 if(data.scheduleData[0].eval_text){
//                                     htmlModal += '<p class="titleno"><b>Evaluación</b></p><p class="textno">'+data.scheduleData[0].eval_text.replace(/(\n)+/g, '<br />')+'</p><br>';       
//                                 }
//                                 if(data.scheduleData[0].ec_text){
//                                     htmlModal += '<p class="titleno"><b>Adecuación Curricular</b></p><p class="textno">'+data.scheduleData[0].ec_text.replace(/(\n)+/g, '<br />')+'</p><br>';       
//                                 }
//                             }
//                             else{

//                                 htmlModal += '<p class="titleno"><b>Actividades de Aprendizaje</b></p><p class="textno">Ninguno</p>';
//                             }            

//                             content_modal.append(htmlModal);
//                             $('.class-detail').modal('show');



//                         }
//                     });

//                     },
//                     lang: 'es',
//                     droppable: false,
//                     slotDuration: '00:30:00',
//                     slotLabelFormat: 'h:mm',
//                     events: function(start, end, timezone, callback){ 
//                         var events = [];
//                         $.ajax({
//                             url: ajax_url+'/v1/schedule/utpcalendar',
//                             type: 'post',
//                             dataType: 'json',
//                             data: {course_name: course_name, course_letter: course_letter},
//                             xhrFields: {
//                               withCredentials: true
//                             },
//                             headers: {
//                               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//                             },
//                             success: function(data){
//                                 console.log(data);
//                                 $.each(data, function(e,v){
//                                   events.push({
//                                     title: v.name+' / '+v.course+' '+v.letter,
//                                     start: v.hora_inicio,
//                                     end: v.hora_fin,
//                                     allDay: false,
//                                     className: v.color,
//                                     color: v.color,
//                                     id: v.main_id,
//                                     type: v.type
//                                   });
//                                 });
//                                 callback(events);      
//                             }
//                         });   
//                     },
//                     eventRender: function (event, element){
//                         console.log(event);
//                         if(event.type==0){
//                           element.prepend( "<i class='book icon large' style='float:right; margin-top:2px;'></i>" );
//                         }
//                         if(event.type==1){
//                          element.prepend( "<i class='file powerpoint outline icon large' style='float:right; margin-top:2px;'></i>" );
//                         } 
//                         if(event.type==2){
//                          element.prepend( "<i class='users icon large' style='float:right; margin-top:2px;'></i>" );
//                         }
//                         element.attr('data-id',event.id);
//                         $.ajax({
//                           url: ajax_url+'/v1/objectives/list',
//                           dataType: 'json',
//                           type: 'post',
//                           data: {id: event.id},
//                           xhrFields: {
//                             withCredentials: true
//                           },
//                           headers: {
//                             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//                           },
//                           success: function(data){
//                             console.log(data);
//                             //alert(111);
//                             $.each(data, function(e,v){
//                               //element.append(v.text);
//                               element.append('<div class="in-event with-popover '+v.color+'" data-id="'+v.objective_id+'">'+v.text+'</div>');
//                             });
//                           }
//                         });
//                       }

//             }); 
//      }

// });

// $('#asignsel').on('click', function(){
//     var asign_name = $('#asignsel').dropdown('get value');
//     var att_info = $('#all-info');
//     att_info.empty();
//     $('#asignsel').dropdown('restore defaults');
//     if(asign_name){
//     $('#loader').modal('toggle');
//        $.ajax({
//             url:ajax_url+'/v1/schedule/asignteach',
//             type: 'post',
//             dataType: 'json',
//             data: {asignname: asign_name},
//             xhrFields: {
//               withCredentials: true
//             },
//             headers: {
//               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//             },
//             success: function(data){
//                 if(data.length == 0){
//                     swal({
//                         title: 'Error',
//                         text: 'No existen planificaciones para su solicitud',
//                         type: 'error',
//                         confirmButtonText: "Continuar",
//                       });
//                 }
//                 else{
//                     $('#loader').modal('show');
//                 $.each(data, function(e,v){
//                     classArray = [];
//                     oaArray = [];
//                     var htmltext;
//                     $.ajax({
//                         url:ajax_url+'/v1/schedule/teachdata',
//                         type: 'post',
//                         dataType: 'json',
//                         async: false,
//                         data: {teacher_id: v.teach_id, asig: v.asig_id, let: v.letter},
//                         xhrFields: {
//                           withCredentials: true
//                         },
//                         headers: {
//                           'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//                         },
//                         success: function(datos){
//                             htmltext ='<div class="ui sixteen wide row"><h1 class="ui dividing header">'+datos[0]+'</h1></div>';
//                             htmltext += '<div class="ui attached row" id="context'+e+'"><div class="ui top attached tabular menu"><div class="active item" data-tab="first">Resumen</div><div class="item" data-tab="second" >Clases</div><div class="item" data-tab="third">Objetivos de Aprendizaje</div></div>';
//                             htmltext +='<div class="ui attached grid active tab centered segment" data-tab="first"><div class="five wide column"><canvas id="'+datos[1]+'" width="300" heigth="300"></canvas></div>';
//                             htmltext +='<div class="five wide column"><canvas id="'+datos[6]+'" width="300" heigth="300"></canvas></div></div>';
//                             classArray = [datos[2], datos[3],datos[1]];
//                             oaArray = [datos[4],datos[5],datos[6]];

//                         }
//                     });

//                     htmltext += '<div class="ui attached grid tab centered segment" data-tab="second">'; // COMIENZO DE LOS DATOS DE CURSO
//                     $.ajax({
//                         url:ajax_url+'/v1/schedule/teachcourses',
//                         type: 'post',
//                         dataType: 'json',
//                         async: false,
//                         data: {teacher_id: v.teach_id, asig: v.asig_id, let: v.letter},
//                         xhrFields: {
//                           withCredentials: true
//                         },
//                         headers: {
//                           'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//                         },
//                         success: function(datos){
//                             var totalClases = datos.plancourses.length;
//                             var noClases = datos.noplancourses.length;
//                             htmltext += '<div class="ui five wide column"><h1>Clases Planificadas: '+totalClases+'</h1><div id="plan" class="ui relaxed divided list scrollable" style="height: 200px; overflow-y: auto;">';
//                             $.each(datos.plancourses, function(e,v){
//                                 var parseo = v.date.split("-");
//                                 var date = parseo[2] + "/" + parseo[1];
//                                 htmltext += '<div class="item" data-id="'+v.id+'" style="cursor: pointer;" ><div class="content"><p class="header">Clase del día '+date+'</p></div></div>'
//                             });
//                             htmltext += '</div></div>';
//                             htmltext += '<div class="ui five wide column"><h1>Clases sin Planificar: '+noClases+'</h1><div id="noplan" class="ui relaxed divided list scrollable" style="height: 200px; overflow-y: auto;">';
//                             $.each(datos.noplancourses, function(e,v){
//                                 var parseo = v.date.split("-");
//                                 var date = parseo[2] + "/" + parseo[1];
//                                 htmltext += '<div class="item" data-id="'+v.id+'" ><div class="content"><p class="header">Clase del día '+date+'</p></div></div>'
//                             });
//                             htmltext += '</div></div>';
//                             htmltext += '</div>'; // FIN DE LOS DATOS DE CURSO


//                         }
//                     });

//                     htmltext += '<div class="ui attached grid tab centered segment" data-tab="third">'; // COMIENZO DE LOS DATOS
//                     $.ajax({
//                         url:ajax_url+'/v1/schedule/teachoas',
//                         type: 'post',
//                         dataType: 'json',
//                         async: false,
//                         data: {teacher_id: v.teach_id, asig: v.asig_id, let: v.letter},
//                         xhrFields: {
//                           withCredentials: true
//                         },
//                         headers: {
//                           'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//                         },
//                         success: function(datos){
//                             var oas = datos.oaseen.length;
//                             var nooas = datos.oaunseen.length;
//                             htmltext += '<div class="ui five wide column"><h1>OA Planificados: '+oas+'</h1><div id="planoa" class="ui relaxed divided list scrollable" style="height: 200px; overflow-y: auto;">';
//                             $.each(datos.oaseen, function(e,v){
//                                 htmltext += '<div class="item" ><div class="content"><p class="header" style="text-align: left;">'+v.OA+': '+v.desc+'</p></div></div>';                           
//                             });
//                             htmltext += '</div></div>';
//                             htmltext += '<div class="ui five wide column"><h1>OA sin Planificar: '+nooas+'</h1><div id="plannooa" class="ui relaxed divided list scrollable" style="height: 200px; overflow-y: auto;">';
//                             $.each(datos.oaunseen, function(e,v){
//                                 htmltext += '<div class="item" ><div class="content"><p class="header" style="text-align: left;">'+v.OA+': '+v.desc+'</p></div></div>';                                
//                             });
//                             htmltext += '</div></div>';
//                             htmltext += '</div></div>'; // FIN DE LOS DATOS DE CURSO
//                             att_info.append(htmltext);
//                             $('#context'+e+' .tabular.menu .item').tab({
//                                 context: '#context'+e+''
//                             });
//                             renderGraph.apply(null,classArray);
//                             renderGraphoa.apply(null,oaArray);
                            
//                         }
//                     });

                    
//                     });
//                 }//end else

//                 $('#loader').modal('toggle');
//             }
//        });
     
//     }
// });


// $('#coursesel').on('click', function(){

//     var course_name = $('#coursesel').dropdown('get value');
//     console.log(course_name);
//     var att_info = $('#all-info');
//     att_info.empty();
//     $('#coursesel').dropdown('restore defaults');
//     if(course_name){
//         $('#loader').modal('toggle');
//        $.ajax({
//             url:ajax_url+'/v1/schedule/courseteach',
//             type: 'post',
//             dataType: 'json',
//             data: {coursename: course_name},
//             xhrFields: {
//               withCredentials: true
//             },
//             headers: {
//               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//             },
//             success: function(data){
//                 if(data.length == 0){
//                     swal({
//                         title: 'Error',
//                         text: 'No existen planificaciones para su solicitud',
//                         type: 'error',
//                         confirmButtonText: "Continuar",
//                       });
//                 }
//                 else{
//                     $.each(data, function(e,v){
//                         classArray = [];
//                         oaArray = [];
//                         var htmltext;
//                         $.ajax({
//                             url:ajax_url+'/v1/schedule/teachdata',
//                             type: 'post',
//                             dataType: 'json',
//                             async: false,
//                             data: {teacher_id: v.teach_id, asig: v.asig_id, let: v.letter},
//                             xhrFields: {
//                               withCredentials: true
//                             },
//                             headers: {
//                               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//                             },
//                             success: function(datos){
//                                 htmltext ='<div class="ui sixteen wide row"><h1 class="ui dividing header">'+datos[0]+'</h1></div>';
//                                 htmltext += '<div class="ui attached row" id="context'+e+'"><div class="ui top attached tabular menu"><div class="active item" data-tab="first">Resumen</div><div class="item" data-tab="second" >Clases</div><div class="item" data-tab="third">Objetivos de Aprendizaje</div></div>';
//                                 htmltext +='<div class="ui attached grid active tab centered segment" data-tab="first"><div class="five wide column"><canvas id="'+datos[1]+'" width="300" heigth="300"></canvas></div>';
//                                 htmltext +='<div class="five wide column"><canvas id="'+datos[6]+'" width="300" heigth="300"></canvas></div></div>';
//                                 classArray = [datos[2], datos[3],datos[1]];
//                                 oaArray = [datos[4],datos[5],datos[6]];

//                             }
//                         });

//                         htmltext += '<div class="ui attached grid tab centered segment" data-tab="second">'; // COMIENZO DE LOS DATOS DE CURSO
//                         $.ajax({
//                             url:ajax_url+'/v1/schedule/teachcourses',
//                             type: 'post',
//                             dataType: 'json',
//                             async: false,
//                             data: {teacher_id: v.teach_id, asig: v.asig_id, let: v.letter},
//                             xhrFields: {
//                               withCredentials: true
//                             },
//                             headers: {
//                               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//                             },
//                             success: function(datos){
//                                 var totalClases = datos.plancourses.length;
//                                 var noClases = datos.noplancourses.length;
//                                 htmltext += '<div class="ui five wide column"><h1>Clases Planificadas: '+totalClases+'</h1><div id="plan" class="ui relaxed divided list scrollable" style="height: 200px; overflow-y: auto;">';
//                                 $.each(datos.plancourses, function(e,v){
//                                     var parseo = v.date.split("-");
//                                     var date = parseo[2] + "/" + parseo[1];
//                                     htmltext += '<div class="item" data-id="'+v.id+'" style="cursor: pointer;" ><div class="content"><p class="header">Clase del día '+date+'</p></div></div>'
//                                 });
//                                 htmltext += '</div></div>';
//                                 htmltext += '<div class="ui five wide column"><h1>Clases sin Planificar: '+noClases+'</h1><div id="noplan" class="ui relaxed divided list scrollable" style="height: 200px; overflow-y: auto;">';
//                                 $.each(datos.noplancourses, function(e,v){
//                                     var parseo = v.date.split("-");
//                                     var date = parseo[2] + "/" + parseo[1];
//                                     htmltext += '<div class="item" data-id="'+v.id+'" ><div class="content"><p class="header">Clase del día '+date+'</p></div></div>'
//                                 });
//                                 htmltext += '</div></div>';
//                                 htmltext += '</div>'; // FIN DE LOS DATOS DE CURSO


//                             }
//                         });

//                         htmltext += '<div class="ui attached grid tab centered segment" data-tab="third">'; // COMIENZO DE LOS DATOS
//                         $.ajax({
//                             url:ajax_url+'/v1/schedule/teachoas',
//                             type: 'post',
//                             dataType: 'json',
//                             async: false,
//                             data: {teacher_id: v.teach_id, asig: v.asig_id, let: v.letter},
//                             xhrFields: {
//                               withCredentials: true
//                             },
//                             headers: {
//                               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//                             },
//                             success: function(datos){
//                                 var oas = datos.oaseen.length;
//                                 var nooas = datos.oaunseen.length;
//                                 htmltext += '<div class="ui five wide column"><h1>OA Planificados: '+oas+'</h1><div id="planoa" class="ui relaxed divided list scrollable" style="height: 200px; overflow-y: auto;">';
//                                 $.each(datos.oaseen, function(e,v){
//                                     htmltext += '<div class="item" ><div class="content"><p class="header" style="text-align: left;">'+v.OA+': '+v.desc+'</p></div></div>';                              
//                                 });
//                                 htmltext += '</div></div>';
//                                 htmltext += '<div class="ui five wide column"><h1>OA sin Planificar: '+nooas+'</h1><div id="plannooa" class="ui relaxed divided list scrollable" style="height: 200px; overflow-y: auto;">';
//                                 $.each(datos.oaunseen, function(e,v){
//                                     htmltext += '<div class="item" ><div class="content"><p class="header" style="text-align: left;">'+v.OA+': '+v.desc+'</p></div></div>';                                
//                                 });
//                                 htmltext += '</div></div>';
//                                 htmltext += '</div></div>'; // FIN DE LOS DATOS DE CURSO
//                                 att_info.append(htmltext);
//                                 $('#context'+e+' .tabular.menu .item').tab({
//                                     context: '#context'+e+''
//                                 });
//                                 renderGraph.apply(null,classArray);
//                                 renderGraphoa.apply(null,oaArray);
                                
//                             }
//                         });
//                     });
//                 }
//                 $('#loader').modal('toggle');
//             }

//        }); 
//     }   
// });

// $('#teachersel').on('click', function(){
//     var teach_id = $('#teachersel').dropdown('get value');
//     var att_info = $('#all-info');
//     att_info.empty();
//     var classArray = [];
//     var oaArray = [];
//     $('#teachersel').dropdown('restore defaults');
//     if(teach_id){
//         $('#loader').modal('toggle');
//         $.ajax({
//             url:ajax_url+'/v1/schedule/teachasig',
//             type: 'post',
//             dataType: 'json',
//             data: {teacher_id: teach_id},
//             xhrFields: {
//               withCredentials: true
//             },
//             headers: {
//               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//             },
//             success: function(data){
//                 if(data.length == 0){
//                     swal({
//                         title: 'Error',
//                         text: 'No existen planificaciones para su solicitud',
//                         type: 'error',
//                         confirmButtonText: "Continuar",
//                       });
//                 }
//                 else{
//                 $.each(data, function(e,v){
//                     classArray = [];
//                     oaArray = [];
//                     var htmltext = null;
//                     $.ajax({
//                         url:ajax_url+'/v1/schedule/teachdata',
//                         type: 'post',
//                         dataType: 'json',
//                         async: false,
//                         data: {teacher_id: teach_id, asig: v.asig_id, let: v.letter},
//                         xhrFields: {
//                           withCredentials: true
//                         },
//                         headers: {
//                           'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//                         },
//                         success: function(datos){
//                             htmltext ='<div class="ui sixteen wide row"><h1 class="ui dividing header">'+datos[0]+'</h1></div>';
//                             htmltext += '<div class="ui attached row" id="context'+e+'"><div class="ui top attached tabular menu"><div class="active item" data-tab="first">Resumen</div><div class="item" data-tab="second" >Clases</div><div class="item" data-tab="third">Objetivos de Aprendizaje</div></div>';
//                             htmltext +='<div class="ui attached grid active tab centered segment" data-tab="first"><div class="five wide column"><canvas id="'+datos[1]+'" width="300" heigth="300"></canvas></div>';
//                             htmltext +='<div class="five wide column"><canvas id="'+datos[6]+'" width="300" heigth="300"></canvas></div></div>';
//                             classArray = [datos[2], datos[3],datos[1]];
//                             oaArray = [datos[4],datos[5],datos[6]];

//                         }
//                     });

//                     htmltext += '<div class="ui attached grid tab centered segment" data-tab="second">'; // COMIENZO DE LOS DATOS DE CURSO
//                     $.ajax({
//                         url:ajax_url+'/v1/schedule/teachcourses',
//                         type: 'post',
//                         dataType: 'json',
//                         async: false,
//                         data: {teacher_id: teach_id, asig: v.asig_id, let: v.letter},
//                         xhrFields: {
//                           withCredentials: true
//                         },
//                         headers: {
//                           'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//                         },
//                         success: function(datos){
//                             var totalClases = datos.plancourses.length;
//                             var noClases = datos.noplancourses.length;
//                             htmltext += '<div class="ui five wide column"><h1>Clases Planificadas: '+totalClases+'</h1><div id="plan" class="ui relaxed divided list scrollable" style="height: 200px; overflow-y: auto;">';
//                             $.each(datos.plancourses, function(e,v){
//                                 var parseo = v.date.split("-");
//                                 var date = parseo[2] + "/" + parseo[1];
//                                 htmltext += '<div class="item" data-id="'+v.id+'" style="cursor: pointer;" ><div class="content"><p class="header">Clase del día '+date+'</p></div></div>';
//                             });
//                             htmltext += '</div></div>';
//                             htmltext += '<div class="ui five wide column"><h1>Clases sin Planificar: '+noClases+'</h1><div id="noplan" class="ui relaxed divided list scrollable" style="height: 200px; overflow-y: auto;">';
//                             $.each(datos.noplancourses, function(e,v){
//                                 var parseo = v.date.split("-");
//                                 var date = parseo[2] + "/" + parseo[1];
//                                 htmltext += '<div class="item" data-id="'+v.id+'" ><div class="content"><p class="header">Clase del día '+date+'</p></div></div>';
//                             });
//                             htmltext += '</div></div>';
//                             htmltext += '</div>'; // FIN DE LOS DATOS DE CURSO


//                         }
//                     });

//                     htmltext += '<div class="ui attached grid tab centered segment" data-tab="third">'; // COMIENZO DE LOS DATOS
//                     $.ajax({
//                         url:ajax_url+'/v1/schedule/teachoas',
//                         type: 'post',
//                         dataType: 'json',
//                         async: false,
//                         data: {teacher_id: teach_id, asig: v.asig_id, let: v.letter},
//                         xhrFields: {
//                           withCredentials: true
//                         },
//                         headers: {
//                           'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//                         },
//                         success: function(datos){
//                             var oas = datos.oaseen.length;
//                             var nooas = datos.oaunseen.length;
//                             htmltext += '<div class="ui five wide column"><h1>OA Planificados: '+oas+'</h1><div id="planoa" class="ui relaxed divided list scrollable" style="height: 200px; overflow-y: auto;">';
//                             $.each(datos.oaseen, function(e,v){
//                                 htmltext += '<div class="item" ><div class="content"><p class="header" style="text-align: left;">'+v.OA+': '+v.desc+'</p></div></div>';                           
//                             });
//                             htmltext += '</div></div>';
//                             htmltext += '<div class="ui five wide column"><h1>OA sin Planificar: '+nooas+'</h1><div id="plannooa" class="ui relaxed divided list scrollable" style="height: 200px; overflow-y: auto;">';
//                             $.each(datos.oaunseen, function(e,v){
//                                 htmltext += '<div class="item" ><div class="content"><p class="header" style="text-align: left;">'+v.OA+': '+v.desc+'</p></div></div>';                                
//                             });
//                             htmltext += '</div></div>';
//                             htmltext += '</div></div>'; // FIN DE LOS DATOS DE CURSO
//                             att_info.append(htmltext);
//                             $('#context'+e+' .tabular.menu .item').tab({
//                                 context: '#context'+e+''
//                             });
//                             renderGraph.apply(null,classArray);
//                             renderGraphoa.apply(null,oaArray);
                            
//                         }
//                     });
//                 });
//             }
//             $('#loader').modal('toggle');
//             }
//         });
//     }
// });

$(document).on('click','#plan .item',function(){
    var id = $(this).data('id');
    var content_modal = $('#detail-class');
    content_modal.empty();
    $('#plan .item').not(this).css('background-color','white');    
    $(this).css('background-color','#e6e6e6');
    $.ajax({
        url:ajax_url+'/v1/schedule/getclassinfo',
        type: 'post',
        dataType: 'json',
        data: {id: id},
        xhrFields: {
          withCredentials: true
        },
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function(data){
            var htmlModal = '<p><b>Asignatura: </b>'+data.asign+'</p>';
            htmlModal += '<p><b>Nivel: </b>'+data.course+'</p>';
            htmlModal += '<p><b>Fecha: </b>'+data.formated+'</p>';
            if(data.arrtag.length > 0){
              htmlModal += '<p><b>Palabras Clave</b></p><p>'
              $.each(data.arrtag, function(e,v){
                if(e < data.arrtag.length-1){
                  htmlModal += v.name+', ';
                }
                else{
                  htmlModal += v.name+'.';
                }
              });
              htmlModal += '</p>';
            }
            if(data.arrab.length > 0){
              htmlModal += '<p><b>Habilidades</b></p>'
              $.each(data.arrab, function(e,v){
                htmlModal += '<p>'+v.desc+'</p>'
              });
            }
            if(data.arrat.length > 0){
              htmlModal += '<p><b>Actitudes</b></p>'
              $.each(data.arrat, function(e,v){
                htmlModal += '<p>'+v.desc+'</p>'
              });
            }
            htmlModal += '<p><b>Objetivos de Aprendizaje</b></p>';
            if(data.arroas.length > 0){
                htmlModal += '<p><b>'+data.unit+'</b></p>';
                $.each(data.arroas, function(e,v){
                    htmlModal += '<p><b>'+v.name+': </b>'+v.desc+'</p>'
                });
            }
            else{
                htmlModal += '<p>Ninguno</p><br>';
            }
            if(data.scheduleData[0].description || data.scheduleData[0].init_text || data.scheduleData[0].mid_text || data.scheduleData[0].end_text || data.scheduleData[0].res_text || data.scheduleData[0].eval_text || data.scheduleData[0].ec_text){
                if(data.scheduleData[0].description.length){
                    htmlModal += '<p><b>Descripción</b></p><p>'+data.scheduleData[0].description.replace(/(\n)+/g, '<br />')+'</p>';       
                }
                if(data.scheduleData[0].init_text){
                    htmlModal += '<p><b>Inicio</b></p><p>'+data.scheduleData[0].init_text.replace(/(\n)+/g, '<br />')+'</p>';       
                }
                if(data.scheduleData[0].mid_text){
                    htmlModal += '<p><b>Desarrollo</b></p><p>'+data.scheduleData[0].mid_text.replace(/(\n)+/g, '<br />')+'</p>';       
                }
                if(data.scheduleData[0].end_end){
                    htmlModal += '<p><b>Cierre</b></p><p>'+data.scheduleData[0].end_text.replace(/(\n)+/g, '<br />')+'</p>';       
                }
                if(data.scheduleData[0].res_text){
                    htmlModal += '<p><b>Recursos de Aprendizaje</b></p><p>'+data.scheduleData[0].res_text.replace(/(\n)+/g, '<br />')+'</p>';       
                }
                if(data.scheduleData[0].eval_text){
                    htmlModal += '<p><b>Evaluación</b></p><p>'+data.scheduleData[0].eval_text.replace(/(\n)+/g, '<br />')+'</p>';       
                }
                if(data.scheduleData[0].ec_text){
                    htmlModal += '<p><b>Adecuación Curricular</b></p><p>'+data.scheduleData[0].ec_text.replace(/(\n)+/g, '<br />')+'</p>';       
                }
            }
            else{

                htmlModal += '<p><b>Actividades de Aprendizaje</b></p><p>Ninguno</p><br>';
            }
            if(data.file){
                htmlModal +='<p><b>Archivo Adjunto</b></p><a href="'+ajax_url+'/uploads/'+data.file+'" target="_blank">'+data.file+'</a>'
            }            

            content_modal.append(htmlModal);
            $('.class-detail').modal('show');



        }
    });
});

var renderGraph = function (total, cursed, id) {

        var canvas = $('#'+id)[0].getContext("2d");

        canvas.width = parent.offsetWidth;
        canvas.height = parent.offsetHeight;

        var config={
                    type: 'pie', 
                    data:{
                        labels: ["Clases Planificadas", "Clases sin Planificar"],
                        datasets: [
                            {
                                data: [cursed, total-cursed],
                                backgroundColor:["#36A2EB","#FF6384"],
                                hoverBackgroundColor:["#36A2EB","#FF6384"],
                                options: { responsive: true }

                            }
                        ]
                    },
                    options: pieOptions
                };
        window.myChart = new Chart(canvas, config);
    }    
var renderGraphoa = function (total, cursed, id) {

        var canvas = $('#'+id)[0].getContext("2d");

        canvas.width = parent.offsetWidth;
        canvas.height = parent.offsetHeight;

        var config={
                    type: 'pie', 
                    data:{
                        labels: ["OA Planificados", "OA sin Planificar"],
                        datasets: [
                            {
                                data: [cursed, total-cursed],
                                backgroundColor:["#36A2EB","#FF6384"],
                                hoverBackgroundColor:["#36A2EB", "#FF6384"],
                                options: { responsive: true }

                            }
                        ]
                    },
                    options: pieOptions
                };
        window.myChart = new Chart(canvas, config);
    }



var pieOptions = {
  events: false,
  animation: {
    duration: 500,
    easing: "easeOutQuart",
    onComplete: function () {
      var ctx = this.chart.ctx;
      ctx.font = "25px Arial";;
      ctx.textAlign = 'center';
      ctx.textBaseline = 'bottom';

      this.data.datasets.forEach(function (dataset) {

        for (var i = 0; i < dataset.data.length; i++) {
          var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model,
              total = dataset._meta[Object.keys(dataset._meta)[0]].total,
              mid_radius = model.innerRadius + (model.outerRadius - model.innerRadius)/2,
              start_angle = model.startAngle,
              end_angle = model.endAngle,
              mid_angle = start_angle + (end_angle - start_angle)/2;

          var x = mid_radius * Math.cos(mid_angle);
          var y = mid_radius * Math.sin(mid_angle);

          ctx.fillStyle = '#fff';
          if (i == 3){ // Darker text color for lighter background
            ctx.fillStyle = '#444';
          }
          var percent = String(Math.round(dataset.data[i]/total*100)) + "%";
          // Display percent in another line, line break doesn't work for fillText
          ctx.fillText(percent, model.x + x, model.y + y + 15);
        }
      });               
    }
  }
};

$('#close-session').on('click', function(){
      Cookies.remove('loggedAsAdm');
      window.location = '/admin-login.html';
});