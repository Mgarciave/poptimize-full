function goToByScroll(id){
          // Reove "link" from the ID
        id = id.replace("link", "");
          // Scroll
        $('html,body').animate({
            scrollTop: $("#"+id).offset().top},
            'slow');
    }

 $(".right.menu > .item > a").click(function(e) { 
          // Prevent a page reload when a link is pressed
        e.preventDefault(); 
          // Call the scroll function
        goToByScroll($(this).attr("id"));           
    });

 $("div > div > div > p.foot-text").click(function(e) { 
          // Prevent a page reload when a link is pressed
        e.preventDefault(); 
          // Call the scroll function
        goToByScroll($(this).attr("id"));           
    });

 $(function(){
    $('#header-nav').data('size','big');
});

$(window).scroll(function(){
    if($(document).scrollTop() > 0)
    {
        if($('#header-nav').data('size') == 'big')
        {
            $('#header-nav').data('size','small');
            $('#header-nav').css('background-color','#66cccc');
            $('#header-nav').stop().animate({
                height:'40px'
            },400);
        }
    }
    else
    {
        if($('#header-nav').data('size') == 'small')
        {
            $('#header-nav').data('size','big');
            $('#header-nav').css('background-color','transparent');
            $('#header-nav').stop().animate({
                height:'60px'
            },400);
        }  
    }
});

$('img#headlink').click(function(e){
    e.preventDefault(); 
    goToByScroll($(this).attr("id")); 
});

$('.back .item').hover(function(){$(this).toggleClass('item-hover');});

$('#payment').on('click', function(){
    $('.blue-ar-l-rn-none').trigger('click');
});

$('#redi-log').on('click', function(){
    window.location=ajax_url+'/login.html';
});

$('.contacto').on('click', function(){
    $('.event-modal').modal('show');
});

$('.send-contact').on('click', function(){
    var name = $('#nombre').val();
    var mail = $('#correo').val();
    var mensaje = $('#message').val();

    if( name && mail && mensaje ){
        $.ajax({
            url:ajax_url+'/v1/contact',
            type: 'post',
            dataType: 'text',
            data: {name: name, email: mail, mensaje: mensaje},
            xhrFields: {
              withCredentials: true
            },
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(data){
                if(data=='yes'){
                  swal({
                      title: "Correo enviado",
                      text: "Hemos recibido su correo de manera exitosa.",
                      type: "success",
                      confirmButtonText: "Cerrar"
                  });
                  $('.event-modal').modal('hide');

                }
                else{
                    swal({
                      title: "Error",
                      text: "Su correo se encuentra erroneo. Pruebe con una dirección de correo válida",
                      type: "error",
                      confirmButtonText: "Cerrar"
                  });
                }
            }
        });
    }
});