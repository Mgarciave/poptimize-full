function renderAnnotations(view,annotations) {
  var container = $(view.element).find('.fc-agenda-slots').parent();
  if ( container.find('#annotationSegmentContainer').length === 0 ) {
    annotationSegmentContainer = $("<div style='position:absolute;z-index:-1;top:0;left:0' id='annotationSegmentContainer'>").prependTo( container );
  }
  else {
    annotationSegmentContainer = container.find('#annotationSegmentContainer');
  }

  var html = '';
  for (var i=0; i < annotations.length; i++) {
    var ann = annotations[i];
    if (ann.start >= view.start && ann.end <= view.end) {
      var top = view.timePosition(ann.start, ann.start);
      var bottom = view.timePosition(ann.end, ann.end);
      var height = bottom - top;
      var dayIndex = $.fullCalendar.dayDiff(ann.start, view.visStart);

      var left = view.colContentLeft(dayIndex) - 2;
      var right = view.colContentRight(dayIndex) + 3;
      var width = right - left;

      var cls = '';
      if (ann.cls) {
        cls = ' ' + ann.cls;
      }

      var colors = '';
      if (ann.color) {
        colors = 'color:' + ann.color + ';';
      }
      if (ann.background) {
        colors += 'background:' + ann.background + ';';
      }

      var body = ann.title || '';

      html += '<div style="position: absolute; ' +
        'top: ' + top + 'px; ' +
        'left: ' + left + 'px; ' +
        'width: ' + width + 'px; ' +
        'height: ' + height + 'px;' + colors + '" ' +
        'class="fc-annotation fc-annotation-skin' + cls + '">' +
        body +
        '</div>';
    }
  }
  annotationSegmentContainer.html(html);
}

/*var monthArray = ['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'];

function getMondays(date) {
    var d = date || new Date(),
        month = d.getMonth(),
        mondays = [];

    for (var i = 1; i < 1; i++) {
      d.setDate(i);

      // Get the first Monday in the month
      while (d.getDay() !== i) {
          d.setDate(d.getDate() + i);
      }

      // Get all the other Mondays in the month
      while (d.getMonth() === month) {
          mondays.push(new Date(d.getTime()));
          d.setDate(d.getDate() + 7);
      }
    }
    return mondays;
}

$.each(monthArray, function(i,v){
  $.each(getMondays(moment().month(v).toDate()), function(i,v){
    console.log(v);
  });
});*/

$(document).on('click','.cancel-button, .close.icon', function(){
  $('.modal').modal('hide');
});

$('.close-button').click(function(){
  $('.modal').modal('hide');
});

$(document).on('click','.event-save',function(){
  var myCalendar = $('#fullcalendar-events');
  myCalendar.fullCalendar();

  var myEvent = {
    title:$('#event-name').val(),
    allDay: false,
    start: localStorage.getItem('start_event'),
    end:localStorage.getItem('start_event'),
    className: 'reminder',
  };
  myCalendar.fullCalendar( 'renderEvent', myEvent );
  /*myCalendar.fullCalendar({
    eventRender: function (event, element)
    {
      console.log(element);
      element.prepend( "<i class='alarm outline icon'></i>" );
    }
  });*/
  $date = myEvent.start.split('T');
  $date = $date[0]+' '+$date[1];
  $date2 = myEvent.end.split('T');
  $date2 = $date2[0]+' '+$date2[1];

  $.ajax({
    url: ajax_url+'/v1/notifications/add',
    dataType: 'json',
    type: 'post',
    data: {titulo: $('#event-name').val(), comentario: $('#alert-text1').val(), fecha: $date, fecha_end: $date2, course: localStorage.getItem('level'), letter: localStorage.getItem('letra'), subject: localStorage.getItem('subject_id')},
    xhrFields: {
      withCredentials: true
    },
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    success: function(data){
      console.log(data);
    }
  });
  /*$.ajax({
    url:ajax_url+'/v1/notifications/add/',
    type: 'post',
    dataType: 'json',
    xhrFields: {
      withCredentials: true
    },
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    data: { titulo: $('#event-name').val(), comentario: $('#alert-text1').val(), fecha: $date, end_date: $date2 },
    success: function(data){
      console.log(data);
    }
  });*/
  $('.event-modal').modal('hide');
});

$(document).on('click', '.logout', function(){
  Cookies.remove('loggedAs');
  window.location = '/login'+extension;
});

$(document).on('click', '.tutorial', function(){
  $('#tutorial').modal('show');
});

$(document).on('click','.save-class-events', function(e){ 
  e.preventDefault();
  // console.log($('.datepicker').val());
  // console.log(moment($('.classtime').timepicker('getTime')).format('HH:mm:ss'));
  // console.log(moment($('.classdate').datepicker('getDate')).format('YYYY-MM-DD'));
  // console.log(moment($('.classdate').datepicker('getDate')).format('YYYY-MM-DD')+' '+moment($('.classtime').timepicker('getTime')).format('HH:mm:ss'));

  //alert($('.datepicker').datepicker( 'getDate' ));
  //alert($(this).closest('.class-modal').attr('data-id'));
  console.log($(".le_first input:checked"));
   //return;
  $_this = $(this);
  if( (($('.classtime').val() == '') && ($('.classdate').val() != '')) || (($('.classtime').val() != '') && ($('.classdate').val() == '') )){
    swal({
            title: 'Error',
            text: 'Por favor ingrese fecha y hora a su recordatorio',
            type: 'error',
            confirmButtonText: "Continuar",
        });
  }
  else{
  $.ajax({
    url:ajax_url+'/v1/schedule/update',
    type: 'post',
    dataType: 'json',
    xhrFields: {
      withCredentials: true
    },
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    data: {
      id: $(this).closest('.class-modal').attr('data-id'),
      description: $('#alert-text2').val(),
      init_text:$('#init-text').val(),
      mid_text: $('#mid-text').val(),
      end_text: $('#end-text').val(),
      res_text: $('#res-text').val(),
      eval_text: $('#eval-text').val(),
      ec_text: $('#ec-text').val(),
      type: $('input[name=class-type]:checked').val(),
      reminder: moment(moment($('.classdate').datepicker('getDate')).format('YYYY-MM-DD')+' '+moment($('.classtime').timepicker('getTime')).format('HH:mm:ss')).format('YYYY-M-DD HH:mm:ss') == 'Invalid date' ? '' : moment(moment($('.classdate').datepicker('getDate')).format('YYYY-MM-DD')+' '+moment($('.classtime').timepicker('getTime')).format('HH:mm:ss')).format('YYYY-M-DD HH:mm:ss')
      //reminder: localStorage.getItem('reminderDate')
    },
    success: function(data){
      // $(".fc-time-grid-event[data-id='" + $_this.closest('.class-modal').attr('data-id') + "']").attr('data-not',moment(localStorage.getItem('reminderDate')+' '+moment($('.timepicker2').timepicker('getTime')).format('HH:mm:ss')).format('YYYY-M-DD HH:mm:ss')).attr('data-desc',$('#alert-text2').val()).attr('data-inittext',$('#init-text').val()).attr('data-midtext',$('#mid-text').val()).attr('data-endtext',$('#end-text').val()).attr('data-restext',$('#res-text').val()).attr('data-evaltext',$('#eval-text').val()).attr('data-ectext',$('#ec-text').val());
      console.log($_this.closest('.class-modal').attr('data-id'));  
      //return;
      if($(".le_first input:checked").length===0){

        //$(".fc-time-grid-event[data-id='" + $_this.closest('.class-modal').attr('data-id') + "']").remove();
        //console.log($(".fc-time-grid-event[data-id='" + $_this.closest('.class-modal')));
        $(".fc-time-grid-event[data-id='" + $_this.closest('.class-modal').attr('data-id') + "']").find('.in-event').remove();
      }
      $('#fullcalendar-events').fullCalendar( 'refetchEvents' );
      $('.class-modal').modal('hide');


      //$_this.attr('data-not',localStorage.getItem('
    }
  });
  }
  

  ////alert(1);
});

function moveEvents (color) {
   $('.added-item').each(function() {
     //alert(2);
       var eventObject = {
           title: $.trim(localStorage.getItem('subject_name')),
           backgroundColor: localStorage.getItem('hex')
       };
       //console.log(eventObject);
       $(this).data('eventObject', eventObject);

       $(this).draggable({
           zIndex: 999,
           revert: true,
           revertDuration: 0
       });

   });
}

$(document).on('click','.le_print',function(){
  alert($('#schedule').fullCalendar( 'getDate' ).format('M'));
});

function countdown( elementName, minutes, seconds )
{
    var element, endTime, hours, mins, msLeft, time;

    function twoDigits( n )
    {
        return (n <= 9 ? "0" + n : n);
    }

    function updateTimer()
    {
        msLeft = (endTime - (+ new Date));
        if ( msLeft < 1000 ) {
            window.location = 'course-calendar'+extension;
        } else {
            time = new Date( msLeft );
            hours = time.getUTCHours();
            mins = time.getUTCMinutes();
            element.innerHTML = (hours ? hours + ':' + twoDigits( mins ) : mins) + ':' + twoDigits( time.getUTCSeconds() );
            setTimeout( updateTimer, time.getUTCMilliseconds() + 500 );
        }
    }

    element = document.getElementById( elementName );
    endTime = ((+new Date) + 1000 * (60*minutes + seconds) + 500);
    updateTimer();
}

var extension = is_local ? '.html' : '';

$(document).ready(function(){
  $(document).on('click','.icon.item',function(){
    $('.ui.sidebar').sidebar('toggle');
  });

  $(document).on('click','.alerts',function(){
    //$(this).find('.menu').toggle();
  });
});

$(function(){

  $('.with-popover').popup();

  /*$('.colorpicker').click(function(){
    $this = $(this);
    $this.popup({
      on: 'hover',
      onCreate: function(){
        $original_class = $this.parent();
        $(document).on('click', '.content div.color', function(){
          $new_class = 'ui button added-item' + ' ' + $(this).attr('class');
          $this.parent().attr('class',$new_class);
        });
      }
    });
  });*/

  $('.colorpicker').popup({
    inline     : true,
    hoverable  : true,
    position   : 'bottom left',
    delay: {
      show: 300,
      hide: 800
    }
  });

  $(document).on('click', '#beta-menu', function(e) {
    /*e.preventDefault();
    swal({
      title: "Función aun en desarrollo",
      text: "La plataforma aun se encuentra en fase Alpha, por lo que este menú aun se encuentra deshabilitado.",
      type: "error",
      confirmButtonText: "Continuar"
    });*/
  });

  $(document).on('click', '.add-course', function() {

      var $cursos = 0;
      $.ajax({
      url:ajax_url+'/v1/courses/get',
      dataType: 'json',
      type: 'post',
      success: function(data){
        if(data > 0){
          $('.add-course-modal').modal('show');
        } 
        else{
          $('#more-courses').modal('show');
        }
      }
    });

      

    
    //countdown( "countdown", 0, 10 );
  });

  $(document).on('click', '.unit-block', function() {
    $('.tabular.menu').find('.item').tab('change tab', 'first');
    $('.event-detail h2').text('Unidad '+$(this).find('span').text());
    $('.event-detail .pointing').attr('href',ajax_url+'/v1/report/unit/print/'+$(this).attr('data-unit-id'));
    $.ajax({
      url:ajax_url+'/v1/units/show/'+$(this).attr('data-unit-id'),
      type: 'post',
      dataType: 'json',
      xhrFields: {
        withCredentials: true
      },
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      success: function(data){
        console.log(data);
        $html_oa = '';
        $html_tags = '';
        $pknows = '';
        $html_habilities = '';
        $knows = '';
        $attitudes = '';
        if(data.pknow.length > 0){
            $('#second').show();
            $.each(data.pknow, function(e,v){
            console.log(v);
            $pknows += '<div class="inline field"><div class="ui checkbox"><input type="checkbox" tabindex="0" class="hidden">'+v.description+'</div></div>';
          });
        }
        else{
          $('#second').hide();
        }
        if(data.attitudes.length > 0){
            $('#fifth').show();
            $.each(data.attitudes, function(e,v){
            console.log(v);
            $attitudes += '<div class="inline field"><div class="ui checkbox"><input type="checkbox" tabindex="0" class="hidden">'+v.description+'</div></div>';
          });
        }
        else{
            $('#fifth').hide();
        }
        if(data.know.length > 0){
            $('#forth').show();
            $.each(data.know, function(e,v){
            console.log(v);
            $knows += '<div class="inline field"><div class="ui checkbox"><input type="checkbox" tabindex="0" class="hidden">'+v.description+'</div></div>';
          });
        }
        else{
           $('#forth').hide();
        }
        if(data.oas.length > 0){
            $('#first').show();
            $.each(data.oas, function(e,v){
            //console.log(v);
            $html_oa += '<div class="inline field"><div class="ui checkbox">OA '+v.num_obj+' / '+v.name_eje+'<br>'+v.description_min+'</div></div>';
          });
        }
        else{
          $('#first').hide();
        }
        $html_tags += '<ul id="double">';
        if(data.tags.length > 0){
            $('#third').show();
            $.each(data.tags, function(e,v){
            //console.log(v);
            $html_tags += '<li><div class="ui checkbox">'+v.name+' <br></div></li>';
          });
        }
        else{
            $('#third').hide();
        }
        $html_tags += '</ul>';
        if(data.abilities.length > 0){
            $('#sixth').show();
            $.each(data.abilities, function(e,v){
            //console.log(v);
            $html_habilities += '<div class="inline field"><div class="ui checkbox">'+v.name_ability+'<br>'+v.description+'</div></div>';
          });
        }
        else{
          $('#sixth').hide();
        }
        
        $('.ooaas').html($html_oa);
        $('.tags').html($html_tags);
        $('.pknows').html($pknows);
        $('.knows').html($knows);
        $('.habilities').html($html_habilities);
        $('.attitudes').html($attitudes);
        $('.event-detail').modal('show');
        $('.sixteen.wide.column').scrollTop(0);
      }
    });
    //countdown( "countdown", 0, 10 );

  });

  $('.printttt').click(function(){
    window.open(ajax_url+'/v1/report/units/print/'+localStorage.getItem('subject_id')+'/?level='+localStorage.getItem('level')+'&letter='+localStorage.getItem('letra'),'_blank');
  })

  $(document).on('click', '.ok-button', function() {
    console.log($('.typehead').val());
    console.log($('#level-dropdown .selected').text().trim() + ' ' + $('#letters-dropdown .selected').text().trim());
    if(($('#commune-dropdown .selected').attr('data-value') == '') || ($('#region-dropdown .selected').attr('data-value') == '') || ($('.typehead').val() == '')){
      swal({
                title: "Error :(",
                text: "Debe completar todos los datos solicitiados",
                type: "error",
                confirmButtonText: "Continuar"
              });
    }
    else{
      $.ajax({
       url:ajax_url+'/v1/courses/course_id',
       type : 'post',
       data : {name: $('#level-dropdown .selected').text().trim(), letter: $('#letters-dropdown .selected').text().trim()},
       dataType: 'json',
       success : function(data) {
          var level = data[0].id;
          localStorage.setItem('subject_name',$('#subjects-dropdown .selected').data('name'));
          localStorage.setItem('school_id',$('.typehead').attr('data-id'));
          localStorage.setItem('level', level);
          localStorage.setItem('school', $('.typehead').val());

          $.ajax({
            url:ajax_url+'/v1/courses/add',
            data: {
              school_id: $('.typehead').attr('data-id'),
              course: $('#level-name').val(),
              colegio: $('input.typehead').attr('data-id'),
              colegio_name: $('.typehead').val(),
              asignatura: $('.subject #subjects-dropdown .active').text().trim(),
              letter: $('#letter-name').val(),
              id: localStorage.getItem('id'),
              region: $('#region-dropdown .selected').attr('data-value'),
              commune: $('#commune-dropdown .selected').attr('data-value')
              },
            dataType: 'json',
            type: 'post',
            beforeSend: function(){
              $('.ok-button').html('<i class="fa fa-spinner fa-spin fa-3x fa-fw" style="font-size:14px;"></i>');
            },
            success: function(data){
              if(data.code != 500){
                countdown( "countdown", 0, 10 );
                $('.course-added').modal('show');
                localStorage.setItem('subject_id',data.aid);
                localStorage.setItem('letra',$('#letter-name').val());
              }else{
                $('.ok-button').html('Guardar');
                swal({
                  title: "Error :(",
                  text: data[0],
                  type: "error",
                  confirmButtonText: "Continuar"
                });
              }

            },
            error: function(data){
              swal({
                title: "Error :(",
                text: "El curso y la asignatura seleccionada no coinciden",
                type: "error",
                confirmButtonText: "Continuar"
              });
              $('.ok-button').html('Guardar');
            }
          });
       }
    });
    }
  });

  $(document).on('click', '.item.class-color', function() {
    ////alert(2);
    localStorage.setItem('hex',$(this).data('color'));
    moveEvents(localStorage.getItem('foreverColor'));
  });

  $(document).on('click', '.class-color', function() {
    $this_ = $(this);
    $('.popup').popup('hide');
    //alert(1);
    $('.popup').removeClass('visible').fadeOut();
    $(this).addClass('border-shadow');
    localStorage.setItem('hex',$this_.data('color'));
    $('.added-item').css('background-color', $this_.data('color')).attr('data-hex',$this_.data('color'));
    $('.added-item.ui.button:hover').css('background-color', $this_.data('color')).attr('data-hex',$this_.data('color'));
    ////alert(1);
    //localStorage.setItem('foreverColor',$(this).data('color'));
    $('.fc-time-grid-event').each(function(){
      console.log($(this).hasClass('no-change'));
      if(!$(this).hasClass('no-change')){
        //$(this).css('background-color', localStorage.getItem('hex'));
      }
      if(!$(this).hasClass('locked')){
        $(this).css('background-color', localStorage.getItem('hex'));
      }

    })
    //$('.fc-time-grid-event').css('background-color', $(this).data('color'));
    ////alert($(this).data('color')).attr('data-hex',$(this).data('color'));
  });

  moveEvents($('.added-item').data('hex'));

  /* HELPERS */

  $('.menu .item').tab();
  // Set selects as REAL dropdowns
  $('select.dropdown, .ui.dropdown').dropdown({
    message: {
      addResult     : 'Add <b>{term}</b>',
      count         : '{count} selected',
      maxSelections : 'Max {maxCount} selections',
      noResults     : 'Sin resultados.'
      }
  });
  // Enable the sidebar pusher for content
  /*$(document).on('click', '.ui.dropdown.icon.item', function() {
    $('.ui.sidebar').sidebar('toggle');
  });*/

  // Popups
  $('.school-course').popup();

  /*$('#fullcalendar').fullCalendar({
    defaultView: 'agendaWeek',
    firstDay: 1,
    allDaySlot: false,
    minTime: '08:00:00',
    maxTime: '19:00:00'
  });*/

  $('.add-class').on('click',function(){
    $('.added-blocks').append('<div class="ui orange button added-item" data-current-color="orange" data-hex="#39cd6b">Asignatura</div><br><br>');
    moveEvents($(this).attr('data-hex'));
  });

  /*$('.added-item').each(function() {
    //alert($(this).attr('data-hex'));
      var eventObject = {
          title: $.trim($(this).text()),
          backgroundColor: $(this).attr('data-hex'),
      }
      //console.log($(this).attr('data-hex'));
      $(this).data('eventObject', eventObject);

      $(this).draggable({
          zIndex: 999,
          revert: true,
          revertDuration: 0
      });

  });*/

  /*$(document).on('click', '.added-item', function() {
    //console.log($(this).attr('data-hex'));
  });*/

  $(document).on('click', '#init-form-toggle', function(e) {
    var leftHeight = $('.main-content.login .row > .column:first-child').height();
    var rightHeight = $('.main-content.login .row > .column:nth-child(2)').height();
    /*if(leftHeight > rightHeight) {
      $('.main-content.login .row > .column:nth-child(2)').height(leftHeight);
    }else{
      $('.main-content.login .row > .column:first-child').height(rightHeight);
    }*/
    var heading = $('#login-register-heading');
    var subHeading = $('#login-register-subheading');
    var oldHeading = heading.text();
    var oldSubHeading = subHeading.text();
    heading.text('Registro de usuario');
    subHeading.html('Si ya tienes un usuario registrado, haz click <a href="javascript:;" id="init-form-toggle" style="color:#38c2c8;">aquí</a>.');
    $('#register-form').toggleClass('hide');
    $('#login-form').toggleClass('hide');
  });

  $(document).on('click', '#reset-password', function(){
    var leftHeight = $('.main-content.login .row > .column:first-child').height();
    var rightHeight = $('.main-content.login .row > .column:nth-child(2)').height();
    /*if(leftHeight > rightHeight) {
      $('.main-content.login .row > .column:nth-child(2)').height(leftHeight);
    }else{
      $('.main-content.login .row > .column:first-child').height(rightHeight);
    }*/
    var heading = $('#login-register-heading');
    var subHeading = $('#login-register-subheading');
    var oldHeading = heading.text();
    var oldSubHeading = subHeading.text();
    heading.text('Recupera tu contraseña');
    subHeading.html('Ingresa tu correo electrónico.');
    $('#reset-form').toggleClass('hide');
    $('#login-form').toggleClass('hide');
  });

  

  /*$(document).on('click', '#login', function(e){
    var email = $('#email').val();
    var password = $('#password').val();
    var msg = '';
    if(email == '' && password == '') {
      msg = 'Debes ingresar tus credenciales para acceder.';
    } else if(email == '') {
      msg = 'Debes ingresar el email registrado para acceder.';
    } else if(password == '') {
      msg = 'Debes ingresar tu contraseña para acceder.';
    } else if(email != 'demo@demo.com') {
      msg = 'El correo ingresado no se encuentra en nuestros registros.';
    } else if(password != '123456') {
      msg = 'La contraseña ingresada no corresponde.';
    }else if(email == 'demo@demo.com' && password == '123456') {
      window.location = 'dashboard.html';
      status = true;
    } else{
      msg = 'Ha ocurrido un error. Por favor vuelve a intentarlo.'
    }

    e.preventDefault();
    if(!status) {
      swal({
        title: "Oh no! :(",
        text: msg,
        type: "error",
        confirmButtonText: "Continuar"
      });
    }
  });*/


  /*$('#fullcalendar-detail').fullCalendar({
      eventAfterAllRender: function() {
          $("#fullcalendar-detail").find('.fc-toolbar > div > h2').empty().append("<div>Curso / Asignatura</div>");
      },
      header: {
          left: 'prev,next today',
          center: 'title',
          right: 'myCustomButton month,basicWeek,basicDay',
      },
      titleFormat: '\'Hello, World!\'',
      editable: true,
      defaultView: 'agendaWeek',
      firstDay: 1,
      allDaySlot: false,
      minTime: '08:00:00',
      maxTime: '19:00:00',
      columnFormat: 'dddd D',
      titleFormat: 'MMMM YYYY',
      eventClick: function(calEvent, jsEvent, view) {
        $('.event-detail').modal('show');
        ////alert('Event: ' + calEvent.title);
        ////alert('Coordinates: ' + jsEvent.pageX + ',' + jsEvent.pageY);
        ////alert('View: ' + view.name);
      },
      lang: 'es',
      droppable: true,

      slotDuration: '00:30:00',
      slotLabelFormat: 'h(:mm)a',
      drop: function(date) {

          var originalEventObject = $(this).data('eventObject');
          var copiedEventObject = $.extend({}, originalEventObject);

          copiedEventObject.start = date;
          copiedEventObject.title = $(this).data('title');
          copiedEventObject.backgroundColor = $(this).data('hex');

          //console.log(copiedEventObject);

          $('#fullcalendar-detail').fullCalendar('renderEvent', copiedEventObject, true);
          //$(this).remove();

      }
  });*/

  /*$(document).on('click', '#register', function(e){
    var email = $('#email').val();
    var password = $('#password').val();
    var msg = '';
    var status = false;
    if(email == '' && password == '') {
      msg = 'Debes ingresar tus credenciales para acceder.';
    } else if(email == '') {
      msg = 'Debes ingresar el email registrado para acceder.';
    } else if(password == '') {
      msg = 'Debes ingresar tu contraseña para acceder.';
    } else if(email != 'felipe@fgacv.net') {
      msg = 'El correo ingresado no se encuentra en nuestros registros.';
    } else if(password != '123456') {
      msg = 'La contraseña ingresada no corresponde.';
    }else if(email == 'felipe@fgacv.net' && password == '123456') {
      window.location = 'http://emol.com';
      status = true;
    } else{
      msg = 'Ha ocurrido un error. Por favor vuelve a intentarlo.'
    }
    //alert(status);
    e.preventDefault();
    if(status) {
      swal({
        title: "Oh no! :(",
        text: msg,
        type: "error",
        confirmButtonText: "Continuar"
      });
    }
  });*/

});

function isEven(n) {
  return n == parseFloat(n) ? !(n%2) : void 0;
}

$body = $('body').attr('id');
if($body == 'inicio'){
  console.log($body);
}
if($body!='login-page'){

  $.ajax({
    url:ajax_url+'/v1/users/auth/get',
    type: 'post',
    dataType: 'json',
    xhrFields: {
      withCredentials: true
    },
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    success: function(data){
      /*if(data[1].length===0){
        $('.floating.labeled.ui').show();
        $('.scrolling.menu').html('<div class="item"><div class="ui red empty circular label"></div><a href="javascript:;">Debes completar tu perfil</a></div>');
      }else{
        localStorage.setItem('id',data[1][0].user_id);
      }*/
      //localStorage.setItem('id',data[1][0].user_id);
    }
  });

  fm_options = {
      position: "right-bottom",
      name_required: true,
      name_label : "Nombre",
      email_label : "Email",
      message_label : "Mensaje",
      email_required : true,
      show_email : true,
      submit_label : "Enviar",
      trigger_label : "Ayúdanos",
      title_label : "",
      message_placeholder: "Tienes algo que decirnos? Algún problema? Ayúdanos a mejorar :)",
      message_required: true,
      show_asterisk_for_required: true,
      feedback_url: ajax_url+'/v1/feedback',
      custom_params: {
          csrf: "my_secret_token",
          user_id: "john_doe",
          feedback_type: "clean"
      },
      delayed_options: {
          send_fail : "No pudimos enviar el mensaje :(",
          send_success : "Muchas gracias! Nos pondremos en contacto a la brevedad."
      }
  };
  //init feedback_me plugin
  fm.init(fm_options);

  $( ".feedback_name" ).wrap( "<div class='ui input'></div>" );
  $( ".feedback_email" ).wrap( "<div class='ui input'></div>" );
  $('.feedback_message').wrap('<div class="field"></div>');

  if(!Cookies.get('loggedAs')){
    window.location = '/login'+extension;
  }
}

if($body=='dashboard'){
  $.ajax({
    url:ajax_url+'/v1/users/auth/get',
    type: 'post',
    dataType: 'json',
    xhrFields: {
      withCredentials: true
    },
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    success: function(data){
      //localStorage.setItem('id',data[1][0].user_id);
    }
  });
}


if($body=='course-calendar'){

  var needtoconfirm = false;


  $('#go-to-units').on('click', function(e){
    //localStorage.removeItem('start');

    //e.preventDefault();
    events = $('#fullcalendar').fullCalendar('clientEvents');
    var start = $('#fullcalendar').fullCalendar('getView').start.format();
    var end = $('#fullcalendar').fullCalendar('getView').end.format();
    console.log(events);
    console.log(start);
    console.log(end);
    var new_e = [];

    //console.log(start);
    $.each(events, function(i,v){

      console.log(v);
      //moment.locale('en');
      //console.log(v);
      console.log(v.start.format());
      var date = v.start.format()
      var now = start;

      if (date > start && date < end) {
         //console.log('califica: '+date);
         if(v.className[0]!='locked'){
           e = {
             day_name: v.start.format('dddd'),
             class: v.className[0],
             start_time: v.start.format('HH:mm:ss'),
             start: v.start.format('Y-MM-DD HH:mm:ss'),
             end: v.end.format('Y-MM-DD HH:mm:ss'),
             subject_id: parseInt(localStorage.getItem('subject_id').replace(/[^0-9]/g,'')),
             color: localStorage.getItem('hex'),
             school_id: 2020,
            level: localStorage.getItem('level'),
            school_name: localStorage.getItem('school'),
          letter: localStorage.getItem('letra')};
           new_e.push(e);
         }
      } else {
         // date is future
      }
      //
      //
    });

    console.log(new_e.length);

    if(new_e.length > 0){
      $.ajax({
        url:ajax_url+'/v1/schedule/clean',
        type: 'post',
        dataType: 'json',
        xhrFields: {
          withCredentials: true
        },
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function(data){
          $.ajax({
            url:ajax_url+'/v1/schedule/add',
            type: 'post',
            dataType: 'json',
            xhrFields: {
              withCredentials: true
            },
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {new_e},
            success: function(data){
              $.each(data, function(e,v){
                console.log(moment(v).format('DD/MM/Y'));
              });
              needtoconfirm = true;
              window.location = '/units'+extension;
            }
          });
        }
      });


    }
    else{
      swal({
        title: "Ups! :(",
        text: 'Debes seleccionar por lo menos un horario',
        type: "error",
        confirmButtonText: "Continuar"
      });

    }

    //e.preventDefault();
    
    
  });

  //localStorage.removeItem('foreverColor');
  //localStorage.removeItem('hex');
  /*$('.start-time').click(function(){
    localStorage.setItem('start',$('.start_value').val());
    location.reload();
  });

  $('h3#subject-name').text(localStorage.getItem('subject_name'));*/

  window.onbeforeunload = function(){
    console.log(needtoconfirm);
    if(!needtoconfirm){
        return 'Debes seleccionar un horario para programar tus cursos';
    }
  };

}

if($body=='profile'){

  $.datepicker.regional['es'] = {
     closeText: 'Cerrar',
     prevText: '<Ant',
     nextText: 'Sig>',
     currentText: 'Hoy',
     monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
     monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
     dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
     dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
     dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
     weekHeader: 'Sm',
     dateFormat: 'dd/mm/yy',
     firstDay: 1,
     isRTL: false,
     showMonthAfterYear: false,
     yearSuffix: ''
     };
     $.datepicker.setDefaults($.datepicker.regional['es']);
    $(function () {
    $("#fecha").datepicker();
    });

  $(".datepicker").datepicker({
    dateFormat: 'dd/mm/yy',
    showOn: "button",
    buttonImage: "images/calendar.png",
    buttonImageOnly: true,
    onClose: function(dateText, inst) {
        $(this).parent().find("[contenteditable=true]").focus().html(dateText).blur();
    }
});

  $('.update-photo').click(function(){
    $('#image').trigger('click');
  });

  $('#image').change(function(){
    var fd = new FormData(document.getElementById("file-form"));
    fd.append("label", "WEBUPLOAD");
    $.ajax({
       url:ajax_url+'/v1/users/auth/avatar_upload',
       type : 'post',
       data : fd,
       processData: false,  // tell jQuery not to process the data
       contentType: false,  // tell jQuery not to set contentType
       success : function(data) {
          $('img.avatar').attr('src',data);
       }
    });
  });

  $(document).on('click','.edit-data-form', function(){
    /*$.ajax({
      url:ajax_url+'/v1/users/auth',
      type: 'post',
      dataType: 'json',
      data: {},
      sucess: function(data){
        console.log(data);
      }*/
      var data = {};
      $('.segment td').each(function(){
        var temp = {};
        if($(this).attr('class') && $(this).find('span').text() != '*************'){
          data[$(this).attr('class')] = $(this).find('span').text();
          //data.push(temp)
        }
      });
      $.ajax({
        url:ajax_url+'/v1/users/auth/edit',
        type: 'post',
        dataType: 'json',
        data: { data },
        success: function(data){
          if(data.code==200){
            swal({
              title: "Datos guardados exitosamente",
              text: "Tu perfil ha sido actualizado exitosamente.",
              type: "success",
              confirmButtonText: "Cerrar"
            });
          }else{
            swal({
              title: "Error al guardar",
              text: data.msg,
              type: "error",
              confirmButtonText: "Cerrar"
            });
          }
        }
      });
  });

  $.ajax({
    url:ajax_url+'/v1/users/auth/get',
    type: 'post',
    dataType: 'json',
    xhrFields: {
      withCredentials: true
    },
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    success: function(data){
      console.log(data);
      $('.user-avatar .email').text(data[0].email);
      $('td.email span').text(data[0].email);
      $('td.name span').text(data[0].name);
      $('td.username span').text(data[0].username);
      $('.user-avatar h3').text(data[0].name);
      $('.user-avatar h2').text(data[0].username);
      if(data[1].length!=0){
        $('td.password span').text();
        $('td.phone span').text(data[1][0].phone);
        $('td.birth_date span').text(data[1][0].birth_date);
        $('img.avatar').attr('src',data[1][0].avatar);
        $('td.profession span').text(data[1][0].profession);
        $('td.specialty span').text(data[1][0].specialty);
        $('td.years_experience span').text(data[1][0].years_experience);
        $('td.init_date span').text(data[1][0].init_date);
        $('td.end_date span').text(data[1][0].end_date);
      }
    }
  });
}

if($body=='units'){

  $('.calendar-continue').click(function(){
    window.location = '/events-calendar'+extension;
  });

  $('.save-calendar').click(function(){
    var arr = [];
    var a = 0;
    $('.unit-block').each(function(){
      a++;
      arr.push([$(this).data('unit-id'),a]);
    });

    for (var i = 0; i < 7; i++) {
      //$d = $('.unit-block')[i];
      //console.log($d.data('unit-id'));
      //arr.push([$(this).data('unit-id'),a]);
      //arr.push([$(this).data('unit-id'),a]);
    }

    console.log(arr);
    $.ajax({
      url: ajax_url+'/v1/units/save/'+localStorage.getItem('subject_id')+'',
      dataType: 'json',
      type: 'post',
      xhrFields: {
        withCredentials: true
      },
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      data: {
        subject_id: localStorage.getItem('subject_id'),
        arr: arr,
        level: localStorage.getItem('level'),
        letter: localStorage.getItem('letra'),
        school: localStorage.getItem('school'),
        /*unidad1: arr[0][1],
        unidad2: arr[1][1],
        unidad3: arr[2][1] ? arr[2][1] : 0 ,
        unidad4: arr[3][1] ? arr[3][1] : 0,
        /*unidad5: arr[4][1] ? arr[4][1] : 0,
        unidad6: arr[5][1] ? arr[5][1] : 0,
        unidad7: arr[6][1] ? arr[6][1] : 0,
        unit_id_1: arr[0][0],
        unit_id_2: arr[1][0],
        unit_id_3: arr[2][0] ? arr[2][0] : 0,
        unit_id_4: arr[3][0] ? arr[3][0] : 0,
        /*unit_id_5: arr[4][0] ? arr[4][0] : 0,
        unit_id_6: arr[5][0] ? arr[5][0] : 0,
        unit_id_7: arr[6][0] ? arr[6][0] : 0*/},
      success: function(data){
        //window.location = '/events-calendar'+extension;
        swal({
          title: "Programación guardada",
          text: "La programación fue guardada exitosamente. Ya puedes imprimirla.",
          type: "success",
          confirmButtonText: "Continuar"
        });
      }
    });

  });

  $("#unit-sort").sortable({
    revert: true,
    update: function(){
      $ac = [];
      $('.unit-block').each(function(){
        console.log($(this).attr('class').split(' ')[1]);
        $ac.push($(this).attr('class').split(' ')[1]);
      });
      $('.custom-calendar td div').remove();
      $arr = [];
      $('.unit-block').each(function(){
        $arr.push(JSON.parse($(this).attr('data-dataObj')));
      });
      //console.log($arr);
      var tot = localStorage.getItem('subject_total');
      $td = 1;
      $old_j = 2;
      $c = 0;
      $arr_test = [];
      $.each($arr, function(e,v){
        $d = v.hrs/tot;
        $arr_test.push($d);
        //console.log($d);
        /*if(isEven($d)){
          console.log($d);
          /*for($j=1;$j<=$d;$j++){
            //$('.custom-calendar td:nth-child('+$j+')').append('<div data-l="1" style="height: 300px; " class="'+$ac[$c]+'"></div>')
            $old_j = $j;
          }
          $old_j = $old_j;
        }else{
          //console.log($d);
          /*
          ////alert($d);
          $entero = Math.floor($d);
          //console.log($entero);
          $old_j = $old_j+1;
          for($k=1;$k<=$entero;$k++){
            //$('table td:empty:eq(0)').append('<div data-l="2" style="height: 300px; background: '+$ac[$c]+';"></div>');
            $('.custom-calendar td:nth-child('+$old_j+')').html('<div data-l="2" style="height: 300px; background: '+$ac[$c]+';" class="'+$ac[$c]+'"></div>')
          }
          $old_j++;
          $c++;
          $resto = ($d-$entero)*100;
          console.log($resto);
          /*
          ////alert($entero);
          //console.log($entero);
          $old_j = $old_j+1;
          console.log($old_j);
          for($k=1;$k<=$entero;$k++){
            //$('table td:empty:eq(0)').append('<div data-l="2" style="height: 300px; background: '+$ac[$c]+';"></div>');
            //$('.custom-calendar td:nth-child('+$old_j+')').append('<div data-l="2" style="height: 300px; background: '+$ac[$c]+';"></div>')
          }
          $old_j++;
          $resto = $d-$entero;
          $height = 300*$resto;
          $rest_height = 300-$height;
          //$('.custom-calendar td:nth-child('+$old_j+')').append('<div data-l="3" style="height: '+$height+'px; " class="'+$ac[$c]+'"></div>')
          $c++;
          //$('.custom-calendar td:nth-child('+$old_j+')').append('<div data-l="4" style="height: '+$rest_height+'px; " class="'+$ac[$c]+'"></div>');
          //console.log($resto*100);*/
        //}
      });
      //console.log($arr_test);
      $posicion = 0;
      $le_array = [];
      $.each($arr_test, function(e,v){
        $posicion++;
        //console.log(v);
        $d = v;
        if(isEven($d)){
          //console.log($d);
          $le_array.push($d);
        }else{
          $entero = Math.floor($d);
          //console.log($entero);
          if($entero>1){
            for (var i = 0; i < $entero; i++) {
              //console.log(100);
              $le_array.push(100);
              //$le_array[$posicion] = 100;
            }
          }else{
            //console.log(100);
            $le_array.push(100);
            //$le_array[$posicion] = 100;
          }
          //console.log(Math.floor(($d-$entero)*100));
          $le_array.push(Math.floor(($d-$entero)*100));
          //$le_array[$posicion] = Math.floor(($d-$entero)*100);
        }
      });
      console.log('------');
      console.log($le_array);
      $saldo = 0;
      $.each($le_array, function(e,v){
        //console.log(e+'=>'+v);
        if($saldo===0){
          if(v==100){
            $td_child = e+3;
            $('.custom-calendar td:nth-child('+$td_child+')').html('<div style="height:330px; background: red;">lalala</div>');
            console.log('columna '+e+'=todo'); // 100
          }else{
            if(v<100){
              $td_child = e+3;
              $html = '';
              console.log('columna '+e+'='+v); //12
              $height = 330*(v/100);
              $html += '<div style="height:'+$height+'px; background: red;">lalala</div>';
              $sigue = e+1;
              $val_sigue = $le_array[$sigue]-v;
              $saldo = v; // 12
              console.log('columna '+e+'='+$val_sigue); // 88
              $height = 330*($val_sigue/100);
              $html += '<div style="height:'+$height+'px; background: green;">lalala</div>';
              $('.custom-calendar td:nth-child('+$td_child+')').html($html);
              console.log('----saldo='+$saldo);
            }
          }
        }else{
          console.log('no se que hacer ahora');
          console.log('oh, wait...');
          console.log('saldo='+$saldo);
          $td_child = e+3;
          $html = '';
          console.log('columna '+e+'='+$saldo);
          $height = 330*($saldo/100);
          $html += '<div style="height:'+$height+'px; background: black;">lalala</div>';
          $s = v-(v-$saldo);
          console.log('++++'+$s);
          console.log('columna '+e+'='+(v-$saldo));
          $height = 330*((v-$saldo)/100);
          $html += '<div style="height:'+$height+'px; background: blue;">lalala</div>';
          console.log('val='+v);
          $('.custom-calendar td:nth-child('+$td_child+')').html($html);
          //$saldo = $saldo-v;
          /*$nuevo_saldo = v-$saldo;

          $sigue = e+1;
          /*if($le_array[$sigue] > $nuevo_saldo){
              $val_sigue = $le_array[$sigue]-$nuevo_saldo;
          }else{
              $val_sigue = $nuevo_saldo-$le_array[$sigue];
          }
          $val_sigue = $le_array[$sigue]-$nuevo_saldo;
          $saldo = $val_sigue;
          if($saldo<0){
            //console.log('1columna '+e+'='+$le_array[$sigue]);
          }else{
            //console.log('2columna '+e+'='+$val_sigue);
          }
          ///console.log('columna '+e+'='+$nuevo_saldo);
          //console.log('----saldo='+$saldo);
          //console.log('---------valor original='+v);
          //console.log('esto me da error: '+$le_array[$sigue]+' - '+$nuevo_saldo);*/
        }

      });
    }
  });

  $.ajax({
    url: ajax_url+'/v1/units/list/'+localStorage.getItem('subject_id'),
    dataType: 'json',
    data: {level: localStorage.getItem('level'), letter: localStorage.getItem('letra'), school: localStorage.getItem('school')},
    type: 'post',
    success: function(data){
      console.log(data.subject[0].weekly_hours);
      var wh = data.subject[0].weekly_hours;
      $html = '';
      $colors = ['yellow-label','green-label', 'blue-label','red-label','orange-label','pink-label','calypse-label'];
      $units = data.orderU.length > 0 ? data.orderU : data.units;
      //$units = data.units;
      $i = 0;
      localStorage.setItem('subject_total',(data.subject[0].weekly_hours)*4);
      /*$i = 0;
      //console.log('mensuales: '+(data.subject[0].weekly_hours)*4);
      var tot = (data.subject[0].weekly_hours)*4;
      $units = data.units;
      $td = 1;
      $old_j = 0;
      $c = 0;
      $ac = ['red', 'blue', 'green', 'yellow'];*/
      $.each($units, function(e,v){

        //console.log(v);
        /*$d = v.hrs/tot;
        if(isEven($d)){
          for($j=1;$j<=$d;$j++){
            $('.custom-calendar td:nth-child('+$j+')').append('<div data-l="1" style="height: 300px; background: '+$ac[$c]+';"></div>')
            $old_j = $j;
          }
        }else{
          ////alert($d);
          $entero = Math.floor($d);
          ////alert($entero);
          /*console.log($entero);
          $old_j++;
          for($k=1;$k<=$entero;$k++){
            $('.custom-calendar td:nth-child('+$old_j+')').append('<div data-l="2" style="height: 300px; background: '+$ac[$c]+';"></div>')
          }
          $old_j++;
          $resto = $d-$entero;
          $height = 300*$resto;
          $rest_height = 300-$height;
          $('.custom-calendar td:nth-child('+$old_j+')').append('<div data-l="3" style="height: '+$height+'px; background: '+$ac[$c]+';"></div>')
          $c++;
          $('.custom-calendar td:nth-child('+$old_j+')').append('<div data-l="4" style="height: '+$rest_height+'px; background: '+$ac[$c]+';"></div>')

          //console.log($resto*100);
        }
        name = v.name.replace( /^\D+/g, '');
        //console.log(name[0]);
        console.log(v.hrs);*/
        //$html += '<div data-unit-info="'+JSON.stringify(v).replace(/'/g, "\\'")+'" class="unit-block '+$colors[$i]+' ui-sortable-handle">Unidad <span>'+name+'</span><small>'+v.hrs+' horas</small></div>';
        name = v.name.replace( /^\D+/g, '');
        console.log(Math.round(v.hrs/wh));
        $html += '<div data-unit-id="'+v.id+'" class="unit-block '+$colors[$i]+' ui-sortable-handle" data-dataObj=\''+JSON.stringify(v).replace(/'/g, "\\'")+'\'>Unidad <span>'+name+'</span><!--<small>'+v.hrs+' horas</small>--><small>'+wh+' horas por semana</small><br><small>'+Math.round(v.hrs/wh)+' semanas</small><br><p class="">'+v.purpose_min+'</p></div>';
        $i++;
      });
      $('#unit-sort').html($html);
      if($units.length > 4){
        $('#unit-sort').height(405);
      }
      var array = [];
      $('.ui-sortable-handle').each(function(){
        array.push($(this).height());
      });
      var height_ = Math.max.apply(Math, array);
      $('.ui-sortable-handle').height(height_);
      $('.unit-block').each(function(){
        //console.log($(this).attr('data-dataObj'));
      });
      //console.log($('.unit-block').attr('data-dataObj'));
    }
  });
}

if($body=='week-schedule-body'){



  //$('.ui.red.label').attr('href',ajax_url+'/v1/report/calendar/print');

  $('#schedule').fullCalendar({
      eventAfterAllRender: function() {
          $("#fullcalendar-detail").find('.fc-toolbar > div > h2').empty().append("<div>Curso / Asignatura</div>");
          //$('.fc-day-header').text($('.fc-day-header').text().replace(/[0-9]/g, ''));
          $('.fc-day-header').each(function(){
            //console.log($(this).text($(this).text().replace(/[0-9]/g, '')));
          });
      },
      editable: false,
      defaultView: 'agendaWeek',
      firstDay: 1,
      allDaySlot: false,
      minTime: '07:00:00',
      maxTime: '19:00:00',
      columnFormat: 'dddd D',
      titleFormat: 'MMMM YYYY',
      eventClick: function(calEvent, jsEvent, view) {
        $('.event-detail').modal('show');
        ////alert('Event: ' + calEvent.title);
        ////alert('Coordinates: ' + jsEvent.pageX + ',' + jsEvent.pageY);
        ////alert('View: ' + view.name);
      },
      lang: 'es',
      droppable: false,

      slotDuration: '00:30:00',
      slotLabelFormat: 'h:mm',
      events: function(start, end, timezone, callback) {
        $.ajax({
          url: ajax_url+'/v1/schedule/list',
          dataType: 'json',
          type: 'post',
          xhrFields: {
            withCredentials: true
          },
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          success: function(data){
            console.log(data);
            var events = [];
            $.each(data, function(e,v){
              events.push({
                title: v.name+' / '+v.course+' ('+v.id+')',
                start: v.hora_inicio,
                end: v.hora_fin,
                allDay: false,
                className: v.color,
                color: v.color,
              });
            });
            console.log(events);
            callback(events);
          }
        });
      },
      drop: function(date) {

          var originalEventObject = $(this).data('eventObject');
          var copiedEventObject = $.extend({}, originalEventObject);

          copiedEventObject.start = date;
          copiedEventObject.title = $(this).data('title');
          copiedEventObject.backgroundColor = $(this).data('hex');

          //console.log(copiedEventObject);

          $('#fullcalendar-detail').fullCalendar('renderEvent', copiedEventObject, true);
          //$(this).remove();

      }
  });

}

if($body=='schedule-body'){

  var eventsfromweek = [];

  $(document).on('click', '.fc-state-default',function(){
    eventsfromweek = [];
  });

  function download(strData, strFileName, strMimeType) {
    var D = document,
        A = arguments,
        a = D.createElement("a"),
        d = A[0],
        n = A[1],
        t = A[2] || "text/plain";

    //build download link:
    a.href = "data:" + strMimeType + "," + escape(strData);

    if (window.MSBlobBuilder) {
        var bb = new MSBlobBuilder();
        bb.append(strData);
        return navigator.msSaveBlob(bb, strFileName);
    } /* end if(window.MSBlobBuilder) */

    if ('download' in a) {
        a.setAttribute("download", n);
        a.innerHTML = "downloading...";
        D.body.appendChild(a);
        setTimeout(function() {
            var e = D.createEvent("MouseEvents");
            e.initMouseEvent("click", true, false, window, 0, 0, 0, 0, 0, false, false,
false, false, 0, null);
            a.dispatchEvent(e);
            D.body.removeChild(a);
        }, 66);
        return true;
    } /* end if('download' in a) */

    //do iframe dataURL download:
    var f = D.createElement("iframe");
    D.body.appendChild(f);
    f.src = "data:" + (A[2] ? A[2] : "application/octet-stream") + (window.btoa ? ";base64"
: "") + "," + (window.btoa ? window.btoa : escape)(strData);
    setTimeout(function() {
        D.body.removeChild(f);
    }, 333);
    return true;
} /* end download() */

  //
  //$('.ui.red.label').attr('href',ajax_url+'/v1/report/calendar/print');

  $('.le-printt').click(function(e){
    $('.fc-right').hide();
    e.preventDefault();
    html2canvas($('#schedule'), {
     logging: true,
     useCORS: true,
     onrendered: function (canvas) {
        var imgData = canvas.toDataURL("image/jpeg");
        var doc = new jsPDF('p');
        doc.addImage(imgData, 'JPEG', 15, 15, 180, 180);
        download(doc.output(), "calendario.pdf", "text/pdf");

     }
   }) ;
   $('.fc-right').show();
  });

  $('.le-print').click(function(e){
    e.preventDefault();
    $('.plan-modal').modal('show');
    //alert($('#schedule').fullCalendar( 'getDate' ).format('M'));
    // http://api-poptimize.fgacv.net/v1/report/calendar/print

    //window.open(ajax_url+'/v1/report/calendar/print/'+$('#schedule').fullCalendar( 'getDate' ).format('M'), '_blank');
    /*$.ajax({
      url: ajax_url+'/v1/report/calendar/print',
      dataType: 'json',
      type: 'post',
      xhrFields: {
        withCredentials: true
      },
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      data: {month: $('#schedule').fullCalendar( 'getDate' ).format('M')},
      success: function(data){
        console.log(data);
      }
    });*/
  });
  $('.plan-print-sch').on('click',function(){
        $impselect = $('input[name=imp]:checked').val();
        if($impselect == "1"){
          var month = $('#monthsel').dropdown('get value');
          // console.log(ajax_url+'/v1/report/calendar/print/'+month);
          window.open(ajax_url+'/v1/report/calendar/print?mes='+month, '_blank');
          
        }
        if($impselect == "2"){
            var posturl = '';
            $.each(eventsfromweek, function(e,v){
              if(e == eventsfromweek.length-1){
                posturl += 'events[]='+v.id;
              }
              else{
                posturl += 'events[]='+v.id+'&';
              }
            });
            
            var url = ajax_url+'/v1/report/calendar/week?'+posturl;
            console.log(url);
            window.open(url, '_blank');          
        }
    
  });

  $('#schedule').fullCalendar({
    height: "auto",
      eventAfterAllRender: function() {
          $("#fullcalendar-detail").find('.fc-toolbar > div > h2').empty().append("<div>Curso / Asignatura</div>");
          //$('.fc-day-header').text($('.fc-day-header').text().replace(/[0-9]/g, ''));
          $('.fc-day-header').each(function(){
            //console.log($(this).text($(this).text().replace(/[0-9]/g, '')));
          });
      },
      editable: false,
      defaultView: 'agendaWeek',
      firstDay: 1,
      allDaySlot: false,
      minTime: '07:00:00',
      maxTime: '20:00:00',
      columnFormat: 'dddd D',
      titleFormat: 'MMMM YYYY',
      eventClick: function(calEvent, jsEvent, view) {
      if($(this).hasClass('holi') || $(this).hasClass('conme')){
                            if($(this).hasClass('conme')){
                              $('#conme-content').empty();
                              $.ajax({
                                url: ajax_url+'/v1/schedule/getconme',
                                dataType: 'json',
                                type: 'post',
                                async: false,
                                xhrFields: {
                                  withCredentials: true
                                },
                                headers: {
                                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                },
                                data: {date: calEvent.start.format()},
                                success: function(dato){
                                  var conmes = '';
                                  $.each(dato, function(e,v){
                                    console.log(v.desc);
                                    conmes += '<p>'+v.desc+'</p>';
                                    if(v.file){
                                      conmes += '<a href="'+ajax_url+'dist/conmes/'+v.file+'" target="_blank" >'+v.file+'</a>';
                                    }
                                  });
                                  $('#conme-content').append(conmes);
                                  $('#conme-modal').modal('show');
                                }
                              });
                            }

                          }
        else{        
        console.log(calEvent.id);
        var content_modal = $('#detail-class');
        content_modal.empty();
        $.ajax({
        url:ajax_url+'/v1/schedule/getclassinfo',
        type: 'post',
        dataType: 'json',
        data: {id: calEvent.id},
        xhrFields: {
          withCredentials: true
        },
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function(data){
            var htmlModal = '<p class="titleno"><b>Asignatura: </b>'+data.asign+'</b></p>';
            htmlModal += '<p class="titleno"><b>Curso: </b>'+data.course+' '+data.letter+'</b></p>';
            htmlModal += '<p class="titleno"><b>Fecha: </b>'+data.formated+'</b></p>';
            if(data.type==0){
              htmlModal += '<p class="titleno"><b>Tipo: </b>Clase</b></p><br>';
            }
            if(data.type==1){
              htmlModal += '<p class="titleno"><b>Tipo: </b>Prueba</b></p><br>';
            }
            if(data.type==2){
              htmlModal += '<p class="titleno"><b>Tipo: </b>Actividad/Trabajo</b></p><br>';
            }
            if(data.arroas.length > 0){
                htmlModal += '<p class="titleno"><b>'+data.unit+'</b></p>';
                htmlModal += '<p class="textno"><b>Objetivos de Aprendizaje:</b></p>';
                $.each(data.arroas, function(e,v){
                    htmlModal += '<p class="textno"><b>'+v.name+': </b>'+v.desc+'</p>'
                });
                htmlModal += '<br>';
            }
            else{
                htmlModal += '<p class="textno"><b>Objetivos de Aprendizaje: </b>Ninguno</p><br>';
            }
            if(data.arrat.length > 0){
              htmlModal += '<p class="titleno" ><b>Actitudes</b></p>'
              $.each(data.arrat, function(e,v){
                htmlModal += '<p class="textno">'+v.desc+'</p>'
              });
              htmlModal += '<br>';
            }
            if(data.arrab.length > 0){
              htmlModal += '<p class="titleno"><b>Habilidades</b></p>'
              $.each(data.arrab, function(e,v){
                htmlModal += '<p class="textno">'+v.desc+'</p>'
              });
              htmlModal += '<br>';
            }
            if(data.arrtag.length > 0){
              htmlModal += '<p class="titleno"><b>Palabras Clave</b></p><p class="textno">'
              $.each(data.arrtag, function(e,v){
                if(e < data.arrtag.length-1){
                  htmlModal += v.name+', ';
                }
                else{
                  htmlModal += v.name+'.';
                }
              });
              htmlModal += '</p><br>';
            }
            if(data.scheduleData[0].description || data.scheduleData[0].init_text || data.scheduleData[0].mid_text || data.scheduleData[0].end_text || data.scheduleData[0].res_text || data.scheduleData[0].eval_text || data.scheduleData[0].ec_text){
                if(data.scheduleData[0].description.length){
                    htmlModal += '<p class="titleno"><b>Descripción</b></p><p class="textno">'+data.scheduleData[0].description.replace(/(\n)+/g, '<br />')+'</p><br>';       
                }
                if(data.scheduleData[0].init_text){
                    htmlModal += '<p class="titleno"><b>Inicio</b></p><p class="textno">'+data.scheduleData[0].init_text.replace(/(\n)+/g, '<br />')+'</p><br>';       
                }
                if(data.scheduleData[0].mid_text){
                    htmlModal += '<p class="titleno"><b>Desarrollo</b></p><p class="textno">'+data.scheduleData[0].mid_text.replace(/(\n)+/g, '<br />')+'</p><br>';       
                }
                if(data.scheduleData[0].end_end){
                    htmlModal += '<p class="titleno"><b>Cierre</b></p><p class="textno">'+data.scheduleData[0].end_text.replace(/(\n)+/g, '<br />')+'</p><br>';       
                }
                if(data.scheduleData[0].res_text){
                    htmlModal += '<p class="titleno"><b>Recursos de Aprendizaje</b></p><p class="textno">'+data.scheduleData[0].res_text.replace(/(\n)+/g, '<br />')+'</p><br>';       
                }
                if(data.scheduleData[0].eval_text){
                    htmlModal += '<p class="titleno"><b>Evaluación</b></p><p class="textno">'+data.scheduleData[0].eval_text.replace(/(\n)+/g, '<br />')+'</p><br>';       
                }
                if(data.scheduleData[0].ec_text){
                    htmlModal += '<p class="titleno"><b>Adecuación Curricular</b></p><p class="textno">'+data.scheduleData[0].ec_text.replace(/(\n)+/g, '<br />')+'</p><br>';       
                }
            }
            else{

                htmlModal += '<p class="titleno"><b>Actividades de Aprendizaje</b></p><p class="textno">Ninguno</p><br>';
            }
            if(data.file){
              htmlModal += '<p class="titleno"><b>Archivo Adjunto</b></p><a href="'+ajax_url+'/uploads/'+data.file+'" target="_blank">'+data.file+'</a>';
            }            

            content_modal.append(htmlModal);
            $('.event-detail').modal('show');



            }
      });
    }
        
      },
      lang: 'es',
      droppable: false,
      slotDuration: '00:60:00',
      slotLabelFormat: 'h:mm',
      events: function(start, end, timezone, callback) {
        $.ajax({
          url: ajax_url+'/v1/schedule/list',
          dataType: 'json',
          type: 'post',
          xhrFields: {
            withCredentials: true
          },
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          success: function(data){
            console.log(data);
            var events = [];
            $.ajax({
                          url: ajax_url+'/v1/schedule/conme',
                          dataType: 'json',
                          type: 'post',
                          xhrFields: {
                            withCredentials: true
                          },
                          headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                          },
                          success: function(data){
                            $.each(data, function(e,v){
                              events.push({
                                allDay: false,
                                start: v.date_init,
                                end: v.date_end,
                                title: 'Efemérides',
                                className: 'conme',
                                color: '#ffffff',
                                textColor: '#000000',
                              });
                            });
                          }

                        });

                        $.ajax({
                          url: ajax_url+'/v1/schedule/holidays',
                          dataType: 'json',
                          type: 'post',
                          xhrFields: {
                            withCredentials: true
                          },
                          headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                          },
                          success: function(data){
                            $.each(data, function(e,v){
                              events.push({
                                allDay: false,
                                start: v.date_init,
                                end: v.date_end,
                                title: v.name,
                                className: 'holi',
                                color: '#d3d3d3'
                              });
                            });
                          }

                        });
            $.each(data, function(e,v){
              events.push({
                title: v.name+' / '+v.course+' '+v.letter,
                start: v.hora_inicio,
                end: v.hora_fin,
                allDay: false,
                className: v.color,
                color: v.color,
                id: v.main_id,
                type: v.type
              });
            });
            $.ajax({
              url: ajax_url+'/v1/notifications/list',
              dataType: 'json',
              type: 'post',
              xhrFields: {
                withCredentials: true
              },
              headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              },
              success: function(data){
                //console.log(data.notifications);
                //var events = [];
                $.each(data.notifications, function(e,v){
                  console.log(v);
                  //if(v.subject_id==localStorage.getItem('subject_id')){
                    /*events.push({
                      allDay:false,
                      className:"reminder",
                      end:moment(v.date).add(25,'minutes'),
                      start:v.date,
                      title:v.name,
                      description: v.description

                    });*/
                  //}
                });
                console.log(events);
                callback(events);
              }

            });


          }
        });
      },
      drop: function(date) {

          var originalEventObject = $(this).data('eventObject');
          var copiedEventObject = $.extend({}, originalEventObject);

          copiedEventObject.start = date;
          copiedEventObject.title = $(this).data('title');
          copiedEventObject.backgroundColor = $(this).data('hex');

          //console.log(copiedEventObject);

          $('#fullcalendar-detail').fullCalendar('renderEvent', copiedEventObject, true);
          //$(this).remove();

      },
      eventRender: function (event, element)
      {
        if(element.hasClass('holi') || element.hasClass('conme')){
          element.prepend( "<i class='alarm outline icon' style='float:left;'></i>" );
        }
        else{
        eventsfromweek.push(event);
        // alert(event.id);
        if(event.type==0){
          element.prepend( "<i class='book icon large' style='float:right; margin-top:2px;'></i>" );
        }
        if(event.type==1){
         element.prepend( "<i class='file powerpoint outline icon large' style='float:right; margin-top:2px;'></i>" );
        } 
        if(event.type==2){
         element.prepend( "<i class='users icon large' style='float:right; margin-top:2px;'></i>" );
        }
        element.attr('data-id',event.id);
        $.ajax({
          url: ajax_url+'/v1/objectives/list',
          dataType: 'json',
          type: 'post',
          data: {id: event.id},
          xhrFields: {
            withCredentials: true
          },
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          success: function(data){
            console.log(data);
            //alert(111);
            $.each(data, function(e,v){
              //element.append(v.text);
              element.append('<div class="in-event with-popover '+v.color+'" data-id="'+v.objective_id+'" data-name="'+v.text+'" >'+v.text+'</div>');
            });
          }
        });
        }
      }
  });
}

if($body=='course-calendar'){

  $('.start-time').click(function(){



    var newEvent = new Object();

    newEvent.title = localStorage.getItem('subject_name');
    newEvent.start = moment($('.menu.start .item.selected').data('value')).format();
    newEvent.end = moment($('.menu.start .item.selected').data('value')).add($('.menu.end .item.selected').data('value'),'minutes').format();
    newEvent.allDay = false;
    newEvent.backgroundColor = localStorage.getItem('hex');
    newEvent.className = localStorage.getItem('hex');

    $('#fullcalendar').fullCalendar( 'renderEvent', newEvent );
    $('#course-date').modal('hide');
    $('.fc-time-grid-event').each(function(){
      console.log($(this).hasClass('no-change'));
      if(!$(this).hasClass('no-change')){
        //$(this).css('background-color', localStorage.getItem('hex'));
      }
      if(!$(this).hasClass('locked')){
             $(this).css('background-color', localStorage.getItem('hex'));
           }

    });
  });

  $(document).on('click','.start.menu .item', function(){
    //$changed = moment($(this).data('value'));
    //$new_changed = $changed.add(45,'minutes');
    //$('.end_value').val($new_changed.format('H:mm:ss'));
    //$('.text.end').text($new_changed.format('H:mm'));
    //$('.menu.end').html('<div class="item active" data-value="'+$new_changed.format('YYYY-MM-DD HH:mm:ss')+'">'+$new_changed.format('H:mm')+'</div>');
  });

  $('#fullcalendar').fullCalendar({
    events: function(start, end, timezone, callback){
      $.ajax({
        url: ajax_url+'/v1/schedule/list',
        dataType: 'json',
        type: 'post',
        xhrFields: {
          withCredentials: true
        },
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function(data){
          var events = [];
          $.each(data, function(e,v){
            console.log(v);
              events.push({
                title: v.name+' / '+v.course+' '+v.letter,
                start: v.hora_inicio,
                end: v.hora_fin,
                allDay: false,
                className: 'locked',
                color: v.color,
                id: v.main_id
              });
          });
          callback(events);
        }
      });
    },
    /*eventRender: function (event, element)
    {

      $.ajax({
        url: ajax_url+'/v1/objectives/list',
        dataType: 'json',
        type: 'post',
        data: {id: event.id},
        xhrFields: {
          withCredentials: true
        },
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function(data){
          //alert(111);
          $.each(data, function(e,v){
            //element.append(v.text);
            element.append('<div class="in-event with-popover '+v.color+'" data-id="'+v.objective_id+'">'+v.text+'</div>');
          });
        }
      });

      //console.log(event);
      le_e = event;
      element.attr('data-id',event.id);
      element.prepend( "<i class='book icon' style='float:left;'></i>" );
      $(element).data('id', event.id);
    },*/
    header: {
        left: 'prev,next today',
        center: 'title',
        right: 'myCustomButton month,agendaWeek',
    },
    defaultView: 'agendaWeek',
    annotations: [{
        start: new Date(y, m, d, 13, 0),
        end: new Date(y, m, d, 15, 30),
        title: 'My 1st annotation', // optional
        cls: 'open', // optional
        color: '#777777', // optional
        background: '#eeeeff' // optional
    }] ,
    dayClick: function(date, jsEvent, view) {

        //alert('Clicked on: ' + date.format());

        //var time = moment.duration("00:15:00");
        //var date_ = date.format('YYYY-MM-DD H:mm:s');
        //alert(date.subtract(time));
        //$five_plus = date.add(5,'minutes');
        //$ten_plus = $five_plus.add(10,'minutes');
        /*$fifteen_plus = date.add(15,'minutes');
        $twenty_plus = date.add(20,'minutes');
        $twentyfive_plus = date.add(15,'minutes');
        $thirty_plus = date.add(30,'minutes');*/
        var j;
        j = 0;
        var $html = '';
        var original_date = date;
        for (var i = 0; i < 12; i++) {
          //alert(j);
          if(j===0){
            //alert(date.format('H:mm'));
            $html = '<div class="item active selected" data-value="'+date.format('YYYY-MM-DD HH:mm:ss')+'">'+date.format('H:mm')+'</div>';
            //console.log(date.format('H:mm'));
            $('.default.text.start').text(date.format('H:mm'));
            $('.text.start').text(date.format('H:mm'));
            $('.start_value').val(date.format('YYYY-MM-DD HH:mm:ss'));
          }else{
            //$('.default.text.start').text(date.format('H:mm'));
            //console.log(j);
            $nh = date.add(5,'minutes').format('H:mm');
            $nhd = date.add(0,'minutes').format('YYYY-MM-DD HH:mm:ss');
            $html += '<div class="item" data-value="'+$nhd+'">'+$nh+'</div>';
            //alert(date.add(j,'minutes').format('H:mm'));
          }
          j = j+5;
        }

        //alert($five_plus);
        //alert($ten_plus);

        //$('.image.content .image').text(date.format());
        //$html = '<div class="item" data-value="'+moment($five_plus).format('H:mm:s')+'">'+moment($five_plus).format('H:mm:s')+'</div>';
        //$html += '<div class="item" data-value="'+moment($ten_plus).format('H:mm:s')+'">'+moment($ten_plus).format('H:mm:s')+'</div>';
        /*$html += '<div class="item" data-value="'+moment($fifteen_plus).format('H:mm:s')+'">'+moment($fifteen_plus).format('H:mm:s')+'</div>';
        $html += '<div class="item" data-value="'+moment($twenty_plus).format('H:mm:s')+'">'+moment($twenty_plus).format('H:mm:s')+'</div>';
        $html += '<div class="item" data-value="'+moment($twentyfive_plus).format('H:mm:s')+'">'+moment($twentyfive_plus).format('H:mm:s')+'</div>';
        $html += '<div class="item" data-value="'+moment($thirty_plus).format('H:mm:s')+'">'+moment($thirty_plus).format('H:mm:s')+'</div>';*/
        $('.ui.selection.dropdown .menu.start').html($html);
        $('#course-date').modal('show');
        //alert(original_date);
        //alert(original_date.add(45,'minutes'));

    },
    firstDay: 1,
    allDaySlot: false,
    columnFormat: 'dddd',
    titleFormat: 'MMMM YYYY',
    minTime: '07:00:00',
    maxTime: '19:00:00',
    lang: 'es',
    droppable: true,
    eventOverlap: false,
    slotEventOverlap: false,
    selectOverlap: false,
    slotDuration: '00:60:00',
    slotLabelFormat: 'H:mm',
    defaultDate: '2017-03-13',
    eventAfterAllRender: function() {
      $('#fullcalendar .fc-day-header').each(function(i,v){
        //console.log($(this).text($(this).text().replace(/[0-9]/g, '')));
      });
        //$("#fullcalendar").find('.fc-toolbar > div > h2').empty().append("<div>Curso / Asignatura</div>");
    },
    eventRender: function(event, element) {
      if(!$(element).hasClass('locked')){
        element.prepend( "<i class='closeon remove icon'></i>" );
      }else{
        //alert(2);
      }

      element.find(".closeon").click(function() {
         $('#fullcalendar').fullCalendar('removeEvents',event._id);
         $('.fc-time-grid-event').each(function(){
           console.log($(this).hasClass('no-change'));
           if(!$(this).hasClass('no-change')){
             //$(this).css('background-color', localStorage.getItem('hex'));
           }
           if(!$(this).hasClass('locked')){
             $(this).css('background-color', localStorage.getItem('hex'));
           }

         });
      });
    },
  });

  /*if(!localStorage.getItem('start')){
    $('.ui.modal')
    .modal('show').modal({
      blurring: true
    });
  }

  var date = new Date();
  var d = date.getDate();
  var m = date.getMonth();
  var y = date.getFullYear();

  $('#fullcalendar').fullCalendar({
      header: {
          left: 'prev,next today',
          center: 'title',
          right: 'myCustomButton month,basicWeek,basicDay',
      },
      eventAfterAllRender: function() {
        $('#fullcalendar .fc-day-header').each(function(i,v){
          console.log($(this).text($(this).text().replace(/[0-9]/g, '')));
        });
          //$("#fullcalendar").find('.fc-toolbar > div > h2').empty().append("<div>Curso / Asignatura</div>");
      },
      editable: true,
      defaultView: 'agendaWeek',
      firstDay: 1,
      allDaySlot: false,
      minTime: localStorage.getItem('start') ? localStorage.getItem('start') : '08:00:00',
      maxTime: '19:00:00',
      columnFormat: 'dddd D',
      titleFormat: 'MMMM YYYY',
      lang: 'es',
      droppable: true,
      eventOverlap: false,
      slotDuration: '00:60:00',
      slotLabelFormat: 'h:mm',
      events: function(start, end, timezone, callback){
        $.ajax({
          url: ajax_url+'/v1/schedule/list',
          dataType: 'json',
          type: 'post',
          xhrFields: {
            withCredentials: true
          },
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          success: function(data){
            var events = [];
            $.each(data, function(e,v){
              events.push({
                title: v.name+' / '+v.course+' ('+v.id+')',
                start: v.hora_inicio,
                end: v.hora_fin,
                allDay: false,
                className: [v.color,'no-change'],
                color: v.color,
                eventOverlap: false,
              });
            });
            callback(events);
          }
        });
      },
      eventRender: function(event, element) {
        element.prepend( "<i class='closeon remove icon'></i>" );
        element.find(".closeon").click(function() {
           $('#fullcalendar').fullCalendar('removeEvents',event._id);
        });
    },
      drop: function(date) {
          var events = $('#fullcalendar').fullCalendar('clientEvents');
          for (i = 0 ; i < events.length; i++){
            //console.log(events[i].shareable, events[i].id);
            var start_date = events[i].start;

            var end_date = events[i].end;
            console.log(moment(end_date).unix());
            console.log(moment(date).unix());
            //console.log(end_date);
            /*
            if (end_date == null){
              end_date = new Date (start_date) ;
              end_date.setHours ( start_date.getHours() + 2 );
            }
            if(moment(start_date).unix() == moment(date).unix()){
              console.log(moment(end_date).unix());
              console.log(moment(date).unix());
              //alert('this is not a shareable resource');
              return false;
            }
            /*if ( (moment(start_date).unix() <= moment(date).unix()) && (moment(end_date).unix() > moment(date).unix()) ){
              alert('this is not a shareable resource');
              return false;
            }

          }
          //console.log(date.add(3,'hours'));
          var originalEventObject = $(this).data('eventObject');
          var copiedEventObject = $.extend({}, originalEventObject);
          console.log(date);
          copiedEventObject.start = date;
          //copiedEventObject.end = date.add(2,'hours');
          copiedEventObject.title = originalEventObject.title+' ('+localStorage.getItem('subject_id')+')';
          copiedEventObject.backgroundColor = originalEventObject.backgroundColor;
          copiedEventObject.className = originalEventObject.backgroundColor;
          copiedEventObject.eventOverlap =  false;

          //console.log(originalEventObject);

          $('#fullcalendar').fullCalendar('renderEvent', copiedEventObject, true);
          //$(this).remove();

      }
  });*/
}

if($body=='detail-adm'){
  console.log(localStorage);
  var datacache;
  function isNumber(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
  }

  function getData(n){
    datacache = n;
  }

  $('#title').text(localStorage.getItem('subject_name')+' / '+localStorage.getItem('level_name')+ ' '+localStorage.getItem('letra'));
  $.ajax({
    url: ajax_url+'/v1/schedule/list-adm',
    dataType: 'json',
    data: {let: localStorage.getItem('letra'), sub: localStorage.getItem('subject_id')},
    type: 'post',
    async: false,
    xhrFields: {
     withCredentials: true
    },
    headers: {
     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    success: function(data, datacache){
      $.each(data, function(e,v){
        var day = v.hora_inicio.split(" ",1);
        var parseo = day[0].split("-"); 
        var fecha = parseo[2] + "/" + parseo[1];
        var htmltext = '<div style="cursor: pointer; "class="item eight wide row" data-type="'+v.type+'" data-pos="'+e+'" data-f="'+v.file+'" data-i="'+v.init_text+'" data-d="'+v.mid_text+'" data-e="'+v.end_text+'" data-r="'+v.res_text+'" data-ev="'+v.eval_text+'" data-ec="'+v.ec_text+'" data-desc="'+v.description+'" data-value="'+v.main_id+'" data-date="'+day+'">';
        if(v.type==0){
          htmltext += '<i class="large book left floated middle aligned icon"></i><div class="left floated content"><span class="header">'+(e+1)+'&nbsp;&nbsp;Clase del día '+fecha+'</span></span></div><i class="ui clearing right floated large remove icon"></i></div>';
        }
        if(v.type==1){
          htmltext += '<i class="large file powerpoint outline left floated middle aligned icon"></i><div class="left floated content"><span class="header">'+(e+1)+'&nbsp;&nbsp;Clase del día '+fecha+'</span></span></div><i class="ui clearing right floated large remove icon"></i></div>';
        }
        if(v.type==2){
          htmltext += '<i class="large users left floated middle aligned icon"></i><div class="left floated content"><span class="header">'+(e+1)+'&nbsp;&nbsp;Clase del día '+fecha+'</span></span></div><i class="ui clearing right floated large remove icon"></i></div>';
        }

        $('#selections').append('<div class="item" data-value="'+day+'">Clase del día '+fecha+'</div>');
        $('.classsort').append(htmltext);
        });
        var catcher = $('.classsort').html();
        getData(catcher);
        
    } 
  });

  $.ajax({
    url: ajax_url+'/v1/schedule/analit',
    dataType: 'json',
    data: {let: localStorage.getItem('letra'), sub: localStorage.getItem('subject_id')},
    type: 'post',
    xhrFields: {
     withCredentials: true
    },
    headers: {
     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    success: function(data){
      var htmltext1 = '<div class="ui one wide column"></div><div class="ui four wide column"><h1>Clases del año</h1><h1>'+ data.scheduletotal +'</h1></div><div class="ui four wide column"><h1>Clases planificadas</h1><h1>'+data.splan+'/'+data.scheduletotal+'</h1></div><div class="ui four wide column oa-info" style="cursor: pointer;"><h1>OA planificados</h1><h1>'+data.oaplan+'/'+data.oatotal+'</h1></div><div class="ui one wide column"></div>';
      $('#class-analitic').append(htmltext1);
      console.log(htmltext1);

    } 
  });

  $(document).on('click', '.oa-info', function(){
    $('#oas-info .content').empty();
    $.ajax({
      url: ajax_url+'/v1/schedule/extra-oa',
      dataType: 'json',
      data: {let: localStorage.getItem('letra'), sub: localStorage.getItem('subject_id')},
      type: 'post',
      xhrFields: {
       withCredentials: true
      },
      headers: {
       'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      success: function(datos){
        var oas = datos.oaseen.length;
        var nooas = datos.oaunseen.length;
        htmltext = '<div class="ui five wide column"><h1>OA Planificados: '+oas+'</h1><div id="planoa" class="ui relaxed divided list scrollable" style="height: 200px; overflow-y: auto;">';
        $.each(datos.oaseen, function(e,v){
          htmltext += '<div class="item" ><div class="content"><p class="header" style="text-align: left;">'+v.OA+': '+v.desc+'</p></div></div>';                           
        });
        htmltext += '</div></div><br><br>';
        htmltext += '<div class="ui five wide column"><h1>OA sin Planificar: '+nooas+'</h1><div id="plannooa" class="ui relaxed divided list scrollable" style="height: 200px; overflow-y: auto;">';
        $.each(datos.oaunseen, function(e,v){
          htmltext += '<div class="item" ><div class="content"><p class="header" style="text-align: left;">'+v.OA+': '+v.desc+'</p></div></div>';                                
        });
        htmltext += '</div></div>';
        $('#oas-info .content').append(htmltext);
        $('#oas-info').modal('show');
      }
    });
  });

  $(document).on('click','.remove.icon', function(){
    var idClass = $(this).parent().data('value');
    $('#class-secure').modal('show'); 
    $('#class-erase').on('click', idClass, function(){
      $.ajax({
        url:ajax_url+'/v1/schedule/deleteclass',
        data: {id: idClass},
        dataType: 'text',
        type: 'post',
        success: function(data){
          swal({ 
           title: "¡Genial!",
           text: "Tu clase ha sido eliminada de forma exitosa.",
           type: "success" 
          },
          function(){
            window.location.reload(true);
        });
          console.log(data);
        }
      });
    });
  });

  $(".classsort").sortable();
  $(document).on('click', '.classsort > .item' ,function(){
    $('#hided').hide();
    $('#nothided').show();
    $('.classsort > .item').not(this).css('background-color','white');    
    $(this).css('background-color','#e6e6e6');
    $('#class-info').empty();
    var file= $(this).data('f');
    var id = $(this).data('value');
    var desc = $(this).data('desc');
    var fecha = $(this).data('date');
    var parseo = fecha.split("-");
    var init = $(this).data('i');
    var mid= $(this).data('d');
    var end = $(this).data('e');
    var res = $(this).data('r');
    var ev = $(this).data('ev');
    var ec = $(this).data('ec');
    var type = $(this).data('type');
    var name_type = '';
    if(type == 0){
      name_type = 'Clase';
    }
    if(type == 1){
      name_type = 'Prueba';
    }
    if(type == 2){
      name_type = 'Actividad/Trabajo';
    }

    var fecha_real = parseo[2] + "/" + parseo[1] + "/" +parseo[0];
    $.ajax({
      url: ajax_url+'/v1/schedule/oa-list',
      dataType: 'json',
      data: {block: id},
      type: 'post',
      xhrFields: {
       withCredentials: true
      },
      headers: {
       'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      success: function(data){
        var htmltext = '';
        htmltext += '<p class="nomargin"><b>Tipo: </b>'+name_type+'</p>';
        htmltext += '<p class="nomargin"><b>Fecha: </b>'+ fecha_real +'</p><br>'; 
        htmltext += '<p>';
        if(data.length > 0){
          htmltext+='<p style="font-size: 1.2rem; margin: 0 0 0 0;"><b>'+ data[1]['name'] +'</b></p><p style="font-size: 1.1rem; margin: 0 0 0 0;"><b>Objetivos de Aprendizaje:</b></p>';
          $.each(data[0], function(e,v){
            htmltext+='<p style="font-size: 1.1rem; margin: 0 0 0 0;"><b>'+v.name+'</b>:'+v.text+'</p>';
          });
          htmltext += '<br>';
        }
        else{
          htmltext+='<p style="font-size: 1.1rem;"><b>Objetivos de Aprendizaje:</b> Ninguno</p>';
        }
        if(data[3]){
          htmltext += '<p style="font-size: 1.1rem; margin: 0 0 0 0;"><b>Actitudes</b></p>';
          $.each(data[3], function(e,v){
            htmltext += '<p style="font-size: 1.1rem; margin: 0 0 0 0;">'+v.desc+'</p>';
          });
          htmltext += '<br>';
        }
        if(data[2]){
          htmltext += '<p style="font-size: 1.1rem; margin: 0 0 0 0;"><b>Habilidades</b></p>';
          $.each(data[2], function(e,v){
            htmltext += '<p style="font-size: 1.1rem; margin: 0 0 0 0;">'+v.desc+'</p>';
          });
          htmltext += '<br>';
        }
        if(data[4]){
          htmltext+= '<p style="font-size: 1.1rem; margin: 0 0 0 0;"><b>Palabras Clave: </b>';
          $.each(data[4], function(e,v){
            if(e < data[4].length-1){
                htmltext += v.name+', ';
              }
            else{
              htmltext += v.name+'.';
            }
          });
          htmltext += '</p><br>';
        }
        if(desc){
          if( desc.length > 0 ){
            htmltext += '<p style="font-size: 1.1rem; margin: 0 0 0 0;"><b>Descripción:</b></p><p style="font-size: 1.1rem; margin: 0 0 0 0;">'+ desc.replace(/(\n)+/g, '<br />') +'</p><br>';
          }
          else{
            htmltext += '<p style="font-size: 1.1rem; margin: 0 0 0 0;"><b>Descripción</p></b><p style="font-size: 1.1rem; margin: 0 0 0 0;">Ninguna</p><br>';                  
          }
        }
        else{
            htmltext += '<p style="font-size: 1.1rem; margin: 0 0 0 0;"><b>Descripción</p></b><p style="font-size: 1.1rem; margin: 0 0 0 0;">Ninguna</p><br>';                            
        }
        if(init || mid || end || res || ev || ec){
          if(init.length > 0 || mid.length > 0 || end.length > 0 || res.length > 0 || ev.length > 0 || ec.length){
            htmltext +='<p style="font-size: 1.1rem; margin: 0 0 0 0;"><b>Actividades de Aprendizaje</p></b>'; 
          }
          if(init){
            if(init.length > 0){
              htmltext += '<p style="font-size: 1.1rem; margin: 0 0 0 0;"><b>Inicio</b></p><p style="font-size: 1.1rem; margin: 0 0 0 0;">'+init.replace(/(\n)+/g, '<br />')+'</p><br>';
            }
          }
          if(mid){
            if(mid.length > 0){
              htmltext += '<p style="font-size: 1.1rem; margin: 0 0 0 0;"><b>Desarrollo</b></p><p style="font-size: 1.1rem; margin: 0 0 0 0;">'+mid.replace(/(\n)+/g, '<br />')+'</p><br>';
            }
          }
          if(end){
            if(end.length > 0){
              htmltext += '<p style="font-size: 1.1rem; margin: 0 0 0 0;"><b>Cierre</b></p><p style="font-size: 1.1rem; margin: 0 0 0 0;">'+end.replace(/(\n)+/g, '<br />')+'</p><br>';
            }
          }
          if(res){
            if(res.length > 0){
              htmltext += '<p style="font-size: 1.1rem; margin: 0 0 0 0;"><b>Recursos de Aprendizaje</b></p><p style="font-size: 1.1rem; margin: 0 0 0 0;">'+res.replace(/(\n)+/g, '<br />')+'</p><br>';
            }
          }
          if(ev){
            if(ev.length > 0){
              htmltext += '<p style="font-size: 1.1rem; margin: 0 0 0 0;"><b>Evaluación</b></p><p style="font-size: 1.1rem; margin: 0 0 0 0;">'+ev.replace(/(\n)+/g, '<br />')+'</p><br>';
            }
          }
          if(ec){
            if(ec.length > 0){
              htmltext += '<p style="font-size: 1.1rem; margin: 0 0 0 0;"><b>Adecuación Curricular</b></p><p style="font-size: 1.1rem; margin: 0 0 0 0;">'+ec.replace(/(\n)+/g, '<br />')+'</p><br>';
            }
          }
        }
        if(file){
          if(file.length > 0){
            htmltext += '<p style="font-size: 1.1rem; margin: 0 0 0 0;"><b>Archivos Adjuntos: </b></p><a style="font-size: 1.1rem; margin: 0 0 0 0;" href="http://app.poptimize.cl/uploads/'+file+'" target="_blank">'+file+'</a><br>';
          }
        }


        $('#class-info').append(htmltext);
      }

    });


  });

  $(document).on('click','#reset', function(){
    $('#hola').children('.classsort').html(datacache);
    $('.classsort').sortable();
  });

  $('#save-order').on('click', function(){
    console.log(localStorage.getItem('letra'));
    console.log(localStorage.getItem('subject_id'));
    var schedules = [];
    $('.classsort > .item').each(function(e,v){
      var arrData = {position: e, date: $(this).data('date'), id: $(this).data('value')};
      schedules.push(arrData);
    });
    $.ajax({
      url: ajax_url+'/v1/schedule/update-schedule',
      dataType: 'text',
      data: {let: localStorage.getItem('letra'), sub: localStorage.getItem('subject_id'), schedules: schedules},
      type: 'post',
      xhrFields: {
        withCredentials: true
      },
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      success: function(data){
        console.log(data);
        swal({ 
           title: "¡Genial!",
           text: "El orden de tus clases fue modificado de forma exitosa.",
           type: "success" 
          },
          function(){
            window.location.reload(true);
        });
      },
      error: function(jqXHR, textStatus, errorThrown){
              alert(errorThrown);
      }    
    });

  });

  $('#to-courses').on('click', function(){
    window.location = '/courses'+extension;    
  });

  $('#refrescar').on('click', function(){
    window.location.reload(true);
  });

  $('.event-move').on('click', function(){
    console.log($('#classsel').dropdown('get value'));
    var fecha = $('#classsel').dropdown('get value');
    var cantidad = $('#quantsel').dropdown('get value');
    console.log(cantidad);
    console.log(localStorage.getItem('letra'));
    console.log(localStorage.getItem('subject_id'));
    if(fecha && isNumber(cantidad)){
      $.ajax({
        url: ajax_url+'/v1/schedule/reschedule',
        dataType: 'text',
        data: {let: localStorage.getItem('letra'), subject_id: localStorage.getItem('subject_id'), quant: cantidad, date: fecha},
        type: 'post',
        xhrFields: {
         withCredentials: true
        },
        headers: {
         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function(data){
          console.log(data);
          if(data == 'ok'){
            $('#exitoso').modal('show');
          }
          else{
            $('#fracaso').modal('show');
          }
        },
        error: function(jqXHR, textStatus, errorThrown){
              alert(errorThrown);
        }    
      });
    }

  });

  $('#move-courses').on('click', function(){
    $('.move-modal').modal('show');
  });

}

if($body=='courses-adm'){

  $(document).on('click','.school-course.link', function(){
    localStorage.setItem('subject_id',$(this).attr('data-subject'));
    localStorage.setItem('level',$(this).attr('data-level'));
    localStorage.setItem('letra',$(this).attr('data-letter'));
    localStorage.setItem('school',$(this).attr('data-school'));
    localStorage.setItem('subject_name', $(this).attr('data-asign-name'));
    localStorage.setItem('level_name', $(this).attr('data-course'));
    window.location = '/detail-adm'+extension;
  });

  $.ajax({
    url:ajax_url+'/v1/courses/list',
    data: {id: localStorage.getItem('id')},
    dataType: 'json',
    type: 'post',
    beforeSend: function(){
      $('#loader').modal('show');
    },
    success: function(data){
      $('#loader').modal('hide');
      ////console.log(data);
      $html_schools = '';
      $html_level = '';
      $html_letters = '';
      $html_subjects = '';
      $html_regions = '';
      $html_commune = '';
      $.each(data.compact.region, function(e,v){
        $html_regions += '<div class="item" data-id="'+v.id+'" data-value="'+v.name+'"> '+v.name+'</div>';
      });
      $.each(data.compact.comuna, function(e,v){
        //console.log(v.region_id);
        //if(v.id_region===selected_region){
          $html_commune += '<div style="display: none;" class="item" data-id="'+v.id+'" data-value="'+v.name+'" data-region="'+v.region_id+'"> '+v.name+'</div>';
        //}

      });
      $.each(data.schools, function(e,v){
        $html_schools += '<div class="item" data-id="'+e+'" data-value="'+v+'"><i class="book icon"></i> '+v+'</div>';
      });
      $.each(data.courses, function(e,v){
        $html_level += '<div class="item" data-value="'+v.name+'" data-id="'+v.id+'"><i class="book icon"></i> '+v.name+'</div>';
      });
      $.each(data.letters, function(e,v){
        $html_letters += '<div class="item" data-value="'+v.letter+'"><i class="book icon"></i> '+v.letter+'</div>';
      });
      $.each(data.subjects, function(e,v){
        ////console.log(v);
        $html_subjects += '<div class="item" data-value="'+v+'" data-id="'+e+'" data-name="'+v+'"><i class="book icon"></i> '+v+'</div>';
      });
      $('#schools-dropdown').html($html_schools);
      $('#level-dropdown').html($html_level);
      $('#letters-dropdown').html($html_letters);
      $('#subjects-dropdown').html($html_subjects);
      $('#region-dropdown').html($html_regions);
      //$('#commune-dropdown').html($html_commune);
      if(data.status=='no hay'){
        $('#courses-list').html('<br><h1>En este momento no tienes cursos registrados</h1><br><br>');
      }
      if(data.status=='hay'){
        $html = '';
        $.each(data.data, function(e,v,i){
          if(e < 1){
            $html += '<tr class="'+v.id+'">';
            $html += '<td>';
              $html += '<div class="ui ribbon label" style="background:#38c2c8;"><a style="color: #fff !important;font-size:14px; opacity: 1;" href="javascript:;" data-course="'+v.curso+'" data-school="'+v.school_name+'" data-letter="'+v.letter+'" data-subject="'+v.asignatura_id+'" data-content="" data-level="'+v.cid+'" data-asign-name="'+v.asignatura+'" class="school-course link">'+v.school_name+'</a></div>';
            $html += '</td>';
            $html += '<td>'+v.curso+' <span>'+v.letter+'</span></td>';
            $html += '<td>'+v.asignatura+'</td>';
          $html += '</tr>';
        }

          else{

          $html += '<tr class="'+v.id+'">';
            $html += '<td>';
              $html += '<div class="ui ribbon label" style="background:#38c2c8;"><a style="color: #fff !important;font-size:14px; opacity: 1;" href="javascript:;" data-course="'+v.curso+'" data-asign-name="'+v.asignatura+'" data-school="'+v.school_name+'" data-letter="'+v.letter+'" data-subject="'+v.asignatura_id+'" data-content="" data-level="'+v.cid+'" class="school-course link">'+v.school_name+'</a></div>';
            $html += '</td>';
            $html += '<td>'+v.curso+' <span>'+v.letter+'</span></td>';
            $html += '<td>'+v.asignatura+'</td>';
          $html += '</tr>';

          }

        });
        $('#courses-list tbody').html($html);
        $('.ui.text.menu .ui.dropdown.item').dropdown({
          message: {
            addResult     : 'Add <b>{term}</b>',
            count         : '{count} selected',
            maxSelections : 'Max {maxCount} selections',
            noResults     : 'Sin resultados.'
          }
        });
      }
    }
  });

}

if($body=='courses'){

  $(document).on('click','.school-course.link', function(){
    localStorage.setItem('subject_id',$(this).attr('data-subject'));
    localStorage.setItem('level',$(this).attr('data-level'));
    localStorage.setItem('letra',$(this).attr('data-letter'));
    localStorage.setItem('school',$(this).attr('data-school'));
    window.location = '/units'+extension;
  });

  $(document).on('click', '.item.delete', function(){
    $tr = $(this).closest('tr');
    var $datos = $(this);
    console.log($datos.data());
    console.log($datos.attr('data-subject'));
    console.log($datos.attr('data-school'));
    $('#confirm-erase').modal('show');
    $('#erase-course-btn').on('click', $datos, function(){
         $.ajax({
      url:ajax_url+'/v1/courses/delete',
      data: {id: $datos.attr('data-id'), course_id: $datos.attr('data-level'), subject_id: $datos.attr('data-subject'), school_name: $datos.attr('data-school')},
      dataType: 'json',
      type: 'post',
      success: function(data){
        $tr.remove();
      },
      error: function(){
        swal({
          title: "Error :(",
          text: 'Hubo un error al momento de realizar su solicitud',
          type: "error",
          confirmButtonText: "Continuar"
        });
      }
    });

    });
 
  });

  var selected_region = false;
  $(document).on('click','#region-dropdown .item',function(){
    $html_commune_ = '';
    $('.ui.dropdown').dropdown('refresh');
    selected_region = $(this).data('id');
    //alert($html_commune.length);
    var $len = 0;
    $($html_commune).each(function(e,v){
      if($(v).data('region')===selected_region){
        $len++;
        //console.log($(v).data());
        $html_commune_ += '<div class="item" data-id="'+$(v).data('id')+'" data-value="'+$(v).text()+'" data-region="'+$(v).data('region')+'"> '+$(v).text()+'</div>';
      }
    });
    //alert($len);
    console.log($html_commune_);
    $('#commune-dropdown').html($html_commune_);
  });

  /*$(document).on('click','#commune-dropdown .item',function(){
    $le_id = $(this).data('id');
    var a = [];
    $.get(ajax_url+'/v1/courses/list', function(data){
      console.log(data);
      //$('#loader').modal('show');
      $.each(data, function(e,v){
          $.each(v.school_formated, function(e,v){
            if($le_id === v.commune){
              a.push(v);
            }
          });
      });
      //$('#loader').modal('hide');
      console.log(a);
      $(".typehead").typeahead({
        source:a,
        afterSelect: function(data){
          console.log(data);
          $('.typehead').attr('data-id',data.id);
        }
      });
        //console.log(data.compact.school_formated)
      },'json');
  });*/

  $(document).on('click', '.edit', function(){
    $('.level').dropdown('set selected',$(this).attr('data-course'));
    $('.letter').dropdown('set selected',$(this).attr('data-letter'));
    $('.subject').dropdown('set selected',$(this).attr('data-subject'));
    $('.add-course-modal').modal('show');
  });

  /*$.get(ajax_url+'/v1/courses/list', function(data){
    $(".typehead").typeahead({
      source:data.compact.school_formated,
      afterSelect: function(data){
        console.log(data);
        $('.typehead').attr('data-id',data.id);
      }
    });
      //console.log(data.compact.school_formated)
    },'json');*/

  $.ajax({
    url:ajax_url+'/v1/courses/list',
    data: {id: localStorage.getItem('id')},
    dataType: 'json',
    type: 'post',
    beforeSend: function(){
      $('#loader').modal('show');
    },
    success: function(data){
      $('#loader').modal('hide');
      ////console.log(data);
      $html_schools = '';
      $html_letters = '';
      $html_subjects = '';
      $html_regions = '';
      $html_commune = '';
      $.each(data.compact.region, function(e,v){
        $html_regions += '<div class="item" data-id="'+v.id+'" data-value="'+v.name+'"> '+v.name+'</div>';
      });
      $.each(data.compact.comuna, function(e,v){
        //console.log(v.region_id);
        //if(v.id_region===selected_region){
          $html_commune += '<div style="display: none;" class="item" data-id="'+v.id+'" data-value="'+v.name+'" data-region="'+v.region_id+'"> '+v.name+'</div>';
        //}

      });
      $.each(data.schools, function(e,v){
        $html_schools += '<div class="item" data-id="'+e+'" data-value="'+v+'"><i class="book icon"></i> '+v+'</div>';
      });

      $.each(data.letters, function(e,v){
        $html_letters += '<div class="item" data-value="'+v.letter+'"><i class="book icon"></i> '+v.letter+'</div>';
      });
      $.each(data.subjects, function(e,v){
        ////console.log(v);
        $html_subjects += '<div class="item" data-value="'+v+'" data-id="'+e+'" data-name="'+v+'"><i class="book icon"></i> '+v+'</div>';
      });
      $('#schools-dropdown').html($html_schools);
      $('#letters-dropdown').html($html_letters);
      $('#subjects-dropdown').html($html_subjects);
      $('#region-dropdown').html($html_regions);
      //$('#commune-dropdown').html($html_commune);
      if(data.status=='no hay'){
        // $('#courses-list').html('<br><h1>En este momento no tienes cursos registrados</h1><br><br>');
        $('.add-course-modal').modal({ detachable:false, observeChanges:true }).modal('show').modal('refresh');
      }
      if(data.status=='hay'){
        $html = '';
        $.each(data.data, function(e,v,i){
          if(e < 1){
            $html += '<tr class="'+v.id+'">';
            $html += '<td>';
              $html += '<div class="ui ribbon label" style="background:#38c2c8;"><a style="color: #fff !important;font-size:14px; opacity: 1;" href="javascript:;" data-school="'+v.school_name+'" data-letter="'+v.letter+'" data-subject="'+v.asignatura_id+'"  data-level="'+v.cid+'" class="school-course link" data-content="">'+v.school_name+'</a></div>';
            $html += '</td>';
            $html += '<td>'+v.curso+' <span>'+v.letter+'</span></td>';
            $html += '<td>'+v.asignatura+'</td>';
            $html += '<td>';
                // $html += '<div class="ui dropdown item">';
                //   $html += '<i class="write icon"></i> Acciones';
                //   $html += '<i class="dropdown icon"></i>';
                //   $html += '<div class="menu">';
                    //$html += '<div class="item edit" data-course="'+v.curso+'" data-letter="'+v.letter+'" data-subject="'+v.asignatura+'">Editar</div>';
                    $html += '<div class="item centered">Curso Gratis!</div>';
                $html += '</div>';
              $html += '</div>';
            $html += '</td>';
          $html += '</tr>';
        }

          else{

          $html += '<tr class="'+v.id+'">';
            $html += '<td>';
              $html += '<div class="ui ribbon label" style="background:#38c2c8;"><a style="color: #fff !important;font-size:14px; opacity: 1;" href="javascript:;" data-school="'+v.school_name+'" data-letter="'+v.letter+'" data-subject="'+v.asignatura_id+'" data-content="" data-level="'+v.cid+'" class="school-course link">'+v.school_name+'</a></div>';
            $html += '</td>';
            $html += '<td>'+v.curso+' <span>'+v.letter+'</span></td>';
            $html += '<td>'+v.asignatura+'</td>';
            $html += '<td>';
                // $html += '<div class="ui dropdown item">';
                //   $html += '<i class="write icon"></i> Acciones';
                //   $html += '<i class="dropdown icon"></i>';
                //   $html += '<div class="menu">';
                    //$html += '<div class="item edit" data-course="'+v.curso+'" data-letter="'+v.letter+'" data-subject="'+v.asignatura+'">Editar</div>';
                    $html += '<div style="cursor: pointer;" class="item delete centered" data-level="'+v.cid+'" data-level="'+v.cid+'" data-school="'+v.school_name+'" data-id="'+v.id+'" data-subject="'+v.asignatura_id+'">Eliminar</div>';
                $html += '</div>';
              $html += '</div>';
            $html += '</td>';
          $html += '</tr>';

          }

        });
        $('#courses-list tbody').html($html);
        $('.ui.text.menu .ui.dropdown.item').dropdown({
          message: {
            addResult     : 'Add <b>{term}</b>',
            count         : '{count} selected',
            maxSelections : 'Max {maxCount} selections',
            noResults     : 'Sin resultados.'
          }
        });
      }
    }
  });
}

if($body=='events-calendar'){

  console.log(localStorage);
  $(document).on('click','.remove.icon.archivo',function(){
    var yo = $(this);
    var id = $(this).closest('.class-modal').data('id');        
    $.ajax({
      url: ajax_url+'/v1/schedule/erase-file',
      dataType: 'text', 
      type: 'post',
      data: {id: id},
        xhrFields: {
        withCredentials: true
       },
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      success: function(datos){
        yo.parent().remove()
      }
    });

  });

  $('#test1').on('click',function(){
    $('.plan-modal').modal('show');
    console.log(localStorage);
  });

  $('.plan-print').on('click',function(){
        $impselect = $('input[name=imp]:checked').val();
        if($impselect == "0"){
          var fecha = moment($('#fecha1').datepicker('getDate')).format('YYYY-MM-DD');
          console.log(fecha);
          var sub = localStorage.getItem('subject_id');
          var letra = localStorage.getItem('letra');
          $.ajax({
            url: ajax_url+'/v1/schedule/plan_data',
           dataType: 'text',
            type: 'post',
            data: {date: fecha, subject_id: sub, let: letra},
            xhrFields: {
              withCredentials: true
            },
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(datos){
            console.log(datos);
            var url=ajax_url+'/v1/report/planification/print/'+datos;
            console.log(url);
                if(!datos){
                  $('#no-plan').modal('show');
                }
                else{
                  window.open(url, '_blank');
                }
                
            },
            error: function(jqXHR, textStatus, errorThrown){
              alert(errorThrown);
            }        
          });

        }
        if($impselect == "1"){
          var month = $('#monthsel').dropdown('get value');
          var sub = localStorage.getItem('subject_id');
          var letra = localStorage.getItem('letra');
          if(month){
              var url = ajax_url+'/v1/report/planification/month?subject_id='+sub+'&letra='+letra+'&mes='+month;
              window.open(url, '_blank');
          }
          
        }
        if($impselect == "2"){
            var course = localStorage.getItem('level');
            var sub = localStorage.getItem('subject_id');
            console.log(course);
            console.log(sub);
            var url = ajax_url+'/v1/report/planification/full?course_id='+course+'&subject_id='+sub;
            window.open(url, '_blank');          
        }
    
  });

  $('#test').click(function(){
    /*$.ajax({
      url: ajax_url+'/v1/report/calendar/print/'+$('#fullcalendar-events').fullCalendar( 'getDate' ).format('M')+'?letter='+localStorage.getItem('letra')+'&level='+localStorage.getItem('level_name')+'&subject_id='+localStorage.getItem('subject_id'),
      dataType: 'json',
      type: 'post',
      xhrFields: {
        withCredentials: true
      },
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      success: function(data){
        if(data.code===500){
          swal({
            title: "No hay datos para generar el documento",
            text: "Por favor intenta cambiar el mes.",
            type: "error",
            confirmButtonText: "Continuar"
          });
        }else{
          var url = ajax_url+'/v1/report/calendar/print/'+$('#fullcalendar-events').fullCalendar( 'getDate' ).format('M')+'?letter='+localStorage.getItem('letra')+'&level='+localStorage.getItem('level_name')+'&subject_id='+localStorage.getItem('subject_id');
          console.log(url);
          window.open(url, '_blank');
        }
      }
    });*/
    var url = ajax_url+'/v1/report/calendar/print/'+$('#fullcalendar-events').fullCalendar( 'getDate' ).format('M')+'?letter='+localStorage.getItem('letra')+'&level='+localStorage.getItem('level_name')+'&subject_id='+localStorage.getItem('subject_id');
    console.log(url);
    window.open(url, '_blank');
   /*var old = $('.fc-center h2').html();
   $('.fc-center h2').html($('.ui.dividing.header').html().split('/')[0]+'<br>'+$('.ui.dividing.header').html().split('/')[1]+'<br>'+$('.fc-center h2').text());
   $('.fc-left').hide();
   $('.fc-right').hide();
   html2canvas($('#fullcalendar-events'), {
     logging: true,
     useCORS: true,
     onrendered: function (canvas) {
        var imgData = canvas.toDataURL("image/jpeg");
        var doc = new jsPDF('l');
        doc.addImage(imgData, 'JPEG', 15, 15, 260, 180);
        download(doc.output(), "AEFC.pdf", "text/pdf");

     }
  }) ;
  $('.fc-center h2').html(old);
  $('.fc-left').show();
  $('.fc-right').show();*/
 });

  $(document).on('click','div.color',function(){
    console.log($('.ui.popup.top.left.transition.visible').remove());
    $('.popup').popup('hide');
  });

  $('input.timepicker2').timepicker({minTime: '8:00 am', maxTime: '8:00 pm'});

  $(document).on('change', '#file2', function(){
    var size = $('#file2')[0].files[0].size;
    var formData = new FormData($('#addon')[0]);
    if(size < 5242880){
      $.ajax({
       url: ajax_url+'/v1/schedule/file-upload',
       type: 'POST',
       data: formData,
       xhrFields: {
          withCredentials: true
        },
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
       async: false,
       cache: false,
       contentType: false,
       enctype: 'multipart/form-data',
       processData: false,
       success: function (data) {
         console.log(data.code);
         if(data.code == 'error'){
          swal({
              title: 'Ups :(',
              text: 'Tu archivo pesa más de 5MB',
              type: 'error',
              confirmButtonText: "Continuar",
          });
          $('#file2').val(null);

         }
         else{
          swal({
              title: '¡Listo!',
              text: 'Tu documento se guardo de manera exitosa',
              type: 'success',
              confirmButtonText: "Continuar",
          });
          $('.attached ul').html('');
          $('.attached ul').append('<li><a style="text-overflow: ellipsis;" target="_blank" href="'+data.file[0].url+'">'+data.file[0].name+'</a><i style="cursor:pointer;" class="archivo remove icon"></i></li>');
          $('#file2').val(null);
         }
       }
     });
    }
    else{
          swal({
              title: 'Ups :(',
              text: 'Tu archivo pesa más de 5MB',
              type: 'error',
              confirmButtonText: "Continuar",
          });
          $('#file2').val(null);
    }
  });

  $('body').on('click', 'button.fc-next-button', function() {
    //console.log($('#fullcalendar-events').fullCalendar('clientEvents'));
    //alert(1);

  });

  $(document).on('click', '.le_first input', function(){
    $this1 = $(this);
    console.log($this1.is(':checked'));
    //console.log('----'+$(this).data('name')+'----')
    $('.added-item').each(function(){
      if($this1.data('name')==$(this).text() && $this1.is(':checked')){
        $(this).remove();
        //$('.fc-time-grid-event').append('<div class="in-event with-popover  color c1 visible " data-content="Explicar el proceso de hominización, reconociendo las principales etapas de la evolución de la especie humana.">OA 1 </div>');
        var block = $('.fc-time-grid-event');
        //$(block).append('<div class="in-event with-popover  color c1 visible " data-content="Explicar el proceso de hominización, reconociendo las principales etapas de la evolución de la especie humana.">OA 1 </div>');
      }
    });
  });


  $('#go-to-start').click(function(){
    window.location = '/courses'+extension;
  });

$(document).on('change','.le_three input[type=checkbox]', function(){
    var arr = [];
    $(this).parent().parent().find('input[type=checkbox]').each(function(){
      //this.checked ? arr.push({tag: })
      arr.push({ability: this.checked ? $(this).attr('data-id') : ""});
      //alert(sThisVal); // acá va
    });
    console.log(arr);

    $.ajax({
      url: ajax_url+'/v1/abilities/add',
      dataType: 'json',
      type: 'post',
      xhrFields: {
        withCredentials: true
      },
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      data: {id: $('.class-modal').attr('data-id'),data: arr, unit: $('.item.active.selected').attr('data-value'), subject: localStorage.getItem('subject_id') },
      success: function(data_){
        //$("#fullcalendar-events").find("[data-id='" + $('.class-modal').attr('data-id') + "']").find('.in-event').remove();
        //$.each(data_, function(e,v){
        //    console.log($("#fullcalendar-events").find("[data-id='" + $('.class-modal').attr('data-id') + "']").append('<div class="in-event with-popover  color c1 visible " data-content="">'+v+'</div>'));
        //});

        //$(block).append('<div class="in-event with-popover  color c1 visible " data-content="Explicar el proceso de hominización, reconociendo las principales etapas de la evolución de la especie humana.">OA 1 </div>');
      }
    });
  });

$(document).on('change','.le_four input[type=checkbox]', function(){
    var arr = [];
    $(this).parent().parent().find('input[type=checkbox]').each(function(){
      //this.checked ? arr.push({tag: })
      arr.push({attitude: this.checked ? $(this).attr('data-id') : ""});
      //alert(sThisVal); // acá va
    });
    console.log(arr);

    $.ajax({
      url: ajax_url+'/v1/attitudes/add',
      dataType: 'json',
      type: 'post',
      xhrFields: {
        withCredentials: true
      },
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      data: {id: $('.class-modal').attr('data-id'),data: arr, unit: $('.item.active.selected').attr('data-value'), subject: localStorage.getItem('subject_id') },
      success: function(data_){
        //$("#fullcalendar-events").find("[data-id='" + $('.class-modal').attr('data-id') + "']").find('.in-event').remove();
        //$.each(data_, function(e,v){
        //    console.log($("#fullcalendar-events").find("[data-id='" + $('.class-modal').attr('data-id') + "']").append('<div class="in-event with-popover  color c1 visible " data-content="">'+v+'</div>'));
        //});

        //$(block).append('<div class="in-event with-popover  color c1 visible " data-content="Explicar el proceso de hominización, reconociendo las principales etapas de la evolución de la especie humana.">OA 1 </div>');
      }
    });
  });

  $(document).on('change','.le_two input[type=checkbox]', function(){
    var arr = [];
    $(this).parent().parent().find('input[type=checkbox]').each(function(){
      //this.checked ? arr.push({tag: })
      arr.push({tag: this.checked ? $(this).attr('data-id') : "", text: $(this).attr('data-name')});
      //alert(sThisVal); // acá va
    });
    console.log(arr);

    $.ajax({
      url: ajax_url+'/v1/tags/add',
      dataType: 'text',
      type: 'post',
      xhrFields: {
        withCredentials: true
      },
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      data: {id: $('.class-modal').attr('data-id'),data: arr, unit: $('.item.active.selected').attr('data-value'), subject: localStorage.getItem('subject_id') },
      success: function(data_){
        console.log(data_)
        //$("#fullcalendar-events").find("[data-id='" + $('.class-modal').attr('data-id') + "']").find('.in-event').remove();
        //$.each(data_, function(e,v){
        //    console.log($("#fullcalendar-events").find("[data-id='" + $('.class-modal').attr('data-id') + "']").append('<div class="in-event with-popover  color c1 visible " data-content="">'+v+'</div>'));
        //});

        //$(block).append('<div class="in-event with-popover  color c1 visible " data-content="Explicar el proceso de hominización, reconociendo las principales etapas de la evolución de la especie humana.">OA 1 </div>');
      }
    });

    /*if($(this).is(':checked')){
      //alert($(this).attr('data-name'));
      //alert(1);
    }else{
      $('.added-blocks').append($(this).attr('data-original-html'));
      $('.with-popover').each(function(){
        $(this).draggable({
           zIndex: 999,
           revert: true,
           revertDuration: 0
         });
       });*/
      /*$('.colorpicker').click(function(){
        moveEvents($('.added-item').data('hex'));

        $('.with-popover').popup();

        $this = $(this);
        $this.popup({
          on: 'click',
          onCreate: function(){
            $original_class = $this.parent();
            $(document).on('click', '.content div.color', function(){
              $new_class = 'ui button added-item' + ' ' + $(this).attr('class');
              $this.parent().attr('class',$new_class);
            });
          }
        });
        $(document).on('click','.fc-time-grid-event', function(){
          //alert(1);
          var oa = $(this).find('.in-event');
          console.log(oa);
          //$('input[type=checkbox]').removeAttr('checked')
          $.each(oa, function(i,v){
            console.log($(v).text());
            ////alert($(v).text());
            $("body").find("[data-name='"+$(v).text()+"']").prop('checked',true);
          });
        });
      });*/
      ////alert($(this).attr('data-original-html'));
    //}
  });

  /////////////////////

  $(document).on('change','.le_first input[type=checkbox]', function(){
    var arr = [];
    $(this).parent().parent().find('input[type=checkbox]').each(function(){
      arr.push({oa: this.checked ? $(this).attr('data-id') : "", text: $(this).attr('data-name')});
      //alert(sThisVal); // acá va
    });
    console.log({id: $('.class-modal').attr('data-id'),data: arr});

    $.ajax({
      url: ajax_url+'/v1/objectives/update',
      dataType: 'json',
      type: 'post',
      xhrFields: {
        withCredentials: true
      },
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      data: {id: $('.class-modal').attr('data-id'),data: arr, unit: $('.item.active.selected').attr('data-value'), subject: localStorage.getItem('subject_id') },
      success: function(data_){
        //alert($('.class-modal').attr('data-id'));
        console.log($("#fullcalendar-events").find("[data-id='" + $('.class-modal').attr('data-id') + "']").find('.in-event').remove());
        $.each(data_, function(e,v){
          console.log(v);
            console.log($("#fullcalendar-events").find("[data-id='" + $('.class-modal').attr('data-id') + "']").append('<div class="in-event with-popover ui-droppable" data-content="">'+v+'</div>'));
        });

        //$(block).append('<div class="in-event with-popover  color c1 visible " data-content="Explicar el proceso de hominización, reconociendo las principales etapas de la evolución de la especie humana.">OA 1 </div>');
      }
    });

    if($(this).is(':checked')){
      //alert($(this).attr('data-name'));
      //alert(1);
    }else{
      var enter_id = $(this).data('id');
      var flag = 0;
      $('.added-blocks .added-item').each(function(e,v){
        if($(this).data('id') == enter_id){
          flag = 1;
        }
      });
      if(flag == 0){
      $('.added-blocks').append($(this).attr('data-original-html'));
      var $wrapper = $('.added-blocks');
      $wrapper.find('.ui-draggable-handle').sort(function(a,b){
        return +a.dataset.id - +b.dataset.id;
      }).appendTo($wrapper);
      $('.with-popover').each(function(){
        $(this).draggable({
           zIndex: 999,
           revert: true,
           revertDuration: 0
         });
       });
      moveEvents($('.added-item').data('hex'));

        $('.with-popover').popup();

      $('.colorpicker').click(function(){
        

        $this = $(this);
        $this.popup({
          on: 'click',
          onCreate: function(){
            $original_class = $this.parent();
            $(document).on('click', '.content div.color', function(){
              $new_class = 'ui button added-item' + ' ' + $(this).attr('class');
              $this.parent().attr('class',$new_class);
            });
          }
        });
        $(document).on('click','.fc-time-grid-event', function(){
          //alert(1);
          var oa = $(this).find('.in-event');
          console.log(oa);
          //$('input[type=checkbox]').removeAttr('checked')
          $.each(oa, function(i,v){
            console.log($(v).text());
            ////alert($(v).text());
            $("body").find("[data-name='"+$(v).text()+"']").prop('checked',true);
          });
        });
      });
      }
    }
  });

  var entityMap = {
    "&": "&amp;",
    "<": "&lt;",
    ">": "&gt;",
    '"': '&quot;',
    "'": '&#39;',
    "/": '&#x2F;'
  };

  function escapeHtml(string) {
    return String(string).replace(/[&<>"'\/]/g, function (s) {
      return entityMap[s];
    });
  }

  $( ".datepicker" ).datepicker({
    maxDate: new Date(2017, 12, 31),
    dateFormat: "d-mm-yy"
  });

  $('#unitssel').click(function(){
    var value = $('#unitssel').dropdown('get value');
    $.ajax({
      url: ajax_url+'/v1/units/show/'+value,
      dataType: 'json',
      type: 'post',
      xhrFields: {
        withCredentials: true
      },
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      success: function(data_){
        $html = '';
        $html_2 = '';
        $html_2_first = '';
        $html_2_second = '';
        $html_2_three = '';
        $html_2_four = '';
        $custom_ = '<div class="ui grid"><div class="eight wide computer column">1</div><div class="eight wide computer column">2</div></div>';
        /*if(data_.oas.length > 10){
          $hm = 0;
          $.each(data_.oas, function(e,v){
            if($hm == 0){
              $html_div = '<div class="ui grid"><div class="eight wide computer column">';
            }
            if($hm > 0 && $hm < 6){
              $html_div += '<div class="ui button added-item with-popover" data-content="'+v.description_min+'" data-title="'+v.name_eje+'" data-current-color="orange" data-hex="" data-subject="Clase de Historia" >OA '+v.num_obj+' <img data-html="<div class=\'header\'>Selecciona el color</div><div class=\'content\'><div class=\'color c1\'></div><div class=\'color c2\'></div><div class=\'color c3\'></div><div class=\'color c4\'></div><div class=\'color c5\'></div><div class=\'color c6\'></div><br><div class=\'color c7\'></div><div class=\'color c8\'></div><div class=\'color c9\'></div><div class=\'color c10\'></div><div class=\'color c11\'></div><div class=\'color c12\'></div></div>" class="colorpicker" src="../dist/img/colorpicker.png" alt="" style="margin-top: -4px;position: relative;float: right;"></div>';
            }
            if($hm==5){
              $html_div += '</div>';
            }if($hm >= 6){
              $html_div += '<div class="eight wide computer column">';
            }
          });


          if($hm > 0 && $hm < 6){
            $html_div
          }
        }*/
        $.each(data_.oas, function(e,v){
          //console.log(v);
          $h = '<div class="ui button added-item with-popover" data-id="'+v.id+'" data-content="'+v.description_min+'" data-title="'+v.name_eje+'" data-current-color="orange" data-name="OA '+v.num_obj+'" data-hex="" data-subject="Clase de Historia" style="font-size:14px; background-color:grey;" >OA '+v.num_obj+'</div>';
          $html += '<div class="ui button added-item with-popover" data-id="'+v.id+'" data-name="OA '+v.num_obj+'"  data-content="'+v.description_min+'" data-title="'+v.name_eje+'" data-current-color="orange" data-hex="" data-subject="Clase de Historia" style="font-size:14px; background-color:grey;" >OA '+v.num_obj+'</div>';
          $html_2_first += '<div class="ui checkbox"><input name="'+v.id+'" type="checkbox" data-id="'+v.id+'" data-original-html="'+escapeHtml($h)+'" tabindex="0" data-name="OA '+v.num_obj+'"><label>OA '+v.num_obj+'</label></div> - '+v.description+'<br><br>';
        });
        //$html += '<div class="ui divider"></div>';
        console.log(data_.tags.length);
        if(data_.tags.length <= 0){
            $('.item.second').hide();
        }
        $.each(data_.tags, function(e,v){
          //console.log(v.name);
          //$html += '<div class="ui button added-item with-popover" data-title="'+v.name+'" data-current-color="orange" data-hex="" >'+v.name+' <img data-html="<div class=\'header\'>Selecciona el color</div><div class=\'content\'><div class=\'color c1\'></div><div class=\'color c2\'></div><div class=\'color c3\'></div><div class=\'color c4\'></div><div class=\'color c5\'></div><div class=\'color c6\'></div><br><div class=\'color c7\'></div><div class=\'color c8\'></div><div class=\'color c9\'></div><div class=\'color c10\'></div><div class=\'color c11\'></div><div class=\'color c12\'></div></div>" class="colorpicker" src="../dist/img/colorpicker.png" alt="" style="margin-top: -4px;position: relative;float: right;"></div>';
          $html_2_second += '<div class="ui checkbox"><input type="checkbox" data-original-html="'+escapeHtml($h)+'" tabindex="0" data-id="'+v.id+'" data-name="'+v.name+' "><label>'+v.name+'</label></div><br><br>';
        });
        console.log(data_.abilities.length);
        if(data_.abilities.length <= 0){
          $('.item.third').hide();
        }
        $.each(data_.abilities, function(e,v){

          $html_2_three += '<div class="ui checkbox"><input type="checkbox" tabindex="0" data-id="'+v.id+'" data-desc="'+v.description+'"><label>'+v.description+'</label></div><br><br>'
        });
        console.log(data_.attitudes.length);
        if(data_.attitudes.length <= 0){
          $('.item.fourth').hide();
        }
        $.each(data_.attitudes, function(e,v){
          $html_2_four += '<div class="ui checkbox"><input type="checkbox" tabindex="0" data-id="'+v.id+'" data-desc="'+v.description+'"><label>'+v.description+'</label></div><br><br>'
        });

        $('.added-blocks').html($html);
        //$('.added-blocks').html($custom_);
        $('.le_first').html($html_2_first);
        $('.le_two').html($html_2_second);
        $('.le_three').html($html_2_three);
        $('.le_four').html($html_2_four);

        $.ajax({
          url: ajax_url+'/v1/tags/list',
          dataType: 'json',
          type: 'post',
          xhrFields: {
            withCredentials: true
          },
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          data: {subject: localStorage.getItem('subject_id'), unit: $('.item.active.selected').attr('data-value')},
          success: function(data_){

            $.each(data_, function(i,val){
              $('.le_two').find('input').each(function(){
                if(val.tag_id==$(this).attr('data-id')){
                  $(this).prop('checked',true);
                }else{
                  $(this).removeAttr('checked');
                }
              });
            });
          }
        });

        $.ajax({
          url: ajax_url+'/v1/abilities/list',
          dataType: 'json',
          type: 'post',
          xhrFields: {
            withCredentials: true
          },
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          data: {subject: localStorage.getItem('subject_id'), unit: $('.item.active.selected').attr('data-value')},
          success: function(data_){

            $.each(data_, function(i,val){
              $('.le_three').find('input').each(function(){
                if(val.id==$(this).attr('data-id')){
                  $(this).prop('checked',true);
                }else{
                  $(this).removeAttr('checked');
                }
              });
            });
          }
        });

        $.ajax({
          url: ajax_url+'/v1/attitudes/list',
          dataType: 'json',
          type: 'post',
          xhrFields: {
            withCredentials: true
          },
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          data: {subject: localStorage.getItem('subject_id'), unit: $('.item.active.selected').attr('data-value')},
          success: function(data_){

            $.each(data_, function(i,val){
              $('.le_four').find('input').each(function(){
                if(val.id==$(this).attr('data-id')){
                  $(this).prop('checked',true);
                }else{
                  $(this).removeAttr('checked');
                }
              });
            });
          }
        });

        moveEvents($('.added-item').data('hex'));

        $('.with-popover').popup();

        $('.colorpicker').click(function(){
          $this = $(this);
          $this.popup({
            on: 'click',
            onCreate: function(){
              $original_class = $this.parent();
              $(document).on('click', '.content div.color', function(){
                $new_class = 'ui button added-item' + ' ' + $(this).attr('class');
                $this.parent().attr('class',$new_class);
              });
            }
          });
        });
      }
    });
  });

  $(document).on('click','.fc-time-grid-event', function(){
    var oa = $(this).find('.in-event');
    console.log(oa);
    //$('input[type=checkbox]').removeAttr('checked')
    $.each(oa, function(i,v){
      console.log($(v).text());
      ////alert($(v).text());
      $("body").find("[data-name='"+$(v).text()+"']").prop('checked',true);
    });
  });

  $.ajax({
    url: ajax_url+'/v1/units/list/'+localStorage.getItem('subject_id'),
    dataType: 'json',
    type: 'post',
    xhrFields: {
      withCredentials: true
    },
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    success: function(data_){
      var first = 0;
      $html = '';
      console.log(data_);
      console.log(data_.units);
      $.each(data_.units, function(e,d){
        if(e==0){
          first = d.id;
        }
        $html += '<div class="item" data-value="'+d.id+'">'+d.name+'</div>';
      });
      $('.unit-dropdown').html($html);
      $('#unitssel').dropdown('refresh');
      $('#unitssel').dropdown('set selected',first);
      var value = $('#unitssel').dropdown('get value');
    $.ajax({
      url: ajax_url+'/v1/units/show/'+value,
      dataType: 'json',
      type: 'post',
      xhrFields: {
        withCredentials: true
      },
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      success: function(data_){
        $html = '';
        $html_2 = '';
        $html_2_first = '';
        $html_2_second = '';
        $html_2_three = '';
        $html_2_four = '';
        $custom_ = '<div class="ui grid"><div class="eight wide computer column">1</div><div class="eight wide computer column">2</div></div>';
        /*if(data_.oas.length > 10){
          $hm = 0;
          $.each(data_.oas, function(e,v){
            if($hm == 0){
              $html_div = '<div class="ui grid"><div class="eight wide computer column">';
            }
            if($hm > 0 && $hm < 6){
              $html_div += '<div class="ui button added-item with-popover" data-content="'+v.description_min+'" data-title="'+v.name_eje+'" data-current-color="orange" data-hex="" data-subject="Clase de Historia" >OA '+v.num_obj+' <img data-html="<div class=\'header\'>Selecciona el color</div><div class=\'content\'><div class=\'color c1\'></div><div class=\'color c2\'></div><div class=\'color c3\'></div><div class=\'color c4\'></div><div class=\'color c5\'></div><div class=\'color c6\'></div><br><div class=\'color c7\'></div><div class=\'color c8\'></div><div class=\'color c9\'></div><div class=\'color c10\'></div><div class=\'color c11\'></div><div class=\'color c12\'></div></div>" class="colorpicker" src="../dist/img/colorpicker.png" alt="" style="margin-top: -4px;position: relative;float: right;"></div>';
            }
            if($hm==5){
              $html_div += '</div>';
            }if($hm >= 6){
              $html_div += '<div class="eight wide computer column">';
            }
          });


          if($hm > 0 && $hm < 6){
            $html_div
          }
        }*/
        $.each(data_.oas, function(e,v){
          //console.log(v);
          $h = '<div class="ui button added-item with-popover" data-id="'+v.id+'" data-content="'+v.description_min+'" data-title="'+v.name_eje+'" data-current-color="orange" data-hex="" data-subject="Clase de Historia" data-name="OA '+v.num_obj+'" style="font-size:14px; background-color:grey;" >OA '+v.num_obj+'</div>';
          $html += '<div class="ui button added-item with-popover" data-id="'+v.id+'" data-content="'+v.description_min+'" data-title="'+v.name_eje+'" data-current-color="orange" data-hex="" data-name="OA '+v.num_obj+'" data-subject="Clase de Historia" style="font-size:14px; background-color:grey;" >OA '+v.num_obj+'</div>';
          $html_2_first += '<div class="ui checkbox"><input name="'+v.id+'" type="checkbox" data-id="'+v.id+'" data-original-html="'+escapeHtml($h)+'" tabindex="0" data-name="OA '+v.num_obj+' "><label>OA '+v.num_obj+'</label></div> - '+v.description+'<br><br>';
        });
        //$html += '<div class="ui divider"></div>';
        // console.log(data_.tags.length);
        if(data_.tags.length <= 0){
            $('.item.second').hide();
        }
        $.each(data_.tags, function(e,v){
          //console.log(v.name);
          //$html += '<div class="ui button added-item with-popover" data-title="'+v.name+'" data-current-color="orange" data-hex="" >'+v.name+' <img data-html="<div class=\'header\'>Selecciona el color</div><div class=\'content\'><div class=\'color c1\'></div><div class=\'color c2\'></div><div class=\'color c3\'></div><div class=\'color c4\'></div><div class=\'color c5\'></div><div class=\'color c6\'></div><br><div class=\'color c7\'></div><div class=\'color c8\'></div><div class=\'color c9\'></div><div class=\'color c10\'></div><div class=\'color c11\'></div><div class=\'color c12\'></div></div>" class="colorpicker" src="../dist/img/colorpicker.png" alt="" style="margin-top: -4px;position: relative;float: right;"></div>';
          $html_2_second += '<div class="ui checkbox"><input type="checkbox" data-original-html="'+escapeHtml($h)+'" tabindex="0" data-id="'+v.id+'" data-name="'+v.name+' "><label>'+v.name+'</label></div><br><br>';
        });
        // console.log(data_.abilities.length);
        if(data_.abilities.length <= 0){
          $('.item.third').hide();
        }
        $.each(data_.abilities, function(e,v){

          $html_2_three += '<div class="ui checkbox"><input type="checkbox" tabindex="0" data-id="'+v.id+'" data-desc="'+v.description+'"><label>'+v.description+'</label></div><br><br>'
        });
        // console.log(data_.attitudes.length);
        if(data_.attitudes.length <= 0){
          $('.item.fourth').hide();
        }
        $.each(data_.attitudes, function(e,v){
          $html_2_four += '<div class="ui checkbox"><input type="checkbox" tabindex="0" data-id="'+v.id+'" data-desc="'+v.description+'"><label>'+v.description+'</label></div><br><br>'
        });

        $('.added-blocks').html($html);
        //$('.added-blocks').html($custom_);
        $('.le_first').html($html_2_first);
        $('.le_two').html($html_2_second);
        $('.le_three').html($html_2_three);
        $('.le_four').html($html_2_four);


        moveEvents($('.added-item').data('hex'));

        $('.with-popover').popup();

        $('.colorpicker').click(function(){
          $this = $(this);
          $this.popup({
            on: 'click',
            onCreate: function(){
              $original_class = $this.parent();
              $(document).on('click', '.content div.color', function(){
                $new_class = 'ui button added-item' + ' ' + $(this).attr('class');
                $this.parent().attr('class',$new_class);
              });
            }
          });
        });
      }
    });

      
    }
  });

  var date = new Date();
  var d = date.getDate();
  var m = date.getMonth();
  var y = date.getFullYear();

  $('body').on('click', 'button.fc-next-button', function() {
    if($('#fullcalendar-events').fullCalendar('getView').start.format('YYYY')=='2017'){
      //alert('Aléjate! No puedes ver esto!!');
      //alert('Tienes que pagar si quieres seguir!');
      //alert('Aléjate maldito bastardo!!');
      //$('.shall-modal').modal('show');
    }
  });

  $(document).on('click','input[type=checkbox]',function(){
    //$('.in-event:contains("'+$(this).data('name')+'")').remove();
  });

  $(document).on('click','.fc-time-grid-event',function(){
    $('[class^="le_"]').parent().scrollTop(0);
    $('.tabular.menu').find('.item').tab('change tab', 'first');
  });




  $('#fullcalendar-events').fullCalendar({
      header: {
          left: 'prev,next today',
          center: 'title',
          right: 'myCustomButton month,agendaWeek',
      },
      editable: false,
      defaultView: 'agendaWeek',
      firstDay: 1,
      allDaySlot: false,
      dayClick: function(calEvent, jsEvent, view) {
        localStorage.setItem('start_event',calEvent.format());
        $('#alert-text1').val($(this).attr('data-description'));
        $('#event-name').val($(this).attr('data-title'));
        $('.event-modal').modal('show');
      },
      eventClick: function(calEvent, jsEvent, view) {
        $('.datepicker').val('');
        $('#alert-text1').val('');
        $('#event-name').val('');
        if($(this).hasClass('reminder')){
          $('#alert-text1').val($(this).attr('data-description'));
          $('#event-name').val($(this).attr('data-title'));
          $('.event-modal')
          .modal('show').modal({
            autofocus: false
          });

        }else{
          if($(this).hasClass('holi') || $(this).hasClass('conme')){
            if($(this).hasClass('conme')){
              $('#conme-content').empty();
              $.ajax({
                url: ajax_url+'/v1/schedule/getconme',
                dataType: 'json',
                type: 'post',
                async: false,
                xhrFields: {
                  withCredentials: true
                },
                headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {date: calEvent.start.format()},
                success: function(dato){
                  var conmes = '';
                  $.each(dato, function(e,v){
                    console.log(v.desc);
                    conmes += '<p>'+v.desc+'</p>';
                    if(v.file){
                      conmes += '<a href="'+ajax_url+'dist/conmes/'+v.file+'" target="_blank" >'+v.file+'</a>';
                    }
                  });
                  $('#conme-content').append(conmes);
                  $('#conme-modal').modal('show');
                }
              });
            }

          }
          else{
          var tipo = $(this).attr('data-type');
          $('.datepicker').val('');
          $('.timepicker2').val('');
          $('.classdate').val('');
          $('.classtime').val('');
          $('#le_value').val($(this).data('id'));
          $('.class-modal').attr('data-id',$(this).data('id'));
          $('#alert-text2').val($(this).attr('data-desc'));
          $('#init-text').val($(this).attr('data-inittext'));
          $('#mid-text').val($(this).attr('data-midtext'));
          $('#end-text').val($(this).attr('data-endtext'));
          $('#res-text').val($(this).attr('data-restext'));
          $('#eval-text').val($(this).attr('data-evaltext'));
          $('#ec-text').val($(this).attr('data-ectext'));
          $("input[name=class-type][value=" + tipo + "]").prop('checked', true);

          // 0000-00-00 00:00:00
          if($('.fc-time-grid-event[data-id='+$(this).data('id')+']').attr('data-not') && ($('.fc-time-grid-event[data-id='+$(this).data('id')+']').attr('data-not') !=  '0000-00-00 00:00:00' && $('.fc-time-grid-event[data-id='+$(this).data('id')+']').attr('data-not') != 'Invalid date')){
              $('.classdate').val((moment($(this).attr('data-not'))).format('D-MM-Y'));
              $('.classtime').val((moment($(this).attr('data-not'))).format('hh:mma'));
          }

          $('.le_two').find('input').each(function(e,v){
            $(v).prop('checked',0);
          });

          $('.le_first').find('input').each(function(e,v){
            $(v).prop('checked',0);
          });

          $('.le_three').find('input').each(function(e,v){
            $(v).prop('checked',0);
          });

          $('.le_four').find('input').each(function(e,v){
            $(v).prop('checked',0);
          });

          if(!$('[name=unit-dropdown]').attr('value') ){

              $.ajax({
              url: ajax_url+'/v1/schedule/unit',
              dataType: 'json',
              type: 'post',
              async: false,
              xhrFields: {
                withCredentials: true
              },
              headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              },
              data: {id: $(this).attr('data-id')},
              success: function(dato){
                console.log(dato);
                console.log(parseInt(dato, 10));
                 if(dato == parseInt(dato, 10) ){
                   $('#unitssel').dropdown('set selected', dato);
                   var value = $('#unitssel').dropdown('get value');

                    $.ajax({
                      url: ajax_url+'/v1/units/show/'+value,
                      dataType: 'json',
                      type: 'post',
                      xhrFields: {
                        withCredentials: true
                      },
                      headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                      },
                      success: function(data_){
                        $html = '';
                        $html_2 = '';
                        $html_2_first = '';
                        $html_2_second = '';
                        $html_2_three = '';
                        $html_2_four = '';
                        $custom_ = '<div class="ui grid"><div class="eight wide computer column">1</div><div class="eight wide computer column">2</div></div>';
                        /*if(data_.oas.length > 10){
                          $hm = 0;
                          $.each(data_.oas, function(e,v){
                            if($hm == 0){
                              $html_div = '<div class="ui grid"><div class="eight wide computer column">';
                            }
                            if($hm > 0 && $hm < 6){
                              $html_div += '<div class="ui button added-item with-popover" data-content="'+v.description_min+'" data-title="'+v.name_eje+'" data-current-color="orange" data-hex="" data-subject="Clase de Historia" >OA '+v.num_obj+' <img data-html="<div class=\'header\'>Selecciona el color</div><div class=\'content\'><div class=\'color c1\'></div><div class=\'color c2\'></div><div class=\'color c3\'></div><div class=\'color c4\'></div><div class=\'color c5\'></div><div class=\'color c6\'></div><br><div class=\'color c7\'></div><div class=\'color c8\'></div><div class=\'color c9\'></div><div class=\'color c10\'></div><div class=\'color c11\'></div><div class=\'color c12\'></div></div>" class="colorpicker" src="../dist/img/colorpicker.png" alt="" style="margin-top: -4px;position: relative;float: right;"></div>';
                            }
                            if($hm==5){
                              $html_div += '</div>';
                            }if($hm >= 6){
                              $html_div += '<div class="eight wide computer column">';
                            }
                          });


                          if($hm > 0 && $hm < 6){
                            $html_div
                          }
                        }*/
                        $.each(data_.oas, function(e,v){
                          //console.log(v);
                          $h = '<div class="ui button added-item with-popover" data-id="'+v.id+'" data-content="'+v.description_min+'" data-title="'+v.name_eje+'" data-current-color="orange" data-hex="" data-name="OA '+v.num_obj+'" data-subject="Clase de Historia" style="font-size:14px; background-color:grey;" >OA '+v.num_obj+'</div>';
                          $html += '<div class="ui button added-item with-popover" data-id="'+v.id+'" data-content="'+v.description_min+'" data-title="'+v.name_eje+'" data-current-color="orange" data-hex="" data-subject="Clase de Historia" data-name="OA '+v.num_obj+'" style="font-size: 14px;backgroud-color: grey;">OA '+v.num_obj+'</div>';
                          $html_2_first += '<div class="ui checkbox"><input name="'+v.id+'" type="checkbox" data-id="'+v.id+'" data-original-html="'+escapeHtml($h)+'" tabindex="0" data-name="OA '+v.num_obj+' "><label>OA '+v.num_obj+'</label></div> - '+v.description+'<br><br>';
                        });
                        //$html += '<div class="ui divider"></div>';
                        console.log(data_.tags.length);
                        if(data_.tags.length <= 0){
                            $('.item.second').hide();
                        }
                        $.each(data_.tags, function(e,v){
                          //console.log(v.name);
                          //$html += '<div class="ui button added-item with-popover" data-title="'+v.name+'" data-current-color="orange" data-hex="" >'+v.name+' <img data-html="<div class=\'header\'>Selecciona el color</div><div class=\'content\'><div class=\'color c1\'></div><div class=\'color c2\'></div><div class=\'color c3\'></div><div class=\'color c4\'></div><div class=\'color c5\'></div><div class=\'color c6\'></div><br><div class=\'color c7\'></div><div class=\'color c8\'></div><div class=\'color c9\'></div><div class=\'color c10\'></div><div class=\'color c11\'></div><div class=\'color c12\'></div></div>" class="colorpicker" src="../dist/img/colorpicker.png" alt="" style="margin-top: -4px;position: relative;float: right;"></div>';
                          $html_2_second += '<div class="ui checkbox"><input type="checkbox" data-original-html="'+escapeHtml($h)+'" tabindex="0" data-id="'+v.id+'" data-name="'+v.name+' "><label>'+v.name+'</label></div><br><br>';
                        });
                        console.log(data_.abilities.length);
                        if(data_.abilities.length <= 0){
                          $('.item.third').hide();
                        }
                        $.each(data_.abilities, function(e,v){

                          $html_2_three += '<div class="ui checkbox"><input type="checkbox" tabindex="0" data-id="'+v.id+'" data-desc="'+v.description+'"><label>'+v.description+'</label></div><br><br>'
                        });
                        console.log(data_.attitudes.length);
                        if(data_.attitudes.length <= 0){
                          $('.item.fourth').hide();
                        }
                        $.each(data_.attitudes, function(e,v){
                          $html_2_four += '<div class="ui checkbox"><input type="checkbox" tabindex="0" data-id="'+v.id+'" data-desc="'+v.description+'"><label>'+v.description+'</label></div><br><br>'
                        });
                        $('.added-blocks').html($html);
                        //$('.added-blocks').html($custom_);
                        $('.le_first').html($html_2_first);
                        $('.le_two').html($html_2_second);
                        $('.le_three').html($html_2_three);
                        $('.le_four').html($html_2_four);

                        $.ajax({
                          url: ajax_url+'/v1/tags/list',
                          dataType: 'json',
                          type: 'post',
                          xhrFields: {
                            withCredentials: true
                          },
                          headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                          },
                          data: {subject: localStorage.getItem('subject_id'), unit: $('.item.active.selected').attr('data-value')},
                          success: function(data_){

                            $.each(data_, function(i,val){
                              $('.le_two').find('input').each(function(){
                                if(val.tag_id==$(this).attr('data-id')){
                                  $(this).prop('checked',true);
                                }else{
                                  //$(this).removeAttr('checked');
                                }
                              });

                            });

                          }

                        });
                        $.ajax({
                        url: ajax_url+'/v1/abilities/list',
                        dataType: 'json',
                        type: 'post',
                        xhrFields: {
                          withCredentials: true
                        },
                        headers: {
                          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        data: {subject: localStorage.getItem('subject_id'), unit: $('.item.active.selected').attr('data-value')},
                        success: function(data_){

                          $.each(data_, function(i,val){
                            $('.le_three').find('input').each(function(){
                              if(val.id==$(this).attr('data-id')){
                                $(this).prop('checked',true);
                              }else{
                                //$(this).removeAttr('checked');
                              }
                            });
                          });
                        }
                      });

                      $.ajax({
                        url: ajax_url+'/v1/attitudes/list',
                        dataType: 'json',
                        type: 'post',
                        xhrFields: {
                          withCredentials: true
                        },
                        headers: {
                          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        data: {subject: localStorage.getItem('subject_id'), unit: $('.item.active.selected').attr('data-value')},
                        success: function(data_){

                          $.each(data_, function(i,val){
                            $('.le_four').find('input').each(function(){
                              if(val.id==$(this).attr('data-id')){
                                $(this).prop('checked',true);
                              }else{
                                //$(this).removeAttr('checked');
                              }
                            });
                          });
                        }
                      });

                        //INFO DE ARCHIVO PARA AGREGAR
                       
                        //FIN ARCHIVO





                        moveEvents($('.added-item').data('hex'));

                        $('.with-popover').popup();

                        $('.colorpicker').click(function(){
                          $this = $(this);
                          $this.popup({
                            on: 'click',
                            onCreate: function(){
                              $original_class = $this.parent();
                              $(document).on('click', '.content div.color', function(){
                                $new_class = 'ui button added-item' + ' ' + $(this).attr('class');
                                $this.parent().attr('class',$new_class);
                              });
                            }
                          });
                        });

                      }
                    });
                 }
              },
              error: function(jqXHR, textStatus, errorThrown){
                alert('error');
              }        
            }).then(console.log('w8 t load'));
            
            $.ajax({
              url: ajax_url+'/v1/tags/list',
              dataType: 'json',
              type: 'post',
              xhrFields: {
                withCredentials: true
              },
              headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              },
              data: {subject: localStorage.getItem('subject_id'), unit: $('.item.active.selected').attr('data-value'), block_id: $(this).attr('data-id')},
              success: function(data_){
                $.each(data_, function(i,val){
                  $('.le_two').find('input').each(function(e,v){
                    if(val.tag_id==$(this).attr('data-id')){
                      $(v).prop('checked', true);
                    }
                  });
                });
                console.log("hola2");
              }
            });
            $.ajax({
              url: ajax_url+'/v1/abilities/list',
              dataType: 'json',
              type: 'post',
              xhrFields: {
                withCredentials: true
              },
              headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              },
              data: {subject: localStorage.getItem('subject_id'), unit: $('.item.active.selected').attr('data-value'), block_id: $(this).attr('data-id')},
              success: function(data_){

                $.each(data_, function(i,val){
                  $('.le_three').find('input').each(function(){
                    if(val.id==$(this).attr('data-id')){
                      $(this).prop('checked',true);
                    }else{
                      //$(this).removeAttr('checked');
                    }
                  });
                });
              }
            });

            $.ajax({
              url: ajax_url+'/v1/attitudes/list',
              dataType: 'json',
              type: 'post',
              xhrFields: {
                withCredentials: true
              },
              headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              },
              data: {subject: localStorage.getItem('subject_id'), unit: $('.item.active.selected').attr('data-value'), block_id: $(this).attr('data-id')},
              success: function(data_){

                $.each(data_, function(i,val){
                  $('.le_four').find('input').each(function(){
                    if(val.id==$(this).attr('data-id')){
                      $(this).prop('checked',true);
                    }else{
                      //$(this).removeAttr('checked');
                    }
                  });
                });
              }
            });

            $.ajax({
              url: ajax_url+'/v1/objectives/list',
              dataType: 'json',
              type: 'post',
              xhrFields: {
                withCredentials: true
              },
              headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              },
              data: {subject: localStorage.getItem('subject_id'), unit: $('.item.active.selected').attr('data-value'), block_id: $(this).attr('data-id')},
              success: function(data_){
                $.each(data_, function(i,val){
                  $('.le_first').find('input').each(function(e,v){
                    if(val.objective_id==$(this).attr('data-id')){
                      $(v).prop('checked', true);
                    }
                  });
                });
                console.log("hola3");
              }
            });

             $.ajax({
                          url: ajax_url+'/v1/schedule/list',
                          dataType: 'json',
                          type: 'post',
                          xhrFields: {
                            withCredentials: true
                          },
                          headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                          },
                          data: {id: $(this).attr('data-id')},
                          success: function(data_){
                            $('.attached ul').html('');
                            if(data_.length===0){
                              $('.attached').addClass('hide');
                            }else{
                              $('.attached').removeClass('hide');
                            }
                            $.each(data_, function(e,v){
                              if(v.name){
                                console.log(v.name);
                                $('.attached ul').append('<li><a style="text-overflow: ellipsis;" target="_blank" href="'+v.url+'">'+v.name+'</a><i style="cursor:pointer;" class="archivo remove icon"></i></li>');
                              }
                            });
                          }
                        });

          }
          else{

            $.ajax({
              url: ajax_url+'/v1/schedule/list',
              dataType: 'json',
              type: 'post',
              xhrFields: {
                withCredentials: true
              },
              headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              },
              data: {id: $(this).attr('data-id')},
              success: function(data_){
                $('.attached ul').html('');
                if(data_.length===0){
                  $('.attached').addClass('hide');
                }else{
                  $('.attached').removeClass('hide');
                }
                $.each(data_, function(e,v){
                  if(v.name){
                    console.log(v.name);
                    $('.attached ul').append('<li><a style="text-overflow: ellipsis;" target="_blank" href="'+v.url+'">'+v.name+'</a><i style="cursor:pointer;" class="archivo remove icon"></i></li>');
                  }
                });
              }
            });

            $.ajax({
              url: ajax_url+'/v1/tags/list',
              dataType: 'json',
              type: 'post',
              xhrFields: {
                withCredentials: true
              },
              headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              },
              data: {subject: localStorage.getItem('subject_id'), unit: $('.item.active.selected').attr('data-value'), block_id: $(this).attr('data-id')},
              success: function(data_){
                $.each(data_, function(i,val){
                  $('.le_two').find('input').each(function(e,v){
                    if(val.tag_id==$(this).attr('data-id')){
                      $(v).prop('checked', true);
                    }
                  });
                });
              }
            });
            $.ajax({
              url: ajax_url+'/v1/abilities/list',
              dataType: 'json',
              type: 'post',
              xhrFields: {
                withCredentials: true
              },
              headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              },
              data: {subject: localStorage.getItem('subject_id'), unit: $('.item.active.selected').attr('data-value'), block_id: $(this).attr('data-id')},
              success: function(data_){

                $.each(data_, function(i,val){
                  $('.le_three').find('input').each(function(){
                    if(val.id==$(this).attr('data-id')){
                      $(this).prop('checked',true);
                    }else{
                      //$(this).removeAttr('checked');
                    }
                  });
                });
              }
            });

            $.ajax({
              url: ajax_url+'/v1/attitudes/list',
              dataType: 'json',
              type: 'post',
              xhrFields: {
                withCredentials: true
              },
              headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              },
              data: {subject: localStorage.getItem('subject_id'), unit: $('.item.active.selected').attr('data-value'), block_id: $(this).attr('data-id')},
              success: function(data_){

                $.each(data_, function(i,val){
                  $('.le_four').find('input').each(function(){
                    if(val.id==$(this).attr('data-id')){
                      $(this).prop('checked',true);
                    }else{
                      //$(this).removeAttr('checked');
                    }
                  });
                });
              }
            });

            $.ajax({
              url: ajax_url+'/v1/objectives/list',
              dataType: 'json',
              type: 'post',
              xhrFields: {
                withCredentials: true
              },
              headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              },
              data: {subject: localStorage.getItem('subject_id'), unit: $('.item.active.selected').attr('data-value'), block_id: $(this).attr('data-id')},
              success: function(data_){
                $.each(data_, function(i,val){
                  $('.le_first').find('input').each(function(e,v){
                    if(val.objective_id==$(this).attr('data-id')){
                      $(v).prop('checked', true);
                    }
                  });
                });
              }
            });

          }
          $('.class-modal').modal('show');
        }
          }
      },
      minTime: '07:00:00',
      maxTime: '20:00:00',
      columnFormat: 'dddd D',
      titleFormat: 'MMMM YYYY',
      lang: 'es',
      droppable: false,

      drop: function(date) {

      },
      slotDuration: '00:60:00',
      slotLabelFormat: 'h:mm',
      events: function(start, end, timezone, callback){
        var events = [];
        $.ajax({
          url: ajax_url+'/v1/schedule/conme',
          dataType: 'json',
          type: 'post',
          xhrFields: {
            withCredentials: true
          },
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          success: function(data){
            $.each(data, function(e,v){
              events.push({
                allDay: false,
                start: v.date_init,
                end: v.date_end,
                title: 'Efemérides',
                className: 'conme',
                color: '#ffffff',
                textColor: '#000000',
              });
            });
          }

        });

        $.ajax({
          url: ajax_url+'/v1/schedule/holidays',
          dataType: 'json',
          type: 'post',
          xhrFields: {
            withCredentials: true
          },
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          success: function(data){
            $.each(data, function(e,v){
              events.push({
                allDay: false,
                start: v.date_init,
                end: v.date_end,
                title: v.name,
                className: 'holi',
                color: '#d3d3d3'
              });
            });
          }

        });
        $.ajax({
          url: ajax_url+'/v1/schedule/list',
          dataType: 'json',
          type: 'post',
          xhrFields: {
            withCredentials: true
          },
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          success: function(data){
            $('input[type=checkbox]').each(function(i,v){
            // console.log($(this).is(':checked'))
            });
            $.each(data, function(e,v){
              //console.log(v);

              //console.log(v.course_id);
              //console.log(localStorage.getItem('level'));
              //console.log(v.letter);
              //console.log(localStorage.getItem('letra'));
              //if(v.subject_id==localStorage.getItem('subject_id') && v.course_id == localStorage.getItem('level') && v.letter == localStorage.getItem('letra')){
              if(v.subject_id==localStorage.getItem('subject_id') && v.school == localStorage.getItem('school') && v.course_id == localStorage.getItem('level') && v.letter == localStorage.getItem('letra')){
                // console.log(v.course_id);
                // console.log(localStorage.getItem('level'));
                // console.log(v);
                //console.log(v.hora_inicio);
                localStorage.setItem('level_name',v.course);
                $('.ui.dividing.header').text(v.name+' / '+v.course+ ' '+localStorage.getItem('letra'));
                events.push({
                  //title: v.name+' / '+v.course+' ('+v.id+')',
                  start: v.hora_inicio,
                  end: v.hora_fin,
                  allDay: false,
                  className: v.color,
                  color: v.color,
                  id: v.main_id,
                  desc: v.description,
                  reminder: v.reminder,
                  inittext: v.init_text,
                  midText: v.mid_text,
                  endText: v.end_text,
                  resText: v.res_text,
                  evalText: v.eval_text,
                  ecText: v.ec_text,
                  type: v.type
                });
              }
            });
            $.ajax({
              url: ajax_url+'/v1/notifications/list',
              dataType: 'json',
              type: 'post',
              xhrFields: {
                withCredentials: true
              },
              headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              },
              success: function(data){
                //console.log(data.notifications);
                //var events = [];
                $.each(data.notifications, function(e,v){
                  // console.log(v);
                  if(v.course==localStorage.getItem('level') && v.letter==localStorage.getItem('letra') && v.subject==localStorage.getItem('subject_id')){
                    events.push({
                      allDay:false,
                      className:"reminder",
                      end:v.date,
                      start:v.date,
                      title:v.name,
                      description: v.description,
                      reminder_id: v.id

                    });
                  }
                });
                //console.log(events);
                callback(events);
                //console.log(events);
              }
            });

          }
        });
      },
      eventRender: function (event, element)
      {

        $.ajax({
          url: ajax_url+'/v1/objectives/list',
          dataType: 'json',
          type: 'post',
          data: {id: event.id},
          xhrFields: {
            withCredentials: true
          },
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          success: function(data){
            //alert(111);
            $.each(data, function(e,v){
              //element.append(v.text);
              element.append('<div class="in-event with-popover '+v.color+'" data-id="'+v.objective_id+'" data-name="'+v.text+'" >'+v.text+'</div>');
            });
          }
        });

        //console.log(event);
        le_e = event;
        // console.log(event);
        element.attr('data-id',event.id);
        if(element.hasClass('reminder')){
          element.prepend( "<i class='closeon remove icon'></i><i class='alarm outline icon' style='float:left;'></i>" );
          element.attr('data-description',event.description);
          element.attr('data-title',event.title);
          element.attr('data-reminder-id',event.reminder_id);
        }else{
          if(element.hasClass('holi') || element.hasClass('conme')){
            element.prepend( "<i class='alarm outline icon' style='float:left;'></i>" );
          }
          else{
            element.attr('data-desc',event.desc);
            element.attr('data-not',event.reminder);
            element.attr('data-inittext', event.inittext);
            element.attr('data-midtext', event.midText);
            element.attr('data-endtext', event.endText);
            element.attr('data-restext', event.resText);
            element.attr('data-evaltext', event.evalText);
            element.attr('data-ectext', event.ecText);  
            element.attr('data-type', event.type);
            if(event.type==0){
              element.prepend( "<i class='book icon large' style='float:right; margin-top:2px;'></i>" );
            }
            if(event.type==1){
              element.prepend( "<i class='file powerpoint outline icon large' style='float:right; margin-top:2px;'></i>" );
            } 
            if(event.type==2){
              element.prepend( "<i class='users icon large' style='float:right; margin-top:2px;'></i>" );
            } 
          }
          
        }
        if( $(element).hasClass('conme') || $(element).hasClass('holi') ){

        }
        else{
          // store the ID for later...
          $(element).data('id', event.id);
          element.droppable({
            drop: function (event, ui)
            {
              console.log(ui);
              console.log(element);
              console.log($(ui.draggable.context).data('name'));
              //alert(1);
              //console.log(ui);
              //alert(le_e.id);
              //alert(ui.helper[0].dataset.id);
              // get the ID I stored above...
              //var rowID = $(this).data('id');
              //console.log(ui);
              $nr = [];

              $(element).find('.with-popover').each(function(e,v){
                $nr.push(

                            {

                          id : $(v).data('id'),
                          text : $(v).text(),
                          class : $(v).attr('class').replace('in-event with-popover','').trim().replace('visible','').trim()
                        }

                      );
                //console.log($(v).data('id'));
              });

              flag = 0;
              $.each($nr, function(e,v){
                if(v.text == $(ui.draggable.context).data('name')){
                  console.log('pille repetido');
                  flag = 1;
                }
              });

              if(flag == 0){
                _class = ui.draggable[0].className.replace('ui button added-item','').replace('ui-draggable-dragging','');
                element.append('<div class="in-event with-popover '+_class+'" data-id="'+ui.helper[0].dataset.id+'" data-content="'+ui.helper[0].dataset.content+'">'+ui.draggable[0].textContent+'</div>');
                $nr = [];

                $(element).find('.with-popover').each(function(e,v){
                  $nr.push(

                              {

                            id : $(v).data('id'),
                            text : $(v).text(),
                            class : $(v).attr('class').replace('in-event with-popover','').trim().replace('visible','').trim()
                          }

                        );
                  //console.log($(v).data('id'));
                });

                var $l = $('.fc-time-grid-event');
                //alert($(element).data('id'));
                $arr = [];
                $arr[0] = $(element).data('id');
                $arr[1] = localStorage.getItem('subject_id');
                //$arr[2] = ∑;
                //console.log($l.data('id'));


                $arr[3] = $nr;

                console.log($arr[3]);

                $nnrr = {
                  'id' : $(element).data('id'),
                  'si': localStorage.getItem('subject_id'),
                  'uid' : $('.item.active.selected').attr('data-value'),
                  'data':$nr
                };

                console.log($arr);

                $.ajax({
                  url: ajax_url+'/v1/objectives/add',
                  dataType: 'json',
                  type: 'post',
                  data: {data: $nnrr},
                  xhrFields: {
                    withCredentials: true
                  },
                  headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  },
                  success: function(data){
                    console.log(data);
                  }
                });

                $('.added-blocks div').each(function(){
                //console.log($(this).data('content'))
                if($(this).data('content')==ui.helper[0].dataset.content){
                  //$(this).next().remove();
                  //$(this).next().next().remove();
                  $(this).remove();
                }
                });

              }
            }
          });
          element.find(".closeon").click(function() {
            $le_id = $(this).parent().attr('data-reminder-id');
             $('#fullcalendar-events').fullCalendar('removeEvents',event._id);
             $('.fc-time-grid-event').each(function(){
               console.log($(this).hasClass('no-change'));
               if(!$(this).hasClass('no-change')){
                 //$(this).css('background-color', localStorage.getItem('hex'));
               }
               if(!$(this).hasClass('locked')){
                 //$(this).css('background-color', localStorage.getItem('hex'));
               }

               $.ajax({
                 url: ajax_url+'/v1/notifications/delete',
                 dataType: 'json',
                 type: 'post',
                 data: {id: $le_id},
                 xhrFields: {
                   withCredentials: true
                 },
                 headers: {
                   'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                 },
                 success: function(data){
                   console.log(data);
                 }
               });

             });
          });  
        }

        
      }
    });
}
