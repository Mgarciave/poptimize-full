moment.prototype.firstDayOfMonth = function() {
  return this.date(1);
};

moment.prototype.nextDay = function(day) {
  while (this.day() !== day) {
    this.add(1, 'd');
  }
  return this;
};

moment.prototype.firstDay = function(day) {
  return this.firstDayOfMonth.nextDay(day);
};

moment.prototype.allDaysRemainingInMonth = function() {
  var day, m, results, t;
  t = this.clone();
  results = [];
  while (t.month() === m || (m = t.month())) {
    day = t.clone();
    t.add(1, 'w');
    results.push(day);
  }
  return results;
};

moment.prototype.allDays = function(day) {
  return this.firstDay(day).allDaysRemainingInMonth();
};