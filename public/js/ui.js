function moveEvents (color) {
   $('.added-item').each(function() {
       var eventObject = {
           title: $.trim($(this).text()),
           backgroundColor: color
       };
       console.log(eventObject);
       $(this).data('eventObject', eventObject);

       $(this).draggable({
           zIndex: 999,
           revert: true,
           revertDuration: 0
       });

   });
}

function countdown( elementName, minutes, seconds )
{
    var element, endTime, hours, mins, msLeft, time;

    function twoDigits( n )
    {
        return (n <= 9 ? "0" + n : n);
    }

    function updateTimer()
    {
        msLeft = endTime - (+new Date);
        if ( msLeft < 1000 ) {
            window.location = 'course-calendar.html';
        } else {
            time = new Date( msLeft );
            hours = time.getUTCHours();
            mins = time.getUTCMinutes();
            element.innerHTML = (hours ? hours + ':' + twoDigits( mins ) : mins) + ':' + twoDigits( time.getUTCSeconds() );
            setTimeout( updateTimer, time.getUTCMilliseconds() + 500 );
        }
    }

    element = document.getElementById( elementName );
    endTime = (+new Date) + 1000 * (60*minutes + seconds) + 500;
    updateTimer();
}

$(document).ready(function(){
  $(document).on('click','.ui.dropdown.icon.item',function(){
    $('.ui.sidebar').sidebar('toggle');
  });
})

$(function(){

  $(document).on('click', '#beta-menu', function(e) {
    e.preventDefault();
    swal({
      title: "Función aun en desarrollo",
      text: "La plataforma aun se encuentra en fase Alpha, por lo que este menú aun se encuentra deshabilitado.",
      type: "error",
      confirmButtonText: "Continuar"
    });
  });

  $(document).on('click', '.add-course', function() {
    $('.add-course-modal').modal('show');
    //countdown( "countdown", 0, 10 );
  });

  $(document).on('click', '.ok-button', function() {
    countdown( "countdown", 0, 10 );
    $('.course-added').modal('show');
  });

  $(document).on('click', '.item.class-color', function() {
    moveEvents($(this).data('color'));
  });

  $(document).on('click', '.class-color', function() {
    $('.added-item').css('background-color', $(this).data('color')).attr('data-hex',$(this).data('color'));
  });

  moveEvents($('.added-item').data('hex'));

  /* HELPERS */

  $('.menu .item').tab();
  // Set selects as REAL dropdowns
  $('select.dropdown, .ui.dropdown').dropdown();
  // Enable the sidebar pusher for content
  /*$(document).on('click', '.ui.dropdown.icon.item', function() {
    $('.ui.sidebar').sidebar('toggle');
  });*/

  // Popups
  $('.school-course').popup();

  /*$('#fullcalendar').fullCalendar({
    defaultView: 'agendaWeek',
    firstDay: 1,
    allDaySlot: false,
    minTime: '08:00:00',
    maxTime: '19:00:00'
  });*/

  /*$('.add-class').on('click',function(){
    $('.added-blocks').append('<div class="ui orange button added-item" data-current-color="orange" data-hex="#39cd6b">Asignatura</div><br><br>');
    moveEvents($(this).attr('data-hex'));
  });*/

  /*$('.added-item').each(function() {
    alert($(this).attr('data-hex'));
      var eventObject = {
          title: $.trim($(this).text()),
          backgroundColor: $(this).attr('data-hex'),
      }
      console.log($(this).attr('data-hex'));
      $(this).data('eventObject', eventObject);

      $(this).draggable({
          zIndex: 999,
          revert: true,
          revertDuration: 0
      });

  });*/

  /*$(document).on('click', '.added-item', function() {
    console.log($(this).attr('data-hex'));
  });*/

  $(document).on('click', '#init-form-toggle', function(e) {
    var leftHeight = $('.main-content.login .row > .column:first-child').height();
    var rightHeight = $('.main-content.login .row > .column:nth-child(2)').height();
    /*if(leftHeight > rightHeight) {
      $('.main-content.login .row > .column:nth-child(2)').height(leftHeight);
    }else{
      $('.main-content.login .row > .column:first-child').height(rightHeight);
    }*/
    var heading = $('#login-register-heading');
    var subHeading = $('#login-register-subheading');
    var oldHeading = heading.text();
    var oldSubHeading = subHeading.text();
    heading.text('Registro de usuario');
    subHeading.html('Si ya tienes un usuario registrado, haz click <a href="javascript:;" id="init-form-toggle">aquí</a>.');
    $('#register-form').toggleClass('hide');
    $('#login-form').toggleClass('hide');
  });

  $(document).on('click', '#login', function(e){
    var email = $('#email').val();
    var password = $('#password').val();
    var msg = '';
    if(email == '' && password == '') {
      msg = 'Debes ingresar tus credenciales para acceder.';
    } else if(email == '') {
      msg = 'Debes ingresar el email registrado para acceder.';
    } else if(password == '') {
      msg = 'Debes ingresar tu contraseña para acceder.';
    } else if(email != 'demo@demo.com') {
      msg = 'El correo ingresado no se encuentra en nuestros registros.';
    } else if(password != '123456') {
      msg = 'La contraseña ingresada no corresponde.';
    }else if(email == 'demo@demo.com' && password == '123456') {
      window.location = 'dashboard';
      status = true;
    } else{
      msg = 'Ha ocurrido un error. Por favor vuelve a intentarlo.'
    }

    e.preventDefault();
    if(!status) {
      swal({
        title: "Oh no! :(",
        text: msg,
        type: "error",
        confirmButtonText: "Continuar"
      });
    }
  });

  $("#unit-sort").sortable({
    revert: true
  });

  $('#fullcalendar-units').fullCalendar({
    defaultView: 'year',
    header: {
    				left: '',
    				center: '',
    				right: ''
    			},
  });

  var date = new Date();
  var d = date.getDate();
  var m = date.getMonth();
  var y = date.getFullYear();

  $('#fullcalendar').fullCalendar({
      header: {
          left: 'prev,next today',
          center: 'title',
          right: 'myCustomButton month,basicWeek,basicDay',
      },
      editable: true,
      defaultView: 'agendaWeek',
      firstDay: 1,
      allDaySlot: false,
      minTime: '08:00:00',
      maxTime: '19:00:00',
      columnFormat: 'dddd D',
      titleFormat: 'MMMM YYYY',
      lang: 'es',
      droppable: true,

      slotDuration: '00:45:00',
      slotLabelFormat: 'h(:mm)a',
      drop: function(date) {

          var originalEventObject = $(this).data('eventObject');
          var copiedEventObject = $.extend({}, originalEventObject);

          copiedEventObject.start = date;
          copiedEventObject.title = $(this).data('title');
          copiedEventObject.backgroundColor = $(this).data('hex');

          console.log(copiedEventObject);

          $('#fullcalendar').fullCalendar('renderEvent', copiedEventObject, true);
          //$(this).remove();

      }
  });

  $('#fullcalendar-detail').fullCalendar({
      eventAfterAllRender: function() {
          $("#fullcalendar-detail").find('.fc-toolbar > div > h2').empty().append("<div>Curso / Asignatura</div>");
      },
      header: {
          left: 'prev,next today',
          center: 'title',
          right: 'myCustomButton month,basicWeek,basicDay',
      },
      titleFormat: '\'Hello, World!\'',
      editable: true,
      defaultView: 'agendaWeek',
      firstDay: 1,
      allDaySlot: false,
      minTime: '08:00:00',
      maxTime: '19:00:00',
      columnFormat: 'dddd D',
      titleFormat: 'MMMM YYYY',
      eventClick: function(calEvent, jsEvent, view) {
        $('.event-detail').modal('show');
        //alert('Event: ' + calEvent.title);
        //alert('Coordinates: ' + jsEvent.pageX + ',' + jsEvent.pageY);
        //alert('View: ' + view.name);
      },
      lang: 'es',
      droppable: true,

      slotDuration: '00:45:00',
      slotLabelFormat: 'h(:mm)a',
      drop: function(date) {

          var originalEventObject = $(this).data('eventObject');
          var copiedEventObject = $.extend({}, originalEventObject);

          copiedEventObject.start = date;
          copiedEventObject.title = $(this).data('title');
          copiedEventObject.backgroundColor = $(this).data('hex');

          console.log(copiedEventObject);

          $('#fullcalendar-detail').fullCalendar('renderEvent', copiedEventObject, true);
          //$(this).remove();

      }
  });

  /*$(document).on('click', '#register', function(e){
    var email = $('#email').val();
    var password = $('#password').val();
    var msg = '';
    var status = false;
    if(email == '' && password == '') {
      msg = 'Debes ingresar tus credenciales para acceder.';
    } else if(email == '') {
      msg = 'Debes ingresar el email registrado para acceder.';
    } else if(password == '') {
      msg = 'Debes ingresar tu contraseña para acceder.';
    } else if(email != 'felipe@fgacv.net') {
      msg = 'El correo ingresado no se encuentra en nuestros registros.';
    } else if(password != '123456') {
      msg = 'La contraseña ingresada no corresponde.';
    }else if(email == 'felipe@fgacv.net' && password == '123456') {
      window.location = 'http://emol.com';
      status = true;
    } else{
      msg = 'Ha ocurrido un error. Por favor vuelve a intentarlo.'
    }
    alert(status);
    e.preventDefault();
    if(status) {
      swal({
        title: "Oh no! :(",
        text: msg,
        type: "error",
        confirmButtonText: "Continuar"
      });
    }
  });*/

});
