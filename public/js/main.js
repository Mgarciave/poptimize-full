function moveEvents (color) {
   $('.added-event').each(function() {
       console.log($(this).data('hex').replace('#',''));
       var eventObject = {
           title: $.trim($(this).text()),
           backgroundColor: color
       };
       console.log(eventObject);
       $(this).data('eventObject', eventObject);

       $(this).draggable({
           zIndex: 999,
           revert: true,
           revertDuration: 0
       });

   });
}

function initCalendar($calendarDiv, displayDate, isMain) {
     $calendarDiv.fullCalendar({
         defaultDate: displayDate,
     });
 }


window.onload=function(){

  jQuery('.tabular.menu .item').tab();

  $('.do-login').click(function(){
    if($('#username').val() == 'fgacv'){
        swal({   title: "Todo correcto! Estamos cargando tus datos.",   text: "Esta ventana se cerrará en algunos segundos",   timer: 5000,   showConfirmButton: false }, function() {
          document.location.href="/app";
      });
    }else{
      swal("Oops... Algo salió mal :(", "Por favor revisa que los datos ingresados sean correctos", "error");
    }
  });

  $('.sidebar.menu a').click(function(){
    $('.sidebar.menu a').removeClass('active');
    $(this).addClass('active');
  });

  $('.datetimepicker').datetimepicker();

  window.onload=function(){
    //$('#calendar').fullCalendar();
    $( ".custom_calendar" ).each(function() {
      //console.log($( this ).data('date'));
      initCalendar($(this), moment($( this ).data('date'), 'YYYY MM DD'), true);
    });
  }

  $('.menu .item')
  .tab()
;

  $('.main-menu-icon').click(function(){
    $('.ui.sidebar').sidebar('toggle');
  });

  $('.ui.dropdown').dropdown();

  var newEventPopup = '<div class="ui flowing popup top left transition hidden">'+
                        '<div class="ui one column divided center aligned grid">'+
                          '<div class="column">'+
                            '<h4 class="ui header">Opciones</h4>'+
                            '<form class="ui form">'+
                              '<div class="field">'+
                                '<input type="text" name="title" placeholder="Título">'+
                              '</div>'+
                              '<div class="field">'+
                                '<select class="ui fluid search dropdown" name="event-color">'+
                                  '<option value="">Color</option>'+
                                  '<option value="orange" data-hex="#f2711c">Naranjo</option>'+
                                  '<option value="red" data-hex="#db2828">Rojo</option>'+
                                  '<option value="green" data-hex="#21ba45">Verde</option>'+
                                '</select>'+
                              '</div>'+
                              '<button class="ui basic button save-button">'+
                                '<i class="save icon"></i>'+
                                  'Guardar'+
                                '</button>'+
                          '</div>'+
                        '</div>'+
                      '</div>';

  jQuery('.new-event').on('click',function(){
    jQuery('#added-ones').append('<div class="row"><div class="ui orange button added-event" data-current-color="orange" data-hex="#f2711c">{{ Asignatura }}</div>'+newEventPopup+'</div><br>');
    moveEvents('#f2711c');
  });

  $(document).on('click', '.added-event', function(){
    $('.dropdown').dropdown();
    $(this).popup({on: 'click'});
  });

  $(document).on('click', '.save-button', function(e){
    e.preventDefault();
    $title = $(this).closest('form').find('input[name=title]').val();
    $color = $(this).closest('form').find('select option:selected').val();
    $currentColor = $(this).closest('.popup').prev().data('current-color');
    $currentColorHex = $(this).closest('form').find('select option:selected').data('hex');
    $(this).closest('.popup').prev().text($title);
    $(this).closest('.popup').prev().removeClass($currentColor).addClass($color);
    $(this).closest('.popup').prev().attr('data-current-color',$color);
    $(this).closest('.popup').prev().attr('data-hex',$currentColorHex);
    moveEvents($currentColorHex);
  });

  $(document).on('click', '.special span', function(){
    $('.fc-time-grid-event').css('background-color',$(this).data('color'));
  });


  var date = new Date();
  var d = date.getDate();
  var m = date.getMonth();
  var y = date.getFullYear();

  jQuery('#calendar').fullCalendar({
      header: {
          left: 'prev,next today',
          center: 'title',
          right: 'myCustomButton month,basicWeek,basicDay',
      },
      customButtons: {
          myCustomButton: {
              text: 'custom!',
              click: function() {
                  $('.fc-myCustomButton-button').popup({
                    popup: '.special.popup',
                    on    : 'click',
                    closable: false,
                    position: 'bottom right'
                  });
              }
          }
      },
      eventAfterAllRender: function(){
        $('.fc-myCustomButton-button').html('<img class="colorpicker" src="img/colorpicker.png">');
      },
      editable: true,
      weekends: false,
      columnFormat: 'dddd D',
      titleFormat: 'MMMM YYYY',
      droppable: true,
      defaultView: 'agendaWeek',
      allDaySlot: false,
      slotDuration: '00:45:00',
      slotLabelFormat: 'h(:mm)a',
      minTime: '08:00:00',
      maxTime: '19:00:00',
      drop: function(date, allDay) {

          var originalEventObject = jQuery(this).data('eventObject');
          var copiedEventObject = jQuery.extend({}, originalEventObject);

          copiedEventObject.start = date;
          copiedEventObject.title = jQuery(this).data('title');

          jQuery('#calendar').fullCalendar('renderEvent', copiedEventObject, true);
              $(this).remove();

      },
      events: [
          {
              title: 'Evaluación',
              start: new Date(y, m, 1),
              backgroundColor: '#21ba45'
          },
          {
              title: 'Evaluación',
              start: new Date(y, m, d-5),
              backgroundColor: '#f2711c'
          },
          {
              id: 999,
              title: 'Evaluación',
              start: new Date(y, m, d-3, 16, 0),
              backgroundColor: '#db2828'
          },
          {
              id: 999,
              title: 'Evaluación',
              start: new Date(y, m, d+4, 16, 0),
              backgroundColor: '#21ba45'
          },
          {
              title: 'Evaluación',
              start: new Date(y, m, d, 10, 30),
              //allDay: false,
              backgroundColor: '#f2711c',
          },
          {
              title: 'Evaluación',
              start: new Date(y, m, d, 12, 0),
              backgroundColor: '#f2711c'
          },
          {
              title: 'Evaluación',
              start: new Date(y, m, d+1, 19, 0),
              backgroundColor: '#db2828'
          },
          {
              title: 'Evaluación',
              start: new Date(y, m, 28),
              backgroundColor: '#21ba45'
          }
      ]
  });

};
