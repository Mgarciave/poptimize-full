# Poptimize Front

El front de la aplicación está construido sobre plantillas HTML 5, CSS 3 y JavaScript.
Todo el contenido es renderizado mediante AJAX.
Los archivos deben ser ubicados en la carpeta pública del servidor.

## Configuración

Para que el front se comunique con la API, sólo es necesario editar el archivo `scripts/config.js` y en la primera línea establecer la variable `ajax_url` con la URL de la API.
En la segunda línea, establecer true en el caso que se esté trabajando en un entorno de desarrollo local. Si los archivos se encuentran en producción, este valor debe ser false.
