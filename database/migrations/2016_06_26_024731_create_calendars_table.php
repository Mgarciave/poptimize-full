<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCalendarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('calendars', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name_class',80);
            $table->string('description',200);
            $table->string('objects',150);
            $table->string('tags',200);

            $table->integer('course_subject_teacher_id')->unsigned();
            $table->foreign('course_subject_teacher_id')->references('id')->on('courses_subjects_teachers')->onDelete('cascade'); 

            $table->integer('school_id')->unsigned();
            $table->foreign('school_id')->references('id')->on('school')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('calendars');
    }
}
