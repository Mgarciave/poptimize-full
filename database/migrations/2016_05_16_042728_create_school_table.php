<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchoolTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('school', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->string('username')->unique();
            $table->string('email')->unique();
            $table->string('address');
            $table->integer('phone');
            $table->string('commune');
            $table->string('region');
            $table->integer('code_rbd');
            $table->string('dependence');
            $table->string('holder');
            $table->string('director');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('school');
    }
}
