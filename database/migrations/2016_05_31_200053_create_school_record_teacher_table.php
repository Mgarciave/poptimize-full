<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchoolRecordTeacherTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('school_record_teacher', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('school_record_id')->unsigned();
            $table->foreign('school_record_id')->references('id')->on('school_records')->onDelete('cascade'); 

            $table->integer('teacher_id')->unsigned();
            $table->foreign('teacher_id')->references('id')->on('teachers')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('school_record_teacher');
    }
}
