<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttitudeUnitTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attitude_unit', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('attitude_id')->unsigned();
            $table->foreign('attitude_id')->references('id')->on('attitudes')->onDelete('cascade'); 

            $table->integer('unit_id')->unsigned();
            $table->foreign('unit_id')->references('id')->on('units')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('attitude_unit');
    }
}
