<?php

use Illuminate\Database\Seeder;

class SubjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('subjects')->insert([
        	'name'     => 'Matemática',
        	'course'   => '5 Básico',
        	'annual_hours'     => '228',
        	'weekly_hours'     => '6',
        	'semester_grade'   => '7',
        ]);

        DB::table('subjects')->insert([
        	'name'     => 'Lenguaje',
        	'course'   => '5 Básico',
        	'annual_hours'     => '',
        	'weekly_hours'     => '',
        	'semester_grade'   => '',
        ]);

        DB::table('subjects')->insert([
            'name'     => 'Historia',
            'course'   => '5 Básico',
            'annual_hours'     => '',
            'weekly_hours'     => '',
            'semester_grade'   => '',
        ]);
    }
}
