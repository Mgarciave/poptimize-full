<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(ProfileSeeder::class);
        $this->call(LoginSeeder::class);
        $this->call(schoolSeeder::class);
        
        $this->call(SchoolListSeeder::class);
        $this->call(SubjectSeeder::class);
        $this->call(SubjectListSeeder::class);
        $this->call(CoursesSeeder::class);
        $this->call(RegionSeeder::class);
        $this->call(TagSeeder::class);

        Model::reguard();
    }
}
