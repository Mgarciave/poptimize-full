<?php

use Illuminate\Database\Seeder;

class SubjectListSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('subject_records')->insert([
        	'name'     => 'Matemática'
        ]);

        DB::table('subject_records')->insert([
        	'name'     => 'Lenguaje'
        ]);

        DB::table('subject_records')->insert([
        	'name'     => 'Historia'
        ]);
    }
}
