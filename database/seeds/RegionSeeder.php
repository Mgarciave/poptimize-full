<?php

use Illuminate\Database\Seeder;

class RegionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('regiones')->insert([
        	'name' => 'Arica y Parinacota'
        ]);

        DB::table('regiones')->insert([
        	'name' => 'Tarapacá'
        ]);

        DB::table('regiones')->insert([
        	'name' => 'Antofagasta'
        ]);

        DB::table('regiones')->insert([
        	'name' => 'Atacama'
        ]);

        DB::table('regiones')->insert([
        	'name' => 'Coquimbo'
        ]);

        DB::table('regiones')->insert([
        	'name' => 'Valparaíso'
        ]);

        DB::table('regiones')->insert([
        	'name' => 'Metropolitana de Santiago'
        ]);

        DB::table('regiones')->insert([
        	'name' => 'OHiggins'
        ]);

        DB::table('regiones')->insert([
        	'name' => 'Maule'
        ]);

        DB::table('regiones')->insert([
        	'name' => 'Bío-Bío'
        ]);

        DB::table('regiones')->insert([
        	'name' => 'La Araucanía'
        ]);

        DB::table('regiones')->insert([
        	'name' => 'Los Ríos'
        ]);

        DB::table('regiones')->insert([
        	'name' => 'Los Lagos'
        ]);

        DB::table('regiones')->insert([
        	'name' => 'Aysén'
        ]);

        DB::table('regiones')->insert([
        	'name' => 'Magallanes'
        ]);        
    }
}
