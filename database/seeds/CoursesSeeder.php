<?php

use Illuminate\Database\Seeder;

class CoursesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('courses')->insert([
        	'name'     => '4 Básico',
        	'letter'   => 'A'
        ]);

        DB::table('courses')->insert([
        	'name'     => '4 Básico',
        	'letter'   => 'B'
        ]);

        DB::table('courses')->insert([
        	'name'     => '4 Básico',
        	'letter'   => 'C'
        ]);

        DB::table('courses')->insert([
        	'name'     => '5 Básico',
        	'letter'   => 'A'
        ]);

        DB::table('courses')->insert([
        	'name'     => '5 Básico',
        	'letter'   => 'B'
        ]);

        DB::table('courses')->insert([
        	'name'     => '5 Básico',
        	'letter'   => 'C'
        ]);
    }
}
