<?php

use Illuminate\Database\Seeder;

class schoolSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('school')->insert([
        	'name'     => 'Sin Colegio'
        ]);
    }
}
