<?php

use Illuminate\Database\Seeder;

class SchoolListSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('school_records')->insert([
        	'name'     => 'ESCUELA DE PARVULOS Y ESPECIAL MUNDO PALABRA DE BUIN',
        	'code_rbd' => '31036'
        ]);
    }
}
