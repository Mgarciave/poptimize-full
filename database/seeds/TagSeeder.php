<?php

use Illuminate\Database\Seeder;

class TagSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tags')->insert([
        	'name'     => 'Tabla de valor posicional'
        ]);

        DB::table('tags')->insert([
        	'name'     => 'Pictórico'
        ]);

        DB::table('tags')->insert([
        	'name'     => 'Simbólico'
        ]);

        DB::table('tags')->insert([
        	'name'     => 'Redondear'
        ]);

        DB::table('tags')->insert([
        	'name'     => 'Aproximar'
        ]);

        DB::table('tags')->insert([
        	'name'     => 'Comparar'
        ]);

        DB::table('tags')->insert([
        	'name'     => 'Ordenar'
        ]);

        DB::table('tags')->insert([
        	'name'     => 'Estimar'
        ]);
        DB::table('tags')->insert([
        	'name'     => 'Estrategias de cálculo'
        ]);
        DB::table('tags')->insert([
        	'name'     => 'Patrones'
        ]);
        DB::table('tags')->insert([
        	'name'     => 'Producto'
        ]);
        DB::table('tags')->insert([
        	'name'     => 'Cociente'
        ]);
        DB::table('tags')->insert([
        	'name'     => 'Resto'
        ]);
        DB::table('tags')->insert([
        	'name'     => 'Decena y centena de mil'
        ]);
        DB::table('tags')->insert([
        	'name'     => 'Resolución de ecuaciones'
        ]);

        DB::table('tags')->insert([
        	'name'     => 'Creación de ecuaciones'
        ]);

        DB::table('tags')->insert([
        	'name'     => 'Incógnita'
        ]);

        DB::table('tags')->insert([
        	'name'     => 'Problemas rutinarios'
        ]);

        DB::table('tags')->insert([
        	'name'     => 'Problemas no rutinarios'
        ]);
    }
}
