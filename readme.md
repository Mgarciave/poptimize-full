# Poptimize API

La API se encuentra construida sobre Laravel 5.1.
Su objetivo es entregar toda la información de la base de datos mediante requests HTTP, usando POST o GET según sea el caso.

## Configuración

El archivo de configuración se encuentra en el archivo propietario de Laravel, que se encuentra en el archivo de entornos `.env` ubicado en la raíz.

## Dependencias
Todas las dependencias son administradas por `composer`, por lo que antes de trabajar en el proyecto, se debe ejecutar `composer update` o `composer install`

## Endpoints

Los endpoints utilizados por la API son los siguientes:

| Endpoint        | Descripción           
| ------------- |-------------
| /report/units/print/{id}      | Genera de unidades PDF según ID de asignatura
| /report/schedule/print      | Genera PDF con la programación      
| /report/unit/print/{id_unit} | Genera PDF con detalle de unidades
| /users/auth|Acción para el login de usuario
|/users/logout|Cierra la sesión del usuario
|/users/auth/get|Obtiene la información del usuario
|/users/auth/edit|Edita información del usuario
|/users/auth/avatar_upload|Sube el avatar del perfil del usuario y lo asigna según ID
|/users/create|Crea usuarios según los parámetros enviados en el formulario de registro
|/courses/list|Genera lista de cursos
|/courses/add|Añade curso
|/courses/delete/|Elimina curso
|/units/list/{id}/|Muestra lista de unidades según asignatura y curso
|/units/show/{id}/|Muestra detalle de unidad según ID
|/units/save/{id}/|Guarda orden de unidades según configuración del usuario
|/schedule/add|Añade bloque de programación al calendario
|/schedule/list|Lista la programación de un curso
|/schedule/cron|Envía recordatorios según tarea programada
|/schedule/file-upload|Añade adjunto a un bloque
